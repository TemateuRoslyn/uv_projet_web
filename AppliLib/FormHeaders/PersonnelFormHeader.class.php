<?php
	namespace AppliLib\FormHeaders;
use AppliLib\Entities\Personnel
;	class PersonnelFormHeader extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest $request)
            {
                parent::__construct($request, new Personnel());
            }
        }
