<?php
	namespace AppliLib\FormHeaders;
use AppliLib\Entities\Conseil__Discipline
;	class Conseil__DisciplineFormHeader extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest $request)
            {
                parent::__construct($request, new Conseil__Discipline());
            }
        }
