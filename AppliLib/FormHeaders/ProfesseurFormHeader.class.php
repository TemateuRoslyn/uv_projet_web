<?php
	namespace AppliLib\FormHeaders;
use AppliLib\Entities\Professeur
;	class ProfesseurFormHeader extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest $request)
            {
                parent::__construct($request, new Professeur());
            }
        }
