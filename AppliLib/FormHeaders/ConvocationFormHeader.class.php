<?php
	namespace AppliLib\FormHeaders;
use AppliLib\Entities\Convocation
;	class ConvocationFormHeader extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest $request)
            {
                parent::__construct($request, new Convocation());
            }
        }
