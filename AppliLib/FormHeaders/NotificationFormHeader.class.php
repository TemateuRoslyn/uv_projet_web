<?php
	namespace AppliLib\FormHeaders;
use AppliLib\Entities\Notification
;	class NotificationFormHeader extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest $request)
            {
                parent::__construct($request, new Notification());
            }
        }
