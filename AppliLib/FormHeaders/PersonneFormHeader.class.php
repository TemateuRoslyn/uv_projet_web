<?php
	namespace AppliLib\FormHeaders;
use AppliLib\Entities\Personne
;	class PersonneFormHeader extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest $request)
            {
                parent::__construct($request, new Personne());
            }
        }
