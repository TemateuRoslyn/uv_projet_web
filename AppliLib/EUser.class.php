<?php
namespace AppliLib;
use AppliLib\FormBuilder\PersonneFormBuilder;

use AppliLib\Entities\Eleve;
use AppliLib\Entities\Parents;
use AppliLib\Entities\Professeur;
use AppliLib\Entities\Personnel;


class EUser {

    const USER_NOM = "nom";
    const USER_ROLE = "role";
    const USER_ID = "id";

    const USER_ROLE_ELEVE_PATH = "/geniusclassrooms";

    // Entites Eleve => tab_php
    public static function parseEleveToTab($elves_entities){
        $datas = [];
        $value = [];
        foreach($elves_entities as $eleve){
            // on recupere tous les champs d'un eleves
            $value['id'] = $eleve['id'];
            $value['nom'] = $eleve['nom'];
            $value['prenom'] = $eleve['prenom'];
            $value['date_de_naisssancce'] = $eleve['date_de_naisssancce'];
            $value['login_'] = $eleve['login_'];
            $value['mot_de_passe'] = $eleve['mot_de_passe'];
            $value['lieu_de_naissance_'] = $eleve['lieu_de_naissance_'];
            $value['photo'] = $eleve['photo'];
            $value['email'] = $eleve['email'];
            $value['sexe'] = $eleve['sexe'];
            $value['Tel'] = $eleve['Tel'];
            $value['role'] = $eleve['role'];
            $value['solvable'] = $eleve['solvable'];
            $value['redoublant'] = $eleve['redoublant'];

            $datas[] = $value;
        }
        return $datas;
    }

     // Entites Faute => tab_php
     public static function parseFautesToTab($elves_entities){
        $datas = [];
        $value = [];
        foreach($elves_entities as $eleve){
            // on recupere tous les champs d'un eleves
            $value['id'] = $eleve['id'];
            $value['nom'] = $eleve['nom'];
            $value['Gravite'] = $eleve['Gravite'];
 
            $datas[] = $value;
        }
        return $datas;
     }

      // Entites Sanction => tab_php
      public static function parseSanctionsToTab($elves_entities){
        $datas = [];
        $value = [];
        foreach($elves_entities as $eleve){
            // on recupere tous les champs d'un eleves
            $value['id'] = $eleve['id'];
            $value['libelle_sanction'] = $eleve['libelle_sanction'];
            $value['niveau_gravite'] = $eleve['niveau_gravite'];
            $value['motif'] = $eleve['motif'];
            $value['duree_validite'] = $eleve['duree_validite'];
            $datas[] = $value;
        }
        return $datas;
     }

    // Entite prof => tab_php
    public static function parseProfToTab($prof_entities){
        $datas = [];
        $value = [];
        foreach($prof_entities as $profs){
            $value['id'] = $profs['id'];
            $value['nom'] = $profs['nom'];
            $value['prenom'] = $profs['prenom'];
            $value['date_de_naisssancce'] = $profs['date_de_naisssancce'];
            $value['login_'] = $profs['login_'];
            $value['mot_de_passe'] = $profs['mot_de_passe'];
            $value['lieu_de_naissance_'] = $profs['lieu_de_naissance_'];
            $value['photo'] = $profs['photo'];
            $value['email'] = $profs['email'];
            $value['sexe'] = $profs['sexe'];
            $value['Tel'] = $profs['Tel'];
            $value['role'] = $profs['role'];
            $value['statut'] = $profs['statut'];

            $datas[] = $value;
        }
        return $datas;
    }

    // Entite parent => tab_php
    public static function parseParentToTab($parent_entities){
        $datas = [];
        $value = [];
        foreach($parent_entities as $parent){
            $value['id'] = $parent['id'];
            $value['nom'] = $parent['nom'];
            $value['prenom'] = $parent['prenom'];
            $value['date_de_naisssancce'] = $parent['date_de_naisssancce'];
            $value['login_'] = $parent['login_'];
            $value['mot_de_passe'] = $parent['mot_de_passe'];
            $value['lieu_de_naissance_'] = $parent['lieu_de_naissance_'];
            $value['photo'] = $parent['photo'];
            $value['email'] = $parent['email'];
            $value['sexe'] = $parent['sexe'];
            $value['Tel'] = $parent['Tel'];
            $value['role'] = $parent['role'];
            $value['profession'] = $parent['profession'];

            $datas[] = $value;
        }
        return $datas;
    }

    // Entite personnel => tab_php
    public static function parsePersonnelToTab($personnel_entities){
        $datas = [];
        $value = [];
        foreach($personnel_entities as $paersonnel_admin){
            $value['id'] = $paersonnel_admin['id'];
            $value['nom'] = $paersonnel_admin['nom'];
            $value['prenom'] = $paersonnel_admin['prenom'];
            $value['date_de_naisssancce'] = $paersonnel_admin['date_de_naisssancce'];
            $value['login_'] = $paersonnel_admin['login_'];
            $value['mot_de_passe'] = $paersonnel_admin['mot_de_passe'];
            $value['lieu_de_naissance_'] = $paersonnel_admin['lieu_de_naissance_'];
            $value['photo'] = $paersonnel_admin['photo'];
            $value['email'] = $paersonnel_admin['email'];
            $value['sexe'] = $paersonnel_admin['sexe'];
            $value['Tel'] = $paersonnel_admin['Tel'];
            $value['role'] = $paersonnel_admin['role'];
            $value['fonction'] = $paersonnel_admin['fonction'];

            $datas[] = $value;
        }
        return $datas;
    }


    // Cette focntion prend une liste et fuile l'objet dont  les identifiants correspondent
    public static function search_login_password($login, $password, $list_php, $list_entities){
        $i = 0;
            foreach ($list_php as $entities) {
                
                if(($entities["login_"] == $login) && ($entities["mot_de_passe"] ==$password)){
                    // on retourne eleve trouvee
                    return $list_entities[$i];
                }
                $i++;
            }
            return NULL;
    }


     // Cette fonction selection la bone entity
     public static function select_entity($e1, $e2, $e3, $e4){
       $selected = $e1;

       if($e2 != NULL){
        $selected = $e2;
       }else if($e3 != NULL){
        $selected = $e3;
       }else if($e4 != NULL){
        $selected = $e4;
       }

       return $selected;
    }

    /**
     * Cette focntion cree l'entite correspondante en fonction du role passe
     * @param role
     */


    // public static function getManagerSelected($role, $le_this){
       
    // }

    
}
