<?php
	namespace AppliLib\Entities;
	 class Cour extends \Library\Entity
            {

        /**
         * 
         * @property Date
         */protected $date_cour;

        /**
         * 
         * @property Date
         */protected $heure_debut;

        /**
         * 
         * @property Date
         */protected $heure_fin;


            /**
             * @param Date $date_cour
             */  
            public function setDate_cour($date_cour){$this->date_cour = $date_cour;}

            /**
             * @param Date $heure_debut
             */  
            public function setHeure_debut($heure_debut){$this->heure_debut = $heure_debut;}

            /**
             * @param Date $heure_fin
             */  
            public function setHeure_fin($heure_fin){$this->heure_fin = $heure_fin;}


            /**
             * @return Date  
             */  
            public function date_cour(){return $this->date_cour;}

            /**
             * @return Date  
             */  
            public function heure_debut(){return $this->heure_debut;}

            /**
             * @return Date  
             */  
            public function heure_fin(){return $this->heure_fin;}
            public function isValid(){return true;}}