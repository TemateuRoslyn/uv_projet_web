<?php
	namespace AppliLib\Entities;
	 class Conseil__Discipline extends \Library\Entity
            {

        /**
         * 
         * @property Date
         */protected $date_cd;

        /**
         * 
         * @property Date
         */protected $heure_debut_cd;

        /**
         * 
         * @property Date
         */protected $heure_fin_cd;


            /**
             * @param Date $date_cd
             */  
            public function setDate_cd($date_cd){$this->date_cd = $date_cd;}

            /**
             * @param Date $heure_debut_cd
             */  
            public function setHeure_debut_cd($heure_debut_cd){$this->heure_debut_cd = $heure_debut_cd;}

            /**
             * @param Date $heure_fin_cd
             */  
            public function setHeure_fin_cd($heure_fin_cd){$this->heure_fin_cd = $heure_fin_cd;}


            /**
             * @return Date  
             */  
            public function date_cd(){return $this->date_cd;}

            /**
             * @return Date  
             */  
            public function heure_debut_cd(){return $this->heure_debut_cd;}

            /**
             * @return Date  
             */  
            public function heure_fin_cd(){return $this->heure_fin_cd;}
            public function isValid(){return true;}}