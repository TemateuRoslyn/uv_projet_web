<?php
	namespace AppliLib\Entities;
	 class Notification extends \Library\Entity
            {

        /**
         * 
         * @property string
         */protected $libelle;

        /**
         * 
         * @property int
         */protected $view;

        /**
         * 
         * @property int
         */protected $id_emeteur;

        /**
         * 
         * @property string
         */protected $nom_emeteur;


            /**
             * @param string $libelle
             */  
            public function setLibelle($libelle){$this->libelle = $libelle;}

            /**
             * @param int $view
             */  
            public function setView($view){$this->view = $view;}

            /**
             * @param int $id_emeteur
             */  
            public function setId_emeteur($id_emeteur){$this->id_emeteur = $id_emeteur;}

            /**
             * @param string $nom_emeteur
             */  
            public function setNom_emeteur($nom_emeteur){$this->nom_emeteur = $nom_emeteur;}


            /**
             * @return string  
             */  
            public function libelle(){return $this->libelle;}

            /**
             * @return int  
             */  
            public function view(){return $this->view;}

            /**
             * @return int  
             */  
            public function id_emeteur(){return $this->id_emeteur;}

            /**
             * @return string  
             */  
            public function nom_emeteur(){return $this->nom_emeteur;}
            public function isValid(){return true;}}