<?php
	namespace AppliLib\Entities;
	 class Membre_Conseil extends \Library\Entity
            {

        /**
         * 
         * @property int
         */protected $id_liste;

        /**
         * 
         * @property int
         */protected $id_chef;

        /**
         * 
         * @property int
         */protected $id_surveillant_G;

        /**
         * 
         * @property int
         */protected $id_representant_E;


            /**
             * @param int $id_liste
             */  
            public function setId_liste($id_liste){$this->id_liste = $id_liste;}

            /**
             * @param int $id_chef
             */  
            public function setId_chef($id_chef){$this->id_chef = $id_chef;}

            /**
             * @param int $id_surveillant_G
             */  
            public function setId_surveillant_G($id_surveillant_G){$this->id_surveillant_G = $id_surveillant_G;}

            /**
             * @param int $id_representant_E
             */  
            public function setId_representant_E($id_representant_E){$this->id_representant_E = $id_representant_E;}


            /**
             * @return int  
             */  
            public function id_liste(){return $this->id_liste;}

            /**
             * @return int  
             */  
            public function id_chef(){return $this->id_chef;}

            /**
             * @return int  
             */  
            public function id_surveillant_G(){return $this->id_surveillant_G;}

            /**
             * @return int  
             */  
            public function id_representant_E(){return $this->id_representant_E;}
            public function isValid(){return true;}}