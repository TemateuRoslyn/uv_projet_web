<?php
	namespace AppliLib\Entities;
	 class Convocation extends \Library\Entity
            {

        /**
         * 
         * @property string
         */protected $libelle;

        /**
         * 
         * @property string
         */protected $date_convocation;

        /**
         * 
         * @property Date
         */protected $date_RV;

        /**
         * 
         * @property string
         */protected $statut;


            /**
             * @param string $libelle
             */  
            public function setLibelle($libelle){$this->libelle = $libelle;}

            /**
             * @param string $date_convocation
             */  
            public function setDate_convocation($date_convocation){$this->date_convocation = $date_convocation;}

            /**
             * @param Date $date_RV
             */  
            public function setDate_RV($date_RV){$this->date_RV = $date_RV;}

            /**
             * @param string $statut
             */  
            public function setStatut($statut){$this->statut = $statut;}


            /**
             * @return string  
             */  
            public function libelle(){return $this->libelle;}

            /**
             * @return string  
             */  
            public function date_convocation(){return $this->date_convocation;}

            /**
             * @return Date  
             */  
            public function date_RV(){return $this->date_RV;}

            /**
             * @return string  
             */  
            public function statut(){return $this->statut;}
            public function isValid(){return true;}}