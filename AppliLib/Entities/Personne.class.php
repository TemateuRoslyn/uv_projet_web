<?php
	namespace AppliLib\Entities;
	 class Personne extends \Library\Entity
            {

        /**
         * 
         * @property string
         */protected $nom;

        /**
         * 
         * @property string
         */protected $prenom;

        /**
         * 
         * @property Date
         */protected $date_de_naisssancce;

        /**
         * 
         * @property string
         */protected $login_;

        /**
         * 
         * @property string
         */protected $mot_de_passe;

        /**
         * 
         * @property string
         */protected $lieu_de_naissance_;

        /**
         * 
         * @property string
         */protected $photo;

        /**
         * 
         * @property string
         */protected $email;

        /**
         * 
         * @property string
         */protected $sexe;

        /**
         * 
         * @property int
         */protected $Tel;

        /**
         * 
         * @property string
         */protected $role;


            /**
             * @param string $nom
             */  
            public function setNom($nom){$this->nom = $nom;}

            /**
             * @param string $prenom
             */  
            public function setPrenom($prenom){$this->prenom = $prenom;}

            /**
             * @param Date $date_de_naisssancce
             */  
            public function setDate_de_naisssancce($date_de_naisssancce){$this->date_de_naisssancce = $date_de_naisssancce;}

            /**
             * @param string $login_
             */  
            public function setLogin_($login_){$this->login_ = $login_;}

            /**
             * @param string $mot_de_passe
             */  
            public function setMot_de_passe($mot_de_passe){$this->mot_de_passe = $mot_de_passe;}

            /**
             * @param string $lieu_de_naissance_
             */  
            public function setLieu_de_naissance_($lieu_de_naissance_){$this->lieu_de_naissance_ = $lieu_de_naissance_;}

            /**
             * @param string $photo
             */  
            public function setPhoto($photo){$this->photo = $photo;}

            /**
             * @param string $email
             */  
            public function setEmail($email){$this->email = $email;}

            /**
             * @param string $sexe
             */  
            public function setSexe($sexe){$this->sexe = $sexe;}

            /**
             * @param int $Tel
             */  
            public function setTel($Tel){$this->Tel = $Tel;}

            /**
             * @param string $role
             */  
            public function setRole($role){$this->role = $role;}


            /**
             * @return string  
             */  
            public function nom(){return $this->nom;}

            /**
             * @return string  
             */  
            public function prenom(){return $this->prenom;}

            /**
             * @return Date  
             */  
            public function date_de_naisssancce(){return $this->date_de_naisssancce;}

            /**
             * @return string  
             */  
            public function login_(){return $this->login_;}

            /**
             * @return string  
             */  
            public function mot_de_passe(){return $this->mot_de_passe;}

            /**
             * @return string  
             */  
            public function lieu_de_naissance_(){return $this->lieu_de_naissance_;}

            /**
             * @return string  
             */  
            public function photo(){return $this->photo;}

            /**
             * @return string  
             */  
            public function email(){return $this->email;}

            /**
             * @return string  
             */  
            public function sexe(){return $this->sexe;}

            /**
             * @return int  
             */  
            public function Tel(){return $this->Tel;}

            /**
             * @return string  
             */  
            public function role(){return $this->role;}
            public function isValid(){return true;}
        }