<?php
	namespace AppliLib\Entities;
	 class Reparation extends \Library\Entity
            {

        /**
         * 
         * @property string
         */protected $demarche_de_mediation;

        /**
         * 
         * @property int
         */protected $id_faute;


            /**
             * @param string $demarche_de_mediation
             */  
            public function setDemarche_de_mediation($demarche_de_mediation){$this->demarche_de_mediation = $demarche_de_mediation;}

            /**
             * @param int $id_faute
             */  
            public function setId_faute($id_faute){$this->id_faute = $id_faute;}


            /**
             * @return string  
             */  
            public function demarche_de_mediation(){return $this->demarche_de_mediation;}

            /**
             * @return int  
             */  
            public function id_faute(){return $this->id_faute;}
            public function isValid(){return true;}}