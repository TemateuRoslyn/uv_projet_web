<?php
	namespace AppliLib\Entities;
	 class Sanction_Prevue extends \Library\Entity
            {


            

        /**
         * 
         * @property string
         */protected $libelle_sanction;

        /**
         * 
         * @property string
         */protected $niveau_gravite;

        /**
         * 
         * @property string
         */protected $motif;

        /**
         * 
         * @property string
         */protected $duree_validite;


            /**
             * @param string $libelle_sanction
             */  
            public function setLibelle_sanction($libelle_sanction){$this->libelle_sanction = $libelle_sanction;}

            /**
             * @param string $niveau_gravite
             */  
            public function setNiveau_gravite($niveau_gravite){$this->niveau_gravite = $niveau_gravite;}

            /**
             * @param string $motif
             */  
            public function setMotif($motif){$this->motif = $motif;}

            /**
             * @param string $duree_validite
             */  
            public function setDuree_validite($duree_validite){$this->duree_validite = $duree_validite;}


            /**
             * @return string  
             */  
            public function libelle_sanction(){return $this->libelle_sanction;}

            /**
             * @return string  
             */  
            public function niveau_gravite(){return $this->niveau_gravite;}

            /**
             * @return string  
             */  
            public function motif(){return $this->motif;}

            /**
             * @return string  
             */  
            public function duree_validite(){return $this->duree_validite;}
            public function isValid(){return true;}}