<?php
	namespace AppliLib\Entities;
	 class Eleve extends \AppliLib\Entities\Personne
            {

        /**
         * 
         * @property string
         */protected $solvable;

        /**
         * 
         * @property string
         */protected $redoublant;


            /**
             * @param string $solvable
             */  
            public function setSolvable($solvable){$this->solvable = $solvable;}

            /**
             * @param string $redoublant
             */  
            public function setRedoublant($redoublant){$this->redoublant = $redoublant;}


            /**
             * @return string  
             */  
            public function solvable(){return $this->solvable;}

            /**
             * @return string  
             */  
            public function redoublant(){return $this->redoublant;}}