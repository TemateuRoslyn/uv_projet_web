<?php
	namespace AppliLib\Entities;
	 class Fautes extends \Library\Entity
            {

        /**
         * 
         * @property string
         */protected $nom;

        /**
         * 
         * @property string
         */protected $Gravite;


            /**
             * @param string $nom
             */  
            public function setNom($nom){$this->nom = $nom;}

            /**
             * @param string $Gravite
             */  
            public function setGravite($Gravite){$this->Gravite = $Gravite;}


            /**
             * @return string  
             */  
            public function nom(){return $this->nom;}

            /**
             * @return string  
             */  
            public function Gravite(){return $this->Gravite;}
            public function isValid(){return true;}}