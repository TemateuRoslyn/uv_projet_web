<?php
	namespace AppliLib\Entities;
	 class Reglement_Iterieure extends \Library\Entity
            {

			/**
			 * 
			 * @property string
			 */protected $nomEtablissement;

			/**
             * @param string $nomEtablissement
             */  
            public function setNomEtablissement($nom){$this->nomEtablissement = $nom;}

			 /**
             * @return string  
             */  
            public function nomEtablissement(){return $this->nomEtablissement;}

			public function isValid(){return true;}
}