<?php
	namespace AppliLib\FormBuilder;
	class ConvocationFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'libelle',
                'placeholder' => ' Champs : LIBELLE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'date_convocation',
                'placeholder' => ' Champs : DATE_CONVOCATION',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'date_RV',
                'placeholder' => ' Champs : DATE_RV',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'statut',
                'placeholder' => ' Champs : STATUT',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
