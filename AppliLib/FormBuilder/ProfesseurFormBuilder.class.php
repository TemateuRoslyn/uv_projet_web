<?php
	namespace AppliLib\FormBuilder;
	class ProfesseurFormBuilder extends PersonneFormBuilder
        {
            public function build() { parent::build();$this->form->add(new \Library\Fields\StringField(array(
                'name' => 'statut',
                'placeholder' => ' Champs : STATUT',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
