<?php
	namespace AppliLib\FormBuilder;
	class FautesFormBuilder extends \Library\FormBuilder
        {
            // les niveaux de gravites
            const FAUTE_TOLERABLE = "TOLERABLE";
            const FAUTE_MOYENNE = "MOYENNE";
            const FAUTE_GRAVE = "GRAVE";
            const FAUTE_EXTREME = "EXTREME";

            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'nom',
                'label' => "Observations",
                'placeholder' => ' Libelle',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\SelectField(array(
                'name' => 'Gravite',
                'placeholder' => 'Sélectionnez le niveau de gravité',
                'options' =>[
                    self::FAUTE_TOLERABLE => "Tolérable",
                    self::FAUTE_MOYENNE => "Moyenne",
                    self::FAUTE_GRAVE => "Grave",
                    self::FAUTE_EXTREME => "Extreme"
                ],
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
