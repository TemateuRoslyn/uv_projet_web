<?php
	namespace AppliLib\FormBuilder;
	class Reglement_IterieureFormBuilder extends \Library\FormBuilder
        {
            public function build() { 
                $this->form->add(new \Library\Fields\StringField(array(
                    'name' => 'nomEtablissement',
                    'placeholder' => 'Saisir le nom de l`etablissement',
                    'label' => 'Entrez un Etablissement',
                    'validators' => array(
                        new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),
    
                    ),
                )));
            }
        }
