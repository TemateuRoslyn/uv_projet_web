<?php
	namespace AppliLib\FormBuilder;
	class PersonneFormBuilder extends \Library\FormBuilder
        {
            const ROLE_SUPER_ADMINISTRATEUR = 0;
            const ROLE_ADMINISTRATEUR = 1;
            const ROLE_SURVEILLANT_GENERAL = 2;
            const ROLE_PERSONNEL_ADMINISTRATIF = 3;
            const ROLE_PROFESSEUR = 4;
            const ROLE_ELEVE = 5;
            const ROLE_PARENT = 6;
            const ROLE_VISITEUR = 7;

            const SEXE_MASCULIN = "Masculin";
            const SEXE_FEMININ = "Feminin";


            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'nom',
                'placeholder' => ' Champs : NOM',
                'label' => 'Nom',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'prenom',
                'placeholder' => ' Champs : PRENOM',
                'label' => 'Prénom',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\DateField(array(
                'name' => 'date_de_naisssancce',
                'placeholder' => ' Champs : DATE_DE_NAISSSANCCE',
                'label' => 'Date de Naissance',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'login_',
                'placeholder' => ' Champs : LOGIN_',
                'label' => 'Login',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\PasswordField(array(
                'name' => 'mot_de_passe',
                'placeholder' => ' Champs : MOT_DE_PASSE',
                'label' => 'Mot de passe',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'lieu_de_naissance_',
                'placeholder' => ' Champs : LIEU_DE_NAISSANCE_',
                'label' => 'Lieu de Naissance',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'photo',
                'placeholder' => ' Champs : PHOTO',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'email',
                'placeholder' => 'Ex: maestros@gmail.com',
                'label' => 'Adresse Email',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\SelectField(array(
                'name' => 'sexe',
                'placeholder' => 'Sexe',
                'options' => [
                    self::SEXE_MASCULIN => 'MASCULIN',
                    self::SEXE_FEMININ => 'FEMININ'
                ],
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\NumberField(array(
                'name' => 'Tel',
                'label' => 'Téléphone',
                'placeholder' => ' Champs : TEL',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )))->add(new \Library\Fields\SelectField(array(
                'name' => 'role',
                'placeholder' => 'Role',
                'options' => [
                    self::ROLE_ADMINISTRATEUR => 'ADMINISTRATEUR',
                    self::ROLE_SURVEILLANT_GENERAL => 'SUERVEILLANT GENERAL',
                    self::ROLE_PERSONNEL_ADMINISTRATIF => 'PERSONNEL ADMINISTRATIF',
                    self::ROLE_PROFESSEUR => 'PROFESSEUR',
                    self::ROLE_ELEVE => 'ELEVE',
                    self::ROLE_PARENT => 'PARENT',
                    self::ROLE_VISITEUR => 'VISITEUR',

                ],
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
