<?php
	namespace AppliLib\FormBuilder;
	class ReparationFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'demarche_de_mediation',
                'placeholder' => ' Champs : DEMARCHE_DE_MEDIATION',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'id_faute',
                'placeholder' => ' Champs : ID_FAUTE',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )));}
        }
