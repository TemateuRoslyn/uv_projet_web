<?php
	namespace AppliLib\FormBuilder;
	class Membre_ConseilFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\DateField(array(
                'name' => 'id_liste',
                'label' => 'Date de programmation',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'id_chef',
                'label' => 'Convier le che ou son adjoint',
                'placeholder' => ' Champs : ID_CHEF',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'id_surveillant_G',
                'label' => 'Convier un menbre du personnel',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'id_representant_E',
                'label' => 'Convier un eleve',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )));}
        }
