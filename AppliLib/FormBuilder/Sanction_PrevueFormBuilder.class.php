<?php
	namespace AppliLib\FormBuilder;
	class Sanction_PrevueFormBuilder extends \Library\FormBuilder
        {

                // les niveaux de gravites
                const FAUTE_BLAME = "BLAME";
                const FAUTE_AVERTISSEMENT = "AVERTISSEMENT";
                const FAUTE_EXCLUSION_PONCTUELLE = "EXCLUSION PONCTUELLE";
                const FAUTE_EXCLUSION_DEFINITIVE = "EXCLUSION DEFINITIVE";

            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'libelle_sanction',
                'label' => 'Libellé',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\SelectField(array(
                'name' => 'niveau_gravite',
                'placeholder' => 'Niveau de gravité',
                'options' =>[
                    self::FAUTE_BLAME => "Blame",
                    self::FAUTE_AVERTISSEMENT => "Avertissement",
                    self::FAUTE_EXCLUSION_PONCTUELLE => "Exclusion Ponctuelle",
                    self::FAUTE_EXCLUSION_DEFINITIVE => "Exclusion Définitive"
                ],
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'motif',
                'label' => 'Motif',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\DateField(array(
                'name' => 'duree_validite',
                'label' => 'Durée de validité',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
