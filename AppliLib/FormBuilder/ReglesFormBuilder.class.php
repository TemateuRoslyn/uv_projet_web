<?php
	namespace AppliLib\FormBuilder;
	class ReglesFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'libelle_regle',
                'label' => 'libellé',
                'placeholder' => ' Champs : LIBELLE_REGLE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
