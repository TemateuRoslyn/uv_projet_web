<?php
	namespace AppliLib\FormBuilder;
	class NotificationFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'libelle',
                'placeholder' => ' Champs : LIBELLE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'view',
                'placeholder' => ' Champs : VIEW',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'id_emeteur',
                'placeholder' => ' Champs : ID_EMETEUR',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'nom_emeteur',
                'placeholder' => ' Champs : NOM_EMETEUR',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
