<?php
	namespace AppliLib\FormBuilder;
	class CourFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\DateField(array(
                'name' => 'date_cour',
                'label' => 'Date du cours',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\DateField(array(
                'name' => 'heure_debut',
                'label' => 'Début',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\DateField(array(
                'name' => 'heure_fin',
                'label' => 'Fin',
                'validators' => array(
                    
                ),
            )));}
        }
