<?php
	namespace AppliLib\FormBuilder;
	class ParentsFormBuilder extends PersonneFormBuilder
        {
            public function build() { parent::build();$this->form->add(new \Library\Fields\StringField(array(
                'name' => 'profession',
                'placeholder' => ' Champs : PROFESSION',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
