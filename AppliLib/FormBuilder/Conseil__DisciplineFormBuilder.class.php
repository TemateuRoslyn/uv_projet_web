<?php
	namespace AppliLib\FormBuilder;
	class Conseil__DisciplineFormBuilder extends \Library\FormBuilder
        {
            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'date_cd',
                'placeholder' => ' Champs : DATE_CD',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'heure_debut_cd',
                'placeholder' => ' Champs : HEURE_DEBUT_CD',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'heure_fin_cd',
                'placeholder' => ' Champs : HEURE_FIN_CD',
                'validators' => array(
                    
                ),
            )));}
        }
