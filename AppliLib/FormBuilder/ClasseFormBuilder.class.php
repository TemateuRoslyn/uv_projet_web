<?php
	namespace AppliLib\FormBuilder;
	class ClasseFormBuilder extends \Library\FormBuilder
        {

            //  les constante uimportantes sur les classes

            const CLASSE_SIXIEME = "SIXIEME";
            const CLASSE_CINQUIEME = "CINQUIEME";
            const CLASSE_QUATRIEME = "QUATRIEME";
            const CLASSE_TROISIEME = "TROISIEME";

            const CLASSE_SECONDE_C = "SECONDE_C";
            const CLASSE_SECONDE_A_ALLEMEND = "SECONDE_A_ALLEMAND";
            const CLASSE_SECONDE_A_ESPAGNOLE = "SECONDE_A_ESPAGNOLE";

            const CLASSE_PREMIERE_C = "PREMIERE_C";
            const CLASSE_PREMIERE_D = "PREMIERE_D";
            const CLASSE_PREMIERE_A_ALLEMAND = "PREMIERE_A_ALLEMAND";
            const CLASSE_PREMIERE_A_ESPAGNOLE = "PREMIERE_A_ESPAGNOLE";
            
            const CLASSE_TERMINALE_C = "TERMINALE_C";
            const CLASSE_TERMINALE_D = "TERMINALE_D";
            const CLASSE_TERMINALE_A_ALLEMAND = "TERMINALE_A_ALLEMAND";
            const CLASSE_TERMINALE_A_ESPAGNOLE = "TERMINALE_A_ESPAGNOLE";


            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'nom_classe',
                'placeholder' => ' Champs : NOM_CLASSE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
