<?php
    namespace AppliLib\FormBuilder;
    use AppliLib\FormBuilder\ClasseFormBuilder;
	class EleveFormBuilder extends PersonneFormBuilder
        {
            const PAYE_OUI = "PAYE";
            const PAYE_NON = "NON_PAYE";

            const STATUS_NOUVEAU = "Nouveau";
            const STATUS_REDOUBLANT = "Redoublant";

            public function build() { 
                parent::build();

                $this->form->add(new \Library\Fields\SelectField(array(
                'name' => 'solvable',
                'options' => [
                    self::PAYE_OUI => 'Payé',
                    self::PAYE_NON => 'Non Payé'
                ],
                'placeholder' => 'SOLVABLE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),
                ),
            )))->add(new \Library\Fields\SelectField(array(
                'name' => 'redoublant',
                'placeholder' => 'Status Academique',
                'options' => [
                    self::STATUS_NOUVEAU => 'Nouveau',
                    self::STATUS_REDOUBLANT => 'Redoublant'
                ],
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
