<?php
	namespace AppliLib\FormBuilder;
	class PersonnelFormBuilder extends PersonneFormBuilder
        {
            public function build() { parent::build();$this->form->add(new \Library\Fields\StringField(array(
                'name' => 'fonction',
                'placeholder' => ' Champs : FONCTION',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )));}
        }
