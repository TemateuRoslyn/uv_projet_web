<?php
    namespace AppliLib\Models;
    use AppliLib\FormBuilder\PersonneFormBuilder;
	class PersonneManager_PDO extends \Library\Managers_PDO
            {
                public function __construct($api, \PDO $dao)
                {
                    parent::__construct($api,$dao);
                }

                public function getConnect(string $login, string $password){
                    return $this->bindFetchRequest(
                        "SELECT *
                        FROM personne
                        WHERE login_ = :login AND mot_de_passe = :password", ["login" => $login, "password" => $password]
                        
                    );
                }
                /**
                 * cette fonction ne connecte que l'administrateur
                 */
                public function getConnectAdmin ( $login,  $password){
                    return $this->bindFetchRequest(
                        "SELECT *
                        FROM personne
                        WHERE `login_` = :logins AND `mot_de_passe` = :passwords AND `role`=:role;"
                        , [
                            "logins" => $login
                            , "passwords" =>  $password
                            , "role" => PersonneFormBuilder::ROLE_ADMINISTRATEUR
                        ]
                        
                    );
                }

            }