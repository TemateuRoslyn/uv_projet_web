<?php

namespace Library;

class FormHandlerFactory extends PObject
{
	protected $managers = null;
	protected $request = null;
	protected $form = null;
	protected $entity = null;
	protected $formHandler = null;
	protected $options = array();

	function __construct(Managers $managers, Entity $entity, HTTPRequest $request, array $options = array())
	{
		$this->request = $request;
		$this->entity = $entity;

		$this->managers = $managers;
		$this->options = $options;
	}

	public function build()
	{
		$classeName = ucfirst($this->entity->className());
		$builder = "AppliLib\FormBuilder\\" . $classeName . "FormBuilder";

		$formBuilder = new $builder($this->managers, $this->entity, $this->options);
		$formBuilder->build();

		$this->form = $formBuilder->form();
		$this->formHandler = new FormHandler($this->form, $this->managers->getManagerOf($classeName), $this->request);
	}

	public function formHandler()
	{
		return $this->formHandler;
	}

	public function setEntity(Entity $entity)
	{
		$this->entity = $entity;
	}

	public function form()
	{
		return $this->form;
	}
}
