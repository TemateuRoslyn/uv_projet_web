<?php

namespace Library;

class FieldUtilitary extends PObject
{
    const TYPE_TEXT = "text";
    const TYPE_PWD = "password";
    const TYPE_NUMBER = "number";
    const TYPE_CHECK = "checkbox";
    const TYPE_DATE = "date";
    const TYPE_EMAIL = "email";
    const TYPE_FILE = "file";
    const TYPE_TEL = "tel";
    const TYPE_TIME = "time";

    const CLASS_FORM = "form-control";

    public static function motifInput(string $name, string $type, $value = "", $class = "", $label = "", $placeholder = "", $id = "", bool $require = false, string $disable = "")
    {
        return self::motifInputBody($name, $type, $value, $class, $label, $placeholder, $id, $require) . " disable='$disable' />";
    }

    public static function motifInputReal(string $name, $value = "", $class = "", $label = "", $placeholder = "", $id = "", bool $require = false, string $disable = "")
    {
        $body = self::motifInputBody($name, "number", $value, $class, $label, $placeholder, $id, $require);
        // var_dump($body);
        $body .= " step='.01' disable='$disable' />";
        return $body;
    }

    protected static function motifInputBody(string $name, string $type, $value = "", $class = "", $label = "", $placeholder = "", $id = "", bool $require = false): string
    {
        $widget = "";
        if (!empty($label)) {
            $widget .= "<label>" . $label . "</label>";
        }
        $optClass = (empty($class)) ? self::CLASS_FORM : $class;
        // throw new \Exception($optClass);

        $widget .= '<input class="' . $optClass . '" type="' . $type . '" name="' . $name . '"';

        if (!empty($value)) {
            $widget .= ' value="' . str_replace('"', "&quot;", htmlspecialchars($value)) . '"';
        }

        if (!empty($placeholder)) {
            $widget .= ' placeholder="' . $placeholder . '"';
        }

        $require_c = ($require) ? "required='required'" : "";
        $filePDF = ($type == "file") ? ' accept="image/x-eps,application/pdf"' : "";

        return $widget .= " $require_c $filePDF ";
    }
}
