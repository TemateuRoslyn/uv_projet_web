<?php

namespace Library;

class PObject
{
	private static $replacesQuote = array("#quot#", "&lt;quote /&gt;", "&lt;quote/&gt;", "<quote />", "<quote/>");

	public function testVar($var)
	{
		var_dump($var);
		throw new \Exception("Test de variable");
	}

	public function testVarXML($node)
	{
		// $dom = new \DOMDocument();
		// $dom->appendChild($dom->importNode($node, true));

		// $dom->saveXML();
		// throw new \Exception("XML");
	}

	public function equalString(string $text1, string $text2): bool
	{
		return strtoupper(trim($text1)) == strtoupper(trim($text2));
	}

	public function stringFormatWithQuote($chaine, string $replace = "<quote />")
	{
		if (array_search($replace, self::$replacesQuote, true)) {
			self::$replacesQuote[] = $replace;
		}
		return str_replace(array("'", '"'), $replace, $chaine);
	}

	public function stringFormatWithQuoteSimple($chaine)
	{
		return str_replace(self::$replacesQuote, "'", $chaine);
	}

	public function validate_int_parameter($int)
	{
		if (!ctype_digit($int) && !is_int($int)) {
			throw new \InvalidArgumentException("La valeur passée en paramètre doit être un nombre entier dans " . get_class($this) . " et non un '" . gettype($int) . "'");
		}

		return $int;
	}

	public function validate_bool_parameter($bool)
	{
		if (!is_bool($bool)) {
			throw new \InvalidArgumentException("La valeur passée en paramètre doit être un booléen dans " . get_class($this) . " et non un '" . gettype($int) . "'");
		}

		return $bool;
	}

	public function validate_string_parameter($string)
	{
		if (!is_string($string)) {
			throw new \InvalidArgumentException("La valeur passée en paramètre doit être une chaine de carractère dans " . get_class($this) . " et non un '" . gettype($string) . "'");
		}

		return $string;
	}

	public function validate_element_list($element, array $elements, $blanc = true)
	{
		if ($blanc) {
			if (!in_array($element, $elements)) {
				throw new \InvalidArgumentException("$element est impropre à la liste d'élements valide dans " . get_class($this));
			}
		} else {
			if (in_array($element, $elements)) {
				throw new \InvalidArgumentException("$element apparait dans la liste noir d'élements invalide dans " . get_class($this));
			}
		}

		return $element;
	}

	public static function parseToArray($obj)
	{
		$liste = [];
		if (is_object($obj)) {
			$classe = new \ReflectionObject($obj);

			foreach ($classe->getProperties() as $attribut) {
				$attribut->setAccessible(true);

				$cle = $attribut->getName();
				$valeur = $attribut->getValue($obj);
				$liste[$cle] = $valeur;

				$attribut->setAccessible(false);
			}
		} else {
			throw new \InvalidArgumentException("Le paramètre doit être un Objet");
		}
		return $liste;
	}

	public function className()
	{
		return substr(get_class($this), strrpos(get_class($this), '\\') + 1);
	}

	public function classNameFrom(string $class): string
	{
		return substr($class, strrpos($class, '\\') + 1);
	}

	public function classParentName()
	{
		return substr(get_parent_class($this), strrpos(get_class($this), '\\') + 1);
	}

	public function properties()
	{
		// var_dump(get_object_vars($this));
		return get_object_vars($this);
	}

	public function propertiesStrict()
	{
		$properties = $this->properties();
		$results = [];

		foreach (array_keys(get_class_vars(get_class($this))) as $property) {
			if (key_exists($property, $properties)) {
				$results[$property] = $properties[$property];
			}
		}

		// var_dump($results);
		return $results;
	}

	protected function setBool($property, $bool)
	{
		$method = 'set' . ucfirst($property);
		if (is_callable(array($this, $method))) {
			if (is_bool($bool)) {
				$this->$property = $bool;
			} elseif (is_int($bool)) {
				$this->$property = $bool;
			}
		}
	}

	protected function setInt($property, $int)
	{
		$method = 'set' . ucfirst($property);
		if (is_callable(array($this, $method))) {
			$int = (int) $int;
			if ($int > 0) {
				$this->$property = $int;
			} else {
				$this->$property = 0;
			}
		}
	}

	protected function setString($property, $string)
	{
		$method = 'set' . ucfirst($property);
		if (is_callable(array($this, $method))) {
			if (is_string($string) || is_int($string)) {
				$this->$property = str_replace("'", "<quote />", $string);
			}
		}
	}

	protected function setDroit($property, $droit)
	{
		$method = 'set' . ucfirst($property);
		if (is_callable(array($this, $method))) {
			if (preg_match("#^[0-1]{3}$#", $droit)) {
				$this->$property = $droit;
			} else {
				throw new \RuntimeException("Droit d'acces invalide : " . $droit);
			}
		}
	}
}
