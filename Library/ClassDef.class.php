<?php

namespace Library;

class ClassDef extends ClassEntityDef
{
    const NAMESPACE_ENTITIES = "Entities";
    const NAMESPACE_MODELS   = "Models";
    const NAMESPACE_FORM_BUILDER  = "FormBuilder";
    const NAMESPACE_FORM_HEADER   = "FormHeaders";

    const NATURE_ASSOCIATION_CLASS = "AssociationClass";
    const NATURE_CLASS = "Class";
    const TYPE_CLASS_APPLICATION = "Application";
    const TYPE_CLASS_CONTROLLER = "Controller";
    const TYPE_CLASS_ENTITY = "Entity";
    const TYPE_CLASS_FORM_BUILDER = "FormBuilder";
    const TYPE_CLASS_FORM_HEADER = "FormHeader";
    const TYPE_CLASS_MANAGERS_PDO = "Manager_PDO";
    const TYPE_CLASS_MANAGERS_XML = "Manager_XML";

    /**
     * @property ClassDef
     */
    protected $parent = null;
    protected $namespace;
    /**
     * @property ClassEntityDef[]
     */
    protected $propertiesDef = array();
    /**
     * @property ClassFunctionDef[]
     */
    protected $functionsDef = array();
    /**
     * @property ClassDef $complementClassDef
     */
    protected $complementClassDef = null;
    /**
     * @param ClassDef[]
     * @param string $type
     * @return ClassDef[]
     */
    public static function listClassDefType(array $list, string $type): array
    {
        $result = [];

        foreach ($list as $key => $def) {
            if ($def->type() == $type) {
                $result[$key] = $def;
            }
        }

        return $result;
    }

    public function complementClassDef(ClassDef $def)
    {
        $this->complementClassDef = $def;
    }

    /**
     * @return ClassDef
     */
    public function getComplementClassDef()
    {
        return $this->complementClassDef;
    }

    public function addProperty(string $visibility, string $type, string $name, string $nature)
    {
        $this->propertiesDef[$name] = new ClassEntityDef(array(
            "name" => $name,
            "type" => $type,
            "visibility" => $visibility,
            "nature" => $nature,
        ));
    }

    public function addFunction(string $visibility, string $returnType, string $nature, string $name, array $parameters = [])
    {
        $this->functionsDef[$name] = new ClassFunctionDef(array(
            "name" => $name,
            "returnType" => $returnType,
            "visibility" => $visibility,
            "nature" => $nature,
            "paramtersDecl" => $parameters,
        ));
    }

    public function haveNamespace($namespace): bool
    {
        // $true = $this->namespace == $namespace ? "true" : "false";
        // var_dump($this->namespace() . " " . $namespace . " " . $this->name . " " . $true);
        return strtolower($this->namespace) == strtolower($namespace);
    }

    public function absoluteName()
    {
        return "\\" . trim(str_replace(array("<?php\n\tnamespace", ";"), array("", "\\"), $this->namespace)) . $this->name;
        // return "\\" . $this->namespace . "\\" . $this->name;
    }

    public function parentName()
    {
        return $this->parent == null ? "" : $this->parent->name();
    }

    public function hasParent(): bool
    {
        return $this->parent != null;
    }

    public function isOwnUtilitary(ClassDef $def, string $type)
    {
        return strtolower($this->name . $type) == strtolower($def->name());
    }

    public function setPropertiesDef(array $properties)
    {
        $this->propertiesDef = $properties;
    }

    /**
     * @return ClassEntityDef[]
     */
    public function propertiesDef()
    {
        return $this->propertiesDef;
    }

    /**
     * @return ClassEntityDef[]
     */
    public function propertiesDefAll()
    {
        if (!$this->hasParent()) {
            return $this->propertiesDef();
        }
        return array_merge($this->propertiesDef, $this->parent()->propertiesDefAll());
    }

    public function setFunctionsDef(array $properties)
    {
        $this->functionsDef = $properties;
    }

    /**
     * @return ClassFunctionDef[]
     */
    public function functionsDef()
    {
        return $this->functionsDef;
    }

    public function setParent(ClassDef $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return ClassDef
     */
    public function parent()
    {
        return $this->parent;
    }

    public function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;
    }

    public function namespace()
    {
        return $this->namespace;
    }

    public function path()
    {
        return trim(str_replace(array("<?php\n\tnamespace", ";"), array("", "\\"), $this->namespace)) . $this->name . ".class.php";
    }

    public function repository()
    {
        return trim(str_replace(array("<?php\n\tnamespace", ";"), array("", "\\"), $this->namespace));
    }
}
