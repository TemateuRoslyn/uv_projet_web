<?php

namespace Library;

use InvalidArgumentException;

class Page extends ApplicationComponent
{
	const VIEW_SAVE_PARAMS = ["action", "module"];
	const NS_ENTITY = "element";
	const NS_ENTITIES_LIST = "list-elements";
	/**
	 * @property StyleSheet_Manager
	 */
	protected $xslManager = null;
	protected $vars = array();
	protected $viewContent = "";
	protected $viewSave = "";
	protected $onlyViewAction = false;

	use TraitXML;

	public function __construct(Application $app, array $pagesNames = array())
	{
		parent::__construct($app);
		$this->xslManager = new StyleSheet_Manager($pagesNames);
	}

	public function initPageVars()
	{
		$this->vars = array();
		$this->xslManager->initSourceVar();
	}

	public function addVar($var, $value)
	{
		if (!is_string($var) || is_numeric($var) || empty($var)) {
			throw new \InvalidArgumentException("Le nom de la variable doit être une chaine de carractère non nulle");
		}

		if (is_array($value) || is_object($value)) {
			// var_dump($value);
			$xmlVar = @$this->xmlArrayForm($var, $value);
			// $this->testVar($xmlVar);
			$this->xslManager->addSourceVar($xmlVar);
			return;
		}

		$this->vars[$var] = $value;
		// $this->xslManager->addSourceParam($var, $value);
		$this->xslManager->setParameter('', $var, $value);
	}

	public function addForm($view)
	{
		$xmlVar = $this->stringToDOMForm($view);
		$this->xslManager->addSourceVar($xmlVar);
	}

	public function getControllerView(): string
	{
		if (empty($this->viewContent)) {
			$this->viewContent = $this->getControllerViewActu();
		}
		// $this->app->currentRoute();
		return $this->viewContent;
	}

	public function getControllerViewActu(): string
	{
		// $rep = dirname(__FILE__) . '/../Applications/' . $this->app->name() . '/Templates';
		$this->initParamRequestVars();
		// var_dump($this->view);
		if (!file_exists($this->model) || !file_exists($this->view)) {
			throw new \RuntimeException("La vue " . $this->model . " spécifiée n'existe pas : model ou style non existant!");
		}
		// $this->testVar($this->view);
		$this->xslManager->setStyle($this->view);
		$this->xslManager->setSource($this->model);
		// $this->testVar($this->app->user()->getAttribute("id"));

		$this->xslManager->setParameter('', 'current_date', date("d-m-Y"));
		$this->xslManager->setParameter('', 'admin', $this->app->user()->adminAuthentificated());
		$this->xslManager->setParameter('', 'user-flash', $this->app->user()->getFlash());
		$this->xslManager->setParameter('', 'idUser', $this->app->user()->getAttribute("id"));
		// $this->testVar($this->app->user()->getAttribute("id"));
		$this->xslManager->setParameter('', 'nbreVisiteTotal', $this->app()->nbreVisiteTotal());
		$this->xslManager->setParameter('', 'nbreVisiteForDay', $this->app()->nbreVisiteForDay());
		$this->xslManager->setParameter('', 'nbreConnectes', $this->app()->nbreConnectes());
		// $this->initSysVar();
		$content = $this->xslManager->generateHTML();
		return (is_null($content)) ? "" : $content;
	}

	protected function initParamRequestVars()
	{
		$data = $this->app()->httpRequest()->getParams();

		$this->addVar("requestScope", $data);
		$this->addVar("sessionScope", $_SESSION);
	}

	public function getGeneratedPage()
	{
		$action_view = $this->getControllerView();
		if ($this->isPDF() || $this->isTextWritter()) {
			return $this->stringFormatWithQuoteSimple($action_view);
		}

		if (in_array($this->viewSave, self::VIEW_SAVE_PARAMS)) {
			$actionSaveView = "saveView" . ucfirst($this->viewSave);
		}
		// $this->testVar($action_view);
		if ($this->onlyViewAction) {
			return $action_view;
		}

		$rep = dirname(__FILE__) . '/../Applications/' . $this->app->name() . '/Templates';
		$this->xslManager->setParameter('', 'content-action-view', $action_view);

		$this->xslManager->setStyle("$rep/layout.xsl");
		$this->xslManager->setSource("$rep/layout.xml");

		return $this->xslManager->generateHTML();
	}

	protected function saveViewAction(string $view)
	{
		// $fp = fopen('data.txt', 'w');
		// fwrite($fp, '1');
		// fwrite($fp, '23');
		// fclose($fp); 
	}

	public function initFiligranne()
	{
		$elements = $this->app->filigranne()->pile();
		$title = (isset($this->vars["title"])) ? $this->vars["title"] : 'lien';

		$elements[count($elements) - 1]->setTitle($title);
		$xmlVar = $this->xmlArrayForm("php.filigranne", $elements);

		$this->xslManager->addSourceVar($xmlVar);
	}

	public function isPDF($page = 0)
	{
		if ($page === 0) return isset($this->vars['page-pdf']);
		$page = $this->validate_bool_parameter($page);

		$this->vars['page-pdf'] = $page;
		return $page;
	}

	public function isTextWritter($page = 0)
	{
		if ($page === 0) return isset($this->vars['page-text']);
		$page = $this->validate_bool_parameter($page);

		$this->vars['page-text'] = $page;
		return $page;
	}

	public function setPDF($pdf = true)
	{
		$this->vars['page-pdf'] = 'page-pdf';
	}

	public function withoutLayout()
	{
		$this->onlyViewAction = true;
	}

	public function setViewSave(string $viewSave)
	{
		$this->viewSave = $viewSave;
	}

	public function setLocationModel($view, $model)
	{
		try {
			$view = $this->validate_string_parameter($view);
			$model = $this->validate_string_parameter($model);
		} catch (\InvalidArgumentException $e) {
			throw new InvalidArgumentException("La vue spécifier est invalide");
		}

		$this->view = $view;
		$this->model = $model;
	}

	private final function initSysVar()
	{
		foreach (array_merge($_GET, $_POST) as $key => $value) {
			$this->addVar("page" . ucfirst($key), $value);
		}

		foreach ($_SESSION as $key => $value) {
			$this->addVar("session" . ucfirst($key), $value);
		}
	}

	public function setXmlManagerPagesNames(array $names)
	{
		// $this->testVar($names);
		$this->xslManager->setPagesNames($names);
	}

	public function model(): string
	{
		return $this->model;
	}

	public function view(): string
	{
		return $this->view;
	}
}
