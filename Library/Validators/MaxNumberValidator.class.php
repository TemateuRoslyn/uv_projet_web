<?php
namespace Library\Validators;

class MaxNumberValidator extends \Library\Validator
{
	protected $maxNumber;

	public function __construct($errorMessage, $maxNumber)
	{
		parent::__construct($errorMessage);
		$this->setMaxNumber($maxNumber);
	}

	public function isValid($value)
	{
		$value = (int)$value;
		// $this->testVar($value);
		return $value <= $this->maxNumber;
	}

	public function setMaxNumber($value)
	{
		$this->maxNumber = (int)$value;
	}
}