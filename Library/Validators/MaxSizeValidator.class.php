<?php
	namespace Library\Validators;

	class MaxSizeValidator extends FileValidator
	{
		protected $maxSize;

		public function __construct ($file_upload, $maxSize)
		{
			parent::__construct($file_upload);
			$this->setMaxSize($maxSize);
		}

		public function isValid($value)
		{
			if ($this->file['size'] < $this->maxSize) 
			{
				return true;			
			}	
			return false;
		}

		public function setMaxSize($size)
		{
			$size = (int) $size;
			if ($size > 0) 
			{
				$this->maxSize = $size;
			}
		}
	}