<?php
	namespace Library\Validators;

	class MoveFileValidator extends FileValidator
	{
		public function __construct()
		{
			$this->errorMessage = "Téléchargement impossible";
		}
		
		public function isValid($value)
		{
			return is_uploaded_file($this->file['tmp_name']);
		}
	}