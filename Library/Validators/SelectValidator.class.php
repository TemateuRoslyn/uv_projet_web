<?php
namespace Library\Validators;

class SelectValidator extends \Library\Validator
{
	protected $optionsInvalides = array();

	public function __construct($errorMessage, array $optionsInvalides)
	{
		parent::__construct($errorMessage);
		$this->setOptionsInvalides($optionsInvalides);
	}

	public function isValid($value)
	{

		if (empty($value) && ((ctype_digit($value) || is_int($value)) && (in_array($value, $this->optionsInvalides) || $value == 0))) {
				// $this->testVar(ctype_digit($value));
			return false;
		}

		return true;
	}

	public function setOptionsInvalides(array $optionsInvalides)
	{
		$this->optionsInvalides = $optionsInvalides;
	}
}