<?php
namespace Library\Validators;

class PhoneNumberValidator extends \Library\Validator
{
	protected $formats = array(
		'/^6[59687][0-9]((\s)*[0-9]{3}){2}$/',
	);

	public function isValid($value)
	{
		foreach ($this->formats as $format) {
			if (preg_match($format, $value)) {
				return true;
			}
		}
		return false;
	}

	public function addFormat($format)
	{
		if (is_string($format)) {
			$this->formats[] = $format;
		}
	}
}