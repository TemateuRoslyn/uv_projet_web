<?php
	namespace Library\Validators;

	class MaxLengthValidator extends \Library\Validator
	{
		protected $maxLength;

		public function __construct($errorMessage, $maxLength)		
		{
			parent::__construct($errorMessage);
			$this->setMaxLength($maxLength);	
		}

		public function isValid($value)
		{
			return strlen($value) <= $this->maxLength;
		}

		public function setMaxLength($value)
		{
			$value = (int) $value;
			if ($value > 0) {
				$this->maxLength = $value;
			}
			else
			{
				throw new \RuntimeException("La longueur maximale doit être un nombre supérieur à 0");
			}
		}
	}