<?php
	namespace Library\Validators;

	class MinNumberValidator extends \Library\Validator
	{
		protected $minNumber;

		public function __construct($errorMessage, $minNumber)		
		{
			parent::__construct($errorMessage);
			$this->setMinNumber($minNumber);	
		}

		public function isValid($value)
		{
			$value = (int) $value;
			return $this->minNumber <= $value;
		}

		public function setMinNumber($value)
		{
			$this->minNumber = (int) $value;
		}
	}