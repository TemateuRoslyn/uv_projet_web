<?php
	namespace Library\Validators;

	class MinLengthValidator extends \Library\Validator
	{
		protected $minLength;

		public function __construct($errorMessage, $minLength)		
		{
			parent::__construct($errorMessage);
			$this->setMinLength($minLength);	
		}

		public function isValid($value)
		{
			return $this->minLength <= strlen($value);
		}

		public function setMinLength($value)
		{
			$value = (int) $value;
			if ($value > 0) {
				$this->minLength = $value;
			}
			else
			{
				throw new \RuntimeException("La longueur minimale doit être un nombre supérieur à 0");
			}
		}
	}