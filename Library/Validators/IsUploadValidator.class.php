<?php
	namespace Library\Validators;

	class IsUploadValidator extends FileValidator
	{
		public function isValid($value)
		{
			switch ($this->file["error"]) 
			{
				case UPLOAD_ERR_NO_FILE:
					$this->errorMessage = "fichier manquant";
					break;
				
				case UPLOAD_ERR_INI_SIZE:
					$this->errorMessage = "fichier dépassant la taille maximale autorisée par le serveur";
					break;

				case UPLOAD_ERR_FORM_SIZE:
					$this->errorMessage = "fichier dépassant la taille maximale autorisée par le formulaire";
					break;

				case UPLOAD_ERR_PARTIAL:
					$this->errorMessage = "fichier transferé partiellement";
					break;
				
				default:
					return true;
					break;
			}

			return false;
		}
	}