<?php
	namespace Library\Validators;

	class TypeValidator extends FileValidator
	{
		protected $extensions_valides = array();
		
		public function __construct ($file_upload, array $extensions_valides)
		{
			parent::__construct($file_upload);
			$this->setExtensionsValides($extensions_valides);
		}

		public function isValid($value)
		{
			// $this->testVar($this->file);
			$extension_upload = strtolower(substr(strrchr($this->file['name'], '.'), 1));
			if (in_array($extension_upload, $this->extensions_valides)) 
			{
				return true;	
			}

			$this->errorMessage = "Extension incorrecte !";	
			return false;
		}

		public function setExtensionsValides(array $extensions_valides)
		{
			foreach ($extensions_valides as $extension) 
			{
				if (is_string($extension)) 
				{
					$this->extensions_valides[] = $extension; 	
				}
				else
				{
					throw new \InvalidArgumentException("L'extension doit être une chaine de carractère");
					
				}
			}
		}
	}