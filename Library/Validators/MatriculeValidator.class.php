<?php
	namespace Library\Validators;

	class MatriculeValidator extends \Library\Validator
	{
		protected $formats = array(
						'/^CM-UDS.*/i',
					);

		public function __construct($errorMessage, array $formats = array())
		{
			parent::__construct($errorMessage);
			foreach ($formats as $format) 
			{
				$this->addFormat($format);	
			}
		}

		public function isValid($value)
		{
			foreach ($this->formats as $format) 
			{
				if (preg_match($format, $value)) 
				{
					return true;	
				}	
			}
			return false;
		}

		public function addFormat($format)
		{
			if (is_string($format)) 
			{
				$this->formats[] = $format;	
			}
		}
	}