<?php
	namespace Library\Validators;

	abstract class FileValidator extends \Library\Validator
	{
		protected $file = null;

		public function __construct($file_upload)
		{
			parent::__construct("Message non définit");

			$this->file = isset($_FILES[$file_upload]) ? $_FILES[$file_upload] : null;
		}

		public function fileExist()
		{
			return !is_null($this->file);
		}
	}