<?php
	namespace Library\Validators;

	class MinSizeValidator extends FileValidator
	{
		protected $minSize;

		public function __construct ($file_upload, $minSize)
		{
			parent::__construct($file_upload);
			$this->setminSize($minSize);
		}

		public function isValid($value)
		{
			if ($this->file['size'] < $this->minSize) 
			{
				return true;			
			}	
			return false;
		}

		public function setminSize($size)
		{
			$size = (int) $size;
			if ($size > 0) 
			{
				$this->minSize = $size;
			}
		}
	}