<?php

namespace Library\Dao;

use Library\PObject;
use Library\Application;
use PDO;
use PDOException;

class DAOFactory extends PObject
{
	/**
	 * @property PDO
	 */
	private static $dao = null;

	public static function getMysqlConnexion()
	{
		if (self::$dao == null) {
			self::$dao = self::establishConnexion();
		}

		return self::$dao;
	}

	private static function establishConnexion()
	{
		$xmlConfig = Application::loadAppConfig();
		$xmlConfig = $xmlConfig->daoConfig;

		$host = $xmlConfig->host;
		$dbname = $xmlConfig->dbname;
		$user = $xmlConfig->user;
		$passwrd = $xmlConfig->passWord;

		try {
			$db = new \PDO("mysql:host=$host;dbname=$dbname", $user, $passwrd);
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

			return $db;
		} catch (PDOException $th) {
			return null;
		}
	}
}
