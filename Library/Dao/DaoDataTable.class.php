<?php

namespace Library\Dao;

use Library\Writters\FilesMakerInterface;
use PDOException;

class DaoDataTable implements FilesMakerInterface
{
    private $dao;
    private $tablesColumns = array();

    public function __construct(\PDO $dao)
    {
        $this->dao = $dao;
        $this->initTablesNames();
        $this->initTablesProperties();
    }

    protected final function initTablesNames()
    {
        $connexion = $this->dao;
        try {
            $tables = array();
            $result = $connexion->query("SHOW TABLES");
            while ($row = $result->fetch(\PDO::FETCH_NUM)) {
                $this->tablesColumns[$row[0]] = array();
            }
        } catch (PDOException $e) {
            // echo $e->getMessage();
        }
    }

    public function classLinkAssociations()
    {
    }

    protected final function initTablesProperties()
    {
        $connexion = $this->dao;

        foreach ($this->tables() as $table) {
            $reponse = $connexion->query("SELECT * FROM $table LIMIT 1,1");

            $nbrColumn = $reponse->columnCount();
            for ($i = 1; $i < $nbrColumn - 2; $i++) {
                $this->tablesColumns[$table][] = $reponse->getColumnMeta($i)['name'];
            }

            $reponse->closeCursor();
        }
    }

    public function tableProperties($table)
    {
        if (isset($this->tablesColumns[$table])) {
            return $this->tablesColumns[$table];
        }
        return array();
    }

    public function tables()
    {
        return array_keys($this->tablesColumns);
    }

    public function getClassDefs()
    {
    }
}
