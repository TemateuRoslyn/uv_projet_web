<?php
namespace Library;

abstract class FormHeader extends PObject
{
	/**
	 * @var HTTPRequest
	 */
	protected $request = null;
	/**
	 * @var Entity
	 */
	protected $entity = null;

	public function __construct(HTTPRequest $request, Entity $entity)
	{
		$this->request = $request;
		$this->entity = $entity;
	}

	protected function dataForm() : array
	{
		$properties = $this->entity->properties();
		$data = [];

		foreach (array_keys($properties) as $property) {
			$data[$property] = $this->request->getParam($property);
		}

		return $data;
	}

	public function entity() : entity
	{
		$this->entity->hydrate($this->dataForm());
		return $this->entity;
	}
}