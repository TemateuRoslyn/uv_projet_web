<?php

namespace Library;

class ClassLinkAssociation extends Entity
{
    protected $signature = "";
    /**
     * @property Owner
     */
    protected $ownerUp = null;
    /**
     * @property Owner
     */
    protected $ownerDown = null;
    /**
     * @property ClassDef
     */
    protected $associationClass = null;

    public function sens(): string
    {
        return $this->ownerUp->classDef()->name() . "_" . $this->ownerDown->classDef()->name();
    }

    public function isValid()
    {
        return true;
    }

    public function setSignature(string $owned)
    {
        $this->signature = $owned;
    }

    public function setOwnerUp(Owner $owned)
    {
        $this->ownerUp = $owned;
    }

    public function setOwnerDown(Owner $owned)
    {
        $this->ownerDown = $owned;
    }

    public function setAssociationClass(ClassDef $def)
    {
        $this->associationClass = $def;
    }

    /**
     * @return Owner
     */
    public function ownerUp()
    {
        return $this->ownerUp;
    }

    /**
     * @return Owner
     */
    public function ownerDown()
    {
        return $this->ownerDown;
    }

    /**
     * @return ClassDef
     */
    public function associationClass()
    {
        return $this->associationClass;
    }

    public function signature(): string
    {
        return $this->signature;
    }
}

class Owner extends Entity
{
    /**
     * @property ClassDef
     */
    protected $classDef = null;
    protected $cardinaliteUp = 0;
    protected $cardinaliteDown = 0;
    protected $aggregation = "";

    public function isValid()
    {
        return true;
    }

    public function setClassDef(ClassDef $def)
    {
        $this->classDef = $def;
    }

    public function setCardinaliteUp(string $def)
    {
        $this->cardinaliteUp = $def;
    }

    public function setCardinaliteDown(string $def)
    {
        $this->cardinaliteDown = $def;
    }

    public function setAggregation(string $def)
    {
        $this->aggregation = $def;
    }

    public function classDef()
    {
        return $this->classDef;
    }

    public function cardinaliteUp()
    {
        return $this->cardinaliteUp;
    }

    public function cardinaliteDown()
    {
        return $this->cardinaliteDown;
    }

    public function aggregation()
    {
        return $this->aggregation;
    }
}
