<?php
	namespace Library;

	abstract class DocumentFormBuilder extends FormBuilder
	{
		protected $auteurs  = array();
		protected $domaines = array();
		protected $uvs      = array();
		protected $editors  = array();

		protected $nature;

		public function __construct(Entity $entity, array $auteurs, array $domaines, array $uvs, array $editors)
		{
			parent::__construct($entity);
			$this->setAuteurs($auteurs);
			$this->setDomaines($domaines);
			$this->setUvs($uvs);
			$this->setEditors($editors);
		}

		public function build()
		{
			$this->form->add(new \Library\Fields\FileField(array(
 					'name' => 'upTitre',
 					'maxSize' => 4156796,
 					'validators' => array(
							new \Library\Validators\NotNullValidator('Spécifier le fichier'),
							new \Library\Validators\MaxSizeValidator('Le fichier est trop volumineux', 4156796),
 						),
 				)))->add(new \Library\Fields\TextField(array(
					'name' => 'description',
					'rows' => 7,
					'cols' => 50,
					'validators' => array(
										new \Library\Validators\NotNullValidator('Merci de spécifier votre commentaire'),
									),
				)))->add(new \Library\Fields\SelectField(array(
					'name' => 'idAuteur',
					'options' => $this->auteurs,
					),
				)))->add(new \Library\Fields\SelectField(array(
					'name' => 'idDomaine',
					'options' => $this->domaines
					
				)))->add(new \Library\Fields\SelectField(array(
					'name' => 'idUnitedevaleur',
					'options' => $this->uvs,
					'validators' => array(        
			 				new \Library\Validators\SelectValidator('Merci de selectionner une Unité de valeur !', array(
			 						'Inconnue',
			 					)),        
						),
				)))->add(new \Library\Fields\SelectField(array(
					'name' => 'idEditor',
					'options' => $this->editors,
					'validators' => array(        
			 				new \Library\Validators\SelectValidator('Merci de selectionner une filiére !', array(
			 						'Maison d\'édition',
			 					)),        
						),

				)));
				
		}

		public function setAuteurs(array $auteurs)
		{
			if (strtolower($auteurs[0]) != 'inconnu(e)' && strtolower($auteurs[0]) != 'inconnu' && strtolower($auteurs[0]) != 'inconnue') 
			{
				array_unshift($auteurs, 'INCONNU(E)');
				array_unique($auteurs);
			}
			array_unshift($auteurs, 'Auteur');
			$this->auteurs = $auteurs;
		}

		public function setDomaines(array $domaines)
		{
			if (strtolower($domaines[0]) != 'inconnu(e)' && strtolower($domaines[0]) != 'inconnu' && strtolower($domaines[0]) != 'inconnue') 
			{
				array_unshift($domaines, 'INCONNU(E)');
				array_unique($domaines);
			}
			array_unshift($domaines, 'Domaine');
			$this->domaines = $domaines;
		}

		public function setUvs(array $uvs)
		{
			if (strtolower($uvs[0]) != 'inconnu(e)' && strtolower($uvs[0]) != 'inconnu' && strtolower($uvs[0]) != 'inconnue') 
			{
				array_unshift($uvs, 'INCONNU(E)');
				array_unique($uvs);
			}
			array_unshift($uvs, 'Unité de valeur');
			$this->uvs = $uvs;
		}

		public function setEditors(array $editors)
		{
			if (strtolower($editors[0]) != 'inconnu(e)' && strtolower($editors[0]) != 'inconnu' && strtolower($editors[0]) != 'inconnue') 
			{
				array_unshift($editors, 'INCONNU(E)');
				array_unique($editors);
			}
			array_unshift($editors, 'Maison d\'édition');
			$this->editors = $editors;
		}

		public function nature()
		{
			return strtoupper($this->nature);
		}
	}