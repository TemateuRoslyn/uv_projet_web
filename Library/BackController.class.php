<?php

namespace Library;

use Library\Dao\DAOFactory;
use AppliLib\NESchool\EPage;
use Library\Dao\DaoDataTable;
use Library\UmlModel\PackageManager_UML;
use Library\Writters\FilesMaker;


class BackController extends ApplicationComponent
{
	/**
	 * @property XMLFileControl
	 */
	protected $xmlFileController = null;
	/**
	 * @property DaoDataTable
	 */
	protected $daoDataTable = null;
	/**
	 * @property PackageManager_UML
	 */
	protected $packageManager = null;
	/**
	 * @property WrittersManager
	 */
	protected $writterManager = null;
	protected $action = '';
	protected $module = '';
	protected $viewAction = '';
	protected $viewActionXML = '';
	protected $viewActionXSL = '';
	protected $viewSave = '';
	/**
	 * @property Page
	 */
	protected $page = null;
	protected $view = '';
	/**
	 * @property Managers
	 */
	protected $managers = null;
	protected $managersXML = null;
	protected $pagesName = array();
	/** @property Tools */
	protected $tools = null;

	function __construct(Application $app, $module, $action, $view, $pageTitle, array $pagesName, string $viewXML = "", string $viewXSL = "", string $viewSave = "")
	{
		parent::__construct($app);

		$this->managers = new Managers('PDO', DAOFactory::getMysqlConnexion());
		$this->managersXML = new Managers('XML', new \DOMDocument("1.0", "UTF-8"));

		$this->tools = new Tools($app, $module, $action);
		// $this->modelesWritter();

		$this->pagesName = $pagesName;
		$this->viewAction = empty($view) ? $action : $view;
		// var_dump($viewXML);
		$this->viewActionXML = $viewXML;
		$this->viewActionXSL = $viewXSL;
		$this->viewSave = $viewSave;
		$this->initContext($app, $module, $action, $pageTitle);
		$this->initContextPage();
	}

	protected function initContext(Application $app, $module, $action, $pageTitle)
	{
		$this->page = new Page($app, $this->pagesName);
		// $this->page->setXmlManagerPagesNames($this->pagesName);
		$this->page->addVar('title', $pageTitle);
		// $this->testVar("");
		$this->setModule($module);
		$this->setAction($action);
		// $this->xmlFileController = new XMLFileControl($this->page, $this->managers);
	}

	protected function initContextPage()
	{
		$model = empty($this->viewActionXML) ? $this->viewAction : $this->viewActionXML;
		$mapping = empty($this->viewActionXSL) ? $this->viewAction : $this->viewActionXSL;
		// var_dump($this->viewAction);
		// $this->testVar("$model $mapping");
		$this->page->setViewSave($this->viewSave);
		$this->useViewM($model, $mapping);
	}

	protected final function modelesWritter(): void
	{
		if ($this->app()->appConfigCanWriteScheme()) {
			$this->writterManager = new WrittersManager();
		}
	}

	public function execute()
	{
		if (!empty($this->action)) {
			// $this->testVar("");
			$this->_execute();
		}

		if (!$this->app()->isForWardMode()) {
			$this->app->httpResponse()->setPage($this->page);
		}
	}

	public function _execute()
	{
		$method = 'execute' . ucfirst($this->action);
		if (!is_callable(array($this, $method))) {
			throw new \RuntimeException("L'action " . $this->action . " n'est pas définie sur le module : " . $this->module);
		}

		if (!$this->app()->isForWardMode()) {
			// var_dump($this->className());ù
			$this->$method($this->app->httpRequest());
		}

		if (!$this->app()->isForWardMode()) {
			$this->app->httpResponse()->setPage($this->page);
		}

		$this->managers->saveXMLFile();
	}

	public function formHandler($classeName, HTTPRequest $request, $modified = false, $options = array()): FormHandler
	{
		$formHeaderFactory = new \Library\FormHeaderFactory($request);
		$classe = $formHeaderFactory->get($classeName);

		if ($modified !== false && $request->method() != "POST") {
			$classe = $this->getElementExist($classeName, $modified);
			$classe->prepareToModify();
		}

		if ($modified !== false && $request->method() == "POST") {
			$classe->setId($modified);
		}

		$formHandlerFactory = new \Library\FormHandlerFactory($this->managers, $classe, $request, $options);
		$formHandlerFactory->build();
		// $this->testVar($classe);

		return $formHandlerFactory->formHandler();
	}

	protected function getElementExist($clasName, $id)
	{
		$element = $this->managers->getManagerOf($clasName)->getUnique($id);
		if (is_null($element)) $this->app->httpResponse()->redirect404();

		return $element;
	}

	public function formHandlerFromEntity(string $classeName, HTTPRequest $request, $entity, $options = array()): FormHandler
	{
		$formHeaderFactory = new \Library\FormHeaderFactory($request);
		$classe = $formHeaderFactory->get($classeName);

		if ($entity != null && $request->method() != "POST") {
			$entity->prepareToModify();
			$classe = $entity;
		}

		if ($entity != null && $request->method() == "POST") {
			$classe->setId($entity->id());
		}

		$formHandlerFactory = new \Library\FormHandlerFactory($this->managers, $classe, $request, $options);
		$formHandlerFactory->build();

		return $formHandlerFactory->formHandler();
	}
	// GETTER //

	public function page(): Page
	{
		return $this->page;
	}

	public function manager(): Managers
	{
		return $this->managers;
	}

	// SETTER //

	public function setPage(Page $page)
	{
		$this->page = $page;
	}

	public function setModule($module)
	{
		if (!is_string($module) || empty($module)) {
			throw new \InvalideArgumentException("Le module doit être une chaine de carractère valide");
		}

		$this->module = $module;
	}

	public function setAction($action)
	{
		if (!is_string($action) || empty($action)) {
			throw new \InvalideArgumentException("L'action doit être une chaine de carractère valide");
		}

		$this->action = $action;
	}

	public function setView($view)
	{
		if (!is_string($view) || empty($view)) {
			throw new \InvalideArgumentException("La vue doit être une chaine de carractère valide");
		}

		$this->view = $view;
		$rep = __DIR__ . '/../Applications/' . $this->app->name() . '/Modules/' . $this->module . '/Views';

		$this->page->setLocationModel($rep . '/' . $this->view . '.xsl', $rep . '/' . $this->view . '.xml');
		// $this->page->addVar("app-module-vue", $this->app->name() . '-' . $this->module . '-' . $this->view);
	}

	protected function useView(string $model, string $controllerName = "", string $appName = "")
	{
		$app = (!empty($appName)) ? $appName : $this->app->name();
		$module = (!empty($controllerName)) ? $controllerName : $this->module;

		$rep = __DIR__ . '/../Applications/' . $app . '/Modules/' . $module . '/Views';
		$this->page->setLocationModel($rep . '/' . $model . '.xsl', $rep . '/' . $model . '.xml');
	}

	/**
	 * @param string $mapping .xml
	 * @param string $model .xsl
	 */
	protected function useViewM(string $mapping, string $model)
	{
		$app = $this->app->name();
		$module = $this->module;

		$rep = __DIR__ . '/../Applications/' . $app . '/Modules/' . $module . '/Views';
		$this->page->setLocationModel($rep . '/' . $model . '.xsl', $rep . '/' . $mapping . '.xml');
	}
}
