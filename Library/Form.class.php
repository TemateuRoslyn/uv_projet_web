<?php

namespace Library;

class Form extends PObject
{
	protected $entitty;
	protected $fields;
	private $fileUpload;

	public function __construct(Entity $entitty, $file_upload = "")
	{
		$this->setEntity($entitty);
	}

	public function add(Field $field)
	{
		$attr = $field->name();
		try {
			$value = $this->entitty->$attr();
		} catch (\TypeError $th) {
			$value = null;
		}

		$value = is_null($value) ? null : (is_string($value) ? $this->stringFormatWithQuote($value) : $value);
		$field->setValue($value);

		$this->fields[] = $field;
		return $this;
	}

	public function upload(FileUpload $file_upload)
	{
		$this->fileUpload = $file_upload;
	}

	public function createView(): string
	{
		$view = '';
		foreach ($this->fields as $field) {
			$view .= '<div class="col-12 mb-3">';
			$view .= $this->stringFormatWithQuote($field->buildWidget());
			$view .= '</div>';
		}
		return $view;
	}

	public function createXMLView(): string
	{
		$view = '<form-data>';
		foreach ($this->fields as $field) {
			$view .= '<data-element>';
			$view .= $field->buildWidget();

			$view .= $field->getIcon();
			$view .= '</data-element>';
		}
		$view .= '</form-data>';

		return $view;
	}

	public function hasFileUpload()
	{
		return isset($this->fileUpload);
	}

	public function isValid()
	{
		if ($this->hasFileUpload() && $this->fileUpload->fileExist() && !$this->fileUpload->isValid())
			return false;

		$valid = true;
		foreach ($this->fields as $field) {
			if (!$field->isValid()) {
				$valid = false;
			}
		}

		return $valid;
	}

	public function entity(): Entity
	{
		return $this->entitty;
	}

	public function setEntity(Entity $entitty)
	{
		$this->entitty = $entitty;
	}

	public function fileUpload()
	{
		return $this->fileUpload;
	}
}
