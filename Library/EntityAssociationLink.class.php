<?php

namespace Library;

class EntityAssociationLink extends Entity
{
    protected $table;
    protected $idTable;
    protected $signature;

    public function isValid()
    {
        return !empty($this->table);
    }

    public function hydrate(array $donnees)
    {
        parent::hydrate($donnees);
        $this->signature = $this->table . $this->idTable;
    }

    public function signature()
    {
        return $this->signature;
    }

    public function setSignature($table)
    {
        $this->signature = $table;
    }

    public function table()
    {
        return $this->table;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function idTable()
    {
        return $this->idTable;
    }

    public function setIdTable($table)
    {
        $this->idTable = $table;
    }

    public function isToEntity(Entity $entity): bool
    {
        return $this->table == $entity->className() && $this->idTable == $entity->id();
    }
}
