<?php

namespace Library;

class  ManagersPideria_PDO extends Managers_PDO
{
    /**
     * Tableau des wclefs d'association $entities => $entity suivant l'ordre 
     * $entities passées à la fonction addAssociations
     */
    private $associations = array();
    protected $tableAxe = "";
    /**
     * @property Entity_PDO
     */
    protected $entity_PDO;

    function __construct($api, $dao)
    {
        $this->api = $api;
        $this->dao = $dao;
    }

    public function addLink(int $idParent, string $parent, int $idChild, string $child): int
    {
        $id_pideria_generalisation = $this->linkGeneralization($parent, $child);
        if ($id_pideria_generalisation) {
            $this->dao->query(
                "INSERT INTO `_pideria_inner_generalisation` 
                 (`id`, `id_pideria_generalisation`, `idTableParent`, `idTable`, `signature`) 
                 VALUES (NULL, '$id_pideria_generalisation', '$idParent', '$idChild', '$idParent.$parent.$idChild.$child')
                 ON DUPLICATE KEY UPDATE
                 `id_pideria_generalisation`=VALUES(`id_pideria_generalisation`),
                 `idTableParent`=VALUES(`idTableParent`),
                 `idTable`=VALUES(`idTable`),
                 `signature`=VALUES(`signature`)
                 "
            );
            return $this->dao->lastInsertId();
        }

        throw new \RuntimeException("Auncune généralisation définie pour les tables $parent => $child");
    }

    public function addAssociations(array $entities, Entity $entityAxe): void
    {
        $sql = "INSERT INTO `_pideria_inner_owner` 
                (`id`, `tableUp`, `idTableUp`, `tableDown`, `idTableDown`, `signature`) 
                VALUES ";

        $pos = 0;
        foreach ($entities as $entity) {
            $signature = $entityAxe->className() . $entityAxe->id() . $entity->className() . $entity->id();
            $this->associations[] = $signature;

            $sql .= "(NULL, '" . $entityAxe->className() . "', 
                    '" . $entityAxe->id() . "', '" . $entity->className() . "', '" . $entity->id() . "', '" . $signature . "')";
            if (++$pos != count($entities)) $sql .= ",";
        }

        $sql .= "ON DUPLICATE KEY UPDATE
        `tableUp`=VALUES(`tableUp`),
        `idTableUp`=VALUES(`idTableUp`),
        `tableDown`=VALUES(`tableDown`),
        `idTableDown`=VALUES(`idTableDown`),
        `signature`=VALUES(`signature`)";
        // echo ($sql);
        $this->dao->query($sql);
    }

    public function addAssociationClass(array $entitiesL, \Library\Entity $entity, array $entitiesR): void
    {
        $this->addAssociations($entitiesL, $entity);
        $associations = $this->associations;

        $this->addAssociations($entitiesR, $entity);
        $associations = array_merge($this->associations, $associations);

        $sql = "INSERT INTO `_pideria_inner_owner_class` 
                (`id`, `signature_pideria_inner_owner`, `nameTable`, `idRegister`, `signature`) 
                VALUES ";

        $pos = 0;
        foreach ($associations as $signature) {
            $sql .= "(NULL, '" . $signature . "', '" . $entity->className() . "', '"  . $entity->id() . "', '" . $signature . $entity->className() . $entity->id() . "')";
            if (++$pos != count($associations)) $sql .= ",";
        }

        $sql .= "ON DUPLICATE KEY UPDATE
        `signature_pideria_inner_owner`=VALUES(`signature_pideria_inner_owner`),
        `nameTable`=VALUES(`nameTable`),
        `idRegister`=VALUES(`idRegister`),
        `signature`=VALUES(`signature`)";
        // echo ($sql);
        $this->dao->query($sql);
    }

    /**
     * @param string|null $parent
     * @param string $childL
     * @param string $childR
     */
    public function linkAssociation($parent, string $childL, string $childR): string
    {
        $parent = ucfirst($parent);

        $childL = ucfirst($childL);
        $childR = ucfirst($childR);

        $message = "SELECT `signature` FROM _pideria_association WHERE (`sens`=:link OR `sens`=:link) AND `associationtable`=:parent ";
        $requete = $this->dao->prepare($message);

        $requete->bindValue(":link", $childL . "_" . $childR);
        $requete->bindValue(":parent", $parent);
        $requete->execute();

        $id = $requete->fetchColumn();
        $requete->closeCursor();

        return $id;
    }

    public function thirdMemberAssociation(string $first, string $second): string
    {
        $message = "SELECT * FROM `_pideria_association` WHERE `associationtable` != 'NULL'";

        $requete = $this->dao->prepare($message);
        $requete->execute();

        foreach ($requete->fetch() as $data) {
            $parentAssoc = $data["associationtable"];
            $secondAssoc = strtolower(substr(strrchr($data["sens"], '_'), 1));
            $firstAssoc = strtolower(str_replace($secondAssoc . "_", "", $data["sens"]));

            if (($firstAssoc == $first || $secondAssoc == $first) && ($firstAssoc == $second || $secondAssoc == $second)) {
                $requete->closeCursor();
                return $parentAssoc;
            }

            if ($parentAssoc == $first && $firstAssoc == $second) {
                $requete->closeCursor();
                return $firstAssoc;
            }

            if ($parentAssoc == $second && $firstAssoc == $first) {
                $requete->closeCursor();
                return $secondAssoc;
            }
        }

        throw new \PDOException("Aucune Association de type classe définie");
    }

    public function linkGeneralization(string $parent, string $child): int
    {
        $parent = ucfirst($parent);
        $child = ucfirst($child);

        $message = "SELECT id FROM _pideria_generalisation WHERE `signature`=:link ";
        $requete = $this->dao->prepare($message);

        $requete->bindValue(":link", $parent . "_" . $child);
        $requete->execute();

        $id = $requete->fetchColumn();
        $requete->closeCursor();

        return (int) $id;
    }

    public function getAssociateClassTo(\Library\Entity $entity, int $debut = -1, int $limit = -1): array
    {
        return [];
    }

    public function getAssociateTo(Entity $entity, string $mapGet = "", string $mapRestrictJoin = "", string $mapRestrictWhere = "", int $debut = -1, int $limit = -1): array
    {
        return [];
    }

    public function setTableAxe(string $tableAxe)
    {
        $this->tableAxe = $tableAxe;
    }

    public function setEntityPDO(Entity_PDO $entity_PDO)
    {
        $this->entity_PDO = $entity_PDO;
    }
}
