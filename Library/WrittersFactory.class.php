<?php

namespace Library;

use Library\Dao\DaoDataTable;
use Library\Dao\DAOFactory;
use Library\UmlModel\PackageManager_UML;
use Library\UmlStates\Parsor\PackageManager_UML as ParsorPackageManager_UML;
use Library\Writters\AbstractMaker;
use Library\Writters\FilesMaker;
use Library\Writters\FilesMakerInterface;
use Library\Writters\PDOMakerClassDef;

class WrittersFactory extends PObject
{
    const PDO_MAKER = "PDO";
    const FILE_MAKER = "Files";

    /**
     * @return AbstractMaker
     */
    public function getMakerFor(string $source, FilesMakerInterface $descriptor)
    {
        $obj = "Library\Writters\\" . $source . "Maker";
        return new $obj($descriptor);
    }

    /**
     * @return AbstractMaker[]
     */
    public function getMakersFrom(FilesMakerInterface $descriptor)
    {
        $class = get_class($descriptor);
        $source0 = "";
        $source1 = "";

        if (PackageManager_UML::class == $class) {
            $source0 = self::PDO_MAKER;
            $source1 = self::FILE_MAKER;

            $obj0 = "Library\Writters\\" . $source0 . "Maker";
            $obj1 = "Library\Writters\\" . $source1 . "Maker";

            // return [new $obj0($descriptor)];
            return [new $obj0($descriptor), new $obj1($descriptor)];
        } else 
            if (DaoDataTable::class == $class || ParsorPackageManager_UML::class == $class) {
            $source1 = self::FILE_MAKER;
            // $obj1 = "Library\Writters\FilesMakerStates";
            $obj1 = "Library\Writters\\" . $source1 . "Maker";

            // return [new $obj0($descriptor)];
            return [new $obj1($descriptor)];
        }
    }

    /**
     * @return FilesMakerInterface
     */
    public function getMakerDescriptor(string $source)
    {
        if (WrittersManager::SOURCE_SCHEMA_UML_STATES == $source) {
            return new ParsorPackageManager_UML();
        }

        if (WrittersManager::SOURCE_SCHEMA_UML == $source) {
            return new PackageManager_UML();
        }

        if (WrittersManager::SOURCE_SCHEMA_DB == $source) {
            return new DaoDataTable(DAOFactory::getMysqlConnexion());
        }

        throw new \RuntimeException("Aucune interface définie pour cette source !");
    }
}
