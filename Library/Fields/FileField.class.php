<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class FileField extends \Library\Field
{
	protected $maxSize;
	protected $typeFile;

	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		if (empty($widget)) {
			$widget .= '<span class="label label-info">Document validé. ' .
				$this->value
				. '</span><span> Veuillez le selectionner à nouveau</span>';
		}

		$widget .= '<input type="hidden" name="MAX_FILE_SIZE" value="' . $this->maxSize . '" /> ';
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_FILE, $this->value, FieldUtilitary::CLASS_FORM, $this->label, "", "", true);
	}

	public function setMaxSize($maxSize)
	{
		$maxSize = (int)$maxSize;

		if ($maxSize > 0) {
			$this->maxSize = $maxSize;
		} else {
			throw new \RuntimeException('La taille maximale doit être un nombre supérieur à 0');
		}
	}
}