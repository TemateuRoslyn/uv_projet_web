<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class PassWordField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_PWD, $this->value, $this->class, $this->label, $this->placeholder);
	}
}