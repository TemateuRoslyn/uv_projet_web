<?php
namespace Library\Fields;

use Library\FieldUtilitary;

class RealField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		$widget .= "" . FieldUtilitary::motifInputReal($this->name, $this->value, FieldUtilitary::CLASS_FORM, $this->label);
		// $this->testVar($widget);
		return $widget;
	}
}