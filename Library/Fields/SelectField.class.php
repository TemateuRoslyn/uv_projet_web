<?php
namespace Library\Fields;

class SelectField extends \Library\Field
{
	protected $options = array();
	protected $multiple;

	protected $show = true;

	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();

		if (!$this->show) {
			return $widget;
		}

		$widget .= '<label>' . $this->placeholder . '</label><select class="form-control required" name="' . $this->name . '"';
		$widget .= ' id="' . $this->name . '"';

		if (isset($this->multiple)) {
			$widget .= ' multiple="multiple"';
		}
		$widget .= '>';

		foreach ($this->options as $value => $champ) {
			if ($value == $this->value) {
				$widget .= ' <option value="' . htmlspecialchars($value) . '" selected="selected">' . htmlspecialchars($champ) . '</option>';
			} else {
				$widget .= ' <option value="' . htmlspecialchars($value) . '">' . htmlspecialchars($champ) . '</option>';
			}
		}

		return $widget .= '</select>';
	}

	public function setOptions(array $options)
	{
		foreach ($options as $key => $value) {
			$this->options[$key] = "$value";
		}
	}

	public function setShow($show)
	{
		if (is_bool($show)) {
			$this->show = $show;
		}
	}
}