<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class NumberField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_NUMBER, $this->value, $this->class, $this->label);
	}
}