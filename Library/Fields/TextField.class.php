<?php
namespace Library\Fields;

class TextField extends \Library\Field
{
	protected $cols;
	protected $rows;

	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		
		$widget .= "<label for='".$this->name."'>".$this->label."</label>";
		$widget .= '<textarea class="autogrow ' . $this->class . '" name="' . $this->name . '"';
		if (!empty($this->cols)) {
			$widget .= ' cols="' . $this->cols . '"';
		}

		if (!empty($this->rows)) {
			$widget .= ' rows="' . $this->rows . '"';
		}

		$widget .= ' placeholder="' . $this->placeholder . '">';
		if (!empty($this->value)) {
			$widget .= htmlspecialchars($this->value);
		}

		return $widget . '</textarea>';
	}

	public function setCols($cols)
	{
		$cols = (int)$cols;
		if ($cols > 0) {
			$this->cols = $cols;
		}
	}

	public function setRows($rows)
	{
		$rows = (int)$rows;
		if ($rows > 0) {
			$this->rows = $rows;
		}
	}
}