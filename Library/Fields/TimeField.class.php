<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class TimeField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_TIME, $this->value, FieldUtilitary::CLASS_FORM, $this->label, $this->placeholder);
	}
}