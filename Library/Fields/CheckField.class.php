<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class CheckField extends \Library\Field
{
	public function buildWidget()
	{
		return FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_CHECK, $this->value, FieldUtilitary::CLASS_FORM, $this->label);
	}
}