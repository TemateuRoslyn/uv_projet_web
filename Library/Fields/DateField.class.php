<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class DateField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_DATE, $this->value, $this->class, $this->label, $this->placeholder);
	}

	public function setValue($value)
	{
		if ($value instanceof \DateTime) {
			parent::setValue($value->format("Y-m-d"));
		} elseif (!empty($value)) {
			parent::setValue(date_create($value)->format("Y-m-d"));
		}
	}
}