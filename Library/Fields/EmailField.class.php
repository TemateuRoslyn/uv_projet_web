<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class EmailField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_EMAIL, $this->value, $this->class, $this->label, $this->placeholder);
	}
}