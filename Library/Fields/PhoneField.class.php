<?php
namespace Library\Fields;

use Library\FieldUtilitary;


class PhoneField extends \Library\Field
{
	public function buildWidget()
	{
		$widget = $this->errorBuildMessage();
		return $widget . "" . FieldUtilitary::motifInput($this->name, FieldUtilitary::TYPE_TEL, $this->value, $this->class, $this->label);
	}
}