<?php

namespace Library;

class FormHeaderFactory extends \Library\PObject
{
	protected $request = null;

	public function __construct(HTTPRequest $request)
	{
		$this->request = $request;
	}

	public function get($classeName)
	{
		$build = "\AppliLib\\FormHeaders\\" . $classeName . "FormHeader";
		$classeEntity = "\AppliLib\\Entities\\" . $classeName;

		$formHeader = new $build($this->request, new $classeEntity);
		return $formHeader->entity();
	}
}
