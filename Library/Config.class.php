<?php
namespace Library;

class Config extends ApplicationComponent
{
	protected $vars = array();
	protected $statDom = null;

	public function get($var)
	{
		if (!$this->vars) {
			$xml = new \DOMDocument;
			$xml->load(__DIR__ . '/../Applications/' . $this->app->name() . '/Config/app.xml');

			$elements = $xml->getElementsByTagName('define');
			foreach ($elements as $element) {
				$this->vars[$element->getAttribute('var')] = $element->getAttribute('value');
			}
		}

		if (isset($this->vars[$var])) {
			return $this->vars[$var];
		}

		return null;
	}

	public function set(string $var, string $value)
	{
		$location = __DIR__ . '/../Applications/' . $this->app->name() . '/Config/app.xml';

		$xml = new \DOMDocument;
		$xml->load($location);

		$elements = $xml->getElementsByTagName('define');
		foreach ($elements as $element) {
			if ($element->getAttribute('var') == "$var") {
				$element->setAttribute("value", $value);
				$xml->save($location);

				return true;
			}
		}

		throw new \RuntimeException("Champs non définit dans " . $this->app->name() . '/Config/app.xml');

	}

	public function setStat($data = array())
	{
		if (is_null($this->statDom)) {
			$location = __DIR__ . '/../Applications/' . $this->app->name() . '/Config/donneesQuestionnaire.xml';

			$this->statDom = new \DOMDocument;
			$this->statDom->load($location);
		}

		$xml = $this->statDom;

		$element = $xml->createElement("reponse");

		$element->setAttribute("numero", $data["numero"]);
		$element->setAttribute("idEnseignant", $data["idEnseignant"]);
		$element->setAttribute("idEleve", $data["idEleve"]);
		$element->setAttribute("note", $data["note"]);

		$xml->firstChild->appendChild($element);
		$xml->save($location);
	}
}