<?php

namespace Library;

class ClassFunctionDef extends ClassEntityDef
{
    const ATTRIB_TYPE = "parameterType";
    const ATTRIB_NAME = "parameterName";
    const ATTRIB_NATURE = "parameterNature";

    /**
     * @property string
     */
    protected $returnType;
    protected $paramtersDecl = [];

    public function addParameter(string $type, string $name)
    {
        $this->paramtersDecl[] = [self::ATTRIB_TYPE => $type, self::ATTRIB_NAME => $name];
    }

    public function setReturnType(string $returnTyper)
    {
        $this->returnType = $returnTyper;
    }

    public function getReturnType()
    {
        return $this->returnType;
    }

    public function setParamtersDecl(array $paramtersDecl)
    {
        $this->paramtersDecl = $paramtersDecl;
    }

    /**
     * @return array
     */
    public function getParametersDecl()
    {
        return $this->paramtersDecl;
    }
}
