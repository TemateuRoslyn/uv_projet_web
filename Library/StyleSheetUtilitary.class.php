<?php

namespace Library;

class StyleSheetUtilitary extends PObject
{
    private static $sheetVars = array();

    public static function addSheetVar(string $var, string $value)
    {
        // var_dump($value);
        self::$sheetVars[$var] = $value;
    }

    public static function sheetVar(string $var)
    {
        return isset(self::$sheetVars[$var]) ? self::$sheetVars[$var] : "";
    }
}
