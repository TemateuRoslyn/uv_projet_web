<?php

namespace Library;

class FileUp extends PObject
{
	protected $upTitre;
	protected $fichier_encrypt;

	protected $validators = array();
	protected $errorMessage;

	public function __construct($upTitre, $fichier_encrypt)
	{
		$this->upTitre = $upTitre;
		$this->fichier_encrypt = $fichier_encrypt;
	}

	public function upload()
	{
		if ($this->fileExist()) {
			return RessourcesManager::upload($this->fichier_encrypt, "imgLabel");
		}
	}

	public function isValid()
	{
		if ($this->fileExist()) {
			// Vérification du type
			$type = $this->fileData('name');
			if (!$this->validators[0]->isValid($type)) {
				$this->errorMessage = $this->validators[0]->errorMessage();
				return false;
			}

			$upload = $this->fileData('tmp_name');
			if (!$this->validators[1]->isValid($upload)) {
				$this->errorMessage = $this->validators[1]->errorMessage();
				return false;
			}

			return true;
		}
		return false;
	}

	public function fileData($key)
	{
		return isset($_FILES[$this->upTitre][$key]) ? $_FILES[$this->upTitre][$key] : null;
	}

	public function fileExist()
	{
		return isset($_FILES[$this->upTitre]);
	}

	public function setUpTitre($upTitre)
	{
		if (is_string($upTitre)) {
			$this->upTitre = $upTitre;
		}
	}

	public function errorMessage()
	{
		return $this->errorMessage;
	}
}
