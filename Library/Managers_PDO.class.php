<?php

namespace Library;

use mysqli;

abstract class Managers_PDO extends Managers_api
{
	use TraitManager;

	const SPACE_MANAGER = "manager5ceec1fdec8b1";
	/**
	 * @property ManagersPideria_PDO
	 */
	private $managersPideria = null;

	public function __construct($api, $dao)
	{
		parent::__construct($api, $dao);
		$this->managersPideria = new ManagersPideria_PDO($api, $dao);
	}

	public static function forceRestoreDataFile(string $filename)
	{
		$sql = file_get_contents($filename);

		$mysqli = new mysqli("localhost", "ecamsc415_school", "klaus651347237", "ecamsc415_schoolengine");

		/* execute multi query */
		$mysqli->multi_query($sql);
	}

	public static function forceRestoreDataFileTmpCount()
	{
		// your config
		$filename = 'yourGigaByteDump.sql';
		$maxRuntime = 8; // less then your max script execution limit


		$deadline = time() + $maxRuntime;
		$progressFilename = $filename . '_filepointer'; // tmp file for progress
		$errorFilename = $filename . '_error'; // tmp file for erro

		($fp = fopen($filename, 'r')) or die('failed to open file:' . $filename);

		// check for previous error
		if (file_exists($errorFilename)) {
			die('<pre> previous error: ' . file_get_contents($errorFilename));
		}

		// activate automatic reload in browser
		echo '<html><head> <meta http-equiv="refresh" content="' . ($maxRuntime + 2) . '"><pre>';

		// go to previous file position
		$filePosition = 0;
		if (file_exists($progressFilename)) {
			$filePosition = file_get_contents($progressFilename);
			fseek($fp, $filePosition);
		}

		$queryCount = 0;
		$query = '';
		while ($deadline > time() and ($line = fgets($fp, 1024000))) {
			if (substr($line, 0, 2) == '--' or trim($line) == '') {
				continue;
			}

			$query .= $line;
			if (substr(trim($query), -1) == ';') {
				if (!mysql_query($query)) {
					$error = 'Error performing query \'<strong>' . $query . '\': ' . mysql_error();
					file_put_contents($errorFilename, $error . "\n");
					exit;
				}
				$query = '';
				file_put_contents($progressFilename, ftell($fp)); // save the current file position for 
				$queryCount++;
			}
		}

		if (feof($fp)) {
			echo 'dump successfully restored!';
		} else {
			echo ftell($fp) . '/' . filesize($filename) . ' ' . (round(ftell($fp) / filesize($filename), 2) * 100) . '%' . "\n";
			echo $queryCount . ' queries processed! please reload or wait for automatic browser refresh!';
		}
	}

	public function elementExist($id)
	{
		$id = (int) $id;
		$sql = 'SELECT COUNT(*) FROM ' . $this->table . ' WHERE id = ' . $id;

		$requete = $this->dao->query($sql);
		return $requete->fetchColumn();
	}

	/**
	 * @param Entity[]
	 * @return void
	 */
	public function saveList(array $entities)
	{
		$message = self::sqlPrepareMSGAddList($entities, $this->table);
		$values = [];

		foreach ($entities as $entity) {
			$values[] = $entity->id();
			foreach ($entity->propertiesSave() as $value) {
				$values[] = $value;
			}
		}
		// $this->testVar($entities);
		$statm = $this->dao->prepare($message);
		$statm->execute($values);
		// $requete = $this->dao->query($message);
		// $this->testVar($message);
	}

	public function save(Entity $entity)
	{
		$id = $this->_save($entity);
		// $_SESSION[$this->unique_id_session_signature($id)] = $entity;

		return $id;
	}

	protected function _save(Entity $entity)
	{
		try {
			$element_id = parent::save($entity);
			$entity->setId($element_id);

			if ($entity->hasXmlModel()) {
				$this->managerXML()->getManagerOf($entity->className())->save($entity->xmlModel());
			}
			return $element_id;
		} catch (\PDOException $e) {
			var_dump($e->getMessage());
			$this->testVar($entity);
		}
	}

	protected function saveOnlyPDO(Entity $entity): int
	{
		// try {
		$element_id = parent::save($entity);
		$entity->setId($element_id);
		// } catch (\PDOException $e) {
		// var_dump($e->getMessage());
		// $this->testVar($entity);
		// }

		return $element_id;
	}

	public function switchToXML(): Managers_XML
	{
		return $this->managerXML()->getManagerOf(\ucfirst($this->table));
	}

	protected function add(Entity $entity)
	{
		$entityPDO = new Entity_PDO($entity);
		$messages = $entityPDO->entityListSqlSave();

		$idParent = $idFils = 0;
		$lastName = "";
		// $this->testVar($messages);
		foreach ($messages as $classeName => $Tabmessage) {
			$message = $Tabmessage[0];

			$req = self::bindValues($this->dao->prepare($message), $entityPDO->entityPropertiesMapGeneralization()[$classeName]);
			$req->execute();
			$lastId = ((int) $Tabmessage[1]) > 0 ? $Tabmessage[1] : $this->dao->lastInsertId();
			// $this->testVar($messages);
			// var_dump("tfx " . $this->dao->lastInsertId());
			if ($idFils == 0 && empty($lastName)) {
				$idParent = $lastId;
				$lastName = $classeName;
				$idFils = $idParent;
				// var_dump("Parent $idParent Fils $idFils tf");
			} else {
				$idFils = $lastId;
				// var_dump("Parent $idParent Fils $idFils");
				$this->managersPideria->addLink($idParent, $lastName, $idFils, $classeName);

				$idParent = $idFils;
				$lastName = $classeName;
			}

			$entity->setId($lastId);
		}
		// $this->testVar($messages);
		return $idParent;
	}

	public function getAssociateClassTo(\Library\Entity $entity, int $debut = -1, int $limit = -1): array
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		// $class = '\AppliLib\Entities\\' . ucfirst($this->managersPideria->thirdMemberAssociation($this->table, $entity->className()));
		$entityPDO = new Entity_PDO(new $class());

		return $this->bindFetchAllRequest($entityPDO->sqlAssociateListClass($entity, $debut, $limit));
	}

	public function getAssociateSchemeLinkTo(\Library\Entity $entity, array $links, int $debut = -1, int $limit = -1)
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		// $class = '\AppliLib\Entities\\' . ucfirst($this->managersPideria->thirdMemberAssociation($this->table, $entity->className()));
		$entityPDO = new Entity_PDO(new $class());
		return $this->bindFetchAllRequest($entityPDO->sqlSelectAssociateListMultipleLinks($entity, $links, $debut, $limit));
	}

	public function getAssociateToScheme(Entity $entity, string $entitySens = "", array $scheme = [], int $debut = -1, int $limit = -1): array
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		$entityPDO = new Entity_PDO(new $class());
		// echo ($entityPDO->sqlSelectAssociateListJoinScheme($entity, $entitySens, $scheme, $debut, $limit));
		$data = $this->bindFetchAllRequest($entityPDO->sqlSelectAssociateListJoinScheme($entity, $entitySens, $scheme, $debut, $limit));
		return $this->configDataStrict($data, $class);
	}

	public function getAssociateToSchemeRestrict(Entity $entity, string $mapGet, string $restrictJOIN, string $restrictWhere = "", string $entitySens = "", array $scheme = []): array
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		$entityPDO = new Entity_PDO(new $class());
		// echo ($entityPDO->sqlSelectAssociateListJoinScheme($entity, $entitySens, $scheme, $debut, $limit));
		if (empty($scheme)) {
			$restrictWhere = empty($restrictWhere) ? "" : " AND $restrictWhere";
			$sql = $entityPDO->sqlSelectAssociateList($entity, $mapGet, $restrictJOIN, $restrictWhere, -1, -1);
		} else {
			$sql = $entityPDO->sqlSelectAssociateListJoinScheme($entity, $entitySens, $scheme, -1, -1) . " $restrictWhere";
		}
		// echo ($sql);
		$data = $this->bindFetchAllRequest($sql);
		return $this->configDataStrict($data, $class);
	}

	public function getAssociateToSchemeUnsigned(string $className, string $entitySens = "", array $scheme = [], int $debut = -1, int $limit = -1): array
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		// $class = '\AppliLib\Entities\\' . ucfirst($this->table);
		$entityPDO = new Entity_PDO(new $class());
		// echo ($entityPDO->sqlSelectAssociateListJoinScheme($entity, $entitySens, $scheme, $debut, $limit));
		$data = $this->bindFetchAllRequest($entityPDO->sqlSelectAssociateListJoinSchemeUnsigned($className, $entitySens, $scheme, $debut, $limit));
		return $this->configDataStrict($data, $class);
	}

	/**
	 * @return Entity[]
	 */
	protected function configDataStrict(array $entities, string $class): array
	{
		$results = [];

		foreach ($entities as $lign) {
			$data = $lign->arrayDescriptions();
			unset($data["descriptions"]);
			unset($data["erreurs"]);
			/** @var Entity */
			$entity = new $class($data);
			$results[] = $entity;
		}

		return $results;
	}

	public function getAssociateTo(Entity $entity, string $mapGet = "", string $mapRestrictJoin = "", string $mapRestrictWhere = "", int $debut = -1, int $limit = -1): array
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		$entityPDO = new Entity_PDO(new $class());
		// echo ($entityPDO->sqlSelectAssociateList($entity, $debut, $limit));
		// echo "<br/>";
		// echo "<br/>";
		// echo "<br/>";
		// echo "<br/>";
		return $this->bindFetchAllRequest($entityPDO->sqlSelectAssociateList($entity, $mapGet, $mapRestrictJoin, $mapRestrictWhere, $debut, $limit));
	}

	// 	SELECT * FROM `_pideria_inner_owner` 
	// WHERE (tableUp = 'Cours' AND idTableUp = 3) OR (tableDown = 'Cours' AND idTableDown = 3)  

	/** @var Int [] tableau des identifiants de la table associée à l'entité $entity */
	public function getAssociateLink(Entity $entity, string $table)
	{
		$tableDown = ucfirst($entity->className());
		$tableDownID = $entity->id();

		$sql = "SELECT * FROM `_pideria_inner_owner` 
				WHERE (tableDown = '$table' AND tableUp = '$tableDown' AND idTableUp = $tableDownID) 
				OR (tableUp = '$table' AND tableDown = '$tableDown' AND idTableDown = $tableDownID)";
		// $this->testVar($sql);
		$requete = $this->dao->prepare($sql);
		$requete->execute();

		$result = [];
		while (($donnees = $requete->fetch()) !== false) {
			if ($donnees["tableDown"] == $tableDown) {
				$result[] = $donnees["idTableUp"];
			} else {
				$result[] = $donnees["idTableDown"];
			}
		}

		return $result;
	}

	/** @return EntityAssociationLink[]  */
	public function getEntitiesAssociationsLinks(Entity $entity, string $table): array
	{
		$tableDown = ucfirst($entity->className());
		$tableDownID = $entity->id();

		$sql = "SELECT * FROM `_pideria_inner_owner` 
				WHERE (tableDown = '$table' AND tableUp = '$tableDown' AND idTableUp = $tableDownID) 
				OR (tableUp = '$table' AND tableDown = '$tableDown' AND idTableDown = $tableDownID)";
		// $this->testVar($sql);
		$requete = $this->dao->prepare($sql);
		$requete->execute();

		$result = [];
		while (($donnees = $requete->fetch()) !== false) {
			if ($donnees["tableDown"] == $tableDown) {
				$result[] = new EntityAssociationLink(["idTable" => $donnees["idTableUp"], "table" => $table]);
			} else {
				$result[] = new EntityAssociationLink(["idTable" => $donnees["idTableDown"], "table" => $table]);
			}
		}

		return $result;
	}

	/** @return EntityAssociation[]  */
	public function getEntitiesAssociations(string $table): array
	{
		$entity_name = ucfirst($this->table);
		$sql = "SELECT * FROM `_pideria_inner_owner` 
				WHERE (tableDown = '$table' AND tableUp = '$entity_name') 
				OR (tableUp = '$table' AND tableDown = '$entity_name')";
		// $this->testVar($sql);
		$requete = $this->dao->prepare($sql);
		$requete->execute();

		$result = [];
		while (($donnees = $requete->fetch()) !== false) {
			$up = new EntityAssociationLink(["idTable" => $donnees["idTableUp"], "table" => $donnees["tableUp"]]);
			$down = new EntityAssociationLink(["idTable" => $donnees["idTableDown"], "table" => $donnees["tableDown"]]);

			$result[] = new EntityAssociation(["id" => $donnees["id"], "tables" => [$up, $down], "signature" => $donnees["signature"]]);
		}

		return $result;
	}

	public function addAssociations(array $entities, Entity $entity): void
	{
		$this->managersPideria->addAssociations($entities, $entity);
	}

	public function removeAssociations(array $entities, Entity $entity, bool $andEntity = false): void
	{
		$associations = $this->getEntitiesAssociationsLinks($entity, ucfirst($this->table));
		$this->combineAssociationsToEntities($entities, $associations);
	}

	public function unValidAssociations(Entity $entity, array $entitiesNames)
	{
		foreach ($entitiesNames as $name) {
			$associations = $this->getEntitiesAssociations($name);
			$this->combineAssociationsToEntities([$entity], $associations);
		}

		foreach ($entity->getDescriptions() as $descript) {
			if (is_a($descript, "Library\EntityAssociationLink")) {
				/** @var EntityAssociationLink $descript */
				// $this->testVar($descript->getDescriptions()[EntityAssociation::ASSOCIATION_SIGN]);
				$sign = $descript->getDescriptions()[EntityAssociation::ASSOCIATION_SIGN];
				$sql = "INSERT INTO _pideria_inner_owner (`idTableUp`, `idTableDown`, `signature`) 
						VALUES (0, 0, '$sign')
						ON DUPLICATE KEY UPDATE 
						idTableUp = -ABS(idTableUp), idTableDown = -ABS(idTableDown)";
				// $this->testVar("");
				// $sql = "UPDATE `_pideria_inner_owner` 
				// SET `idTableUp` = '-3', `idTableDown` = '-3', `signature` = '-Enseignant3Classe3' 
				// WHERE `_pideria_inner_owner`.`id` = 1";
				// $this->dao->exec("DELETE FROM _pideria_inner_owner WHERE $attrib = '$attrib_value'");
				$this->dao->exec($sql);
			}
		}
	}

	public function addAssociationClass(array $entitiesL, \Library\Entity $entity, array $entitiesR): void
	{
		$this->managersPideria->addAssociationClass($entitiesL, $entity, $entitiesR);
	}

	public function getList($order = 'id', $debut = -1, $limit = -1)
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		$obj = new $class();
		$entityPDO = new Entity_PDO($obj);
		// var_dump($entityPDO->sqlAttribsSelect());
		$sql = $entityPDO->sqlAttribsSelect();
		$sql .= ' ORDER BY ' . $order;

		if ($debut != -1 || $limit != -1) {
			$sql .= ' LIMIT ' . (int) $limit . ' OFFSET ' . (int) $debut;
		}

		return $this->bindFetchAllRequest($sql);
	}

	protected function _getList($order = 'id', $debut = -1, $limit = -1)
	{
		$sql = 'SELECT * FROM ' . $this->table . ' ORDER BY ' . $order;

		if ($debut != -1 || $limit != -1) {
			$sql .= ' LIMIT ' . (int) $limit . ' OFFSET ' . (int) $debut;
		}

		return $this->bindFetchAllRequest($sql);
	}

	private function unique_id_session_signature(int $id): string
	{
		return self::SPACE_MANAGER . "getUnique" . $this->table . "ID$id";
	}

	public function getUnique($id)
	{
		// $name = $this->unique_id_session_signature($id);
		// // $this->testVar($this->_getUniquePDO($id));
		// if (!isset($_SESSION[$name])) {
		// 	$_SESSION[$name] = $this->_getUniquePDO($id);
		// }

		return $this->_getUniquePDO($id);
	}

	protected function _getUniquePDO(int $id)
	{
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);
		$obj = new $class(["id" => $id]);
		$entityPDO = new Entity_PDO($obj);
		// var_dump($entityPDO->sqlAttribsSelect());
		$requete = $this->dao->query($entityPDO->sqlAttribsSelect());
		$requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\AppliLib\Entities\\' . ucfirst($this->classeTable));

		if ($element = $requete->fetch()) {
			$element->setId($element->id);
			$this->configEntity($element);

			$element->setDateInsert(new \DateTime($element->dateInsert));
			$element->setDateModif(new \DateTime($element->dateModif));

			$requete->closeCursor();
			return $element;
		}

		$requete->closeCursor();
		return null;
	}

	protected function configEntity(Entity $entity)
	{
		foreach ($entity as $property => $value) {
			try {
				$name = "set" . \ucfirst($property);
				$entity->$name($this->stringFormatWithQuote($value));
			} catch (\TypeError $th) {
			} catch (\Error $th) {
				$entity->$property = $this->stringFormatWithQuote($value);
			}
		}
	}

	public function count($id = 0)
	{
		return $this->dao->query('SELECT COUNT(*) FROM ' . $this->table)->fetchColumn();
	}

	protected function modify(Entity $entity)
	{
		$entityPDO = new Entity_PDO($entity);
		$messages = $entityPDO->entityListSqlModify();
		// var_dump("kjjsdkds");
		foreach ($messages as $classeName => $message) {
			// var_dump($message);
			$req = self::bindValues($this->dao->prepare($message), $entityPDO->entityPropertiesMapGeneralization()[$classeName]);
			$req->execute();

			$req->closeCursor();
		}
		// $this->testVar($messages);
		return $entity->id();
	}

	protected function modifyTMP(Entity $entity)
	{
		$message = self::sqlPrepareMSGModify($entity);
		// $this->testVar($message);
		$req = self::bindValues($this->dao->prepare($message), $entity->propertiesSave());
		// var_dump($message);
		$req->execute();

		return $entity->id();
	}

	public function delete($id)
	{
		$this->dao->exec('DELETE FROM ' . $this->table . ' WHERE id = ' . (int) $id);
		$this->managerXML()->getManagerOf(ucfirst($this->table))->delete($id);
	}

	public function deleteAssociation($attrib_value, string $attrib = self::ASSOCIAION_ID)
	{
		$this->dao->exec("DELETE FROM _pideria_inner_owner WHERE $attrib = '$attrib_value'");
	}

	public function deleteOn($property, $value)
	{
		$table = $this->table;
		$req = $this->dao->prepare("DELETE FROM $table WHERE $property =:$value");

		$req->bindValue(":$value", $value);
		$req->execute();
	}

	protected function queryFetch($sql)
	{
		$requete = $this->dao->query($sql);

		$requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\AppliLib\Entities\\' . ucfirst($this->classeTable));

		if ($element = $requete->fetch()) {
			$element->setId($element->id);

			$element->setDateInsert(new \DateTime($element->dateInsert));
			$element->setDateModif(new \DateTime($element->dateModif));

			$requete->closeCursor();
			return $element;
		}

		$requete->closeCursor();
		return null;
	}

	protected function bindFetchFilterAttribs(Entity $entity): Entity
	{
		$propertiesVal = $entity->properties();
		$class = '\AppliLib\Entities\\' . ucfirst($this->classeTable);

		return new $class($propertiesVal);
	}

	protected function bindFetch(\PDOStatement $requete)
	{
		$requete->execute();
		$requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\AppliLib\Entities\\' . ucfirst($this->classeTable));

		if ($element = $requete->fetch()) {
			$element->setId($element->id);
			$this->configEntity($element);

			@$element->setDateInsert(new \DateTime($element->dateInsert));
			@$element->setDateModif(new \DateTime($element->dateModif));

			$requete->closeCursor();
			return $this->bindFetchFilterAttribs($element);
		}

		$requete->closeCursor();
		return null;
	}

	public function bindFetchAll(\PDOStatement $requete)
	{
		$requete->execute();
		$requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\AppliLib\Entities\\' . ucfirst($this->classeTable));

		$liste = $requete->fetchAll();

		foreach ($liste as $element) {
			@$element->setId($element->id);
			$element = $this->bindFetchFilterAttribs($element);
			$this->configEntity($element);

			@$element->setDateInsert(new \DateTime($element->dateInsert));
			@$element->setDateModif(new \DateTime($element->dateModif));
		}

		$requete->closeCursor();
		return $liste;
	}


	public function bindFetchRequest(string $sql, array $data = array())
	{
		$requete = $this->dao->prepare($sql);
		foreach ($data as $key => $value) {
			$requete->bindValue(":$key", $value);
		}

		return $this->bindFetch($requete);
	}

	public function bindFetchAllRequest(string $sql, array $data = array()): array
	{
		$requete = $this->dao->prepare($sql);
		foreach ($data as $key => $value) {
			$requete->bindValue(":$key", $value);
		}
		// $this->testVar($data);
		return $this->bindFetchAll($requete);
	}

	protected function get_by_fixing_attribute(string $attribute, string $id, string $order = 'id')
	{
		$sql = 'SELECT * FROM ' . $this->table . ' WHERE ' . $attribute . '=:id ORDER BY ' . $order;
		return $this->bindFetchAllRequest($sql, array(
			"id" => $id
		));
	}

	public function getLastId()
	{
		return $this->dao->query('SELECT MAX(id) FROM ' . $this->table)->fetchColumn();
		// return $this->dao->lastInsertId();
	}
}
