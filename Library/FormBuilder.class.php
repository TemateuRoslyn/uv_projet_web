<?php

namespace Library;

abstract class FormBuilder extends PObject
{
	protected $form;
	protected $options = array();
	protected $managers = null;

	use FormLocationTrait;

	const DEFAULT_OPTION = "---------------------Veuillez faire un choix dans la liste---------------------";

	public function __construct(Managers $managers, Entity $entity, array $options = array())
	{
		$this->managers = $managers;
		$this->setForm(new Form($entity));

		$this->options = $options;
	}

	abstract public function build();

	public function setForm(Form $form)
	{
		$this->form = $form;
	}

	public function form(): Form
	{
		return $this->form;
	}

	protected function formatSelectManagerList(string $module, string $functionName, $paramUnique): array
	{
		$liste = $this->managers->getManagerOf($module)->$functionName($paramUnique);
		$result = array(self::DEFAULT_OPTION);
		// $this->testVar($liste);
		foreach ($liste as $entity) {
			$result[$entity->id()] = $this->stringFormatWithQuote($entity->formatName());
		}

		return $result;
	}
}
