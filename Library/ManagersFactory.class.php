<?php

namespace Library;

use Library\Dao\DAOFactory;

class ManagersFactory extends PObject
{
    protected static $pdoInstance = null;

    public static function getInstancePDO(): Managers
    {
        if (self::$pdoInstance == null) {
            self::$pdoInstance = new Managers('PDO', DAOFactory::getMysqlConnexion());
        }
        return self::$pdoInstance;
    }
}
