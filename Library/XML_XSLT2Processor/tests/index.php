<?php
require "../XSLT2Processor.php";

class XSL2Test extends XML_XSLT2Processor
{
    protected $i;


    public function testSimpleTransformation()
    {
        $this->i->importStylesheet('stylesheets/simple.xsl');
        $this->assertEquals(
            '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Untitled Document</title></head><body>This is a simple well formed XML document.</body></html>',
            $this->i->transformToXML('sources/well-formed.xml'),
            report_errors($this->i)
        );
    }

    public static function main()
    {
        $xslt = new XSL2Test();
        $xslt->testSimpleTransformation();
    }
}

XSL2Test::main();