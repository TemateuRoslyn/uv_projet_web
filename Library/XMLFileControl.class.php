<?php

namespace Library;

class XMLFileControl extends PObject
{
    /**
     * @var Managers
     */
    protected $managers = null;
    /**
     * @var Page
     */
    protected $page = null;
    const XMLFILENAMESPACE = "5ce81ee189ca3";

    public function __construct(Page $page, Managers $manager)
    {
        // libxml_use_internal_errors(true);

        $this->page = $page;
        $this->managers = $manager;

        $GLOBALS[self::XMLFILENAMESPACE] = $this;
    }

    public static function styleSheetController(): bool
    {
        $xmlFile = $GLOBALS[self::XMLFILENAMESPACE];
        $errors = libxml_get_errors();
        $container = [];

        foreach ($errors as $error) {
            $filePath = str_replace("\\", "/", $error->file);
            // var_dump($filePath);
            if (!empty($filePath) && !isset($container[$filePath])) {
                $container[$filePath] = true;
                // if(file_exists($error->file)) \unlink($error->file);/eleve//eleve/
                \unlink($error->file);
                $fileName = str_replace("\\", "/", \substr($error->file, strrpos($error->file, "/") + 1));
                $table = \ucfirst(str_replace("s.xml", "", $fileName));
                var_dump($table);

                $entities = $xmlFile->managers->getManagerOf($table)->getList();
                $managerXML = $xmlFile->managers->getManagerOf($table)->switchToXML();

                $managerXML->saveList($entities);
            }
        }

        return (empty($errors)) ? true : false;
    }
}
