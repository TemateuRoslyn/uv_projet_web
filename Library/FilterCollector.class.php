<?php
namespace Library;

use Library\Interfaces\Filter;


class FilterCollector extends ApplicationComponent
{
    protected $filters = array();

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->collect();
    }

    private final function collect()
    {
        $filterPath = __DIR__ . '/../Applications/' . $this->app->name() . '/Config/filters.xml';

        if (file_exists($filterPath)) {
            $xml = new \DOMDocument;
            $xml->load($filterPath);
            $filters = $xml->getElementsByTagName('filter');

            foreach ($filters as $filter) {
                $this->filters[] = new FilterConfig(
                    $filter->getAttribute('url'),
                    $filter->getAttribute('module'),
                    $filter->getAttribute('action')
                );
            }
        }
        // $this->testVar($this->filters);
    }

    public function filtersFor($url) : array
    {
        $result = [];

        foreach ($this->filters as $filter) {
            if (($varsValues = $filter->match($url)) !== false) {
                $filter->setUrl($url);
                $result[] = $this->filterForConfig($filter);
            }
        }

        return $result;
    }

    protected function filterForConfig(FilterConfig $config) : Filter
    {
        if ($config->module() != null) {
            $filterClass = 'Applications\\' . $this->app->name() . '\\Modules\\' . $config->module() . '\\' . $config->action() . 'Filter';
        } else {
            $filterClass = 'Applications\\' . $this->app->name() . '\\' . $config->action() . 'Filter';
        }

        $filter = new $filterClass();
        $filter->init($config);

        return $filter;
    }
}
