<?php
	namespace Library;

	abstract class LinkManager_PDO extends Managers_PDO
	{
		public function getLinksOn($attr, $id)
		{
			$id = (int) $id;
			$sql = 'SELECT * FROM '.$this->table.' WHERE '.$attr.' = '.$id;

			$requete = $this->dao->query($sql);
			$requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\InterfacesDAO\\'.ucfirst($this->table));

			$liste = $requete->fetchAll();
			foreach ($liste as $element) 
			{
				$element->setDateInsert(new \DateTime($element->dateInsert()));
				$element->setDateModif(new \DateTime($element->dateModif()));
				
			}
			$requete->closeCursor();
			return $liste;
		}

		abstract public function getLinks($id);
		abstract public function getLinksReverse($id);
	}