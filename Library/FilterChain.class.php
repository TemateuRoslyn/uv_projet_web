<?php
namespace Library;

class FilterChain extends ApplicationComponent
{
    protected $filters = array();
    protected $chainBlock = false;

    public function __construct(\Library\Application $app, $url)
    {
        parent::__construct($app);
        $filterCollector = new FilterCollector($app);

        $this->filters = $filterCollector->filtersFor($url);
    }

    public function doFilter(HTTPRequest $request, HTTPResponse $response)
    {
        $this->chainBlock = false;
        if (!empty($this->filters)) {
            $this->chainBlock = true;
            $filter = array_shift($this->filters);

            $filter->doFilter($request, $response, $this);
            $filter->destroy();
        }

        if ($this->chainBlock) {
            $response->redirect404();
        }
    }
}
