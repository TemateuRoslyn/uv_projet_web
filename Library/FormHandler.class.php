<?php

namespace Library;

class FormHandler extends PObject
{
	/** @property Form */
	protected $form;
	/** @property Managers_api */
	protected $manager;
	/** @property HTTPRequest */
	protected $request;

	public function __construct(\Library\Form $form, Managers_api $manager, \Library\HTTPRequest $request)
	{
		$this->setForm($form);
		$this->setManager($manager);
		$this->setRequest($request);
	}

	public function process(): bool
	{
		if ($this->request->method() == 'POST' && $this->form->isValid() && $this->upLoad($this->form->entity())) {
			if (property_exists($this->form->entity(), 'motDePasse'))
				$this->form->entity()->setMotDePasse(md5($this->form->entity()->motDePasse()));

			$this->form->entity()->setId($this->manager->save($this->form->entity()));
			return true;
		}

		if ($this->request->method() == 'POST' && !$this->form->isValid()) {
			$this->request->app()->user()->setFlash("Formulaire Non valide");
		}

		return false;
	}

	public function upLoad(Entity $entity)
	{
		if ($this->request->fileExists("upTitre")) {
			return RessourcesManager::upload($entity->fichier(), "upTitre");
		}
		// $this->testVar($entity);
		return true;
	}

	public function setForm(\Library\Form $form)
	{
		$this->form = $form;
	}

	public function setManager(\Library\Managers $manager)
	{
		$this->manager = $manager;
	}

	public function setRequest(\Library\HTTPRequest $request)
	{
		$this->request = $request;
	}

	public function form(): Form
	{
		return $this->form;
	}
}
