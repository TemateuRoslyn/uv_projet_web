<?php

namespace Library;

abstract class Conteneur extends Stats\StatsUtilitary
{
	/**
	 * @var Managers
	 */
	protected $managers = null;

	public function __construct(Managers $managers)
	{
		$this->managers = $managers;
	}

	protected function set(string $attrib, $element)
	{
		$GLOBALS[$attrib] = $element;
		// $_SESSION[$attrib] = $element;
	}

	protected function get($attrib)
	{
		if (isset($GLOBALS[$attrib])) {
			return $GLOBALS[$attrib];
		}
		return null;
	}

	protected function setExist($attrib): bool
	{
		return isset($GLOBALS[$attrib]);
	}

	public function initSet($attrib, $element): bool
	{
		if (!$this->setExist($attrib)) {
			$this->set($attrib, $element);
			return true;
		}
		return false;
	}

	public function managers(): Managers
	{
		return $this->managers;
	}
}
