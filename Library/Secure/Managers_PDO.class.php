<?php

namespace Library\Secure;

use Library\Managers as LibraryManagers;
use Library\PObject;
use PDO;

class Managers_PDO extends PObject
{
    /** @property LibraryManagers */
    protected $managers = null;

    public function __construct(LibraryManagers $managers)
    {
        $this->managers = $managers;
    }

    public function saveLink(string $link): void
    {
        /** @var PDO */
        $dao = $this->managers->dao();
    }

    public function hasLink(string $link): bool
    {
        /** @var PDO */
        $dao = $this->managers->dao();
    }

    public function namespaceLinks(string $namespace): array
    {
    }

    public function getEntitiesLinks(array $entities): array
    {
        $namespace = implode("_", $entities);
        return $this->namespaceLinks($namespace);
    }
}
