<?php

namespace Library\Secure;

use Library\Application;
use Library\ApplicationComponent;
use Library\Managers;

class Controller extends ApplicationComponent
{
    /** @property Managers_PDO */
    protected $managers = null;

    public function __construct(Application $app, Managers $managers)
    {
        parent::__construct($app);
        $this->managers = new Managers_PDO($managers);
    }
}
