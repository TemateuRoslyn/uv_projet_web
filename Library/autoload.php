<?php

function smsAutoload($class)
{
	static $classes = null;
	static $path = null;
	if ($path === null) {
		$path = dirname(__FILE__) . '/Plugins/SMSApicom/SMSApi';
	}

	$cname = strtolower(str_replace("\\", "", $class));

	if (!isset($classes[$cname])) {

		if (preg_match('/^smsapi/', $cname)) {

			$classmap = include $path . '/autoload_classmap.php';

			if (isset($classmap[$cname])) {
				$class = explode("\\", $classmap[$cname]);
			} else {
				$class = explode("\\", $class);
			}

			unset($class[0]);
			$class = implode(DIRECTORY_SEPARATOR, $class);

			$classes[$cname] = $path . DIRECTORY_SEPARATOR . $class . '.php';
		}
	}

	if (isset($classes[$cname])) {
		require $classes[$cname];
	}
}

function autoload($class)
{
	// var_dump($class);
	$pathXsl = dirname(dirname(__FILE__)) . '/' . str_replace(array("Genkgo\Xsl", '\\'), array("Library/XML_XSLT2Processor/xslMaster/src", '/'), $class) . '.php';
	$path = dirname(dirname(__FILE__)) . '/' . str_replace('\\', '/', $class) . '.class.php';

	if (is_file($path)) {
		require $path;
	} else if (is_file($pathXsl)) {
		require $pathXsl;
	} else {
		smsAutoload($class);
	}
}

spl_autoload_register('autoload');
