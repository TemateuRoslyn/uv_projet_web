<?php

namespace Library;

use Genkgo\Xsl\XsltProcessor;

class StyleSheet_Manager extends \xsltProcessor
{
	protected $style = null; //xsl file
	protected $source = null; //xml model file

	protected $pagesNames = array();
	protected $varsSource = array();

	function __construct(array $pages)
	{
		$this->setPagesNames($pages);
		// parent::__construct();
		$this->style = new \domDocument();
		$this->source = new \domDocument();
	}

	static function script_for_internet_explorer()
	{
		return '
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --><!-- WARNING: Respond.js doesn \'t work if you view the page via file:// -->
		  <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		  <![endif]-->';
	}

	public function text($value)
	{
		var_dump($value);
	}

	public function generateHTML()
	{
		// $styleSheet = new DOMDocument();
		// $styleSheet->load('../test/Stubs/Xsl/ForEachGroup/group-by-element.xsl');

		// $processor = new XsltProcessor();
		// $processor->importStyleSheet($styleSheet);

		// $data = new DOMDocument();
		// $data->load('../test/Stubs/packages.xml');
		// $transpilerResult = $processor->transformToXml($data);
		// libxml_clear_errors();
		$this->registerPHPFunctions();
		$this->importStylesheet($this->style);

		$source = $this->transformToXml($this->source);
		// libxml_clear_errors();

		return $source;
	}

	public function source()
	{
		return $this->source;
	}

	public function addSourceVar(\DOMNode $node)
	{
		$this->varsSource[] = $node;
	}

	protected function getRelativeElementToAddVarStyleContent($firstElement)
	{
		// var_dump($firstElement);
		if (!is_null($firstElement)) {
			$nextElement = $firstElement->nextSibling;
			if (!is_null($nextElement) && ($firstElement->nodeName != "xsl:variable" || $firstElement->nodeName != "xsl:template")) {
				return $this->getRelativeElementToAddVarStyleContent($nextElement);
			}
		}
		// var_dump($firstElement);
		return $firstElement;
	}

	// public function setStyle($name)
	// {
	// 	$varDescript = "";
	// 	// var_dump($this->pagesNames);
	// 	foreach ($this->pagesNames as $page) {
	// 		$url = $this->formatPageDataPathRelative($page);
	// 		$varName = $page . "sAll";

	// 		$varDescript .= '<xsl:variable name="' . $varName . '" select="document(\'' . $url . '\')/root" />';
	// 	}

	// 	$content = str_replace('<!--DATA-FILES-->', $varDescript, file_get_contents($name));
	// 	file_put_contents("tmp_stylesheet__.xsl", $content);
	// 	var_dump($content);
	// 	$this->style->loadXML($content);
	// 	// var_dump($this->style->firstChild->firstChild->nextSibling);
	// }

	public function setStyle($name)
	{
		// $this->style = new \domDocument();
		$this->style->load($name);

		if (!empty($this->pagesNames)) {
			$firstElement = $this->getRelativeElementToAddVarStyleContent($this->style->firstChild->firstChild);
		}
		$xsltRoot = $this->style->firstChild;
		// var_dump($firstElement->nextSibling->nextSibling);
		// var_dump($firstElement->getAttribute("name"));
		foreach ($this->pagesNames as $name) {
			$url = $this->formatPageDataPathRelative($name);
			$varElement = $this->style->createElementNS("http://www.w3.org/1999/XSL/Transform", "xsl:variable");

			$varElement->setAttribute("name", $name . "sAll");
			$varElement->setAttribute("select", "document('" . $url . "')/root");

			if (!is_null($firstElement)) {
				$xsltRoot->insertBefore($varElement, $firstElement);
			} else {
				$this->style->appendChild($varElement);
			}
		}
		// $this->style->save("tmp_stylesheet__.xsl");
	}

	public function setSource($name)
	{
		// $this->source = new \DOMDocument();
		$this->source->load($name);

		$root = $this->source->firstChild;

		foreach ($this->varsSource as $node) {
			$root->appendChild($this->source->importNode($node, true));
		}

		$this->source->xinclude();
		// $this->source->save("C:/wamp64/www/e-school/test_save.xml");
	}

	public function setPagesNames(array $pages)
	{
		// var_dump($pages);
		foreach ($pages as $page) {
			if (!empty($page)) {
				$this->pagesNames[] = $page;
			}
		}
	}

	public function addSourceParam(string $var, string $value)
	{
		$dom = new \DOMDocument;
		$element = $dom->createElement($var, $value);

		$this->addSourceVar($element);
	}

	public function initSourceVar()
	{
		$this->varsSource = array();
	}

	protected function formatPageDataPathRelative(string $name): string
	{
		return str_replace("\\", "/", dirname(dirname(__FILE__)) . '/Web/buildXml/' . $name . 's.xml');
	}
}
