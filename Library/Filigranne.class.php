<?php
namespace Library;

class Filigranne extends ApplicationComponent
{
	public function __construct(Application $app)
	{
		parent::__construct($app);
		if (!isset($_SESSION["filigranne"])) $this->reset();
	}

	public function actualise(Route $path)
	{
		if ($path->level() == "root") {
			$this->reset();
			$this->addPath($path);
		} else {
			if (($pos = $this->routeExist($path)) !== false) $this->reduceTo($pos + 1);
			else $this->addPath($path);
		}
	}

	public function reduceTo($position)
	{
		$newPath = array();

		for ($i = 0; $i < $position; $i++) {
			$newPath[] = $_SESSION["filigranne"][$i];
		}

		$_SESSION["filigranne"] = $newPath;
	}

	public function routeExist(Route $route)
	{
		$filigranne = $_SESSION["filigranne"];

		$pos = 0;
		foreach ($filigranne as $fil_path) {
			if ($fil_path->match($route)) {
				return $pos;
			}
			$pos++;
		}

		return false;
	}

	public function addPath(Route $route)
	{
		$path = new PathUse(array("url" => $route->pattern(), "location" => $this->app->httpRequest()->requestURI()));

		$_SESSION["filigranne"][] = $path;
	}

	public function reset()
	{
		$_SESSION["filigranne"] = array();
	}

	public function pathCount()
	{
		return count($_SESSION["filigranne"]);
	}

	public function current()
	{
			// var_dump($this->pathCount());
		if ($this->pathCount() > 0)
			return $this->pile()[$this->pathCount() - 1];

		return $this->pile()[0];
	}

	public function previous()
	{
		return $this->pile()[$this->pathCount() - 2];
	}

	public function reduceIntervalLeaveCurrentPath($length_after_start)
	{
		$endpath = $this->current();

		$tmp = array_slice($this->pile(), 0, $length_after_start);
		$tmp[] = $endpath;

		$_SESSION["filigranne"] = $tmp;

	}

	public function depile()
	{
		return array_pop($_SESSION["filigranne"]);
	}

	public function pile()
	{
			// $result = array();
			// foreach ($_SESSION["filigranne"] as $path) 
			// {
			// 	$result[] = $path->arrayXML();	
			// }

		return (isset($_SESSION["filigranne"])) ? $_SESSION["filigranne"] : array();
	}
}