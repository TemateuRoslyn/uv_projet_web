<?php
	namespace Library;

	class ClassMakerFiles
	{
		public function  objectProperties($obj)
		{
			if (is_object($obj)) 
			{
				$classe = new \ReflectionObject($obj);
		
				foreach ($classe->getProperties() as $attribut) 
				{
					$attribut->setAccessible(true);
					
					$cle    = $attribut->getName();
					$valeur = $attribut->getValue($obj);
					$liste[$cle] = $valeur;

					$attribut->setAccessible(false);		
				}
				return $liste;	
			} 
			else 
			{
				throw new \InvalidArgumentException("Le paramètre doit être un Objet");
				
			}
		}

		// ================================================================= 
		public function filePHP(array $properties, $name = 'ClassName', $namespace ='')
		{
			$location = empty($namespace) ? $name : "$namespace/$name";
			$monfichier = fopen("$location.class.php", "w");

			$this->writeHeader($monfichier, $name, $namespace);
			$this->writeProperties($monfichier, $properties);

			$this->writeGetters($monfichier, $properties);
			$this->writeSetters($monfichier, $properties);

			$this->writeFoot($monfichier);
			fclose($monfichier);
		}

		protected function writeGetters($ressource, array $properties)
		{
			fwrite($ressource, "\n// GETTERS //");

			foreach ($properties as $property) 
			{
				fwrite($ressource, "
					\n\tpublic function ".$property."()\r\t{\r\t\treturn \$this->".$property.";\r\t}"
				);
			}
			
			fwrite($ressource, "\n");
		}

		protected function writeSetters($ressource, array $properties)
		{
			fwrite($ressource, "\n// SETTERS //");
			foreach ($properties as $property) 
			{
				fwrite($ressource, "\n\tpublic function set".ucfirst($property)."($".$property.")\r\t{\r\t\t\$this->set('".$property."', $".$property.");\r\t}");
			}	
		}

		protected function writeProperties($ressource, array $properties)
		{
			foreach ($properties as $property) 
			{	
				fwrite($ressource, "protected $".$property."; \r\t");
			}
		}

		protected function writeHeader($ressource, $class, $namespace)
		{
			fwrite($ressource, "<?php \r namespace $namespace;\r\r class ".ucfirst($class)." \r { \r\t");
		}

		protected function writeFoot($ressource)
		{
			fputs($ressource, "\r }");
		}
	}