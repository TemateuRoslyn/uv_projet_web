<?php
namespace Library\StatRef;

use Library\PObject;

abstract class StatRefManager_XML extends PObject
{
    const XML_ID = "xml:id";

    protected $vars = array();
    protected $domRoot = null;
    protected $file = "";

    public function __construct(string $fileName, string $entityName, string $attribKey = "xml:id")
    {
        $this->file = __DIR__ . "/$fileName";

        $this->domRoot = new \DOMDocument;
        $this->domRoot->load($this->file);

        $elements = $this->domRoot->getElementsByTagName($entityName);
        foreach ($elements as $element) {
            $this->vars[$element->getAttribute($attribKey)] = $element;
        }
    }
}
