<?php
namespace Library\StatRef;

class StatRefManagerConnecte_XML extends StatRefManager_XML
{
    const ATTRIB_IP = "ip";
    const ATTRIB_TIMESTAMP = "timestamp";
    const ATTRIB_PAGE = "page";

    protected $time_old = 0;

    public function __construct()
    {
        parent::__construct("listeConnectes.xml", "connecte", self::ATTRIB_IP);
        $this->time_old = time() - (60 * 5);
    }

    public function addConnecte()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $page = substr($_SERVER['PHP_SELF'], 1);
        $time = time();

        $xml_document = $this->domRoot->createElement("connecte");

        $attr_id = $this->domRoot->createAttribute('xml:id');
        $attr_id->value = \uniqid("_");

        $xml_document->appendChild($attr_id);
        $xml_document->setIdAttribute('xml:id', true);

        $xml_document->setAttribute(self::ATTRIB_IP, $ip);
        $xml_document->setAttribute(self::ATTRIB_PAGE, $page);
        $xml_document->setAttribute(self::ATTRIB_TIMESTAMP, $time);

        $this->domRoot->firstChild->appendChild($xml_document);
        $this->domRoot->save($this->file);

        $this->vars[$ip] = $xml_document;
    }

    public function countConnectes() : int
    {
        $this->refreshList();
        $ip = $_SERVER['REMOTE_ADDR'];

        if (isset($this->vars[$ip])) {
            $this->refreshConnecte();
        } else {
            $this->addConnecte();
        }

        return count($this->vars);
    }

    public function refreshConnecte()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if (isset($this->vars[$ip])) {
            $connecte = $this->vars[$ip];
            $connecteOld = $this->domRoot->getElementById($connecte->getAttribute("xml:id"));

            $connecteOld->setAttribute(self::ATTRIB_PAGE, substr($_SERVER['PHP_SELF'], 1));
            $connecteOld->setAttribute(self::ATTRIB_TIMESTAMP, \time());

            $this->domRoot->save($this->file);
        } else {
            $this->addConnecte();
        }
    }

    public function refreshList()
    {
        $newVars = [];
        foreach ($this->vars as $ip => $connecte) {
            if ($connecte->getAttribute(self::ATTRIB_TIMESTAMP) < $this->time_old) {
                $root = $this->domRoot->getElementsByTagName("connectes")[0];

                $old = $this->domRoot->getElementById($connecte->getAttribute(self::XML_ID));
                // var_dump($old->getAttribute(self::XML_ID));
                $root->removeChild($connecte);
                $this->domRoot->save($this->file);
            } else {
                $newVars[$ip] = $connecte;
            }
        }

        $this->vars = $newVars;
    }
}
