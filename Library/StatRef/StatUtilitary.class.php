<?php

namespace Library\StatRef;

class StatUtilitary
{
    public static function incrementeNombreVisite(): int
    {
        $nb_visites = file_get_contents(__DIR__ . '/nombreVisites.txt');
        $nb_visites++;

        file_put_contents(__DIR__ . '/nombreVisites.txt', $nb_visites);
        // self::actualiseGeneralCompt();

        return (int) $nb_visites;
    }

    public static function actualiseGeneralCompt(): int
    {
        $manager = new StatRefManagerVisite_XML();
        return $manager->nombreVisiteInCurrentDate();
    }

    public static function actualiseListConnectes(): int
    {
        $manager = new StatRefManagerConnecte_XML();
        return $manager->countConnectes();
        // return 0;
    }
}
