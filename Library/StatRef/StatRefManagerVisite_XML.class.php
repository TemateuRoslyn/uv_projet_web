<?php

namespace Library\StatRef;

use Library\PObject;

class StatRefManagerVisite_XML extends StatRefManager_XML
{

    public function __construct()
    {
        parent::__construct("nombreVisitesParJours.xml", "visite", "date");
    }

    public function visitesInDate(\DateTime $date)
    {
        if (isset($this->vars[$date->format('d-m-Y')])) {
            return $this->vars[$date->format('d-m-Y')];
        }

        return null;
    }

    protected function getForFixedAttribHasValue(string $attr, string $var): array
    {
        $result = [];
        foreach ($this->vars as $visite) {
            if ($visite->getAttribute($attr) == $var) {
                $result[] = $visite;
            }
        }

        return $result;
    }

    protected function initCompteurVisiteForCurrentDate()
    {
        $xml_document = $this->domRoot->createElement("visite");

        $attr_id = $this->domRoot->createAttribute('xml:id');
        $attr_id->value = \uniqid("_");

        $xml_document->appendChild($attr_id);
        $xml_document->setIdAttribute('xml:id', true);

        $date = new \DateTime;
        $xml_document->setAttribute("date", $date->format('d-m-Y'));
        $xml_document->setAttribute("nombre", "1");

        $this->domRoot->firstChild->appendChild($xml_document);
        $this->domRoot->save($this->file);
    }

    protected function incrementeNombreVisiteInCurrentDate()
    {
        $date = (new \DateTime())->format('d-m-Y');
        $element = $this->vars[$date];
        $nombre = (int) $element->getAttribute("nombre");

        $elementOld = $this->domRoot->getElementById($element->getAttribute("xml:id"));
        $elementOld->setAttribute("nombre", ++$nombre);

        $this->domRoot->save($this->file);
    }

    public function nombreVisiteInCurrentDate(): int
    {
        $visite = $this->visitesInDate(new \DateTime());
        if ($visite == null) {
            $this->initCompteurVisiteForCurrentDate();
            return 1;
        }

        $this->incrementeNombreVisiteInCurrentDate();
        return (int) $visite->getAttribute("nombre");
    }
}
