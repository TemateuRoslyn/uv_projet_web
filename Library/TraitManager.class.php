<?php

namespace Library;

trait TraitManager
{
	protected $managersXML = null;

	public function managerXML()
	{
		if (is_null($this->managersXML)) {
			$this->managersXML = new Managers('XML', new \DOMDocument('1.0', 'utf-8'));
		}

		return $this->managersXML;
	}

	/**
	 * @param Entity[]
	 */
	public static function sqlPrepareMSGAddList(array $entities, string $table): string
	{
		$keysSaves = Entity_PDO::sqlKeysSave(ucfirst($table));
		$message = "INSERT INTO `$table` $keysSaves VALUES ";

		$length = count($entities);
		$nbre = 0;
		// echo $message . '<br/>';
		foreach ($entities as $entity) {
			// echo Entity_PDO::sqlDataSaveParamMultiple($entity) . '<br/>';
			/** @var Entity $entity */
			$message .= Entity_PDO::sqlDataSaveParamMultiple($entity);
			if ($length != ++$nbre) $message .= ",";
		}

		// $message .= 'dateModif = NOW()';
		// $message .= " WHERE id = " . $entity->id();
		// echo ($message . " " . Entity_PDO::sqlDuplicate($table));
		return $message . " " . Entity_PDO::sqlDuplicate($table);
	}

	/**
	 * Méthode SQL de modification de plusieurs entités 
	 * @return string
	 */
	public static function sqlPrepareMSGModifyList(array $entities): string
	{
		$message = "";
		foreach ($entities as $entity) {
			$message .= self::sqlPrepareMSGModify($entity) . ";";
		}
		return $message;
	}

	public static function sqlPrepareMSGAdd(Entity $entity)
	{
		$donnees = $entity->propertiesSave();
		$table = strtolower($entity->className());

		$message = 'INSERT INTO ' . $table . ' SET ';
		$keys = array_keys($donnees);
		foreach ($keys as $key) {
			if (!is_null($donnees[$key])) {
				$message .= "$key =:$key, ";
			}
		}

		$message .= 'dateModif = NOW(), ';
		$message .= 'dateInsert = NOW()';
		// var_dump($message);
		return $message;
	}

	/**
	 * Méthode SQL de modification d'une entité 
	 * @return string
	 */
	public static function sqlPrepareMSGModify(Entity $entity): string
	{
		$donnees = $entity->propertiesSave();
		$table = strtolower($entity->className());

		$message = 'UPDATE ' . $table . ' SET ';
		foreach (array_keys($donnees) as $key) {
			if (!is_null($donnees[$key])) {
				$message .= $key . ' = :' . $key . ', ';
			}
		}

		$message .= 'dateModif = NOW()';
		$message .= " WHERE id = " . $entity->id();

		return $message;
	}

	public static function bindValues(\PDOStatement $request, array $donnees)
	{
		foreach ($donnees as $key => $value) {
			if (!is_null($value)) {
				if ($value instanceof \DateTime) {
					$request->bindValue(':' . $key, $value->format("Y-m-d H:i:s"));
				} else {
					$request->bindValue(':' . $key, $value);
				}
			}
		}

		return $request;
	}
}
