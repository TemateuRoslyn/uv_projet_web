<?php

namespace Library;

use Library\Session\Controller;
use Library\StatRef\StatUtilitary;
use Library\UmlStates\Parsor\PackageManager_UML;

abstract class Application extends PObject
{
	const CONFIG_SCHEME_ATTRIB_ACTUALIZE = "actualyzeScheme";
	const CONFIG_SCHEME_ATTRIB_ACTUALIZE_YES = "yes";
	/**
	 * @property PackageManager_UML
	 */
	protected $packageAppManager = null;
	/**
	 * @property HTTPRequest
	 */
	protected $httpRequest;
	/**
	 * @property HTTPResponse
	 */
	protected $httpResponse;
	/** @var FilterChain */
	protected $filterChain;
	/** @property Route */
	protected $currentRoute = null;
	/** @property BackController */
	protected $currentController = null;
	protected $name;

	protected $currentDate;
	protected $config;
	protected $user;
	/** @property Controller */
	protected $sessionController;
	protected $filigranne = null;

	protected $forwardURI = false;
	private static $appFileConfig = null;

	protected $nbreVisiteTotal = 0;
	protected $nbreVisiteForDay = 0;
	protected $nbreConnectes = 0;

	public function __construct()
	{
		// var_dump($_SESSION);
		$this->httpRequest = new HTTPRequest($this);
		$this->httpResponse = new HTTPResponse($this);
		$this->config = new Config($this);
		$this->sessionController = new Controller($this, ManagersFactory::getInstancePDO());
		$this->user = new User($this);
		$this->filigranne = new Filigranne($this);
		// var_dump($this->user()->adminAuthentificated());
		$this->name = '';
		$this->currentDate = getdate()["year"];

		if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
			try {
				$this->nbreVisiteTotal = @StatUtilitary::incrementeNombreVisite();
				$this->nbreVisiteForDay = @StatUtilitary::actualiseGeneralCompt();
				$this->nbreConnectes = @StatUtilitary::actualiseListConnectes();
			} catch (\Error $e) {
				$this->nbreVisiteTotal = 0;
				$this->nbreVisiteForDay = 0;
				$this->nbreConnectes = 0;
			}
		}
		$this->init_states();
		// $this->httpRequest->setAttribute("SID", SID);
	}

	private function init_states()
	{
		// $this->packageAppManager = new PackageManager_UML();
		// $classes = $this->packageAppManager->getClassDefs();
		if ($this->appConfigCanWriteScheme()) {
			$this->writterManager = new WrittersManager();
		}
		// $this->testVar("");
	}

	public function forwardApp(string $appName, string $uri)
	{
		$name = "\Applications\\$appName\\$appName" . "Application";
		$app = new $name();

		$route = $app->getMatchRoute($uri);
		$controller = $app->getControllerForURI($uri);

		$method = 'execute' . ucfirst($route->action());
		if (!is_callable(array($controller, $method))) {
			throw new \RuntimeException("L'action " . $this->action . " n'est pas définie sur le module : " . $this->module . " : Forward App Mode");
		}

		$this->forwardURI = true;
		$controller->$method($app->httpRequest);
		if ($this->currentRoute->hasViewAction()) {
			$controller->page()->setLocationModel($this->currentController->page()->view(), $this->currentController->page()->model());
		}
		if ($this->currentRoute->hasViewActionXSL()) {
			$controller->page()->setLocationModel($this->currentController->page()->view(), $controller->page()->model());
		}
		if ($this->currentRoute->hasViewActionXML()) {
			$controller->page()->setLocationModel($controller->page()->view(), $this->currentController->page()->model());
		}

		$this->httpResponse()->setPage($controller->page());
	}

	public function forWardURI(string $uri, bool $filter = true)
	{
		$route = $this->getMatchRoute($uri);
		$controller = $this->getControllerForURI($uri, $filter);

		$method = 'execute' . ucfirst($route->action());
		if (!is_callable(array($controller, $method))) {
			throw new \RuntimeException("L'action " . $this->action . " n'est pas définie sur le module : " . $this->module);
		}

		$this->forwardURI = true;
		$controller->$method($this->httpRequest);
		if ($this->currentRoute->hasViewAction()) {
			$controller->page()->setLocationModel($this->currentController->page()->view(), $this->currentController->page()->model());
		}
		if ($this->currentRoute->hasViewActionXSL()) {
			$controller->page()->setLocationModel($this->currentController->page()->view(), $controller->page()->model());
		}
		if ($this->currentRoute->hasViewActionXML()) {
			$controller->page()->setLocationModel($controller->page()->view(), $this->currentController->page()->model());
		}
		$this->httpResponse()->setPage($controller->page());
	}

	private function getInitRouter($contextURI = ""): Router
	{
		$this->filterChain = new FilterChain($this, empty($contextURI) ? $this->httpRequest->requestURI() : $contextURI);
		$router = new \Library\Router;

		$xml = new \DOMDocument;
		$xml->load(__DIR__ . '/../Applications/' . $this->name . '/Config/routes.xml');
		$routes = $xml->getElementsByTagName('route');

		foreach ($routes as $route) {
			$vars = array();
			if ($route->hasAttribute('vars')) {
				$vars = explode(',', $route->getAttribute('vars'));
			}

			$level = ($route->hasAttribute("level")) ? $route->getAttribute("level") : '';
			$title = ($route->hasAttribute("title")) ? $route->getAttribute("title") : $this->name;
			$pages = ($route->hasAttribute("xml-pages-data")) ? $route->getAttribute("xml-pages-data") : '';
			// $this->testVar($route->getAttribute('module'));
			$router->addRoute(new Route(
				$route->getAttribute('url'),
				$route->getAttribute('module'),
				$route->getAttribute('action'),
				$route->getAttribute('view'),
				$level,
				$title,
				$vars,
				$pages,
				$route->getAttribute('view-xml'),
				$route->getAttribute('view-xsl')
			));
		}

		return $router;
	}

	public function getController(): BackController
	{
		$router = $this->getInitRouter();
		$this->currentController = $this->getMatchRouteController($router, $this->httpRequest->requestURI());
		$this->filterChain->doFilter($this->httpRequest, $this->httpResponse);

		return $this->currentController;
	}

	public function getControllerForURI(string $url, bool $filter = true): BackController
	{
		$router = $this->getInitRouter($url);
		if ($filter) $this->filterChain->doFilter($this->httpRequest, $this->httpResponse);

		return $this->getMatchRouteController($router, $url);
	}

	private function getMatchRouteController(Router $router, $url): BackController
	{
		try {
			$matchedRoute = $router->getRoute($url);
			$this->currentRoute = $matchedRoute;
		} catch (\RuntimeException $e) {
			if ($e->getCode() == \Library\Router::NO_ROUTE) {
				$this->httpResponse->redirect404();
			}
		}

		$_GET = array_merge($_GET, $matchedRoute->vars());
		$controlerClass = 'Applications\\' . $this->name . '\\Modules\\' . $matchedRoute->module() . '\\' . $matchedRoute->module() . 'Controller';
		// $this->testVar($matchedRoute->viewActionXSL());
		$this->filigranne->actualise($matchedRoute);
		return new $controlerClass($this, $matchedRoute->module(), $matchedRoute->action(), $matchedRoute->viewAction(), $matchedRoute->title(), $matchedRoute->getXmlPagesNames(), $matchedRoute->viewActionXML(), $matchedRoute->viewActionXSL(), $matchedRoute->viewSave());
	}

	private function getMatchRoute(string $uri): Route
	{
		$router = $this->getInitRouter();
		$matchedRoute = $router->getRoute($uri);

		return $matchedRoute;
	}

	public function httpRequest(): HTTPRequest
	{
		return $this->httpRequest;
	}

	public function httpResponse(): HTTPResponse
	{
		return $this->httpResponse;
	}

	public function currentController(): BackController
	{
		return $this->currentController;
	}

	public function currentRoute(): Route
	{
		return $this->currentRoute;
	}

	public function name(): string
	{
		return $this->name;
	}

	public function config(): Config
	{
		return $this->config;
	}

	public function user(): User
	{
		return $this->user;
	}

	public function isForWardMode(): bool
	{
		return $this->forwardURI;
	}

	public function filigranne(): Filigranne
	{
		return $this->filigranne;
	}

	public function nbreVisiteTotal(): int
	{
		return $this->nbreVisiteTotal;
	}

	public function nbreVisiteForDay(): int
	{
		return $this->nbreVisiteForDay;
	}

	public function nbreConnectes(): int
	{
		return $this->nbreConnectes;
	}

	private function refreshFiligrane(Route $route)
	{
		$filigrane = $this->filigrane();
		if (in_array($route, $filigrane)) {
			while (end($filigrane) != $route) {
				unset($filigrane[count($filigrane) - 1]);
			}
		} else {
			$filigrane[] = $route;
		}
		$this->user->setAttribute('filigrane', $filigrane);
	}

	public static function loadAppConfig(): \SimpleXMLElement
	{
		if (is_null(self::$appFileConfig)) {
			self::$appFileConfig = simplexml_load_file(dirname(__DIR__) . "/AppliLib/appConfig.xml");
		}
		return self::$appFileConfig;
	}

	public function run()
	{
		$controller = $this->getController();
		$controller->execute();

		// $this->httpResponse->setPage($controller->page());
		$this->httpResponse->send();
	}

	public function appConfigCanWriteScheme(): bool
	{
		$xmlConfig = Application::loadAppConfig();
		$persistence = $xmlConfig->persistence;
		// var_dump($persistence);
		if ($persistence != null) {
			$attributes = $persistence->attributes();
			// var_dump($persistence);
			return @isset($attributes[Application::CONFIG_SCHEME_ATTRIB_ACTUALIZE]) && $attributes[Application::CONFIG_SCHEME_ATTRIB_ACTUALIZE] == Application::CONFIG_SCHEME_ATTRIB_ACTUALIZE_YES;
		}
		// var_dump("lkl");
		return false;
	}
}
