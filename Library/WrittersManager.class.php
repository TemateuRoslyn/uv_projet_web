<?php

namespace Library;

class WrittersManager extends PObject
{
    const SOURCE_SCHEMA_UML = "UML";
    const SOURCE_SCHEMA_UML_STATES = "UML_STATES";
    const SOURCE_SCHEMA_PHP = "PHP";
    const SOURCE_SCHEMA_DB = "DB";

    const PATH_SCHEME_MAPPING_UML = "uml_pideria.xml";
    private const ATTRIB_SCHEME_FROM = "from";

    /**
     * @property WrittersFactory
     */
    protected $writterFactory = null;

    public function __construct()
    {
        $this->writterFactory = new WrittersFactory();
        $this->actualise();
    }

    public function actualise(): void
    {
        $source = $this->sourceScheme();
        $makerDescriptor = $this->writterFactory->getMakerDescriptor($source);
        $makers = $this->writterFactory->getMakersFrom($makerDescriptor);
        // $this->testVar($makers);
        foreach ($makers as $maker) {
            $maker->actualiseFiles();
        }
    }

    protected function actualiseFromPHP()
    {
    }

    protected function actualiseFromDB()
    {
    }

    protected function actualiseFromUML()
    {
    }

    public function sourceScheme(): string
    {
        $xmlConfig = Application::loadAppConfig();
        $schemeMapping = $xmlConfig->schemeMapping;

        if ($schemeMapping != null) {
            $attributes = $schemeMapping->attributes();
            return $attributes[self::ATTRIB_SCHEME_FROM];
        }

        return self::SOURCE_SCHEMA_DB;
    }
}
