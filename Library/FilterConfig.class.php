<?php

namespace Library;

class FilterConfig extends PObject
{
    protected $url;
    protected $module;
    protected $action;

    public function __construct($url, $module, $action)
    {
        $this->url = $url;
        $this->module = $module;
        $this->action = $action;
    }

    public function match($url)
    {
        if (@preg_match('`^' . $this->url . '$`', $url, $matches)) {
            return $matches;
        } else {
            return false;
        }
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function url()
    {
        return $this->url;
    }

    public function module()
    {
        return $this->module;
    }

    public function action()
    {
        return $this->action;
    }
}
