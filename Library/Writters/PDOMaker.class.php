<?php

namespace Library\Writters;

use Library\ClassDef;

class PDOMaker extends AbstractMaker
{
    public function __construct(FilesMakerInterface $descriptor)
    {
        parent::__construct($descriptor);
    }

    public function actualiseFiles()
    {
        $classDefs = $this->fileDescriptor->getClassDefs();
        // var_dump($classDefs);
        foreach ($classDefs as $def) {
            if ($def->type() == ClassDef::TYPE_CLASS_ENTITY) {
                $maker = new PDOMakerClassDef($def);
                $maker->writeFile();
            }
        }

        $this->createTableConfigs();
    }

    public function createTableConfigs()
    {
        $classLinksAssociations = $this->fileDescriptor->classLinkAssociations();
        foreach ($classLinksAssociations as $assoc) {

            $maker = PDOMakerClassDef::makerClassLinkAssoc($assoc);
            $maker->writeFile();
        }
    }
}
