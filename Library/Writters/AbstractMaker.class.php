<?php

namespace Library\Writters;

use Library\ClassDef;
use Library\PObject;

abstract class AbstractMaker extends PObject
{
    /**
     * @property FilesMakerInterface
     */
    protected $fileDescriptor = null;

    static $SUFFIX_PATTERN = [];

    public function __construct(FilesMakerInterface $descriptor)
    {
        $this->fileDescriptor = $descriptor;
        self::$SUFFIX_PATTERN = [
            ClassDef::NAMESPACE_ENTITIES => str_replace(FileMakerClassDef::NAMESPACE_LIBRARY, "", FileMakerClassDef::ENTITY_DEFAULT_PARENT),
            ClassDef::NAMESPACE_MODELS => str_replace(FileMakerClassDef::NAMESPACE_LIBRARY, "", FileMakerClassDef::MODELS_DEFAULT_PARENT_PDO),
        ];
    }

    abstract public function actualiseFiles();

    public static function haveModel(ClassDef $def, array $classDefs): bool
    {
        return self::haveType(ClassDef::NAMESPACE_MODELS, $def, $classDefs);
    }

    public static function haveType(string $type, ClassDef $def, array $classDefs): bool
    {
        // var_dump($prefix);
        if (!$def->haveNamespace($type)) {
            // var_dump("not same");
            foreach ($classDefs as $defO) {
                // if ($defO->haveNamespace($type) && strpos($def->name(), $defO->name()) === 0) {
                if ($defO->name() == $def->name() . "" . self::$SUFFIX_PATTERN[$type]) {
                    // var_dump($defO->name() . " for " . $def->name());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return ClassDef
     */
    public static function getHaveModel(ClassDef $def, array $classDefs)
    {
        return self::getHaveType(ClassDef::NAMESPACE_MODELS, $def, $classDefs);
    }

    /**
     * @return ClassDef
     */
    public static function getHaveType(string $type, ClassDef $def, array $classDefs)
    {
        if (!$def->haveNamespace($type)) {
            foreach ($classDefs as $defO) {
                if ($defO->name() == $def->name() . "" . self::$SUFFIX_PATTERN[$type]) {
                    return $defO;
                }
            }
        }
        return null;
    }
}
