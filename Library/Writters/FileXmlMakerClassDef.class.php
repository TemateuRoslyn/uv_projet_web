<?php

namespace Library\Writters;

use Library\Application;
use Library\ClassDef;
use Library\ClassEntityDef;
use Library\ClassFunctionDef;
use Library\Collectors\TunelCollector;
use Library\PropertySwitch;

class FileXmlMakerClassDef extends AbstractMapping
{
    const SUFFIX_COLLECTOR_ENTITIES = AbstractMapping::TUNEL_MAPPING_COLLECTOR_NAME . "entities";
    const PREFIX_VIEW_ENTITY = "view-";
    const PREFIX_VIEW_DATA_ENTITY = "data-";
    const SUFFIX_VIEW_DATA_ENTITY = "-list";

    public function __construct(ClassDef $def)
    {
        $this->classDef = $def;
        // parent::__construct($map->classDef());
    }

    /**
     * @param string $path Chemin relatif de votre projet
     * @param string $content Contenu à écrire
     */
    public function writeFileContent($path, $content)
    {
        $filename = FileMakerClassDef::$pathWorkspace . $path;
        if ($this->classDef->type() == ClassDef::TYPE_CLASS_ENTITY) {
            $filenamePath = ClassEntityMaker::PathWorkspaceView . $path;
            $filename = substr($filenamePath, strrpos($filenamePath, "\\"));
            $dir = str_replace($filename, "", $filenamePath);

            @mkdir($dir, 0777, true);
            $filename = $dir . $filename;
            TunelCollector::addElement(self::SUFFIX_COLLECTOR_ENTITIES, $this->classDef);
        }
        // var_dump($filename);
        if (!is_file($filename)) {
            $monfichier = fopen($filename, "w");
            fwrite($monfichier, $content);
            fclose($monfichier);
        }
    }

    protected function classDescription(): string
    {
        $propertiesChain = "";
        // $functionsChain = $this->formatFunctionDescrip();
        // $settesChain = "";
        // $gettersChain = "";

        foreach ($this->classDef->propertiesDefAll() as $def) {
            $propertiesChain .= $this->property($def);
            // $settesChain .= $this->setter($def);
            // $gettersChain .= $this->getter($def);
        }
        $propertiesChain .= $this->propertyForEach();
        // $functionsChain = empty($functionsChain) ? "" : "\n" . $functionsChain . "\n";
        $content = $this->classMotif();
        $content = str_replace(self::BODY, $propertiesChain  . "\n", $content);

        // var_dump($propertiesChain);
        return $content;
    }

    public function writeFile()
    {
        $path = $this->classDef->repository();
        $path = str_replace("AppliLib\\", "", $path);
        // @mkdir($path);
        // var_dump($path);
        $path .= strtolower($this->classDef->name() . ".xsl");
        // $this->testVar($this->classDef->path());AppliLib
        $this->writeFileContent($path, $this->classDescription());
    }

    public function writeFileViewsAction()
    {
        $listFuncts = $this->classDef->functionsDef();

        foreach ($listFuncts as $funct) {
            $name = strtolower(str_replace("execute", "", $funct->name()));
            // var_dump($name);
            // $this->testVar("");
            $this->writeFileContent($this->classDef->repository() . "\Views\\" . $name . ".xml", $this->formatFunctionDescViewXML());
            $this->writeFileContent($this->classDef->repository() . "\Views\\" . $name . ".xsl", $this->formatFunctionDescViewXSL());
        }
    }

    /**
     * @param ClassDef[] $classDefs
     */
    public function writeGroupLoadParamXSL(array $classDefs)
    {
        $motif = $this->classMotif();
        $params = "";
        $applyCalls = "";
        $applyInclude = "";
        $applyLoadData = "";

        foreach ($classDefs as $classDef) {
            $entityName = self::PREFIX_VIEW_ENTITY . strtolower($classDef->name());
            $entityNameList = self::PREFIX_VIEW_ENTITY . strtolower($classDef->name()) . self::SUFFIX_VIEW_DATA_ENTITY;

            $entityNameData = self::PREFIX_VIEW_DATA_ENTITY . strtolower($classDef->name());
            $entityNameDataList = self::PREFIX_VIEW_DATA_ENTITY . strtolower($classDef->name()) . self::SUFFIX_VIEW_DATA_ENTITY;

            $applyInclude .= "\n\t<xsl:include href=\"./Entities/" . strtolower($classDef->name()) . ".xsl\" />";
            $applyLoadData .= "\n\t\t\t\t<xsl:with-param name=\"" . $entityName . "\" select=\"\$" . $entityNameData . "\" />";
            $applyLoadData .= "\n\t\t\t\t<xsl:with-param name=\"" . $entityNameList . "\" select=\"\$" . $entityNameDataList . "\" />";

            $params .= "\n\t\t<xsl:param name=\"" . $entityName . "\" />";
            $params .= "\n\t\t<xsl:param name=\"" . $entityNameList . "\" />";

            $applyCalls .= "\n\t\t\t\t<xsl:with-param name=\"" . $entityName . "\" select=\"\$" . $entityName . "\" />";
            $applyCalls .= "\n\t\t\t\t<xsl:with-param name=\"" . $entityNameList . "\" select=\"\$" . $entityNameList . "\" />";
        }
        // Content Replicate
        $content = "$applyInclude\n\n\t<xsl:template match=\"child::node() | attribute::*\">$params";
        $content .= "\n\t\t<xsl:copy>\n\t\t\t<xsl:apply-templates select=\"@*\" />\n\t\t\t<xsl:apply-templates select=\"node()\">$applyCalls\n\t\t\t</xsl:apply-templates>\n\t\t</xsl:copy>\n\t</xsl:template>";
        // Content Load
        $content .= "\n\n\t<xsl:template match=\"content\" priority=\"10\">";
        $content .= "\n\t\t\t<xsl:apply-templates select=\"node()\">$applyLoadData\n\t\t\t</xsl:apply-templates>\n\t</xsl:template>";

        $content = str_replace(self::BODY, $content  . "\n", $motif);
        $this->writeFileContent("AppViews\wp-params.xsl", $content);
    }

    protected function formatFunctionDescViewXSL()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<xsl:stylesheet version=\"2.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n\t<xsl:output method=\"html\" encoding=\"UTF-8\" />\n\t<xsl:include href=\"../../../../../AppViews/libxsl.xsl\" />\n\n</xsl:stylesheet>";
    }

    protected function formatFunctionDescViewXML()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<content></content>";
    }

    public function classMotif(): string
    {
        // if ($this->classDef->type() == ClassDef::TYPE_CLASS_APPLICATION) {
        //     return $this->classMotifApp();
        // }

        // if ($this->classDef->type() == ClassDef::TYPE_CLASS_CONTROLLER) {
        //     return $this->classMotifController();
        // }

        return $this->classMotifEntity();
    }

    public function classMotifEntity()
    {
        $entityName = self::PREFIX_VIEW_DATA_ENTITY . strtolower($this->classDef->name());
        $content  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $content .= "\n<xsl:stylesheet version=\"2.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">";
        $content .= "\n\t<xsl:variable name=\"" . $entityName . "\" select=\"//pageScope//element//" . $this->classDef->name() . "\" />";
        $content .= "\n\t<xsl:variable name=\"" . $entityName . self::SUFFIX_VIEW_DATA_ENTITY . "\" select=\"//pageScope//list-elements//" . $this->classDef->name() . "\" />";
        $content .=  self::BODY;
        $content .= "\n</xsl:stylesheet>";

        return $content;
    }

    protected function entityDefType(ClassEntityDef $def): string
    {
        return $def->isNaturel() ? PropertySwitch::umlToPHP($def) : $def->type();
    }

    public function property(ClassEntityDef $attrib)
    {
        // $type = $this->entityDefType($attrib);
        // $visibility = $attrib->visibility();
        // $default = "";
        // if ($this->classDef->haveNamespace(ClassDef::NAMESPACE_ENTITIES)) {
        //     $visibility = "protected";
        // }

        // if ($attrib->haveDefaultValue()) {
        //     $default = " = " . $attrib->defaultValue();
        //     // var_dump($this->classDef->name());
        // }
        $entityName = self::PREFIX_VIEW_ENTITY . strtolower($this->classDef->name());
        $result = "\n\n\t<xsl:template match=\"" . $entityName . "-" . $attrib->name() . "\">";
        $result .= "\n\t\t<xsl:param name=\"" . $entityName . "\" />";
        $result .= "\n\t\t<!-- " . $attrib->comment() . " -->";
        $result .= "\n\t\t<xsl:value-of select=\"\$" . $entityName . "/@" . $attrib->name() . "\" disable-output-escaping=\"yes\"/>";
        $result .= "\n\t</xsl:template>";

        return $result;
    }

    protected function propertyForEach()
    {
        $entityName = self::PREFIX_VIEW_ENTITY . strtolower($this->classDef->name());
        $result  = "\n\n\t<xsl:template match=\"" . $entityName . "-tab\">";
        $result .= "\n\t\t<xsl:param name=\"" . $entityName . "-list\"/>";
        $result .= "\n\t\t<xsl:variable name=\"context\" select=\".\" />";

        $result .= "\n \n\t\t<xsl:for-each select=\"\$" . $entityName . "-list\">";
        $result .= "\n\t\t\t<xsl:variable name=\"element\" select=\".\" />";
        $result .= "\n\t\t\t<xsl:for-each select=\"\$context\">";

        $result .= "\n\t\t\t\t<xsl:apply-templates>";
        $result .= "\n\t\t\t\t\t<xsl:with-param name=\"" . $entityName . "\" select=\"\$element\" />";
        $result .= "\n\t\t\t\t</xsl:apply-templates>";

        $result .= "\n\t\t\t</xsl:for-each>";
        $result .= "\n\t\t</xsl:for-each>";
        $result .= "\n\t</xsl:template>";

        return $result;
    }
}
