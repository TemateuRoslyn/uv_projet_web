<?php

namespace Library\Writters;

use Library\ClassDef;

class FilesMaker extends AbstractMaker
{

    public function __construct(FilesMakerInterface $descriptor)
    {
        parent::__construct($descriptor);
    }

    public function actualiseFiles()
    {
        $classDefs = $this->fileDescriptor->getClassDefs();
        $groupeClassDefs = $this->groupForEntities($classDefs);
        // $this->testVar($groupeClassDefs);
        foreach ($groupeClassDefs as $groupe) {
            // var_dump(array_keys($groupe));
            if (isset($groupe[strtolower(ClassDef::NAMESPACE_ENTITIES)])) {
                $maker = new ClassEntityMakerClassDef($groupe[strtolower(ClassDef::NAMESPACE_ENTITIES)]);
                if (isset($groupe[strtolower(ClassDef::NAMESPACE_MODELS)])) {
                    // var_dump("ljdklj");
                    $maker->setEntityModel($groupe[strtolower(ClassDef::NAMESPACE_MODELS)]);
                }

                $maker->writeFile();
                $this->createFilesBuilds($maker, $classDefs);
            } else {
                if (isset($groupe[strtolower(ClassDef::NAMESPACE_MODELS)])) {
                    $maker = new ClassEntityMakerClassDef($groupe[strtolower(ClassDef::NAMESPACE_MODELS)]);

                    $maker->writeFile();
                    $this->createFilesBuilds($maker, $classDefs);
                }
            }
        }
    }

    /**
     * @param ClassDef[]
     */
    public function groupForEntities(array $classDefs): array
    {
        $results = [];
        foreach ($classDefs as $def) {
            $entityName = ClassEntityMakerClassDef::rootEntityName($def);
            // var_dump($entityName);
            if (!empty($entityName)) {
                if (!isset($results[$entityName])) {
                    $results[$entityName] = [];
                }
                // var_dump($entityName . " " . $def->name() . " " . $def->namespace());
                $tmp = $results[$entityName];
                $tmp[$def->namespace()] = $def;

                $results[$entityName] = $tmp;
                // var_dump(array_keys($tmp));
            }
        }
        // $this->testVar($results);
        return $results;
    }

    public function createFilesBuilds(ClassEntityMakerClassDef $maker, array $classDefs)
    {
        // if (!self::haveModel($maker->classdef(), $classDefs) && !$maker->classDef()->haveNamespace(ClassDef::NAMESPACE_MODELS)) {
        // var_dump("have not : " . $maker->classDef()->name());
        $maker->createFileManager();
        // }

        $maker->createFileFormBuilder();
        $maker->createFileFormHeader();
    }
}
