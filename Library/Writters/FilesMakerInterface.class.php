<?php

namespace Library\Writters;

use Library\ClassDef;
use Library\ClassLinkAssociation;

interface FilesMakerInterface
{
    /**
     * @return ClassDef[]
     */
    public function getClassDefs();

    /**
     * @return ClassLinkAssociation[]
     */
    public function classLinkAssociations();
}
