<?php

namespace Library\Writters;

use Library\Application;


class ClassEntityMaker
{
    private static $pathWorkspace = __DIR__ . "\..\..\AppliLib\\";
    public const PathWorkspaceView = __DIR__ . "\..\..\AppViews\\";

    const PATH_ENTITIES = "Entities\\";
    const PATH_ENTITIES_FB = "FormBuilder\\";
    const PATH_ENTITIES_FH = "FormHeaders\\";
    const PATH_ENTITIES_MA = "Models\\";

    const HEAD = "<?php\n\tnamespace AppliLib\Entities;";
    const HEAD_FB = "<?php\n\tnamespace AppliLib\FormBuilder;";
    const HEAD_FH = "<?php\n\tnamespace AppliLib\FormHeaders;";
    const HEAD_MA = "<?php\n\tnamespace AppliLib\Models;";
    const HEAD_APP = "<?php\n\tnamespace Applications";
    const HEAD_MODULE = "<?php\n\tnamespace Applications\#app#\Modules\#module#;";

    const BODY = "#body#";
    const SUFFIX = "";

    private $properties = array();
    private $className;
    private $className_FB;
    private $className_FH;
    private $className_MA;
    private $className_MA_XML;

    public function __construct($className)
    {
        $this->className_FB = $className . "FormBuilder" . self::SUFFIX;
        $this->className_FH = $className . "FormHeader" . self::SUFFIX;
        $this->className_MA = $className . "Manager_PDO" . self::SUFFIX;
        $this->className_MA_XML = $className . "Manager_XML" . self::SUFFIX;

        $this->className = $className;
    }

    public function writeFileContent($path, $content)
    {
        $filename = self::$pathWorkspace . $path . ".class.php";
        if (!is_file($filename)) {
            $monfichier = fopen($filename, "w");
            fwrite($monfichier, $content);
            fclose($monfichier);
        }
    }

    public function writeFile()
    {
        $propertiesChain = "";
        $settesChain = "";
        $gettersChain = "";

        foreach ($this->properties as $table) {
            $propertiesChain .= $this->property($table);
            $settesChain .= $this->setter($table);
            $gettersChain .= $this->getter($table);
        }

        $content = $this->classMotif($this->className);
        $content = str_replace(self::BODY, $propertiesChain . "\n" . $settesChain . "\n" . $gettersChain, $content);

        $this->writeFileContent(self::PATH_ENTITIES . $this->className, $content);
    }

    public function createFileManager()
    {
        $content = $this->classMotifManager();
        $this->writeFileContent(self::PATH_ENTITIES_MA . $this->className_MA, $content);

        $appConfig = Application::loadAppConfig();
        $models = $appConfig->modelsConfig;

        if (!is_null($models) && $models->duplication == "XML") {
            $content = $this->classMotifManagerXML();
            $this->writeFileContent(self::PATH_ENTITIES_MA . $this->className_MA_XML, $content);
        }
    }

    public function createFileFormBuilder()
    {
        $formBuild = "\$this->form";
        foreach ($this->properties as $property) {
            $formBuild .= "->add(new \Library\Fields\StringField(array(
                'name' => '" . $property . "',
                'placeholder' => ' Champs : " . strtoupper($property) . "',
                'validators' => array(
                    new \Library\Validators\MaxLengthValidator('La valeur spécifiée est trop longue (30 caractères maximum)', 30),
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),
                ),
            )))";
        }

        $formBuild .= ";";
        $content = $this->classMotifFomBuilder();
        $content = str_replace(self::BODY, $formBuild, $content);

        $this->writeFileContent(self::PATH_ENTITIES_FB . $this->className_FB, $content);
    }

    public function createFileFormHeader()
    {
        $content = $this->classMotifFomHeader();
        $this->writeFileContent(self::PATH_ENTITIES_FH . $this->className_FH, $content);
    }

    public function classMotif($className)
    {
        $content = self::HEAD;
        $content .= "\n\tclass " . ucfirst($className) . " extends \Library\Entity
            {\n" .
            self::BODY
            . "}";

        return $content;
    }

    public function classMotifManager()
    {
        $content = self::HEAD_MA . "\n";
        $content .= "\tclass " . ucfirst($this->className_MA) . " extends \Library\Managers_PDO
            {
                public function __construct(\$api, \PDO \$dao)
                {
                    parent::__construct(\$api,\$dao);
                }

            }\n";

        return $content;
    }

    public function classMotifManagerXML()
    {
        $content = self::HEAD_MA . "\n";
        $content .= "\tclass " . ucfirst($this->className_MA_XML) . " extends \Library\Managers_XML
            {
                public function __construct(\$api, \DOMDocument \$dao)
                {
                    parent::__construct(\$api,\$dao);
                }

            }\n";

        return $content;
    }

    public function classMotifFomBuilder()
    {
        $content = self::HEAD_FB . "\n";
        $content .= "\tclass " . ucfirst($this->className_FB) . " extends \Library\FormBuilder
        {
            public function build() {" .
            self::BODY
            . "}
        }\n";

        return $content;
    }

    public function classMotifFomHeader()
    {
        $content = self::HEAD_FH . "\n";
        $content .= "use AppliLib\Entities\\" . ucfirst($this->className) . "\n;";
        $content .= "\tclass " . ucfirst($this->className_FH) . " extends \Library\FormHeader
        {
            public function __construct(\Library\HTTPRequest \$request)
            {
                parent::__construct(\$request, new " . \ucfirst($this->className) . "());
            }
        }\n";

        return $content;
    }

    public function property($name)
    {
        return "protected $" . $name . ";\n";
    }

    public function setter($name)
    {
        return "\npublic function set" . ucfirst($name) . "($" . $name . ")" .
            "{" .
            "\$this->" . $name . " = $" . $name . ";" .
            "}\n";
    }

    public function getter($name)
    {
        return "\npublic function " . $name . "()" .
            "{" .
            "return \$this->" . $name . ";" .
            "}\n";
    }

    public function setProperties(array $properties)
    {
        $this->properties = $properties;
    }
}
