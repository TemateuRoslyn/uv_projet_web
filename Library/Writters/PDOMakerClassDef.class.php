<?php

namespace Library\Writters;

use Library\ClassDef;
use Library\ClassEntityDef;
use Library\ClassLinkAssociation;
use Library\Dao\DAOFactory;
use Library\Owner;
use Library\PropertySwitch;
use PDOException;

class PDOMakerClassDef extends AbstractMapping
{
    const ENGINE_MyISAM = "MyISAM";
    const ENGINE_INNODB = "InnoDB";
    const CHARSET = "latin1";

    public function __construct(ClassDef $def)
    {
        parent::__construct($def);
    }

    public function writeFile()
    {
        $this->writeFileContent("", $this->classDescription());
        $this->writeFileAssociation();
    }

    protected function writeFileAssociation()
    {
        if ($this->classDef->hasParent()) {
            $this->writeFileContent("", $this->classMotifAssociationParent());
            $this->writeFileContent("", $this->classMotifAssociationInnerOwnerClass());
            $this->writeFileContent("", $this->classMotifAssociationInnerParent());
            $this->writeFileContent("", $this->classMotifSecureNamespace());
            $this->writeFileContent("", $this->classMotifManagerSession());
            $this->writeFileContent("", $this->classMotifDefineAssociationParent());
        }
    }

    public function writeFileContent($path, $content)
    {
        self::writeINDB($content);
    }

    static function writeINDB(string $sql)
    {
        if (!empty($sql)) {
            // echo $sql . "<br/><br/><br/>";
            $dao = DAOFactory::getMysqlConnexion();
            if ($dao != null) {
                $req = $dao->prepare($sql);
                try {
                    $req->execute();
                    $req->closeCursor();
                } catch (PDOException $th) {
                    $req->closeCursor();
                }
            }
        }
    }

    protected function classDescription(): string
    {
        $properties = "";
        $properDefs = $this->classDef->propertiesDef();

        foreach ($properDefs as $def) {
            $properties .= $this->propertyMappSql($def) . ",";
        }

        $body = $this->classMotif();
        return str_replace(self::BODY, $properties, $body);
    }

    public function classMotif(): string
    {
        return "CREATE TABLE IF NOT EXISTS `" . strtolower($this->classDef->name()) . "` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            " . self::BODY . "
            `dateInsert` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `dateModif` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY `identifiant` (`id`)
            ) ENGINE=" . self::ENGINE_MyISAM . " DEFAULT CHARSET=" . self::CHARSET . ";";
    }

    protected function classMotifDefineAssociationParent(): string
    {
        return " INSERT INTO `_pideria_generalisation` 
                (`id`, `parent`, `child`, `signature`, `dateInsert`, `dateModif`) 
                VALUES (NULL, '" . $this->classDef->parent()->name() . "', '" . $this->classDef->name() . "', '" . $this->classDef->parent()->name() . "_" . $this->classDef->name() . "',
                CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
    }

    protected final function classMotifAssociationParent(): string
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_generalisation` ( 
            `id` INT NOT NULL AUTO_INCREMENT , 
            `parent` VARCHAR(256) NOT NULL , 
            `child` VARCHAR(256) NOT NULL , 
            `signature` VARCHAR(256) NOT NULL , 
            `dateInsert` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
            `dateModif` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
            PRIMARY KEY (`id`),
            UNIQUE KEY (`signature`)
            ) ENGINE = InnoDB;";
    }

    protected final function classMotifAssociationInnerParent(): string
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_inner_generalisation` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `id_pideria_generalisation` int(11) NOT NULL,
            `idTableParent` int(11) NOT NULL,
            `idTable` int(11) NOT NULL,
            `signature` varchar(256) NOT NULL,
            UNIQUE KEY `id` (`id`),
            UNIQUE KEY `signature` (`signature`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    }

    protected final function classMotifAssociationInnerOwnerClass(): string
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_inner_owner` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `tableUp` varchar(255) DEFAULT NULL,
            `idTableUp` bigint(20) DEFAULT NULL,
            `tableDown` varchar(255) DEFAULT NULL,
            `idTableDown` bigint(20) DEFAULT NULL,
            `signature` varchar(256) NOT NULL,
            UNIQUE KEY `id` (`id`),
            UNIQUE KEY `signature` (`signature`)
          ) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
    }

    protected final function classMotifManagerSession(): string
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_manager_session` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_ip` varchar(256) NOT NULL,
            `session_id` varchar(256) NOT NULL,
            `timeInsert` double NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `user_ip` (`user_ip`),
            UNIQUE KEY `session_id` (`session_id`)
          ) ENGINE=MEMORY DEFAULT CHARSET=latin1;";
    }

    protected final function classMotifSecureNamespace(): string
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_secure_namespace` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `link` varchar(256) NOT NULL,
            `namespace` varchar(256) NOT NULL,
            UNIQUE KEY `id` (`id`),
            UNIQUE KEY `link` (`link`)
          ) ENGINE=MEMORY DEFAULT CHARSET=latin1;";
    }

    protected function propertyMappSql(ClassEntityDef $def): string
    {
        return "`" . $def->name() . "` " . PropertySwitch::umlToSql($def) . " NOT NULL";
    }

    /**
     * @return PDOMakerClassLinkAssociation
     */
    public static function makerClassLinkAssoc(ClassLinkAssociation $link)
    {
        return new PDOMakerClassLinkAssociation($link);
    }
}

class PDOMakerClassLinkAssociation extends AbstractMapping
{
    /**
     * @property ClassLinkAssociation
     */
    private $classLinkAssociation = null;

    public function __construct(ClassLinkAssociation $def)
    {
        $this->classLinkAssociation = $def;
    }

    public function writeFile()
    {
        // var_dump($this->classLinkAssociation->ownerDown()->classDef()->name());
        // $this->testVar($this->classLinkAssociation->ownerUp()->classDef()->name());
        // try {
        $dao = DAOFactory::getMysqlConnexion();
        if ($dao !== null) {
            $this->writeFileContent("", $this->classDescription());
            $idAssociation = $dao->lastInsertId();

            PDOMakerClassDef::writeINDB($this->mapOwner($this->classLinkAssociation->ownerDown(), $idAssociation));
            PDOMakerClassDef::writeINDB($this->mapOwner($this->classLinkAssociation->ownerUp(), $idAssociation));
        }
        // } catch (PDOException $th) {
        // }
    }

    public function writeFileContent($path,  $content): void
    {
        PDOMakerClassDef::writeINDB($content);
    }

    protected function classDescription(): string
    {
        PDOMakerClassDef::writeINDB($this->classMotif());
        PDOMakerClassDef::writeINDB($this->ownerTable());
        PDOMakerClassDef::writeINDB($this->innerOwnerTable());

        $associationtable = "NULL";
        if ($this->classLinkAssociation->associationClass() != null) {
            $associationtable = $this->classLinkAssociation->associationClass()->name();
        }

        return
            " INSERT INTO `_pideria_association` 
            (`id`, `associationtable`, `signature`, `sens`, `dateInsert`, `dateModif`) 
            VALUES (NULL, '$associationtable', '" . $this->classLinkAssociation->signature() . "', '" . $this->classLinkAssociation->sens() . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
    }

    public function classMotif()
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_association` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `associationtable` varchar(100) DEFAULT NULL,
            `signature` VARCHAR(255) NOT NULL , 
            `sens` VARCHAR(255) NOT NULL , 
            `dateInsert` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `dateModif` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            UNIQUE KEY `signature` (`signature`),
            PRIMARY KEY `identifiant` (`id`)
            ) ENGINE=" . PDOMakerClassDef::ENGINE_INNODB . " DEFAULT CHARSET=" . PDOMakerClassDef::CHARSET . ";";
    }

    protected function mapOwner(Owner $def, int $idAssociation): string
    {
        return
            " INSERT INTO `_pideria_owner` 
            (`id`, `idAssociation`, `classassociation`, `cardinaliteUp`, `cardinaliteDown`, `aggregation`, `signature`, `dateInsert`, `dateModif`) 
            VALUES (NULL, $idAssociation, '" . $def->classDef()->name() . "', '" . $def->cardinaliteUp() . "', '" . $def->cardinaliteDown() . "', '" . $def->aggregation() . "','" . $idAssociation . "" . $def->classDef()->name() . "', 
            CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
    }

    protected function ownerTable()
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_owner` ( 
            `id` bigint(20) NOT NULL AUTO_INCREMENT , 
            `idAssociation` bigint(20) NOT NULL,
            `classassociation` VARCHAR(256) NOT NULL , 
            `cardinaliteUp` VARCHAR(25) NOT NULL , 
            `cardinaliteDown` VARCHAR(25) NOT NULL ,
            `aggregation` VARCHAR(25) NULL , 
            `signature` VARCHAR(255) NOT NULL , 
            `dateInsert` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
            `dateModif` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
            UNIQUE KEY `id` (`id`),
            UNIQUE KEY `signature` (`signature`),
            KEY `idAssociation` (`idAssociation`),
            CONSTRAINT `_pideria_owner_association_contraints` 
            FOREIGN KEY (`idAssociation`) 
            REFERENCES `_pideria_association` (`id`) ON DELETE CASCADE
            ) ENGINE=" . PDOMakerClassDef::ENGINE_INNODB . " DEFAULT CHARSET=" . PDOMakerClassDef::CHARSET . ";";
    }

    protected function innerOwnerTable()
    {
        return "CREATE TABLE IF NOT EXISTS `_pideria_inner_owner` ( 
            `id` bigint(20) NOT NULL AUTO_INCREMENT , 
            `tableUp` VARCHAR(255) NULL,
            `idTableUp` bigint(20) NULL,
            `tableDown` VARCHAR(255) NULL,
            `idTableDown` bigint(20) NULL,
            UNIQUE KEY `id` (`id`)
            ) ENGINE=" . PDOMakerClassDef::ENGINE_MyISAM . " DEFAULT CHARSET=" . PDOMakerClassDef::CHARSET . ";";
    }
}
