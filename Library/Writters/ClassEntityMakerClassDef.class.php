<?php

namespace Library\Writters;

use Library\ClassDef;

class ClassEntityMakerClassDef extends FileMakerClassDef
{
    /**
     * @property ClassDef
     */
    protected $entityModel = null;

    public function setEntityModel(ClassDef $model)
    {
        $this->entityModel = $model;
        // var_dump($this->entityModel->name() . " Model");
    }

    public function classMotifManager()
    {
        // var_dump($this->classDef->name() . " Models");
        if ($this->entityModel == null) {
            return parent::classMotifManager();
        }

        return $this->classMotifWithClassDef($this->entityModel);
    }

    protected function classMotifWithClassDef(ClassDef $manager)
    {
        // var_dump($manager->name() . " Manager");
        $maker = new FileMakerClassDef($manager);
        return $maker->classDescription();
    }

    public static function rootEntityName(ClassDef $def): string
    {
        if ($def->haveNamespace(ClassDef::NAMESPACE_ENTITIES)) {
            return $def->name();
        }

        if ($def->haveNamespace(ClassDef::NAMESPACE_MODELS) && strpos(FilesMaker::$SUFFIX_PATTERN[ClassDef::NAMESPACE_MODELS], $def->name()) !== 0) {
            // var_dump(FilesMaker::$SUFFIX_PATTERN[ClassDef::NAMESPACE_MODELS]);
            return str_replace(FilesMaker::$SUFFIX_PATTERN[ClassDef::NAMESPACE_MODELS], "", $def->name());
        }

        return '';
    }
}
