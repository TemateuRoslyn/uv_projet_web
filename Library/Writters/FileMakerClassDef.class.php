<?php

namespace Library\Writters;

use Library\Application;
use Library\ClassDef;
use Library\ClassEntityDef;
use Library\ClassFunctionDef;
use Library\PropertySwitch;

class FileMakerClassDef extends AbstractMapping
{
    const DIR_CONFIG = "AppliLib";
    public static $pathWorkspace = __DIR__ . "\..\..\\";
    // private static $pathWorkspace = __DIR__ . "\..\..\\" . self::DIR_CONFIG . "\\";

    const PATH_ENTITIES = "Entities\\";
    const PATH_ENTITIES_FB = "FormBuilder\\";
    const PATH_ENTITIES_FH = "FormHeaders\\";
    const PATH_ENTITIES_MA = "Models\\";

    const HEAD = "<?php\n\tnamespace " . self::DIR_CONFIG . "\Entities;";
    const HEAD_FB = "<?php\n\tnamespace " . self::DIR_CONFIG . "\FormBuilder;";
    const HEAD_FH = "<?php\n\tnamespace " . self::DIR_CONFIG . "\FormHeaders;";
    const HEAD_MA = "<?php\n\tnamespace " . self::DIR_CONFIG . "\Models;";

    const SUFFIX = "";

    const NAMESPACE_LIBRARY = "\Library\\";
    const ENTITY_DEFAULT_PARENT = self::NAMESPACE_LIBRARY . "Entity";
    const CONTROLLER_DEFAULT_PARENT =  "ABackController";
    const APP_DEFAULT_PARENT = self::NAMESPACE_LIBRARY . "\Application";
    const MODELS_DEFAULT_PARENT_PDO = self::NAMESPACE_LIBRARY . "Managers_PDO";
    const MODELS_DEFAULT_PARENT_XML = self::NAMESPACE_LIBRARY . "Managers_XML";
    const FORM_BUILDER_DEFAULT_PARENT = self::NAMESPACE_LIBRARY . "FormBuilder";
    const FORM_HEADER_DEFAULT_PARENT = self::NAMESPACE_LIBRARY . "FormHeader";

    private $className;
    private $className_FB;
    private $className_FH;
    private $className_MA;
    private $className_MA_XML;

    public function __construct(ClassDef $classDef)
    {
        parent::__construct($classDef);
        $className = $classDef->name();

        $this->className_FB = $className . self::SUFFIX;
        $this->className_FH = $className . self::SUFFIX;
        $this->className_MA = $className .  self::SUFFIX;
        $this->className_MA_XML = $className . self::SUFFIX;

        $this->className = $className;
    }

    protected function classDescription(): string
    {
        $propertiesChain = "";
        $functionsChain = $this->formatFunctionDescrip();

        $settesChain = "";
        $gettersChain = "";

        foreach ($this->classDef->propertiesDef() as $def) {
            $propertiesChain .= $this->property($def);
            $settesChain .= $this->setter($def);
            $gettersChain .= $this->getter($def);
        }

        $functionsChain = empty($functionsChain) ? "" : "\n" . $functionsChain . "\n";
        $content = $this->classMotif();
        $content = str_replace(self::BODY, $propertiesChain . $functionsChain . $settesChain . "\n" . $gettersChain, $content);

        // var_dump($propertiesChain);
        return $content;
    }

    public function writeFile()
    {
        if ($this->classDef->type() == ClassDef::TYPE_CLASS_FORM_BUILDER) {
            $this->createFileFormBuilder();
        } else {
            $this->writeFileContent(ucfirst($this->classDef->path()), $this->classDescription());
        }
    }

    public function createFileManager()
    {
        $content = $this->classMotifManager();
        $this->writeFileContent(self::PATH_ENTITIES_MA . $this->className_MA, $content);

        $appConfig = Application::loadAppConfig();
        $models = $appConfig->modelsConfig;

        if (!is_null($models) && $models->duplication == "XML") {
            $content = $this->classMotifManagerXML();
            $this->writeFileContent(self::PATH_ENTITIES_MA . $this->className_MA_XML, $content);
        }
    }

    public function createFileFormBuilder()
    {
        $formBuild = $this->formBuilderAddDescription();
        $content = $this->classMotifFomBuilder();

        $content = str_replace(self::BODY, $formBuild, $content);
        $this->writeFileContent($this->classDef->path(), $content);
    }

    protected function formBuilderAddDescription(): string
    {
        $def = $this->classDef->getComplementClassDef();
        $formBuild = "\$this->form";
        foreach ($def->propertiesDef() as $property) {
            $validators = PropertySwitch::umlToValidator($property);
            $validatorsDescr = "";

            foreach ($validators as $description) {
                $validatorsDescr .= "new " . $description . ",\n";
            }

            $formBuild .= "->add(new \Library\Fields\StringField(array(
                'name' => '" . $property->name() . "',
                'placeholder' => ' Champs : " . strtoupper($property->name()) . "',
                'validators' => array(
                    $validatorsDescr
                ),
            )))";
        }

        $formBuild .= ";";
        return $formBuild;
    }

    public function createFileFormHeader()
    {
        $content = $this->classMotifFomHeader();
        $this->writeFileContent(self::PATH_ENTITIES_FH . $this->className_FH, $content);
    }

    public function classMotif(): string
    {
        if ($this->classDef->type() == ClassDef::TYPE_CLASS_APPLICATION) {
            return $this->classMotifApp();
        }

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_CONTROLLER) {
            return $this->classMotifController();
        }

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_ENTITY) {
            return $this->classMotifEntity();
        }

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_FORM_BUILDER) {
            return $this->classMotifFomBuilder();
        }

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_MANAGERS_PDO) {
            return $this->classMotifManager();
        }

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_MANAGERS_XML) {
            return $this->classMotifManagerXML();
        }

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_FORM_HEADER) {
            return $this->classMotifFomHeader();
        }
        // var_dump($this->classDef);
    }

    // public function classMotif()
    // {
    //     // var_dump($this->classDef->name() . " MOTIF");
    //     if ($this->classDef->haveNamespace(ClassDef::NAMESPACE_MODELS)) {
    //         return $this->classMotifManager();
    //     }

    //     return $this->classMotifEntity();
    // }

    public function classMotifController()
    {
        $content = $this->classDef->namespace();

        if ($this->classDef->hasParent()) {
            $parent = $this->classDef->parent()->absoluteName();
            // var_dump($this->classDef->parent()->namespace() . " " . $this->classDef->parent()->name());
        } else $parent = self::CONTROLLER_DEFAULT_PARENT;
        // $this->testVar(self::CONTROLLER_DEFAULT_PARENT);
        $content .= "\n\t" . $this->classDef->nature() . " class " . ucfirst($this->classDef->name()) . " extends " . $parent . "
            {\n" .
            self::BODY
            . "}";

        return $content;
    }

    public function classMotifEntity(string $head = "")
    {
        $content = empty($head) ? self::HEAD : $head;
        $parent = "";

        if ($this->classDef->hasParent()) {
            $parent = ($this->classDef->parent()->haveNamespace(ClassDef::NAMESPACE_ENTITIES)) ? $this->classDef->parent()->name() : $this->classDef->parent()->absoluteName();
            // var_dump($this->classDef->parent()->namespace() . " " . $this->classDef->parent()->name());
        } else $parent = self::ENTITY_DEFAULT_PARENT;

        $content .= "\n\t" . $this->classDef->nature() . " class " . ucfirst($this->classDef->name()) . " extends " . $parent . "
            {\n" .
            self::BODY
            . "}";

        return $content;
    }

    public function classMotifApp()
    {
        $content = $this->classDef->namespace() . "\n";
        $parent = "";
        $className =  $this->classDef->name();
        $appName = str_replace(ClassDef::TYPE_CLASS_APPLICATION, "", $className);
        // var_dump($className . " Manager");
        if ($this->classDef->hasParent()) {
            $parent =  $this->classDef->parent()->absoluteName();
            // var_dump($this->classDef->parent()->namespace() . " " . $this->classDef->parent()->name());
        } else $parent = self::APP_DEFAULT_PARENT;

        $content .= "\tclass " . ucfirst($className) . " extends " . $parent . "
            {
                public function __construct()
                {
                    \$this->name = \"" . ucfirst($appName) . "\";
                }

                " . self::BODY . "
            }\n";

        return $content;
    }

    public function classMotifManager()
    {
        $content = self::HEAD_MA . "\n";
        $parent = "";
        $className = $this->classDef->haveNamespace(ClassDef::NAMESPACE_MODELS) ? $this->classDef->name() : $this->className_MA;
        // var_dump($className . " Manager");
        if ($this->classDef->hasParent()) {
            $parent = ($this->classDef->parent()->haveNamespace(ClassDef::NAMESPACE_MODELS)) ? $this->classDef->parent()->name() : $this->classDef->parent()->absoluteName();
            // var_dump($this->classDef->parent()->namespace() . " " . $this->classDef->parent()->name());
        } else $parent = self::MODELS_DEFAULT_PARENT_PDO;

        $content .= "\tclass " . ucfirst($className) . " extends " . $parent . "
            {
                public function __construct(\$api, \PDO \$dao)
                {
                    parent::__construct(\$api,\$dao);
                }

                " . self::BODY . "
            }\n";

        return $content;
    }

    public function classMotifManagerXML()
    {
        $content = self::HEAD_MA . "\n";
        $content .= "\tclass " . ucfirst($this->className_MA_XML) . " extends " . self::MODELS_DEFAULT_PARENT_XML . "
            {
                public function __construct(\$api, \DOMDocument \$dao)
                {
                    parent::__construct(\$api,\$dao);
                }

            }\n";

        return $content;
    }

    public function classMotifFomBuilder()
    {
        $content = self::HEAD_FB . "\n";
        $complement = "";

        $parent = $this->classDef->getComplementClassDef();
        if ($parent != null && $parent->parent() != null) {
            $parent = $parent->parent()->name() . ClassDef::TYPE_CLASS_FORM_BUILDER;
            $complement = "parent::build();";
        } else {
            $parent = self::FORM_BUILDER_DEFAULT_PARENT;
        }

        $content .= "\tclass " . ucfirst($this->classDef->name()) . " extends " . $parent . "
        {
            public function build() { $complement" .
            self::BODY
            . "}
        }\n";

        return $content;
    }

    public function classMotifFomHeader()
    {
        $entityNameBase = str_replace($this->classDef->type(), "", ucfirst($this->className));
        $content = self::HEAD_FH . "\n";
        $content .= "use " . self::DIR_CONFIG . "\Entities\\" . $entityNameBase . "\n;";
        $content .= "\tclass " . ucfirst($this->className) . " extends " . self::FORM_HEADER_DEFAULT_PARENT . "
        {
            public function __construct(\Library\HTTPRequest \$request)
            {
                parent::__construct(\$request, new " . $entityNameBase . "());
            }
        }\n";

        return $content;
    }

    protected function formatFunctionDescrip(): string
    {
        $format = "";
        $listFuncts = $this->classDef->functionsDef();

        foreach ($listFuncts as $funct) {
            $format .= $this->formatFunctionDesc($funct);
        }

        return $format;
    }

    protected function formatFunctionParams(ClassFunctionDef $funct): string
    {
        $paramFormat = "";
        $listParams = $funct->getParametersDecl();

        foreach ($funct->getParametersDecl() as $param) {
            $type = $param[ClassFunctionDef::ATTRIB_TYPE];
            if (strpos("[]", $type) !== false) {
                $type = "array ";
            }

            $paramFormat .= $type . " $" . $param[ClassFunctionDef::ATTRIB_NAME];
            if (count($listParams) > 1) {
                array_shift($listParams);
                $paramFormat .= ",";
            }
        }

        return $paramFormat;
    }

    public function formatFunctionDesc(ClassFunctionDef $funct)
    {
        return "\n" . $funct->visibility() . " function " . lcfirst($funct->name()) . "(" . $this->formatFunctionParams($funct) . "){}";
    }

    protected function entityDefType(ClassEntityDef $def): string
    {
        return $def->isNaturel() ? PropertySwitch::umlToPHP($def) : $def->type();
    }

    public function property(ClassEntityDef $attrib)
    {
        $type = $this->entityDefType($attrib);
        $visibility = $attrib->visibility();
        $default = "";

        if ($this->classDef->type() == ClassDef::TYPE_CLASS_ENTITY) {
            $visibility = "protected";
        }

        if ($attrib->haveDefaultValue()) {
            $default = " = " . $attrib->defaultValue();
            // var_dump($this->classDef->name());
        }

        return "
        /**
         * " . $attrib->comment() . "
         * @property $type
         */" .
            $visibility . " $" . $attrib->name() . $default . ";\n";
    }

    public function setter(ClassEntityDef $attrib)
    {
        $type = $this->entityDefFunctionType($attrib);
        $descript = $this->entityDefFunctionTypeDescript($attrib);
        return "\n$descript
            public function set" . ucfirst($attrib->name()) . "($" . $attrib->name() . ")" .
            "{" .
            "\$this->" . $attrib->name() . " = $" . $attrib->name() . ";" .
            "}";
    }

    public function getter(ClassEntityDef $attrib)
    {
        $type = $this->entityDefFunctionType($attrib);
        $descript = $this->entityDefFunctionReturnDescript($attrib);
        // var_dump($type);
        return "\n$descript
            public function " . $attrib->name() . "()"  .
            "{" .
            "return \$this->" . $attrib->name() . ";" .
            "}";
    }

    protected function entityDefFunctionType(ClassEntityDef $def): string
    {
        if ($def->isTypeTable()) {
            return "array";
        }
        return $this->entityDefType($def);
    }

    protected function entityDefFunctionTypeDescript(ClassEntityDef $def): string
    {
        $descript = "";
        // if ($def->isTypeTable()) {
        $descript = "
            /**
             * @param " . $this->entityDefType($def) . " $" . $def->name() . "
             */  ";
        // }
        return $descript;
    }

    protected function entityDefFunctionReturnDescript(ClassEntityDef $def): string
    {
        $descript = "";
        // if ($def->isTypeTable()) {
        $descript = "
            /**
             * @return " . $this->entityDefType($def) . "  
             */  ";
        // }
        return $descript;
    }
}
