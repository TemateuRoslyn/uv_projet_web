<?php

namespace Library\Writters;

use Library\ClassDef;
use Library\PObject;

abstract class AbstractMapping extends PObject
{
    const BODY = "#body#";

    const TUNEL_MAPPING_COLLECTOR_NAME = "tunelMappingCollectorName";
    /**
     * @property ClassDef
     */
    protected $classDef;
    /**
     * @property FileXmlMakerClassDef $fileXmlMaker
     */
    protected $fileXmlMaker = null;

    public function __construct(ClassDef $def)
    {
        $this->classDef = $def;
        $this->fileXmlMaker = new FileXmlMakerClassDef($def);
    }

    public function writeFileContent($path, $content)
    {
        $filename = FileMakerClassDef::$pathWorkspace . $path;
        @mkdir(substr($filename, 0, strrpos($filename, "\\")), 0777, true);
        // $this->testVar(substr($filename, 0, strrpos($filename, "\\")));
        if ($this->classDef->type() == ClassDef::TYPE_CLASS_CONTROLLER) {
            @mkdir(substr($filename, 0, strrpos($filename, "\\")) . "\Views", 0777, true);
            $this->fileXmlMaker->writeFileViewsAction();
        } else if ($this->classDef->type() == ClassDef::TYPE_CLASS_ENTITY) {
            $this->fileXmlMaker->writeFile();
        }

        if (!is_file($filename)) {
            $monfichier = fopen($filename, "w");
            fwrite($monfichier, $content);
            fclose($monfichier);
        }
    }

    protected abstract function classDescription(): string;
    public abstract function writeFile();
    public abstract function classMotif();

    /**
     * @return ClassDef
     */
    public function classDef()
    {
        return $this->classDef;
    }

    public function fileXmlMaker()
    {
        return $this->fileXmlMaker;
    }
}
