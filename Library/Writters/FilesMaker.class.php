<?php

namespace Library\Writters;

use Library\ClassDef;
use Library\Collectors\TunelCollector;

class FilesMaker extends AbstractMaker
{

    public function __construct(FilesMakerInterface $descriptor)
    {
        parent::__construct($descriptor);
    }

    public function actualiseFiles()
    {
        $classDefs = $this->fileDescriptor->getClassDefs();
        // $this->testVar($classDefs);
        $maker = null;
        foreach ($classDefs as $def) {
            // var_dump(array_keys($groupe));
            $maker = new ClassEntityMakerClassDef($def);
            $maker->writeFile();
        }

        if ($maker !== null) {
            $classDefs = TunelCollector::getList(FileXmlMakerClassDef::SUFFIX_COLLECTOR_ENTITIES);
            $maker->fileXmlMaker()->writeGroupLoadParamXSL($classDefs);
        }
    }
}
