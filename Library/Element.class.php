<?php
	namespace Library;

	class Element extends Entity
	{
		protected $chemin;
		protected $idMembre;

		public function chemin()
		{
			return $this->chemin;
		}

		public function idMembre()
		{
			return $this->idMembre;
		}
		
		public function setChemin($chemin)
		{
			if (is_string($chemin)) 
			{
				$this->chemin = $chemin;
			}
		}

		public function setIdMembre($idMembre)
		{
			$idMembre = (int) $idMembre;
			if ($idMembre > 0) {
				$this->idMembre = $idMembre;
			}
			else
				$this->idMembre = 0;
		}
	}