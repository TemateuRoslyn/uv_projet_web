<?php

namespace Library\UmlStates\Parsor;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMXPath;
use Library\ClassDef;
use Library\ClassEntityDef;
use Library\ClassFunctionDef;
use Library\ClassLinkAssociation;
use Library\ClassPropertyDef;
use Library\Owner;
use Library\UmlModel\PackageManager_UML as UmlModelPackageManager_UML;
use Library\Writters\ClassEntityMaker;
use Library\Writters\FilesMakerInterface;
use Library\WrittersManager;

class PackageManager_UML extends DOMDocument implements FilesMakerInterface
{
    /**
     * Application context definition
     * @property DOMElement
     */
    protected $packageElements = array();
    /**
     * Tableau de clef l'ID du package
     * @property ClassDef
     */
    protected $classDefs = [];

    public function __construct()
    {
        $this->load(__DIR__ . "./../../../AppliLib/UMLSqueleton/uml_states_charts-6.xml");
        $this->packageElements = $this->getElementsByTagName("packagedElement");

        foreach ($this->packageElements as $element) {
            if ($element->getAttribute("name") == "applications") {
                // var_dump("kldkf");
                $this->packageElements = $element;
            }
        }
    }

    /**
     * @return ClassDef[]
     */
    public function getClassDefs()
    {
        if (empty($this->classDefs)) {
            $packageElements = $this->listAllStatesRoleAppPackage();
            // $packageElements = array_merge($packageElements, $this->listAllPseudoStatesRoleAppPackage());
            // var_dump(count($packageElements));
            $results = [];
            foreach ($packageElements as $packageApp) {
                // var_dump($packageApp);
                $packageModules = $this->listAllStatesModuleFromStatesRoleAppPackage($packageApp);
                // var_dump(count($packageModules));
                foreach ($packageModules as $packageModule) {

                    $results[$packageModule->getAttribute('xmi:id')] = $this->classDefFromPackage($packageModule, $packageApp->getAttribute("name"));
                }

                $results[$packageApp->getAttribute('xmi:id')] = new ClassDef(array(
                    "nature" => "",
                    "comment" => "",
                    "name" => $packageApp->getAttribute("name") . "Application",
                    "type" => ClassDef::TYPE_CLASS_APPLICATION,
                    "visibility" => "public",
                    "parent" => $parent = null,
                    "propertiesDef" => [],
                    "functionsDef" => [],
                    "namespace" => ClassEntityMaker::HEAD_APP . "\\" . $packageApp->getAttribute("name") . ";"
                ));
                // var_dump($results[$packageApp->getAttribute('xmi:id')]->path());
            }

            $this->classDefs = $results;
        }

        return $this->classDefs;
    }

    /**
     * @return ClassLinkAssociation[]
     */
    public function classLinkAssociations()
    {
        $packageAssociations = $this->listElementsPackageHaveAttribValue("xmi:type", "uml:Association");
        $packageAssociations = array_merge($packageAssociations, $this->listElementsPackageHaveAttribValue("xmi:type", "uml:AssociationClass"));
        // var_dump($packageAssociations);
        $results = [];
        // foreach ($packageAssociations as $package) {
        //     $results[] = $this->classLinkAssociationFromPackage($package);
        // }

        return $results;
    }

    /**
     * @return ClassDef
     */
    public function classDefFromPackage(DOMElement $packageModule, string $appName)
    {
        $properties = $this->classAttribsFromPackage($packageModule);
        $functions = $this->classFuncsFromPackage($packageModule, "execute");

        return new ClassDef(array(
            "nature" => "",
            "comment" => "",
            "name" => $packageModule->getAttribute("name") . "Controller",
            "type" => ClassDef::TYPE_CLASS_CONTROLLER,
            "visibility" => "public",
            "parent" =>  null,
            "propertiesDef" => $properties,
            "functionsDef" => $functions,
            "namespace" => $namespace = str_replace(array("#app#", "#module#"), array($appName, $packageModule->getAttribute("name")), ClassEntityMaker::HEAD_MODULE)
        ));
        // var_dump($namespace);
    }

    /**
     * @return ClassFunctionDef[]
     */
    public function classFuncsFromPackage(DOMElement $package, string $prefixName = ""): array
    {
        $data = [];
        // var_dump($package);
        $region = $this->getStateRegion($package);
        if ($region != null) {
            // var_dump($package->getAttribute("name"));
            // var_dump($region);
            $ownedOperations = $this->listElementsPackageHaveAttribValueON($region->childNodes, "xmi:type", "uml:State");
            foreach ($ownedOperations as $funct) {
                // $elementType = $this->getElementById($funct->getAttribute("type"));
                $params = $this->getFunctParams();
                // var_dump($attrib->nodeName);
                $data[] = new ClassFunctionDef([
                    "nature" => "",
                    "name" => $prefixName . $funct->getAttribute("name"),
                    "returnType" => $params[UmlModelPackageManager_UML::ATTRIB_PARAM_RETURN],
                    "visibility" => "public",
                    "paramtersDecl" => $params[UmlModelPackageManager_UML::ATTRIB_PARAM_INTOUT],
                ]);
            }
        }

        return $data;
    }

    public function getFunctParams(): array
    {
        $params = [];
        $returnType = null;

        $type = $this->formatFunctParam();
        $params[] = $type;

        return [
            UmlModelPackageManager_UML::ATTRIB_PARAM_INTOUT => $params,
            UmlModelPackageManager_UML::ATTRIB_PARAM_RETURN => $returnType
        ];
    }

    public function formatFunctParam()
    {
        return [
            ClassFunctionDef::ATTRIB_NAME => "request",
            ClassFunctionDef::ATTRIB_TYPE => "\Library\HTTPRequest",
            ClassFunctionDef::ATTRIB_NATURE => ""
        ];
    }

    /**
     * @return ClassEntityDef[]
     */
    public function classAttribsFromPackage(DOMElement $package): array
    {
        $data = [];

        // $ownedAttributes = $package->getElementsByTagName("ownedAttribute");
        // foreach ($ownedAttributes as $attrib) {
        //     $elementType = $this->getElementById($attrib->getAttribute("type"));
        //     // var_dump($attrib->nodeName);
        //     $data[] = new ClassEntityDef([
        //         "defaultValue" => $this->defaultValueFromElementType($attrib),
        //         "comment" => $this->elementComment($elementType),
        //         "nature" => substr($elementType->getAttribute("xmi:type"), 4),
        //         "name" => $attrib->getAttribute("name"),
        //         "type" => $this->typeFromElementType($attrib),
        //         "visibility" => $attrib->getAttribute("visibility"),
        //     ]);
        // }

        return $data;
    }

    /**
     * @return DOMElement
     */
    protected function listAllPseudoStatesRoleAppPackage()
    {
        $results = [];
        foreach ($this->packageElements->childNodes as $region) {
            $statesElements = $this->listElementsPackageHaveAttribValueON($region->childNodes, "xmi:type", "uml:Pseudostate");
            $results = array_merge($statesElements, $results);
        }

        return $results;
    }

    /**
     * @return DOMElement
     */
    protected function listAllStatesRoleAppPackage()
    {
        // $results = [];
        $region = $this->getStateRegion($this->packageElements);
        // var_dump($region);
        // foreach ($regions as $region) {
        //     var_dump($region);
        $statesElements = $this->listElementsPackageHaveAttribValueON($region->childNodes, "xmi:type", "uml:State");
        // $results = array_merge($statesElements, $results);
        // }
        // var_dump(count($statesElements));
        return $statesElements;
    }

    /**
     * @return DOMElement[]
     */
    protected function listAllStatesModuleFromStatesRoleAppPackage($packageApp)
    {
        // $results = [];
        $region = $this->getStateRegion($packageApp);
        if (!is_null($region)) {
            // foreach ($regions as $region) {
            $statesElements = $this->listElementsPackageHaveAttribValueON($region->childNodes, "xmi:type", "uml:State");
            // $results = array_merge($statesElements, $results);
            // }
            return $statesElements;
        }
        return [];
    }

    /**
     * @return DOMElement[]
     */
    public function listAllStatesPackage(): array
    {
        return  $this->listElementsPackageHaveAttribValue("xmi:type", "uml:State");
    }

    /**
     * @return DOMElement[]
     */
    public function listAllPseudoStatesPackage()
    {
        return  $this->listElementsPackageHaveAttribValue("xmi:type", "uml:Pseudostate");
    }

    /**
     * @return DOMElement[]
     */
    public function listElementsPackageHaveAttribValue(string $attrib, string $value): array
    {
        $data = [];
        foreach ($this->packageElements as $element) {
            // var_dump($attrib);
            // var_dump($element->getAttribute($attrib));
            if ($element->getAttribute($attrib) == $value) {
                $data[] = $element;
            }
        }

        return $data;
    }

    /**
     * @return DOMElement
     */
    public function getStateRegion($packageState)
    {
        foreach ($packageState->childNodes as $element) {
            // var_dump($element->getAttribute($attrib));
            if (!is_a($element, "DOMText") &&  $element->tagName == "region") {
                return $element;
            }
        }

        return null;
    }

    /**
     * @return DOMElement[]
     */
    public function listElementsPackageHaveAttribValueON($root, string $attrib, string $value): array
    {
        $data = [];
        foreach ($root as $element) {
            // var_dump($element->getAttribute($attrib));
            if (!is_a($element, "DOMText") && $element->getAttribute($attrib) == $value) {
                $data[] = $element;
            }
        }

        return $data;
    }

    function getElementById($id)
    {
        $xpath = new DOMXPath($this);
        return $xpath->query("//*[@xmi:id='$id']")->item(0);
    }

    public function getFirstChildNotTextNode(DOMElement $element)
    {
        if ($element->hasChildNodes()) {
            foreach ($element->childNodes as $child) {
                if ($child->nodeType == XML_ELEMENT_NODE) {
                    return $child;
                }
            }
        }
        return null;
    }
}
