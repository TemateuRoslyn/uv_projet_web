<?php

namespace Library\UmlStates\Entities;

use Library\UmlModel\UMLElement;

class State extends UMLElement
{
    const PROPERTY_KIND_TERMINATE = "terminate";
    const PROPERTY_KIND_JUNCTION = "junction";
    const PROPERTY_KIND_JOIN = "join";

    protected $kind = "";
    /**
     * @property array Transition
     */
    protected $activities = [];
    /**
     * @property array UMLElement
     */
    protected $region = [];

    public function setKind($kind)
    {
        $this->kind = $kind;
    }

    public function kind()
    {
        return $this->kind;
    }
}
