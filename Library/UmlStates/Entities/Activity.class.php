<?php

namespace Library\UmlStates\Entities;

use Library\UmlModel\UMLElement;

class Activity extends UMLElement
{
    /** 
     * @property array Transition
     */
    protected $actions = [];

    /**
     * @param Transition []
     */
    public function setTransitions($transitions)
    {
        $this->actions = $transitions;
    }
}
