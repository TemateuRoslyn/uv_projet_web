<?php

namespace Library\UmlStates\Entities;

use Library\UmlModel\UMLElement;

class Transition extends UMLElement
{
    /**
     * @property State
     */
    protected $target;
    /**
     * @property State
     */
    protected $source;

    public function setSource(State $source)
    {
        $this->source = $source;
    }

    /**
     * @return State
     */
    public function source()
    {
        return $this->source;
    }

    public function setTarget(State $target)
    {
        $this->target = $target;
    }

    /**
     * @return State
     */
    public function target()
    {
        return $this->target;
    }
}
