<?php

namespace Library;

class PropertySwitch extends PObject
{
    const UML_STRING = "String";
    const UML_DATE = "Date";
    const UML_LONG = "long";
    const UML_CHAR = "char";
    const UML_BOOLEAN = "boolean";
    const UML_INT = "int";
    const UML_FLOAT = "float";
    const UML_DOUBLE = "double";

    private const PRE_DEF_VALIDATOR_MAX_LENGTH = "\Library\Validators\MaxLengthValidator('La valeur spécifiée est trop longue (30 caractères maximum)', 30)";
    private const PRE_DEF_VALIDATOR_NUMBER = "\Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1)";
    private const PRE_DEF_VALIDATOR_NOT_NULL = "\Library\Validators\NotNullValidator('Merci de spécifier une valeur')";

    private const UML_TO_VALIDATOR = [
        self::UML_STRING => [
            self::PRE_DEF_VALIDATOR_NOT_NULL
        ],
        self::UML_DATE => [],
        self::UML_LONG => [],
        self::UML_CHAR => [
            self::PRE_DEF_VALIDATOR_NOT_NULL
        ],
        self::UML_BOOLEAN => [],
        self::UML_INT => [
            self::PRE_DEF_VALIDATOR_NUMBER
        ],
        self::UML_FLOAT => [
            self::PRE_DEF_VALIDATOR_NUMBER
        ],
        self::UML_DOUBLE => [
            self::PRE_DEF_VALIDATOR_NUMBER
        ],
    ];

    private const UML_TO_PHP = [
        self::UML_STRING => "string",
        self::UML_DATE => "Date",
        self::UML_LONG => "long",
        self::UML_CHAR => "string",
        self::UML_BOOLEAN => "bool",
        self::UML_INT => "int",
        self::UML_FLOAT => "float",
        self::UML_DOUBLE => "double",
    ];

    private const UML_TO_SQL = [
        self::UML_STRING => "text",
        self::UML_DATE => "date",
        self::UML_LONG => "DOUBLE",
        self::UML_CHAR => "varchar(256)",
        self::UML_BOOLEAN => "BOOLEAN",
        self::UML_INT => "INT",
        self::UML_FLOAT => "FLOAT",
        self::UML_DOUBLE => "DOUBLE",
    ];

    public static function umlToValidator(ClassEntityDef $def): array
    {
        // var_dump($def->nature());
        if ($def->nature() != ClassDef::NATURE_CLASS && $def->nature() != ClassDef::NATURE_ASSOCIATION_CLASS) {
            return self::UML_TO_VALIDATOR[$def->type()];
        }
        return [];
    }

    public static function umlToSql(ClassEntityDef $def): string
    {
        if ($def->nature() != ClassDef::NATURE_CLASS && $def->nature() != ClassDef::NATURE_ASSOCIATION_CLASS) {
            return self::UML_TO_SQL[$def->type()];
        }

        // var_dump($def->nature());
        return self::UML_TO_SQL[self::UML_CHAR];
    }

    public static function umlToPHP(ClassEntityDef $def): string
    {
        return $def->isTypeTable() ? self::UML_TO_PHP[str_replace("[]", "", $def->type())] . "[]" : self::UML_TO_PHP[$def->type()];
    }
}
