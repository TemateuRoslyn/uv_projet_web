<?php
namespace Library;

require_once('PDFModels/fpdf/fpdf.php');

class XMLPage extends \FPDF
{
    protected $dom = null;
    protected $position = 'C';
    protected $pageWidth = 0;
    protected $border = 0;

    const CELL_H = 10;
    const POS = 'C';

    public function loadXMLData($file)
    {
        $this->dom = new \DOMDocument('1.0');
        $this->dom->loadXML($file);

        $this->pageWidth = $this->GetPageWidth() - 15;
        $this->SetCreator("schoolengine.cm", true);
    }

    public function urlLogo()
    {
        return $this->dom->getElementsByTagName("logo")['0']->getAttribute("url");
    }

    public function Header()
    {
        $lignes = $this->getElementLignes("header");

        $this->SetFontSize(10);
        $this->writeLignes($lignes);

        $this->Image($this->urlLogo(), $this->pageWidth / 2 - 7, 7, 30);
        $this->Ln();
    }

    public function Body()
    {
        $this->SetFont('Arial', '', 14);
        $this->AddPage();

        $lignes = $this->getElementLignes("body");
        $this->Cell($this->pageWidth, 0, '', 'T', 1, 1);
            // $this->writeLn(2);
// test($lignes);
        $this->writeLignes($lignes);
        $this->writeLn(1);
    }

    public function Output($dest = '', $name = '', $isUTF8 = true)
    {
        $this->body();
        parent::Output($dest, $name, $isUTF8);
    }

    public function Footer()
    {
        $lignes = $this->getElementLignes("footer");

        $this->Cell($this->pageWidth, 0, '', 'T', 1, 1);
        $this->writeLignes($lignes);
    }

    protected function getElementLignes($name)
    {
        $bloc = $this->dom->getElementsByTagName("$name")[0];
        return $bloc->getElementsByTagName("ligne");
    }

    protected function writeLignes(\DOMNodeList $lignes)
    {
        foreach ($lignes as $ligne) {
            $cells = $ligne->getElementsByTagName('cell');
            $fraction = $this->pageWidth / $cells->length;

            $marginTop = 0;
            if ($ligne->hasAttribute("margin-top")) {
                $marginTop = (int)$ligne->getAttribute("margin-top");
            }
            $this->writeLn($marginTop);

            foreach ($cells as $cell) {
                $position = self::POS;
                if ($cell->hasAttribute("position")) {
                    $position = ucfirst(substr($cell->getAttribute("position"), 0, 1));
                }

                $this->Cell($fraction, self::CELL_H, $cell->nodeValue, $this->border, 0, $position);
            }
            $this->Ln();
        }
    }

    public function writeLn($nbre)
    {
        while ($nbre-- > 0) {
            $this->Ln();
        }
    }
}