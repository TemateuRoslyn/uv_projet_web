<?php
	namespace Library;

	class FileUpload extends PObject
	{
		protected $filename; //Correspond à l'attribut name du champ input du formulaire
		protected $errorMessage;
		protected $typeValidator;
		protected $isUploadValidator;
		protected $maxSizeValidator;

		const EXTENSIONS_VALIDES = array(
				"all"                   => array('png', 'jpg', 'docx', 'doc', 'pdf', 'mp3', 'mp4', 'txt'),
				"image"                 => array('png', 'jpg'),
				"oeuvre_literraire"     => array('docx', 'doc', 'pdf'),
				"article"               => array('docx', 'doc', 'pdf', 'txt'),
				"document_audio_visuel" => array('mp3', 'mp4'),
			);

		public function __construct ($errorMessage, $extension_valides_index, $filename, $maxSize = 17419965)
		{
			$this->setErrorMessage( $errorMessage );
			$this->setFilename ($filename);

			$this->typeValidator = new Validators\TypeValidator("Le type spécifier n'est pas valide", self::EXTENSIONS_VALIDES[$extension_valides_index]);	
			$this->isUploadValidator = new Validators\IsUploadValidator('Le fichier n\'a pas pu être téléchargé');
			
			$this->maxSizeValidator = new Validators\MaxSizeValidator('Fichier trop volumineux !', $maxSize);
		}

		public function isValid()
		{
			if ($this->fileExist()) 
			{
				// Vérification du type
				$type = $this->fileData('name');
				if (!$this->typeValidator->isValid($type))
				{
					$this->setErrorMessage($this->typeValidator);
					return false;
				}

				$size = $this->fileData('size');
				if (!$this->maxSizeValidator->isValid($size)) 
				{
					$this->setErrorMessage($this->maxSizeValidator);
					return false;
				}

				$upload = $this->fileData('tmp_name');
				if (!$this->isUploadValidator->isValid($upload)) 
				{
					$this->setErrorMessage($this->isUploadValidator);
					return false;
				}

				return true;	
			}
			return false;
		}

		public function fileData($key)
		{
			return isset($_FILES[$this->filename][$key]) ? $_FILES[$this->filename][$key] : null;
		}

		public function fileExist()
		{
			return isset($_FILES[$this->filename]);
		}

		public function setFilename($filename)
		{
			if (is_string($filename)) 
			{
				$this->filename = $filename;
			}
		}

		public function errorMessage()
		{
			return $this->errorMessage;
		}

		public function setErrorMessage($errorMessage)
		{
			if (is_string($errorMessage)) 
			{
				$this->errorMessage = $errorMessage;
			}
		}
	}