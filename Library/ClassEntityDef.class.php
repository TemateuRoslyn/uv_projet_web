<?php

namespace Library;

use Library\Entity;

class ClassEntityDef extends Entity
{
    protected $comment;
    protected $nature;
    protected $name;
    protected $type;
    protected $visibility;
    protected $defaultValue;

    public function isValid()
    {
        return !empty($this->name());
    }

    public function isNaturel()
    {
        return $this->nature != "Class" && $this->nature != "AssociationClass";
    }

    public function isTypeTable(): bool
    {
        return strpos($this->type, '[]') !== false;
    }

    public function haveDefaultValue(): bool
    {
        return !empty($this->defaultValue);
    }

    public function comment()
    {
        return $this->comment;
    }

    public function nature()
    {
        return $this->nature;
    }

    public function defaultValue()
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(string $value)
    {
        $this->defaultValue = $value;
    }

    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    public function visibility()
    {
        return $this->visibility;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function type()
    {
        return $this->type;
    }
}
