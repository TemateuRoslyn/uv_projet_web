<?php

namespace Library;

class EspaceUser extends PObject
{
	protected $managers = null;
	protected $user;
	protected $vars = array();

	public function __construct(Managers $managers, User $user)
	{
		$this->managers = $managers;
		$this->user = $user;
	}

	public function isFromSchool($idEtablissement)
	{
		if (!isset($this->vars["ets"])) {
			$profile = ucfirst($this->user->profile());

			if ($profile == "Eleve" || $profile == "Enseignant") {
				$has = "has$profile";
			} else {
				$has = "hasAdministrateur";
			}

			$this->vars['ets'] = $this->managers->getManagerOf("Etablissement")->$has($idEtablissement, $this->user->id());
		}

		return $this->vars['ets'];
	}

	public function containClasse($idClasse)
	{
		if (!isset($this->vars["classe$idClasse"])) {
			$this->vars["classe$idClasse"] = $this->managers->getManagerOf("Etablissement")->hasClasse($this->user->getAttribute("idEtablissement"), $idClasse);
		}

		return $this->vars["classe$idClasse"];
	}

	public function containEleve($idEleve)
	{
		if (!isset($this->vars["eleve$idEleve"])) {
			$this->vars["eleve$idEleve"] = $this->managers->getManagerOf("Etablissement")->hasEleve($this->user->getAttribute("idEtablissement"), $idEleve);
		}

		return $this->vars["eleve$idEleve"];
	}

	public function containEnseignant($idEnseignant)
	{
		if (!isset($this->vars["enseignant$idEnseignant"])) {
			$this->vars["enseignant$idEnseignant"] = $this->managers->getManagerOf("Etablissement")->hasEnseignant($this->user->getAttribute("idEtablissement"), $idEnseignant);
		}

		return $this->vars["enseignant$idEnseignant"];
	}
}
