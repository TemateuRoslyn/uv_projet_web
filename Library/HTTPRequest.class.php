<?php

namespace Library;

class HTTPRequest extends ApplicationComponent
{
	public function cookieData($key)
	{
		return isset($_COOKIE[$key]) ? $_COOKIE[$key] : null;
	}

	public function cookieExists($key)
	{
		return isset($_COOKIE[$key]);
	}

	public function setAttribute($name, $value)
	{
		$name = $this->validate_string_parameter($name);
		$_POST[$name] = $value;
	}

	public function getAttribute($name)
	{
		$name = $this->validate_string_parameter($name);
		return isset($_POST[$name]) ? $_POST[$name] : null;
	}

	public function getData($key)
	{
		return isset($_GET[$key]) ? $_GET[$key] : null;
	}

	public function getExists($key)
	{
		return isset($_GET[$key]);
	}

	public function method()
	{
		return $_SERVER['REQUEST_METHOD'];
	}

	public function postData($key)
	{
		return isset($_POST[$key]) ? $_POST[$key] : null;
	}

	public function postExists($key)
	{
		return isset($_POST[$key]);
	}

	public function getParam($name)
	{
		if ($this->postExists($name)) {
			return $this->postData($name);
		}

		return $this->getData($name);
	}

	public function getParams()
	{
		return array_map(function ($element) {
			return urldecode($element);
		}, array_merge($_GET, $_POST));
	}

	public function fileData($name, $key)
	{
		return isset($_FILES[$name][$key]) ? $_FILES[$name][$key] : null;
	}

	public function fileExists($name)
	{
		return isset($_FILES[$name]);
	}

	public function requestURI(): string
	{
		return $_SERVER['REQUEST_URI'];
	}

	public function getUpFile(string $name): UPFile
	{
		return new UPFile($name);
	}

	public function forward(BackController $parent, $url)
	{
		$controler = $this->app()->getControllerForURI($url);
		$controler->execute();

		$parent->setPage($controler->page());
	}

	public function getParamNames(): array
	{
		return array_keys($_GET);
	}

	public function postParamNames(): array
	{
		return array_keys($_POST);
	}

	public function paramNames(): array
	{
		return array_keys(array_merge($_POST, $_GET));
	}

	public function paramValues(): array
	{
		return array_merge($_POST, $_GET);
	}
}

class UPFile extends PObject
{
	protected $file = array();

	const EXT_JPG = "jpg";
	const EXT_JPGE = "jpeg";
	const EXT_GIF = "gif";
	const EXT_PNG = "png";
	const EXT_PDF = "pdf";
	const EXT_MP4 = "mp4";
	const EXT_MP3 = "mp3";

	const IMG_WIDTH = 0;
	const IMG_HEIGHT = 1;

	protected $nameAfterMove = "";

	public function __construct(string $name)
	{
		if (isset($_FILES[$name]) && !empty($_FILES[$name])) {
			$this->file = $_FILES[$name];
		} else {
			throw new \RuntimeException("Le fichier uploadé est inexistant");
		}
	}

	public function fileName(string $prefix = ""): string
	{
		return $prefix . $this->getAttribute("name");
	}

	public function fileUpName(): string
	{
		return $this->getAttribute("tmp_name");
	}

	public function fileSize(): int
	{
		return (int) $this->getAttribute("size");
	}

	public function hasTransfertError(): bool
	{
		$error = (int) $this->getAttribute("error");
		return $error > 0;
	}

	public function hasMissFileError(): bool
	{
		$error = (int) $this->getAttribute("error");
		return $error == UPLOAD_ERR_NO_FILE;
	}

	public function hasOverSizePHPAuthorizeError(): bool
	{
		$error = (int) $this->getAttribute("error");
		return $error == UPLOAD_ERR_INI_SIZE;
	}

	public function hasOverSizeFormAuthorizeError(): bool
	{
		$error = (int) $this->getAttribute("error");
		return $error == UPLOAD_ERR_FORM_SIZE;
	}

	public function hasPartialStateError(): bool
	{
		$error = (int) $this->getAttribute("error");
		return $error == UPLOAD_ERR_PARTIAL;
	}

	public function validateExtensions(array $extensions = array()): bool
	{
		$extension_upload = strtolower(substr(strrchr($this->fileName(), '.'), 1));
		return in_array($extension_upload, $extensions);
	}

	public function fileExtension(): string
	{
		return strtolower(substr(strrchr($this->fileName(), '.'), 1));
	}

	public function imageSizes(int $p): int
	{
		$image_sizes = getimagesize($this->fileUpName());
		if ($p == self::IMG_WIDTH) return $image_sizes[self::IMG_WIDTH];
		if ($p == self::IMG_HEIGHT) return $image_sizes[self::IMG_HEIGHT];
	}

	/**
	 * Déplace le fichier par défaut vers le repertoire /Web/ en le préfixant par un uniqid()_pideria
	 * @param string $path Répertoire à partir de /Web/ devant contenir le fichier
	 */
	public function moveFile(string $path = ""): bool
	{
		// chmod("/../Web/media_videos/", 0777);

		$this->nameAfterMove = $this->fileName(uniqid(md5(time())) . "_pideria");
		return move_uploaded_file($this->fileUpName(), dirname(__FILE__) . "/../Web/$path" . $this->nameAfterMove);
	}

	public function nameAfterMove(): string
	{
		return $this->nameAfterMove;
	}

	public function extension(): string
	{
		return strtolower(substr(strrchr($this->getAttribute("type"), '/'), 1));
	}

	public function type(): string
	{
		return strtolower(str_replace("/" . $this->extension(), "", $this->getAttribute("type")));
	}

	public function getAttribute(string $attr): string
	{
		if (!is_null($this->file) && isset($this->file[$attr])) {
			return $this->file[$attr];
		}

		return null;
	}
}
