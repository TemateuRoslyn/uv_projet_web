<?php

namespace Library;

abstract class Managers_XML extends Managers_api
{
	protected $file;
	protected $rep = "buildXml/";
	// Tableau de tableau de clef "nom de la classe fille" => "nom de l'attribut d'association fils - pere" 
	protected $childrenLink = array();

	public function __construct($api, $dao)
	{
		parent::__construct($api, $dao);
		// libxml_use_internal_errors(true);
		// if (is_null($dao->documentURI)) $this->loadFile();
		$this->loadFile();
		$this->dao->formatOutput = true;
		// var_dump($this);
	}

	protected function loadFile()
	{
		// $internalError = \libxml_use_internal_errors(true);
		$this->dao = new \DOMDocument("1.0", "UTF-8");
		$this->file = dirname(__FILE__) . "/../Web/" . $this->rep . $this->table . "s.xml";
		// var_dump($this->file);
		if (!file_exists($this->file)) {
			$root = $this->dao->createElement('root');
			$root->setAttribute("class", "\AppliLib\Entities\\" . ucfirst($this->table));

			$this->dao->appendChild($root);
			// $this->dao->save($this->file);
		}

		if (file_exists($this->file)) {
			try {
				$this->dao->load($this->file);
			} catch (\Exception $e) {
				$this->testVar('Une erreur de chargemment est survenue!');
			}
		}
		$GLOBALS[self::NAMESPACE_SINGLETON_XML][\ucfirst($this->table)] = $this;
	}

	protected function listeElements()
	{
		return $this->dao->getElementsByTagName(ucfirst($this->table));
	}

	public function deleteAssociation($attrib_value, string $attrib = self::ASSOCIAION_ID)
	{
	}

	protected function objectFirstHaveMultipleAttribsValues(array $attribsValues): array
	{
		$liste = $this->listeElements();
		$results = [];

		foreach ($liste as $element) {
			$elementMach = true;
			foreach ($attribsValues as $attrib => $value) {

				if ($element->getAttribute($attrib) != $value) {
					$elementMach = false;
					break;
				}
			}

			if ($elementMach) {
				$results[] = $this->object_get($element);
			}
		}

		return $results;
	}

	protected function objectsHaveAttribValue(string $attrib, string $value): array
	{
		$liste = $this->listeElements();
		$results = array();
		// var_dump($liste[0]);
		foreach ($liste as $admin) {
			if ($admin->getAttribute(trim($attrib)) == $value) {
				$results[] = $this->object_get($admin);
			}
		}

		return $results;
	}

	protected function objectFirstHaveAttribValue(string $attrib, string $value): Entity
	{
		$liste = $this->listeElements();
		foreach ($liste as $admin) {
			if ($admin->getAttribute($attrib) == $value) {
				return $this->object_get($admin);
			}
		}
		return null;
	}

	public function saveFile()
	{
		$this->dao->save($this->file);
	}

	public function getList($debut = -1, $limite = -1)
	{
		$liste = $this->listeElements();

		$debut = ($debut < 0) ? $debut = 0 : $debut;
		$limite = ($limite < 0) ? 0 : ($limite < count($liste)) ? $limite : count($liste);

		$result = array();
		for ($i = $debut; $i < $limite; $i++) {
			$result[] = $this->object_get($liste[$i]);
		}

		return $result;
	}

	// public function setAttribute($id, $property, $value)
	// {
	// 	$element = $this->dao->getElementById($id);

	// 	$element->setAttribute("feesValidated", 1);
	// }

	public function xmlRepresentationOfObject(Entity $object)
	{
		$object_var = $object->propertiesSave();
		$xml_entity = $this->dao->createElement($object->className());

		foreach ($object_var as $attr => $value) {
			if (!is_array($value)) {
				$element = $this->dao->createAttribute($attr);

				if ($attr == "dateModif" || $attr == "dateInsert") {
					$element->value = (new \DateTime())->format('d-m-Y H:i:s');
				} else {
					if ($value instanceof \DateTime) {
						$element->value = $value->format('d-m-Y H:i:s');
					} else {
						$element->value = str_replace(array("'", "&nbsp;"), array("<quote/>", ""), $value);
					}
				}

				$xml_entity->appendChild($element);
			}
		}

		return $xml_entity;
	}

	public function count()
	{
		return count($this->dao->getElementsByTagName($this->table));
	}

	public function getUnique($id)
	{
		if (($element = $this->dao->getElementById("_$id")) != null) {
			return $this->object_get($element);
		}

		return null;
	}

	public function save(Entity $entity)
	{
		if (!is_null($this->dao->getElementById("_" . $entity->id()))) {
			$id = $this->modify($entity);
			return $id;
		}

		return $this->add($entity);
	}

	protected function domEntityFromClass(Entity $document): \DOMElement
	{
		$xml_document = $this->xmlRepresentationOfObject($document);

		$attr_id = $this->dao->createAttribute('xml:id');
		$attr_id->value = "_" . $document->id();

		$xml_document->appendChild($attr_id);
		$xml_document->setIdAttribute('xml:id', true);

		$date = new \DateTime;
		$xml_document->setAttribute("dateModif", $date->format('d-m-Y H:i:s'));
		$xml_document->setAttribute("dateInsert", $date->format('d-m-Y H:i:s'));
		$xml_document->setAttribute("id", $document->id());

		return $xml_document;
	}

	public function saveList(array $liste)
	{
		foreach ($liste as $entity) {
			$domEntity = $this->domEntityFromClass($entity);
			$this->dao->firstChild->appendChild($domEntity);
		}

		// $this->dao->save($this->file);
	}

	public function add(Entity $document)
	{
		// $document->setExist(true);
		$xml_document = $this->domEntityFromClass($document);

		if ($this->dao->firstChild !== null) {
			$this->dao->firstChild->appendChild($xml_document);
		}
		// $this->dao->save($this->file);
	}

	public function modify(Entity $object)
	{
		$old_element = $this->dao->getElementById("_" . $object->id());
		$root = $this->dao->firstChild;

		$properties = $object->propertiesSave();
		foreach ($properties as $name => $value) {
			if (!is_null($value)) {
				if ($value instanceof \DateTime) {
					$old_element->setAttribute($name, $value->format('d-m-Y H:i:s'));
				} elseif (!is_array($value)) {
					$old_element->setAttribute($name, str_replace("'", "<quote/>", $value));
				}
			}
		}

		$date = new \DateTime;
		$old_element->setAttribute("dateModif", $date->format('d-m-Y H:i:s'));

		// $this->dao->save($this->file);
	}

	protected function changeAttribute($id, $attr, $value)
	{
		$old_element = $this->dao->getElementById("_$id");

		if (is_null($old_element)) {
			throw new \RuntimeException("L'élément spécifié est inexistant");
		}

		$old_element->setAttribute("$attr", str_replace("'", "<quote/>", $value));

		// $this->dao->save($this->file);
	}

	public function delete($id)
	{
		$root = $this->dao->getElementsByTagName("root")[0];

		$old = $this->dao->getElementById("_$id");
		if (!is_null($old)) $root->removeChild($old);

		foreach ($this->childrenLink as $link) {
			foreach ($link as $class => $attribName) {
				$children = $this->getManagerOf($class)->objectsHaveAttribValue($attribName, $id);
				// $this->testVar($children);
				foreach ($children as $child) {
					$this->getManagerOf($class)->delete($child->id());
				}
			}
		}

		// $this->dao->save($this->file);
	}

	public function object_get(\DOMElement $element): Entity
	{
		// var_dump($this->dao->documentURI);
		$class = "\AppliLib\Entities\\" . ucfirst($this->table);
		$attr_values = PObject::parseToArray(new $class);

		$attr_values["id"] = "";
		$attr_values["dateModif"] = "";
		$attr_values["dateInsert"] = "";

		$result = array();
		foreach ($attr_values as $attr => $value) {
			if (preg_match('/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $element->getAttribute($attr))) {
				$result[$attr] = new \DateTime($element->getAttribute($attr));
			} else
				$result[$attr] = $element->getAttribute($attr);
		}

		return new $class($result);
	}

	public function __destruct()
	{
	}

	public function addAssociations(array $entities, \Library\Entity $entity): void
	{
	}

	public function removeAssociations(array $entities, Entity $entity, bool $andEntity = false): void
	{
	}

	public function addAssociationClass(array $entitiesL, \Library\Entity $entity, array $entitiesR): void
	{
	}

	public function getAssociateTo(Entity $entity, string $mapGet = "", string $mapRestrictJoin = "", string $mapRestrictWhere = "", int $debut = -1, int $limit = -1): array
	{
		return [];
	}

	public function getAssociateClassTo(\Library\Entity $entity, int $debut = -1, int $limit = -1): array
	{
		return [];
	}
}
