<?php

namespace Library;

/**
 * Contrôle la création et la mise à jour des données globales du Supper Tableau $GLOBALS
 */
class GlobalVarsUtilitaryManager extends PObject
{
    const DEFAULT_NAMESPACE = "default_namespace";
    protected static $_PRIVATES_ATTRIBUTES = "5c388d1aee615";

    public static function getVarNamespace(string $namespace): array
    {
        if (!empty($namespace)) {
            if (!isset($GLOBALS[$namespace])) $GLOBALS[$namespace] = [];
            return $GLOBALS[$namespace];
        }
        throw new \RuntimeException("Namespace Vide Incorrect");
    }

    /**
     * Retourne null si l'élément n'a pas été trouvé.
     */
    public static function getVar(string $attribute, string $namespace = "")
    {
        if (empty($namespace)) $namespace = self::DEFAULT_NAMESPACE;
        $data = self::getVarNamespace($namespace);

        return (isset($data[$attribute])) ? $data[$attribute] : null;
    }

    private static function setVar(string $attribute, $valeur, string $namespace = "")
    {
        if (!self::isUniqueAttribute($attribute, $namespace)) {
            self::_setVar($attribute, $valeur, $namespace);
            return;
        }

        throw new \RuntimeException("L'attribut est unique et ne peut être modifiée.");
    }

    private static function _setVar(string $attribute, $valeur, string $namespace = "")
    {
        if (empty($namespace)) $namespace = self::DEFAULT_NAMESPACE;
        if (!isset($GLOBALS[$namespace])) $GLOBALS[$namespace] = [];

        $GLOBALS[$namespace][$attribute] = $valeur;
    }

    public static function setUniqueVar(string $attribute, $valeur, string $namespace = "")
    {
        $privatesSpace = self::getVarNamespace(self::$_PRIVATES_ATTRIBUTES);
        if (!isset($privatesSpace["$namespace::$attribute"])) {
            $GLOBALS[self::$_PRIVATES_ATTRIBUTES][$namespace . "::" . $attribute] = true;
            self::_setVar($attribute, $valeur, $namespace);
        }

        throw new \RuntimeException("L'attribute est unique dans le contexte.");
    }

    protected static function isUniqueAttribute(string $attribute, string $namespace = ""): bool
    {
        if (!isset($GLOBALS[self::$_PRIVATES_ATTRIBUTES])) $GLOBALS[self::$_PRIVATES_ATTRIBUTES] = [];
        return isset($GLOBALS[self::$_PRIVATES_ATTRIBUTES][$namespace . "::" . $attribute]);
    }

    public static function namespaceExist(string $namespace): bool
    {
        return !empty($namespace) && isset($GLOBALS[$namespace]);
    }

    public static function attributeExist(string $attribute, string $namespace = ""): bool
    {
        if (empty($namespace)) $namespace = self::DEFAULT_NAMESPACE;
        return self::namespaceExist($namespace) && isset($GLOBALS[$namespace][$attribute]);
    }
}
