<?php

namespace Library;

class EntityAssociation extends Entity
{
    const ASSOCIATION_SIGN = "assocSignature";
    const ASSOCIATION_ID = "assocId";

    /** @var EntityAssociationLink [] */
    protected $tables = [];
    protected $signature = "";

    public function isValid()
    {
        return !empty($tables);
    }

    /** @return EntityAssociationLink [] */
    public function tables()
    {
        return $this->tables;
    }

    public function setTables(array $tables)
    {
        $this->tables = $tables;
    }

    public function signature()
    {
        return $this->signature;
    }

    public function setSignature(string $signature)
    {
        $this->signature = $signature;
    }

    public function hasEntityAssociationLinkHasName(string $name): int
    {
        foreach ($this->tables as $link) {

            if ($link->table() == $name) {
                return $link->idTable();
            }
        }

        return 0;
    }

    public function isToEntity(Entity $entity)
    {
        $index = 0;

        foreach ($this->tables as $link) {
            // var_dump($this->tables);
            if ($link->isToEntity($entity)) {
                $this->collectEntityLink($entity);
                // $this->testVar($this->tables);
            }
        }

        return $index;
    }

    protected function collectEntityLink(Entity $entity)
    {
        foreach ($this->tables as $link) {
            if (!$link->isToEntity($entity)) {
                $data = $entity->getDescriptions();

                $link->addDescription(self::ASSOCIATION_SIGN, $this->signature());
                $link->addDescription(self::ASSOCIATION_ID, $this->id());
                $data[] = $link;

                $entity->setDescriptions($data);
            }
        }
    }

    // public function arrayDescriptions(): array
    // {
    //     $result = parent::arrayDescriptions();
    //     unset($result["tables"]);
    //     $nbre = 1;

    //     foreach ($this->tables as $table) {
    //         /** @var EntityAssociationLink $table */
    //         $tmp = $table->arrayDescriptions();
    //         foreach ($tmp as $tpN => $tpV) {
    //             $result[$tpN . $nbre] = $tpV;
    //         }
    //         $nbre++;
    //     }

    //     return $result;
    // }
}
