<?php
namespace Library\UmlModel;

use Library\Entity;

class UMLElementGroup extends UMLElement
{
    /**
     * @var UMLElement[]
     */
    protected $umlElements = array();

    public function addElement(UMLElement $element)
    {
        $this->umlElements[$element->id()] = $element;
    }
}