<?php

namespace Library\UmlModel;

use Library\Entity;

class UMLElement extends Entity
{
    protected $nature;
    protected $name;
    protected $type;
    protected $visibility;

    public function nature()
    {
        return $this->nature;
    }

    public function isValid()
    {
        return !empty($this->name);
    }

    public function setNature($nature)
    {
        $this->nature = $nature;
    }

    public function visibility()
    {
        return $this->visibility;
    }

    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function name()
    {
        return $this->name;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function type()
    {
        return $this->type;
    }
}
