<?php

namespace Library\UmlModel;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMXPath;
use Library\ClassDef;
use Library\ClassEntityDef;
use Library\ClassFunctionDef;
use Library\ClassLinkAssociation;
use Library\ClassPropertyDef;
use Library\Owner;
use Library\Writters\ClassEntityMaker;
use Library\Writters\FilesMakerInterface;
use Library\WrittersManager;

class PackageManager_UML extends DOMDocument implements FilesMakerInterface
{
    const ATTRIB_PARAM_INTOUT = "paramdirectioninout";
    const ATTRIB_PARAM_RETURN = "paramdirectionreturn";

    /**
     * @property DOMElement[]
     */
    protected $packageElements = array();
    /**
     * Tableau de clef l'ID du package
     * @property ClassDef[]
     */
    protected $classDefs = [];
    /**
     * Tableau de clef l'ID du package
     * @property ClassDef[]
     */
    protected $classDefsEntitiesType = [];

    public function __construct()
    {
        $this->load(__DIR__ . "/../../AppliLib/UMLSqueleton/" . WrittersManager::PATH_SCHEME_MAPPING_UML);
        $this->packageElements = $this->getElementsByTagName("packagedElement");
    }

    /**
     * @return ClassDef[]
     */
    public function getClassDefs()
    {
        if (empty($this->classDefsEntitiesType)) {
            $packageElements = $this->listAllClassPackage();
            $packageElements = array_merge($packageElements, $this->listAllAssociationClassPackage());

            $results = [];
            $resultsEntitiesType = [];
            foreach ($packageElements as $package) {
                $results[$package->getAttribute('xmi:id')] = $this->classDefFromPackage($package);
                if ($results[$package->getAttribute('xmi:id')]->type() == ClassDef::TYPE_CLASS_ENTITY) {
                    $resultsEntitiesType[$package->getAttribute('xmi:id')] = $results[$package->getAttribute('xmi:id')];
                }
            }

            $this->classDefs = $results;
            $this->classDefsEntitiesType = array_merge($results, $this->completeEntitiesDefClass($resultsEntitiesType, $results));
        }

        return $this->classDefsEntitiesType;
    }

    /**
     * @param ClassDef[] $defEntities
     * @param ClassDef[] $allDef
     * @return ClassDef[]
     */
    protected function completeEntitiesDefClass(array $defEntities, array $allDef): array
    {
        $results = [];
        $outils = [ClassDef::TYPE_CLASS_FORM_BUILDER, ClassDef::TYPE_CLASS_FORM_HEADER, ClassDef::TYPE_CLASS_MANAGERS_PDO, ClassDef::TYPE_CLASS_MANAGERS_XML];

        foreach ($defEntities as $dEntity) {
            foreach ($outils as $key) {
                $results[] = $this->outilForEntity($dEntity, $allDef, $key);
            }
        }

        return $results;
    }

    protected function outilForEntity(ClassDef $def, array $allDef, string $key): ClassDef
    {
        foreach ($allDef as $iDef) {
            if ($def->isOwnUtilitary($iDef, $key)) {
                return $iDef;
            }
        }

        return $this->createOutilForEntity($def, $key);
    }

    protected function createOutilForEntity(ClassDef $def, string $type): ClassDef
    {
        $namespace = ClassEntityMaker::HEAD_MA;
        if ($type == ClassDef::TYPE_CLASS_FORM_BUILDER) {
            $namespace = ClassEntityMaker::HEAD_FB;
        }

        if ($type == ClassDef::TYPE_CLASS_FORM_HEADER) {
            $namespace = ClassEntityMaker::HEAD_FH;
        }

        $defR = new ClassDef(array(
            "nature" => "",
            "comment" => "",
            "name" => $def->name() . $type,
            "type" => $type,
            "visibility" => "public",
            "parent" => $parent = null,
            "propertiesDef" => [],
            "functionsDef" => [],
            "namespace" => $namespace
        ));

        $defR->complementClassDef($def);
        return $defR;
    }

    /**
     * @return ClassLinkAssociation[]
     */
    public function classLinkAssociations()
    {
        $packageAssociations = $this->listElementsPackageHaveAttribValue("xmi:type", "uml:Association");
        $packageAssociations = array_merge($packageAssociations, $this->listElementsPackageHaveAttribValue("xmi:type", "uml:AssociationClass"));
        // var_dump($packageAssociations);
        $results = [];
        foreach ($packageAssociations as $package) {
            $results[] = $this->classLinkAssociationFromPackage($package);
        }

        return $results;
    }

    /**
     * @return ClassLinkAssociation
     */
    protected function classLinkAssociationFromPackage(DOMElement $package)
    {
        $link = new ClassLinkAssociation();
        $results = $this->associationOwneds($package);
        $associateClass = null;

        if ($package->getAttribute("xmi:type") == "uml:AssociationClass") {
            $associateClass = $this->classDefs[$package->getAttribute("xmi:id")];
        }

        return new ClassLinkAssociation([
            "signature" => $package->getAttribute("xmi:id"),
            "associationClass" => $associateClass,
            "ownerUp" => $results[0],
            "ownerDown" => $results[1],
        ]);
    }

    /**
     * @return Owner[]
     */
    protected function associationOwneds(DOMElement $package)
    {
        $results = [];
        $elements = $package->getElementsByTagName("ownedEnd");
        // var_dump(count($elements));
        foreach ($elements as $element) {
            $classRef = $this->classDefs[$element->getAttribute("type")];
            // var_dump($classRef->name());
            $upperValue = $element->getElementsByTagName("upperValue")[0];
            $lowerValue = $element->getElementsByTagName("lowerValue")[0];

            $results[] = new Owner([
                "classDef" => $classRef,
                "aggregation" => $element->getAttribute("aggregation"),
                "cardinaliteUp" => $upperValue->getAttribute("value"),
                "cardinaliteDown" => $lowerValue->getAttribute("value")
            ]);
        }

        return $results;
    }

    /**
     * @return ClassDef
     */
    public function classDefFromPackage(DOMElement $package)
    {
        $properties = $this->classAttribsFromPackage($package);
        $functions = $this->classFuncsFromPackage($package);

        $parentLink = $package->getElementsByTagName("generalization");
        $parent = null;

        foreach ($parentLink as $link) {
            $parent = $this->getElementById($link->getAttribute("general"));
        }

        return new ClassDef(array(
            "nature" => $package->getAttribute("isAbstract") == "true" ? "abstract" : "",
            "comment" => $this->elementComment($package),
            "name" => $package->getAttribute("name"),
            "type" => $package->parentNode != null ? $this->formatClassDefType($package->parentNode->getAttribute("name")) : $package->getAttribute("name"),
            "visibility" => $package->getAttribute("visibility"),
            "parent" => $parent != null ? $this->classDefFromPackage($parent) : null,
            "propertiesDef" => $properties,
            "functionsDef" => $functions,
            "namespace" => $package->parentNode != null ? $this->formatNamespace($package->parentNode->getAttribute("name")) : ClassEntityMaker::HEAD
        ));
    }

    protected function formatClassDefType($xmlNamespace): string
    {
        if (strtolower($xmlNamespace) == strtolower(ClassDef::NAMESPACE_MODELS)) {
            return ClassDef::TYPE_CLASS_MANAGERS_PDO;
        }

        if (strtolower($xmlNamespace) == strtolower(ClassDef::NAMESPACE_FORM_BUILDER)) {
            return ClassDef::TYPE_CLASS_FORM_BUILDER;
        }

        if (strtolower($xmlNamespace) == strtolower(ClassDef::NAMESPACE_FORM_HEADER)) {
            return ClassDef::TYPE_CLASS_FORM_HEADER;
        }

        return ClassDef::TYPE_CLASS_ENTITY;
    }

    protected function formatNamespace($xmlNamespace): string
    {
        if (strtolower($xmlNamespace) == strtolower(ClassDef::NAMESPACE_MODELS)) {
            return ClassEntityMaker::HEAD_MA;
        }

        if (strtolower($xmlNamespace) == strtolower(ClassDef::NAMESPACE_FORM_BUILDER)) {
            return ClassEntityMaker::HEAD_FB;
        }

        if (strtolower($xmlNamespace) == strtolower(ClassDef::NAMESPACE_FORM_HEADER)) {
            return ClassEntityMaker::HEAD_FH;
        }

        return ClassEntityMaker::HEAD;
    }

    protected function elementComment(DOMElement $package): string
    {
        $comment = "";
        if ($package->getElementsByTagName("ownedComment")->length) {
            $comment = $package->getElementsByTagName("ownedComment")[0];
            $comment = $package->getElementsByTagName("body")[0]->nodeValue;
            // var_dump($comment);
        }
        return $comment;
    }

    /**
     * @return ClassEntityDef[]
     */
    public function classAttribsFromPackage(DOMElement $package): array
    {
        $data = [];

        $ownedAttributes = $package->getElementsByTagName("ownedAttribute");
        foreach ($ownedAttributes as $attrib) {
            $elementType = $this->getElementById($attrib->getAttribute("type"));
            // var_dump($attrib->nodeName);
            $data[] = new ClassEntityDef([
                "defaultValue" => $this->defaultValueFromElementType($attrib),
                "comment" => $this->elementComment($elementType),
                "nature" => substr($elementType->getAttribute("xmi:type"), 4),
                "name" => $attrib->getAttribute("name"),
                "type" => $this->typeFromElementType($attrib),
                "visibility" => $attrib->getAttribute("visibility"),
            ]);
        }

        return $data;
    }

    protected function typeFromElementType(DOMElement $attrib): string
    {
        $typeName = $this->getElementById($attrib->getAttribute("type"))->getAttribute("name");
        if ($attrib->hasChildNodes()) {
            $upValueList = $attrib->getElementsByTagName("upperValue");
            // var_dump($elementType->getAttribute("xmi:id"));
            if ($upValueList->length != 0 && $upValueList[0]->getAttribute("value") == "*") {
                // var_dump($typeName);
                $typeName .= "[]";
            }
        }
        return $typeName;
    }

    protected function defaultValueFromElementType(DOMElement $attrib): string
    {
        if ($attrib->hasChildNodes()) {
            $upValueList = $attrib->getElementsByTagName("defaultValue");
            if ($upValueList->length != 0) {
                // var_dump($attrib->getAttribute("xmi:id"));
                return $upValueList[0]->getAttribute("value");
            }
        }
        return "";
    }

    /**
     * @return ClassFunctionDef[]
     */
    public function classFuncsFromPackage(DOMElement $package): array
    {
        $data = [];

        $ownedOperations = $package->getElementsByTagName("ownedOperation");
        foreach ($ownedOperations as $funct) {
            $elementType = $this->getElementById($funct->getAttribute("type"));
            $params = $this->getFunctParams($funct);
            // var_dump($attrib->nodeName);
            $data[] = new ClassFunctionDef([
                "nature" => "",
                "name" => $funct->getAttribute("name"),
                "returnType" => $params[self::ATTRIB_PARAM_RETURN],
                "visibility" => $funct->getAttribute("visibility"),
                "paramtersDecl" => $params[self::ATTRIB_PARAM_INTOUT],
            ]);
        }

        return $data;
    }

    public function getFunctParams(DOMElement $packageOperation): array
    {
        $params = [];
        $returnType = null;

        $ownedParameters = $packageOperation->getElementsByTagName("ownedParameter");
        foreach ($ownedParameters as $operation) {
            $type = $this->formatFunctParam($operation);
            if ($returnType[ClassFunctionDef::ATTRIB_NAME] == $packageOperation->getAttribute("name")) {
                $returnType = $type;
            } else {
                $params[] = $type;
            }
        }

        return [
            self::ATTRIB_PARAM_INTOUT => $params,
            self::ATTRIB_PARAM_RETURN => $returnType
        ];
    }

    public function formatFunctParam(DOMElement $element)
    {
        $name = $element->getAttribute("name");
        $elementTypeNode = $this->getElementById($element->getAttribute("type"));
        $elementType = $elementTypeNode->getAttribute("name");

        $elementNature = $elementTypeNode->getAttribute("xmi:type");
        $elementNature = substr($elementNature, 4);

        if ($element->getAttribute("direction") == "return" && $element->hasChildNodes()) {
            $first = $this->getFirstChildNotTextNode($element);

            if ($first != null && $first->getAttribute("xmi:type") == "uml:LiteralUnlimitedNatural") {
                $elementType = "$elementType\[]";
                $elementNature = "LiteralUnlimitedNatural";
            }
        }
        return [
            ClassFunctionDef::ATTRIB_NAME => $name,
            ClassFunctionDef::ATTRIB_TYPE => $elementType,
            ClassFunctionDef::ATTRIB_NATURE => $elementNature
        ];
    }

    /**
     * @return DOMElement[]
     */
    public function listAllClassPackage(): array
    {
        return  $this->listElementsPackageHaveAttribValue("xmi:type", "uml:Class");
    }

    /**
     * @return DOMElement[]
     */
    public function listAllAssociationClassPackage()
    {
        return  $this->listElementsPackageHaveAttribValue("xmi:type", "uml:AssociationClass");
    }

    /**
     * @return DOMElement[]
     */
    public function listElementsPackageHaveAttribValue(string $attrib, string $value): array
    {
        $data = [];
        foreach ($this->packageElements as $element) {
            // var_dump($attrib);
            // var_dump($element->getAttribute($attrib));
            if ($element->getAttribute($attrib) == $value) {
                $data[] = $element;
            }
        }

        return $data;
    }

    function getElementById($id)
    {
        $xpath = new DOMXPath($this);
        return $xpath->query("//*[@xmi:id='$id']")->item(0);
    }

    public function getFirstChildNotTextNode(DOMElement $element)
    {
        if ($element->hasChildNodes()) {
            foreach ($element->childNodes as $child) {
                if ($child->nodeType == XML_ELEMENT_NODE) {
                    return $child;
                }
            }
        }
        return null;
    }
}
