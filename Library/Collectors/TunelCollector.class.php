<?php

namespace Library\Collectors;

use Library\PObject;

class TunelCollector extends PObject
{
    protected static $file_data = [];

    /**
     * Ajoute un element dans une file précise, si la file n'existe pas, elle est créée
     */
    public static function addElement(string $key_file, $element): void
    {
        if (!isset(self::$file_data[$key_file])) {
            self::$file_data[$key_file] = [];
        }

        self::$file_data[$key_file][] = $element;
    }

    public static function getList(string $key_file): array
    {
        if (isset(self::$file_data[$key_file])) {
            return self::$file_data[$key_file];
        }
        return [];
    }
}
