<?php

namespace Library;

class Tools extends ApplicationComponent
{
    /** @property Tools[] */
    protected $tools = [];
    protected $action = "";
    protected $module = "";

    public function __construct(Application $app, string $module, string $action)
    {
        parent::__construct($app);
        $this->module = $module;
        $this->action = $action;
    }

    public function getToolsOf(string $action = "", string $module = "", string $app = ""): Tools
    {
        if (!is_string($module) || empty($module)) {
            throw new \InvalidArgumentException('Le module spécifié est invalide');
        }

        $app = empty($app) ? $this->app()->name() : $app;
        $action = empty($action) ? $this->action : $action;
        $module = empty($module) ? $this->module : $module;

        $namespace = "$app.$module.$action";
        if (!isset($this->tools[$namespace])) {
            $controlerClass = 'Applications\\' . $app . '\\Modules\\' . $module . '\\Tools\\' . $action . "Tools";
            $this->tools[$namespace] = new $controlerClass($this->app);
        }
        // $this->testVar($this->namespace);
        return $this->tools[$namespace];
    }
}
