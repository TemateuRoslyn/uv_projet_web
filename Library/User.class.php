<?php

namespace Library;

class User extends ApplicationComponent
{
	const TIME_LEAVE = 60 * 10;
	const PROFILES = array("delegue", "visitor", "intendant", "secretaire", "proviseur", "enseignant", "eleve", "censeur", "surveillant_g", "sup-admin", "adminService", "adminservice");

	public function __construct(\Library\Application $app)
	{
		parent::__construct($app);
		// $this->sessionControle();
	}

	public static final function sessionControle()
	{
		if (!isset($_SESSION["sessionControle.time"])) {
			$_SESSION["sessionControle.time"] = time();
		}

		if (time() - $_SESSION["sessionControle.time"] > User::TIME_LEAVE) {
			session_destroy();
			// $this->app->httpResponse()->redirect($this->app->httpRequest()->requestURI());
		}

		$_SESSION["sessionControle.time"] = time();
	}

	public function getAttribute($attr)
	{
		return isset($_SESSION[$attr]) ? $_SESSION[$attr] : null;
	}

	public function profile()
	{
		return (isset($_SESSION["profile"])) ? $_SESSION["profile"] : "visitor";
	}

	public function id()
	{
		if (!$this->hasProfile("visitor")) {
			return (isset($_SESSION["profile"])) ? (isset($_SESSION["id" . ucfirst($_SESSION["profile"])])) ?
				$_SESSION["id" . ucfirst($_SESSION["profile"])] : null : null;
		}

		return '';
	}

	public function adminAuthentificated()
	{
		return isset($_SESSION['admin_auth']) && $_SESSION['admin_auth'] === true;
	}

	public function setAdminAuthentificated($isAdmin = true)
	{

		if (!is_bool($isAdmin)) {
			throw new \InvalidArgumentException("La valeur spécifiée à la méthode User::setAuthentificated() doit être un boolean");
		}
		// $this->testVar("auth" . $isAdmin);
		$_SESSION["admin_auth"] = $isAdmin;
	}

	public function getFlash()
	{
		$flash = (isset($_SESSION['flash'])) ? $_SESSION['flash'] : '';;
		unset($_SESSION['flash']);

		return $flash;
	}

	public function hasFlash()
	{
		return isset($_SESSION['flash']);
	}

	public function isAuthentificated()
	{
		return isset($_SESSION['auth']) && $_SESSION['auth'] === true;
	}

	public function setAttribute($attr, $value)
	{
		$_SESSION[$attr] = $value;
	}

	public function setAuthentificated($authentificated = true)
	{
		if (!is_bool($authentificated)) {
			throw new \InvalidArgumentException("La valeur spécifiée à la méthode User::setAuthentificated() doit être un boolean");
		}

		$_SESSION['auth'] = $authentificated;
	}

	public function setFlash(string $value)
	{
		$_SESSION['flash'] = $value;
	}

	public function setProfile($profile)
	{
		// $profile = $this->validate_element_list(lcfirst($profile), self::PROFILES);
		// $this->testVar($profile);
		$_SESSION['profile'] = lcfirst($profile);
	}

	public function hasProfile($profile)
	{
		// $profile = $this->validate_element_list(strtolower($profile), self::PROFILES);
		$profile = lcfirst($profile);
		return isset($_SESSION["profile"]) ? strtolower($_SESSION["profile"]) == strtolower($profile) : false;
	}

	public function hasAttribute($attr)
	{
		return isset($_SESSION[$attr]);
	}

	public function moveAttribute($attr)
	{
		if ($this->hasAttribute($attr)) {
			unset($_SESSION[$attr]);
		}
	}

	public function moveAttributes()
	{
		unset($_SESSION);
	}

	public function attributes()
	{
		return $_SESSION;
	}
}
