<?php

namespace Library;


abstract class Entity extends ArrayEntity
{
	private $id;
	protected const SPACE_SAVE_SESSION = "pideria_entity_update_manager";
	private $spaceEntity;
	private $isToModify = false;

	public function hydrate(array $donnees)
	{
		// throw new \Exception("Error Processing Request", 1);
		foreach ($donnees as $attribut => $valeur) {
			$method = 'set' . ucfirst($attribut);
			if (is_callable(array($this, $method)) && !is_null($valeur)) {
				// var_dump($attribut);
				// var_dump(preg_match('/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $valeur));
				if (is_string($valeur) && (preg_match('/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $valeur) ||
					preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $valeur))) {
					// var_dump($valeur);
					$valeur = new \DateTime($valeur);
					// var_dump($valeur);
				}
				$this->$method($valeur);
			} else if (is_string($valeur)) {
				$this->addDescription($attribut, $valeur);
			}
		}
	}

	//Liste des getters
	public function id()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
		$this->spaceEntity = self::SPACE_SAVE_SESSION . $this->className() . $id;
	}

	public function getParentID(string $parent): int
	{
		if ($this->modifierDeclarationExist()) {
			// var_dump("klkslkds");
			// var_dump($_SESSION);
			return $_SESSION[$this->spaceEntity]["id" . ucfirst($parent)];
		}
		return $this->_getParentID($parent);
	}

	protected function _getParentID(string $parent): int
	{
		$id = "id" . ucfirst($parent);
		// var_dump($this->getDescriptions());
		if (property_exists($this, $id)) {
			return $this->$id;
		} else if (isset($this->getDescriptions()[$id])) {
			return $this->getDescriptions()[$id];
		}
		return 0;
	}

	public function prepareToModify(bool $actualize = false)
	{
		if (empty($this->id)) {
			throw new \RuntimeException("Aucun identifiant définit pour la modification de l'entité");
		}

		$parents = $this->classListParent();
		foreach ($parents as $parentT) {
			$parent = str_replace("AppliLib\Entities\\", "", $parentT);
			if (get_class($this) != $parentT && $this->getParentID($parent) == 0) {
				throw new \RuntimeException("Aucun identifiant parent: " . $parent . " définit pour la modification de l'entité");
			}
		}

		if (!$this->modifierDeclarationExist() && (!$this->isToModify || $actualize)) {
			// var_dump("klkslkds");
			$_SESSION[$this->spaceEntity] = $this->_listParentIDs();
			$this->isToModify = true;
		}
	}

	protected function modifierDeclarationExist(): bool
	{
		return isset($_SESSION[$this->spaceEntity]);
	}

	public function listParentIDs(): array
	{
		if ($this->modifierDeclarationExist()) {
			return $_SESSION[$this->spaceEntity];
		}
		return $this->_listParentIDs();
	}

	protected function _listParentIDs(): array
	{
		$classNames = $this->classListParent();
		$result = [];
		// var_dump($this);
		foreach ($classNames as $name) {
			$result["id" . ucfirst($this->classNameFrom($name))] = $this->getParentID($this->classNameFrom($name));
		}

		return $result;
	}

	public function hasXmlModel(): bool
	{
		$appConfig = Application::loadAppConfig();
		$models = $appConfig->modelsConfig;

		if (!is_null($models) && $models->duplication == "XML") return true;
		return false;
	}

	public function xmlModel(): Entity
	{
		if ($this->hasXmlModel()) {
			return $this;
		}

		throw new \RuntimeException("Model d'interface XML non définie !");
	}

	public function isNew()
	{
		// $this->testVar($this);
		return (int) $this->id < 1;
	}

	public function setExist($exist = true)
	{
		$this->setBool("exist", $exist);
	}

	public function parentObjectVar(): array
	{
		$class = \get_parent_class($this);
		$properties = get_class_vars($class);

		$result = [];
		foreach (array_keys($properties) as $key) {
			$result[$key] = $this->$key();
		}

		return $result;
	}

	public function parentObject(): Entity
	{
		$class = \get_parent_class($this);
		return new $class(get_object_vars($this));
	}

	public function propertiesSave()
	{
		$listeProperties = $this->propertiesMapGeneralization();
		return array_shift($listeProperties);
	}

	/**
	 * Tableau d'identifiant le nom d'une entité puis de ses parents
	 * et de valeurs leurs propriétés définies
	 */
	public function propertiesMapGeneralization(): array
	{
		$classes = $this->classListParent();
		$current_properties = $this->properties();
		$result = [];

		foreach ($classes as $name) {

			$data = [];
			$class_properties = array_keys(get_class_vars($name));
			// var_dump($class_properties);
			foreach ($class_properties as $key) {
				if ($key != "id" && key_exists($key, $current_properties)) {
					$data[$key] = $current_properties[$key];
					unset($current_properties[$key]);
				}
			}

			$result[$this->classNameFrom($name)] = $data;
		}

		// $result[$this->classNameFrom($current_class)] = $current_properties;
		return $result;
	}

	protected function propertiesEscape(string $property)
	{
		$data = ['id', 'erreurs', 'descriptions', 'dateModif'];
		return in_array($property, $data);
	}

	/** 
	 * Liste des classes à partir de l'object courant
	 */
	public function classListParent($entity_class = ""): array
	{
		$entity_class = $entity_class == "" ? get_class($this) : $entity_class;
		$result = [$entity_class];

		if (!$this->isClassEntity(get_parent_class($entity_class))) {
			$result = array_merge($this->classListParent(get_parent_class($entity_class)), $result);
		}

		return $result;
	}

	// public function propertiesSave()
	// {
	// 	$class = \get_class($this);
	// 	$entityCouche = new $class(get_object_vars($this));

	// 	$properties = $entityCouche->properties();
	// 	return $properties;
	// }

	public function arrayDescriptions(): array
	{
		$data = $this->properties();
		// $this->testVar($this);
		$data["descriptions"] = $this->getDescriptions();
		$data["dateInsert"] = $this->dateInsert();
		$data["dateModif"] = $this->dateModif();
		$data["erreurs"] = $this->erreurs();
		$data["id"] = $this->id;
		// $this->testVar($data);
		return $data;
	}

	public function formatName(): string
	{
		return "";
	}

	public function hasParentToMap(): bool
	{
		return get_parent_class($this) != "Library\\Entity";
	}

	public final function isClassEntity(string $class): bool
	{
		return $class == "Library\\Entity";
	}
	// public function name(): string
	// {
	// 	return str_replace("\AppliLib\Entities\\", "", get_class($this));
	// }
	public abstract function isValid();
}

class Entity_PDO extends PObject
{
	const PREFIX_ASSOCIATION = "idAssociation";
	const SENS_UP = "Up";
	const SENS_DOWN = "Down";
	const NEXT_SENS_TABLE = ["Up" => "Down", "Down" => "Up"];

	/**
	 * @property Entity
	 */
	protected $entity = null;
	protected $entityPropertiesMapGeneralization = array();
	protected $tableAxe = "";

	public function __construct(Entity $entity)
	{
		$this->entity =	$entity;
	}

	public function sqlPrepareMSGAdd(Entity $entity)
	{
	}
	/**
	 * Tableau de requêtes d'insertion des entités à partir du père le plus en dessous de la classe Entity
	 * jusqu'au fils
	 */
	public function entityListSqlSave(): array
	{
		$listProperties = $this->entityPropertiesMapGeneralization();
		// $listProperties = array_reverse($listProperties);
		// $this->testVar($listProperties);
		$result = [];
		foreach ($listProperties as $classeName => $properties) {
			$id = $this->entity->getParentID($classeName) == 0 ? $this->entity->id() : $this->entity->getParentID($classeName);
			if ($id) {
				$message = 'UPDATE ' . strtolower($classeName) . ' SET ';
				$message .= $this->sqlForArrayData($properties);
				// $this->testVar($this->entity);
				$message .= 'dateModif = NOW()';
				$message .= " WHERE id = $id";
			} else {
				$message = 'INSERT INTO ' . strtolower($classeName) . ' SET ';
				$message .= $this->sqlForArrayData($properties);

				$message .= 'dateModif = NOW(), ';
				$message .= 'dateInsert = NOW()';
			}
			$result["$classeName"] = [$message, $id];
		}

		return $result;
	}

	/**
	 * Tableau de requêtes d'insertion des entités à partir du père le plus en dessous de la classe Entity
	 * jusqu'au fils
	 */
	public function entityListSqlInsert(): array
	{
		$listProperties = $this->entityPropertiesMapGeneralization();
		// $listProperties = array_reverse($listProperties);
		// $this->testVar($listProperties);
		$result = [];
		foreach ($listProperties as $classeName => $properties) {
			$message = 'INSERT INTO ' . strtolower($classeName) . ' SET ';
			$message .= $this->sqlForArrayData($properties);

			$message .= 'dateModif = NOW(), ';
			$message .= 'dateInsert = NOW()';
			$result[$classeName] = $message;
		}

		return $result;
	}
	/**
	 * Tableau de requêtes de modification des entités à partir du père le plus en dessous de la classe Entity
	 * jusqu'au fils
	 */
	public function entityListSqlModify(): array
	{
		$listProperties = $this->entityPropertiesMapGeneralization();
		// $listProperties = array_reverse($listProperties);
		// $this->testVar($listProperties);
		$result = [];
		foreach ($listProperties as $classeName => $properties) {
			$message = 'UPDATE ' . strtolower($classeName) . ' SET ';
			$message .= $this->sqlForArrayData($properties);
			$id = $this->entity->getParentID($classeName) == 0 ? $this->entity->id() : $this->entity->getParentID($classeName);
			// $this->testVar($this->entity);
			$message .= 'dateModif = NOW()';
			$message .= " WHERE id = $id";
			$result[$classeName] = $message;
		}

		return $result;
	}

	public function entityPropertiesMapGeneralization(): array
	{
		if (empty($this->entityPropertiesMapGeneralization)) {
			$this->entityPropertiesMapGeneralization = $this->entity->propertiesMapGeneralization();
		}

		return $this->entityPropertiesMapGeneralization;
	}

	protected function sqlForArrayData(array $donnees): string
	{
		$keys = array_keys($donnees);
		$message = "";

		foreach ($keys as $key) {
			if (!is_null($donnees[$key])) {
				$message .= "$key =:$key, ";
			}
		}

		return $message;
	}

	public function sqlAttribsSelect(): string
	{
		if (!$this->entity->hasParentToMap()) {
			return $this->sqlAttribsSelectUnique();
		}
		// echo ($this->sqlAttribsSelectHierachic());
		return $this->sqlAttribsSelectHierachic();
	}

	protected function sqlAttribsSelectHierachic(): string
	{
		$table = strtolower($this->entity->className());
		$sqlAdd = "";
		if (!$this->entity->isNew()) {
			$sqlAdd = " WHERE $table.id = " . $this->entity->id();
		}
		return $this->sqlAttribSelectJoin() . $sqlAdd;
	}

	/**
	 * @param string $completeSqlSelect Toujours mettre un "," en fin pour délimiter les attributs ajoutés
	 */
	public function sqlAttribSelectJoin(string $completeSqlSelect = ""): string
	{
		$table = strtolower($this->entity->className());
		$result = "SELECT $table.id, ";

		$result .= $this->sqlAttribsEntity() . "$completeSqlSelect $table.dateInsert, $table.dateModif ";
		if ($this->entity->hasParentToMap()) {
			$result .= $this->sqlAttribsEntityGeneralizationJoins();
		} else {
			$result .= " FROM $table ";
		}

		return $result;
	}

	public function sqlOnlyAttribsSelectJoinTable(string $table): string
	{
		$class = "\AppliLib\Entities\\" . ucfirst($table);
		$entity_PDO = new Entity_PDO(new $class());

		return $entity_PDO->sqlAttribsEntityStrictTable($table);
	}

	/**
	 * @param string $completeSqlSelect Toujours mettre un "," en fin pour délimiter les attributs ajoutés
	 */
	public function sqlOnlyAttribSelectJoin(string $completeSqlSelect = ""): string
	{
		$result = "";

		$result .= $this->sqlAttribsEntity() . "$completeSqlSelect ";
		if ($this->entity->hasParentToMap()) {
			$result .= $this->sqlAttribsEntityGeneralizationJoins();
		}

		return $result;
	}

	public function sqlSelectAssociateList(Entity $entity, string $mapGet = "", string $restrictJoin = "", string $restrictWhere = "", int $debut = -1, int $limit = -1): string
	{
		$prefix = uniqid($entity->className());
		$mapGet .= " " . $prefix . ".id id" . ucfirst($entity->className()) . ", ";

		$sql = $this->sqlAttribSelectJoin(" $mapGet _pio.idTableUp " . self::PREFIX_ASSOCIATION . $this->entity->className() . ", ");
		$sql .= " $restrictJoin JOIN _pideria_inner_owner _pio 
                ON _pio.tableUp = '" . ucfirst($entity->className()) . "'
				AND _pio.tableDown = '" . ucfirst($this->entity->className()) . "'
                " . ($entity->isNew() ? "" : " AND _pio.idTableUp = " . $entity->id()) . "
				AND _pio.idTableDown = " . strtolower($this->entity->className()) . ".id";

		$sql .= " JOIN " . strtolower($entity->className()) . " $prefix  ON $prefix" . ".id = _pio.idTableUp";
		$sqlWHERE = $this->sqlWhereEntity($entity, $prefix);

		$sql .= empty($sqlWHERE) ? "" : " WHERE $sqlWHERE ";
		$sql .= empty($restrictWhere) ? "" : " $restrictWhere ";

		if ($debut != -1 || $limit != -1) {
			$sql .= " LIMIT $debut, $limit ";
		}
		// echo $sql;
		// $this->testVar("");
		return $sql;
	}

	protected function sqlWhereEntity(Entity $entity, string $prefix = ""): string
	{
		$data = $entity->propertiesStrict();
		$data = array_filter($data, function ($element) {
			return !empty($element);
		});

		$result = "";
		$prefix = empty($prefix) ? lcfirst($entity->className()) : $prefix;

		$pos = 0;
		$length = count($data);
		foreach ($data as $attrib => $value) {
			$result .= $prefix . "." . $attrib . " = " . $value;
			if (++$pos < $length) $result .= " AND ";
		}

		return $result;
	}

	public function sqlSelectAssociateListJoinScheme(Entity $entity, string $entitySens, array $scheme, int $debut = -1, int $limit = -1): string
	{
		$sql = $joinLink = "";
		$links = array_keys($scheme);

		$tableFirst = array_shift($links);
		$tableFirstSens = array_shift($scheme);
		// var_dump($links);
		$joinLink =  $this->sqlOnlyAttribsSelectJoinTable($tableFirst);
		$sql .= " JOIN _pideria_inner_owner _pio 
                ON _pio.table" . $tableFirstSens . " = '" . ucfirst($tableFirst) . "'
				AND _pio.table" . self::NEXT_SENS_TABLE[$tableFirstSens] . " = '" . ucfirst($this->entity->className()) . "'
				AND _pio.idTable" . self::NEXT_SENS_TABLE[$tableFirstSens] . " = " . strtolower($this->entity->className()) . ".id ";

		$pioSpeudo = "_pio";
		$sql .= " JOIN " . strtolower($tableFirst) . " ON " . strtolower($tableFirst) . ".id = $pioSpeudo.idTable$tableFirstSens";
		foreach ($scheme as $tableNext => $sens) {

			$readSens = $tableFirst . $tableNext . $sens;
			$newSign = "_pio$readSens";
			$joinLink .= $this->sqlOnlyAttribsSelectJoinTable($tableNext);

			$sql .= " JOIN _pideria_inner_owner $newSign
			ON $newSign.table" . $sens . " = '" . strtolower($tableNext) . "'
			AND $newSign.table" . self::NEXT_SENS_TABLE[$sens] . " = $pioSpeudo.table" . self::NEXT_SENS_TABLE[$sens] . "
			AND $newSign.idTable" . self::NEXT_SENS_TABLE[$sens] . " = $pioSpeudo.idTable" . self::NEXT_SENS_TABLE[$sens];
			$sql .= " JOIN " . strtolower($tableNext) . " ON " . strtolower($tableNext) . ".id = $newSign.idTable$sens";

			$pioSpeudo = $newSign;
			$tableFirst = $tableNext;
		}

		$tableLast = $entity->className();
		$newSign = "_pio$tableLast";

		$sql .= " JOIN _pideria_inner_owner $newSign
			ON $newSign.table" . $entitySens . " = '" . ucfirst($tableLast) . "'
			AND $newSign.idTable" . $entitySens . " = " . $entity->id() . "
			AND $newSign.table" . self::NEXT_SENS_TABLE[$entitySens] . " = $pioSpeudo.table" . self::NEXT_SENS_TABLE[$entitySens] . "
			AND $newSign.idTable" . self::NEXT_SENS_TABLE[$entitySens] . " = $pioSpeudo.idTable" . self::NEXT_SENS_TABLE[$entitySens];

		if ($debut != -1 || $limit != -1) {
			$sql .= " LIMIT $debut, $limit ";
		}

		return $this->sqlAttribSelectJoin($joinLink) . $sql;
	}

	public function sqlSelectAssociateListMultipleLinks(Entity $entity, array $links, int $debut = -1, int $limit = -1): string
	{
		$sql = $joinLink = "";
		$firstLink = array_pop($links);
		$tableDown = substr(strrchr($firstLink, '_'), 1);
		$tableUp = str_replace("_" . $tableDown, "", $firstLink);

		$joinLink =  " _pio.idTableUp id" . ucfirst($tableUp) . ", ";
		$sql .= " JOIN _pideria_inner_owner _pio 
                ON _pio.tableUp = '$tableUp'
				AND _pio.tableDown = '" . ucfirst($this->entity->className()) . "'
				AND _pio.idTableDown = " . strtolower($this->entity->className()) . ".id";

		$pioSpeudo = "_pio";
		$firstLink = array_shift($links);

		foreach (array_reverse($links) as $readSens) {
			$tableDown = substr(strrchr($readSens, '_'), 1);
			$tableUp = str_replace("_" . $tableDown, "", $readSens);
			$newSign = "_pio$readSens";
			$joinLink .=  " $newSign.idTableUp id" . ucfirst($tableUp) . ", ";

			$sql .= " JOIN _pideria_inner_owner $newSign
			ON $newSign.tableUp = '$tableUp'
			AND $newSign.tableDown = $pioSpeudo.tableUp
			AND $newSign.idTableDown = $pioSpeudo.idTableUp";

			$pioSpeudo = $newSign;
		}

		$newSign = "_pio$firstLink";
		$tableDown = substr(strrchr($firstLink, '_'), 1);
		$tableUp = str_replace("_" . $tableDown, "", $firstLink);

		$joinLink .=  " $newSign.idTableUp id" . ucfirst($tableUp) . ", ";
		$sql .= " JOIN _pideria_inner_owner $newSign
			ON $newSign.tableUp = $pioSpeudo.tableUp
			AND $newSign.tableDown = '$tableDown'
			AND $newSign.idTableDown = " . $entity->id();

		if ($debut != -1 || $limit != -1) {
			$sql .= " LIMIT $debut, $limit ";
		}
		// echo ($sql);
		return $this->sqlAttribSelectJoin($joinLink) . $sql;
	}

	public function sqlAssociateListClass(Entity $entity, int $debut = -1, int $limit = -1)
	{
		$sql = $this->sqlAttribSelectJoin(" _pioc.nameTable tableAssociation, _pioc.idRegister " . self::PREFIX_ASSOCIATION . ", ");
		$sql .= " JOIN _pideria_inner_owner _pio 
				 ON _pio.tableDown = '" . $this->entity->className() . "' 
				 AND _pio.idTableDown = " . $this->entity->className() . ".id
				 JOIN _pideria_inner_owner_class _pioc
				 ON _pioc.signature_pideria_inner_owner = _pio.signature
				 JOIN _pideria_inner_owner _pioa
				 ON _pioa.tableUp = _pioc.nameTable
				 AND _pioa.idTableUp = _pioc.idRegister
				 WHERE _pioa.tableDown = '" . $entity->className() . "'
				 AND _pioa.idTableDown = " . $entity->id();

		if ($debut != -1 || $limit != -1) {
			$sql .= " LIMIT $debut, $limit ";
		}

		return $sql;
	}

	protected function sqlAttribsSelectUnique(): string
	{
		$table = lcfirst($this->entity->className());

		$id = (int) $this->entity->id();
		$sql = "SELECT *, id id" . ucfirst($table) . " FROM $table ";
		if ($id > 0) {
			$sql .= " WHERE id = $id";
		}
		// var_dump($sql);
		// var_dump($this->entity);
		return $sql;
	}

	protected final function sqlAttribsEntity(): string
	{
		$entitiesProperties = $this->entityPropertiesMapGeneralization();
		$message = "";

		foreach ($entitiesProperties as $name => $properties) {
			$attribs = array_keys($properties);
			foreach ($attribs as $attrib) {
				$message .= strtolower($name) . ".$attrib,";
			}

			$message .= " " . strtolower($name) . ".id id" . ucfirst($name) . ", ";
		}

		return $message;
	}

	protected final function sqlAttribsEntityStrictTable(string $table): string
	{
		$entitiesProperties = $this->entityPropertiesMapGeneralization();
		$message = "";

		$attribs = array_keys($entitiesProperties[$table]);
		foreach ($attribs as $attrib) {
			$message .= strtolower($table) . ".$attrib,";
		}

		$message .= " " . strtolower($table) . ".id id" . ucfirst($table) . ", ";

		return $message;
	}

	protected final function sqlAttribsEntityGeneralizationJoins(): string
	{
		$listClasses = array_reverse($this->entity->classListParent());
		// var_dump($listClasses);
		$current_class_name = strtolower($this->classNameFrom(array_shift($listClasses)));
		$current_class_parent_name = strtolower($this->classNameFrom(array_shift($listClasses)));

		$message = " FROM $current_class_name JOIN _pideria_generalisation _pid_gen ON _pid_gen.signature = '" . $current_class_parent_name . "_" . $current_class_name . "'";
		$message .= " JOIN _pideria_inner_generalisation _pid_in_gen ON _pid_in_gen.id_pideria_generalisation = _pid_gen.id ";
		$message .= " JOIN $current_class_parent_name ON $current_class_parent_name" . ".id = _pid_in_gen.idTableParent AND _pid_in_gen.idTable = $current_class_name" . ".id";

		$current_class_name = $current_class_parent_name;
		foreach ($listClasses as $name) {
			$current_class_parent_name = strtolower($this->classNameFrom($name));

			$message .= " JOIN _pideria_generalisation _pid_gen$current_class_name ON _pid_gen$current_class_name.signature = '" . $current_class_parent_name . "_" . $current_class_name . "'";
			$message .= " JOIN _pideria_inner_generalisation _pid_in_gen$current_class_name ON _pid_in_gen$current_class_name.id_pideria_generalisation = _pid_gen$current_class_name.id ";
			$message .= " JOIN $current_class_parent_name ON $current_class_parent_name" . ".id = _pid_in_gen$current_class_name.idTableParent AND _pid_in_gen$current_class_name.idTable = $current_class_name" . ".id";

			$current_class_name = $current_class_parent_name;
		}

		return $message;
	}

	public static function sqlDataSaveParamMultiple(Entity $entity): string
	{
		$data = $entity->propertiesSave();
		$length = count($data);

		$nbre = 0;
		$message = "(NULL,";

		foreach ($data as $value) {
			$message .= "'" . $value . "'";
			if ($length != ++$nbre) $message .= ",";
		}

		// $messageKeys .= ", dateInsert, dateModif()";
		return $message . ")";
	}

	public static function sqlDuplicate(string $entityName): string
	{
		$class = "\AppliLib\Entities\\$entityName";
		$entity = new $class();

		$data = $entity->propertiesSave();
		$length = count($data);

		$nbre = 0;
		$messageKeys = " ON DUPLICATE KEY UPDATE ";

		foreach (array_keys($data) as $key) {

			$messageKeys .= "`$key`=VALUES(`$key`)";
			if ($length != ++$nbre) $messageKeys .= ",";
		}

		// $messageKeys .= ", dateInsert, dateModif()";
		return $messageKeys;
	}

	public static function sqlKeysSave(string $entityName): string
	{
		$class = "\AppliLib\Entities\\$entityName";
		$entity = new $class();

		$data = $entity->propertiesSave();
		$length = count($data);

		$nbre = 0;
		$messageKeys = "(`id`,";

		foreach (array_keys($data) as $key) {

			$messageKeys .= "`$key`";
			// $keysSave[] = $key;
			if ($length != ++$nbre) $messageKeys .= ",";
		}

		// $messageKeys .= ", dateInsert, dateModif()";
		return $messageKeys . ")";
	}

	public function setTableAxe(string $tableAxe)
	{
		$this->tableAxe = $tableAxe;
	}
}
