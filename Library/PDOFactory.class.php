<?php

namespace Library;

class PDOFactory extends PObject
{
	protected $structures = array();

	public function __construct()
	{
		$this->initialise();
	}

	/**
	 * @return \PDO
	 */
	public static function getMysqlConnexion()
	{
		// return;
		$db = new \PDO('mysql:host=localhost;dbname=school_engine', 'root', '');
		$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		return $db;
	}

	public function newEntities(array $db_entities)
	{
		$tables = array();
		$dossier = dir(realpath(__DIR__ . '\Entities\XML\\'));
		while (false !== ($entree = $dossier->read())) {
			if (is_file(__DIR__ . '\Entities\DAO\\' . $entree)) {
				$tables[] = lcfirst(str_replace('.class.php', '', $entree));
			}
		}

		$dossier->close();

		$db_new_entities = array();
		$bdd = self::getMysqlConnexion();

		foreach ($db_entities as $name) {
			if (!in_array($name, $tables)) {
				$db_new_entities[] = $name;
			}
		}

		return $db_new_entities;
	}

	public function actualiseEntities(array $entities)
	{
		$entities = $this->newEntities($entities);
		foreach ($entities as $name) {
			$this->makeFile(ucfirst($name), '\Library\Entity');
		}

		//Novelle entité
		// $this->makeFile(ucfirst('cours'), '\Library\Entity');
		// =======================================================================
	}

	public function initialise()
	{
		$bdd = self::getMysqlConnexion();
		try {
			$tables = array();
			$result = $bdd->query("SHOW TABLES");
			while ($row = $result->fetch(\PDO::FETCH_NUM)) {
				$tables[] = $row[0];
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
		// $this->testVar($tables);

		$this->actualiseEntities($tables);

		foreach ($tables as $name) {
			$table = array();
			$table['name'] = $name;

			$properties = array();
			$reponse = $bdd->query("SELECT * FROM $name LIMIT 1,1");

			$nbrColumn = $reponse->columnCount();
			for ($i = 0; $i < $nbrColumn; $i++) {
				$properties[] = $reponse->getColumnMeta($i)['name'];
			}

			$table['properties'] = $properties;
			$this->structures[$name]  = $table;

			$reponse->closeCursor();
		}
	}

	public function parseToCompatible(Entity $entity)
	{
		$entityName = $entity->className();

		$validKeys = $this->getStructure($entityName)['properties'];
		if (!is_null($validKeys)) {
			$listePropertiesValues = PObject::parseToArray($entity);
			$result = array();
			foreach ($validKeys as $key) {
				if (array_key_exists($key, $listePropertiesValues)) {
					$result[$key] = $listePropertiesValues[$key];
				}
			}
			return $result;
		}
		throw new \RuntimeException("Aucune table ne correspond à l'entité '$entityName'");
	}

	public function getStructure($table)
	{
		if (is_string($table)) {
			$table = lcfirst($table);
			if (isset($this->structures[$table])) {
				return $this->structures[$table];
			}
			return null;
		} else {
			throw new \InvalidArgumentException("L'argument doit être une chaine de caractère");
		}
	}

	public function tableProperties($table)
	{
		if (is_string($table)) {
			$bdd = self::getMysqlConnexion();
			$properties = array();
			$reponse = $bdd->query('SELECT * FROM ' . lcfirst($table) . ' WHERE id = 1');

			$nbrColumn = $reponse->columnCount();
			for ($i = 0; $i < $nbrColumn; $i++) {
				$info = array();
				$info['name'] = $reponse->getColumnMeta($i)['name'];
				$info['type'] = $reponse->getColumnMeta($i)['native_type'];
				$properties[] = $info;
			}

			return $properties;
		} else
			throw new \InvalidArgumentException("Le nom de la table doit être une chaine de caractère!");
	}

	protected function writeClassHeader($ressource, $class, $parent = '')
	{
		if (!empty($parent)) {
			fwrite($ressource, "<?php \r namespace Library\Entities\XML;\r\r class " . ucfirst($class) . " extends $parent \r { \r\t");
		} else {
			fwrite($ressource, "<?php \r namespace Library\Entities\XML;\r\rclass " . ucfirst($class) . " \r { \r\t");
		}
	}

	protected function writeClassProperties($ressource, array $properties, $parent)
	{
		$inherited = PObject::parseToArray(new $parent);

		$properties_cmp = array();
		foreach ($properties as $property) {
			$properties_cmp[] = $property['name'];
		}

		$real_properties = array();
		foreach ($properties as $property) {
			if (!in_array($property['name'], array_keys($inherited))) {
				fwrite($ressource, "protected $" . $property['name'] . "; \r\t");
				$real_properties[] = $property;
			}
		}

		return $real_properties;
	}

	protected function writeClassGetters($ressource, array $properties)
	{
		fwrite($ressource, "\n// GETTERS //");

		foreach ($properties as $property) {
			fwrite(
				$ressource,
				"
					\n\tpublic function " . $property['name'] . "()\r\t{\r\t\treturn \$this->" . $property['name'] . ";\r\t}"
			);
		}

		fwrite($ressource, "\n");
	}

	protected function writeClassSetters($ressource, array $properties)
	{
		fwrite($ressource, "\n// SETTERS //");
		foreach ($properties as $property) {
			switch ($property['type']) {
				case 'DATETIME':
					fwrite($ressource, "
							\n\tpublic function set" . ucfirst($property['name']) . "(\Datetime $" . $property['name'] . ")\r\t{\r\t\t\$this->" . $property['name'] . " = $" . $property['name'] . ";\r\t}
						");
					break;

				case 'VAR_STRING' || 'STRING':
					fwrite($ressource, "
							\n\tpublic function set" . ucfirst($property['name']) . "($" . $property['name'] . ")\r\t{\r\t\t\$this->setString('" . $property['name'] . "', $" . $property['name'] . ");\r\t}
						");
					break;

				case 'LONG':
					fwrite($ressource, "
							\n\tpublic function set" . ucfirst($property['name']) . "($" . $property['name'] . ")\r\t{\r\t\t\$this->setInt('" . $property['name'] . "', $" . $property['name'] . ");\r\t}
						");
					break;

				default:
					fwrite($ressource, "
							\n\tpublic function set" . ucfirst($property['name']) . "($" . $property['name'] . ")\r\t{\r\t\t\$this->" . $property['name'] . " = $" . $property['name'] . ";\r\t}
						");
					break;
			}
		}
	}

	protected function writeClassFoot($ressource)
	{
		fputs($ressource, "\r }");
	}

	public function makeFile($class, $parent = '')
	{
		$monfichier = fopen(__DIR__ . "\Entities\XML\\" . $class . ".class.php", "w");
		$this->writeClassHeader($monfichier, $class, $parent);

		$properties = $this->tableProperties($class);
		$properties = $this->writeClassProperties($monfichier, $properties, $parent);

		$this->writeClassGetters($monfichier, $properties);
		$this->writeClassSetters($monfichier, $properties);

		$this->writeClassFoot($monfichier);
		fclose($monfichier);
	}
}
