<?php

namespace Library;

trait TraitXML
{
	public function stringToDOMForm($view)
	{
		// var_dump($view);
		$dom = new \DOMDocument();
		$dom->loadXML($view);
		return $dom->getElementsByTagName("form-data")[0];
	}

	public function xmlObject(ArrayEntity $object)
	{
		$dom = new \DOMDocument;
		$object_var = $object->arrayDescriptions();
		// $this->testVar($object_var);
		$xml_entity = $dom->createElement($object->className());
		foreach ($object_var as $attr => $value) {
			// if (!is_array($value)) {
			// try {
			$element = $dom->createAttribute($attr);

			if (is_a($value, \get_class(new \DateTime))) {
				$element->value = $value->format('d-m-Y H:i:s');
			} elseif (is_object($value)) {
				$element = $dom->importNode($this->xmlObject($value));
			} elseif (is_array($value)) {
				$element = $dom->importNode($this->xmlArrayForm($attr, $value), true);
			} else {
				$xml_entity->setAttribute($attr, $this->stringFormatWithQuote(trim($value)));
				continue;
				// $element->value = $this->stringFormatWithQuote(trim($value));
			}

			$xml_entity->appendChild($element);
		}

		return $xml_entity;
	}

	public function xmlArrayForm($name_root, array $properties)
	{
		$dom = new \DOMDocument;
		$xml_root = $dom->createElement($name_root);

		foreach ($properties as $key => $value) {
			$name = str_replace(" ", "", (is_numeric($key)) ? "xmlelement" : $key);
			// var_dump(str_replace(" ", "", $name));
			// var_dump($key);
			$element = $dom->createElement($name);

			if (is_numeric($value) || is_string($value)) {
				$attr_element = $dom->createAttribute("value");
				$attr_element->value = $this->stringFormatWithQuote(trim($value));

				$element->appendChild($attr_element);
			} elseif (is_object($value)) {
				$element->appendChild($dom->importNode($this->xmlObject($value), true));
			} elseif (is_array($value)) {
				$xml_root->appendChild($dom->importNode($this->xmlArrayForm($name, $value), true));
				continue;
			}

			$xml_root->appendChild($element);
		}

		return $xml_root;
	}

	public function xmlLineariseDOM($root_name, array $entities)
	{
		$dom = new \DOMDocument;
		$root = $dom->createElement($root_name);

		foreach ($entities as $key => $entity) {
			if (!is_null($entity)) {
				$attr_element = $dom->createAttribute("cle");
				$attr_element->value = $key;

				$element = $dom->importNode($this->xmlObject($entity), true);
				$element->appendChild($attr_element);

				$root->appendChild($element);
			}
		}

		return $root;
	}
}
