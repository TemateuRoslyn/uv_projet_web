<?php
namespace Library\Plugins;

use \Library\Plugins\Osms\Osms;

class FOsms
{
    protected $config = array('clientId' => 'pufiwr9InqB7N6YzdcrVu4QCG4E9ygb5', 'clientSecret' => 'Nt9rZ6E2jgiiSCJ5');
    protected $osms = null;
    protected $statut = "";

    public function _construct()
    {
        $this->osms = new Osms($this->config);
    }

    public function sendSms($senderNumber, $receiveNumber, $message)
    {
        $this->osms = new Osms($this->config);
        $response = $this->osms->sendSms($senderNumber, $receiveNumber, $message);
        $this->statut = (empty($response["error"])) ? "success" : $response["error"];
    }

    public function statut()
    {
        return $this->statut;
    }
}