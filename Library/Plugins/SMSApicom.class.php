<?php
namespace Library\Plugins;

use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\SmsapiException;

class SMSApicom
{
    protected $smsapi = null;
    protected $response = null;
    protected $error = "";

    // const HEADER = 'e-school';
    const HEADER = 'CSBM-AKABA';
    const USER_EMAIL = 'etogacosmas@gmail.com';
    const USER_PASS = "klaus690777525";

    public function __construct()
    {
        $client = new Client(self::USER_EMAIL);
        $client->setPasswordHash(md5(self::USER_PASS));

        $this->smsapi = new SmsFactory();
        $this->smsapi->setClient($client);
    }

    public function sendSMS($numberReceive, $message, string $header = self::HEADER)
    {
        try {
            $actionSend = $this->smsapi->actionSend();

            $actionSend->setTo($numberReceive);
            $actionSend->setText($message);
            $actionSend->setSender($header);

            $this->response = $actionSend->execute();

        } catch (SmsapiException $exception) {
            $this->error = $exception->getMessage();
        }
    }

    /**
     * fourni la liste des réponses de sms envoyés
     * @return array 
     */
    public function responses()
    {
        return $this->response->getList();
    }
}
    