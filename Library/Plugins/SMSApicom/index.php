<?php

use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\SmsapiException;

require_once __DIR__ . '/smsapi/Autoload.php';

$client = new Client('etogacosmas@gmail.com');
$client->setPasswordHash(md5("klaus690777525"));

$smsapi = new SmsFactory;
$smsapi->setClient($client);

try {
	$actionSend = $smsapi->actionSend();

	$actionSend->setTo('+237690777525');
	$actionSend->setText('Hello World!!');
	$actionSend->setSender('e-school');

	$response = $actionSend->execute();

	foreach ($response->getList() as $status) {
		echo $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus();
	}
} catch (SmsapiException $exception) {
	echo 'ERROR: ' . $exception->getMessage();
}