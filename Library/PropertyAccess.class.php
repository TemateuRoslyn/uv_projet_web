<?php
	namespace Library;

/***************************************
Hierarchie des droits d'accès:
	** Proprietaire
	** Groupe
***************************************
*/
	class PropertyAccess extends Managers
	{
		protected $document;
		protected $groupe;
		protected $currentUser;

		private $droit;
	
		public function __construct($api, $dao, User $user)
		{
			parent::__construct($api, $dao);
			$this->currentUser = $user;
		}

		public function initDroit()
		{
			if ($this->isProprietaire()) 
			{
				$this->droit = $this->getManagerOf('Document')->getUnique($this->document->id())->propertyAccess();	
			}
			elseif (($id = $this->isGroupe()) !== false) 
			{
				$partage = $this->getManagerOf('Partagelink')->partageLink($id, $this->document->id());

				$this->droit = $partage->shareDroit();		
			}
		}

		public function view()
		{
			return new ViewsManager\PropertyView(array(
					"read"          => $this->read(),
					"write"         => $this->write(),
					"share"         => $this->partage(),
					"proprietaire"  => $this->isProprietaire(),
					"groupe"        => $this->isGroupe(),
					"entity"        => $this->document,
				));
		}
		
/*************************************************
	Droit de l'utilisateur sur le document 
****/
		public function read()
		{
			return (int) substr($this->droit, 0, 1);
		}

		public function write()
		{
			return (int) substr($this->droit, 1, 1);
		}

		public function partage()
		{
			return (int) substr($this->droit, 2, 1);
		}

// SETTERS ET MOTEUR D'INITIALISATION DES DROITS
		public function userPropertiesView(Entities\InterfacesDAO\Document $document, Entities\InterfacesDAO\Groupe $groupe)	
		{
			$this->document = $document;
			$this->groupe = $groupe;
			$this->initDroit();
		}

		public function setGroupe(Entities\InterfacesDAO\Groupe $groupe)
		{
			$this->groupe = $groupe;
		}

		public function setDocument(Entities\InterfacesDAO\Document $document)
		{
			$this->document = $document;
		}

/****************************************************/

		public function isProprietaire()
		{
			return $this->currentUser->getAttribute('id') == $this->document->idMembre();
		}

		public function isGroupe()
		{
			$id = $this->getManagerOf("Partagelink")->partagelinkExistOn($this->groupe->id(), $this->document->id());
			if ($id) 
			{
				return $this->groupe->id();	
			}

			return false;
		}
	}