<?php
namespace Library;

class PathUse extends ArrayEntity
{
	protected $url;
	protected $location;
	protected $title = '';

	public function match(Route $route)
	{
		return $this->url == $route->pattern() && $this->location == $route->url();
	}

	public function url()
	{
		return $this->url;
	}

	public function location()
	{
		return $this->location;
	}

	public function arrayXML()
	{
		return array("location" => $this->location, "title" => $this->title);
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function setUrl($url)
	{
		$this->url = $url;
	}

	public function setLocation($location)
	{
		$this->location = $location;
	}
}