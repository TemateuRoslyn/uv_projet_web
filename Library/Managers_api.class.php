<?php

namespace Library;

abstract class Managers_api extends Managers
{
	const ASSOCIAION_ID = "id";
	const ASSOCIAION_SIGNATURE = "signature";

	/**
	 * Méthode permettant d'ajouter une entité
	 * @param $entity L'entité à ajouter
	 * @return void
	 */
	abstract protected function add(Entity $entity);

	/**
	 * Méthode permettant d'enrégistrer une intité.
	 * @param $entity L'entité à enrégistrer 
	 * @return void
	 */
	public function save(Entity $entity)
	{
		if ($entity->isValid()) {
			// $this->testVar($entity->isNew());
			$hisId = $entity->isNew() ? $this->add($entity) : $this->modify($entity);
			$entity->setId($hisId);
			// var_dump($hisId);
			return $hisId;
		} else {
			throw new \RuntimeException("L'entité doit être validé pour être enrégistré");
		}
	}

	/**
	 * Méthode retournant une liste d'entités demandée
	 * @param $debut int La première enité à selectionner
	 * @param $limite int Le nombre d'entité à selectionner
	 * @return array La liste des entités. chaque entrée est une instance de Entity
	 */
	abstract public function getList($debut = -1, $limite = -1);

	/**
	 * Méthode retournant une entité spécifique
	 * @param $debut int La première entité sélectionnée
	 * @return Entity l'entité demandée
	 */
	abstract public function getUnique($id);

	/**
	 * Méthode renvoyant le nombre d'entité total
	 * @return int
	 */
	abstract public function count();

	/**
	 * Méthode perméttant de modifier une entité
	 * @param $entity Entity l'entité à modifier
	 * @return void
	 */
	abstract protected function modify(Entity $entity);

	/**
	 * Méthode permettant de supprimer une entité
	 * @param $id int L'identifiant de l'entité à supprimer
	 * @return void
	 */
	abstract public function delete($id);

	/**
	 * Méthode permettant de supprimer une association entre entitées
	 * @param $id int L'identifiant de l'entité à supprimer
	 * @return void
	 */
	abstract public function deleteAssociation($attrib_value, string $attrib = self::ASSOCIAION_ID);

	/**
	 * Méthode permettant de définir une association
	 * Se lit à $entity on associe les éléments de $entities
	 * @param array $entities Tableau des entités à associer
	 * @param Entity $entity L'entité servant d'axe d'association
	 * @return void
	 */
	abstract public function addAssociations(array $entities, Entity $entity): void;

	/**
	 * Méthode permettant de supprimer une association
	 * Se lit à $entity on supprime son association aux éléments de $entities
	 * @param array $entities Tableau des entités à associer
	 * @param Entity $entity L'entité servant d'axe d'association
	 * @param bool $andEntity L'entité servant d'axe d'association sera supprimé ou pas en son tour
	 * @return void
	 */
	abstract public function removeAssociations(array $entities, Entity $entity, bool $andEntity = false): void;

	/**
	 * Méthode permettant de supprimer une association
	 * Se lit à $entity on supprime son association aux éléments de $entities
	 * @param array $entities Tableau des entités à associer
	 * @param Entity $entity L'entité servant d'axe d'association
	 * @param bool $andEntity L'entité servant d'axe d'association sera supprimé ou pas en son tour
	 * @return void
	 */
	public function unValidAssociations(Entity $entity, array $entitiesNames)
	{
	}

	/**
	 * Méthode permettant de définir une association de type Classe
	 * Se lit à $entity on associe les éléments de $entitiesL et ceux de $entitiesR
	 * @param array $entitiesL Tableau des entités à associer à droite
	 * @param Entity $entity L'entité servant d'axe d'association
	 * @param array $entitiesR Tableau des entités à associer à gauche
	 * @return void
	 */
	abstract public function addAssociationClass(array $entitiesL, Entity $entity, array $entitiesR): void;

	/**
	 * Liste des entités associées, en spécifiant le type, 
	 * seuls les éléments correspondant à ce dernier seront selectionnés
	 * @param Entity $entity L'entité servant d'axe d'association
	 * @param string $mapGet complément d'attributs de sélection
	 * @param string $mapRestrictJoin complément de paramètres de restriction prefix JOIN
	 * @param string $mapRestrictWhere complément de paramètres de restriction prefix WHERE
	 * @param int $debut de sélection
	 * @param int $limit de sélection
	 */
	abstract public function getAssociateTo(Entity $entity, string $mapGet = "", string $mapRestrictJoin = "", string $mapRestrictWhere = "", int $debut = -1, int $limit = -1): array;

	/**
	 * Liste des entités associées, en spécifiant le type, la classe relative au Manager 
	 * est utilisé comme Classe d'association
	 * @param Entity $entity L'entité servant d'axe d'association
	 * @param int $debut de sélection
	 * @param int $limit de sélection
	 */
	abstract public function getAssociateClassTo(Entity $entity, int $debut = -1, int $limit = -1): array;

	/**
	 * Liste des liens d'association entre entités associées, à la $table, 
	 * seuls les éléments correspondant à ce dernier seront selectionnés
	 * @param string $table la table associée
	 * @return EntityAssociation []
	 */
	public function getEntitiesAssociations(string $table): array
	{
		return [];
	}

	/**
	 * Liste des liens d'association entre entités associées à la $table, 
	 * seuls les éléments correspondant à ce dernier seront selectionnés
	 * @param Entity $entity de référence
	 * @param string $table la table associée
	 * @return EntityAssociationLink []
	 */
	public function getEntitiesAssociationsLinks(Entity $entity, string $table): array
	{
		return [];
	}
}
