<?php

namespace Library\Session;

use Library\Managers as LibraryManagers;
use Library\PObject;

class Managers_PDO extends PObject
{
    /** @property LibraryManagers */
    protected $managers = null;

    public function __construct(LibraryManagers $managers)
    {
        $this->managers = $managers;
    }

    public function getSessionID(string $ip): string
    {
        /** @var PDO */
        $dao = $this->managers->dao();
        return (string) $dao->query("SELECT `session_id` FROM `_pideria_manager_session` WHERE user_ip='$ip'")->fetchColumn();
    }

    public function saveSessionID(string $ip, string $session_id)
    {
        /** @var PDO */
        $dao = $this->managers->dao();
        $dao->exec("INSERT INTO `_pideria_manager_session` (`id`, `user_ip`, `session_id`, `timeInsert`) VALUES (NULL, '$ip', '$session_id', TIME(NOW()))");
    }
}
