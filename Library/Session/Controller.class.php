<?php

namespace Library\Session;

use Library\Application;
use Library\ApplicationComponent;
use Library\Managers;

class Controller extends ApplicationComponent
{
    /** @property Managers_PDO */
    protected $managers = null;

    public function __construct(Application $app, Managers $managers)
    {
        parent::__construct($app);
        $this->managers = new Managers_PDO($managers);

        $this->start();
    }

    protected function start(): void
    {
        if (empty(session_id())) {
            session_set_cookie_params(3600 * 24 * 60, "/");
            session_start();
        }
        // $ip = $_SERVER["REMOTE_ADDR"];
        // $session_id = $this->managers->getSessionID($ip);
        // // var_dump($session_id);
        // session_set_cookie_params(3600, "/");
        // if (empty($session_id)) {
        //     $session_id = md5(microtime(true));
        //     session_id($session_id);

        //     session_start();
        //     $this->managers->saveSessionID($ip, $session_id);
        // } else {
        //     session_id($session_id);
        //     session_start();
        // }
    }
}
