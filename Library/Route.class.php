<?php

namespace Library;

class Route extends PObject
{
	protected $pattern;
	protected $level;
	protected $title;
	protected $action;
	protected $viewSave;
	protected $viewAction;
	protected $viewActionXML;
	protected $viewActionXSL;
	protected $module;
	protected $url;
	protected $varsNames;
	protected $vars = array();
	protected $xmlPagesData;

	public function __construct($url, $module, $action, $viewAction, $level, $title, array $varsNames, string $pagesNames, $viewActionXML = "", $viewActionXSL = "", $viewSave = "")
	{
		$this->setUrl($url);
		$this->setModule($module);
		$this->setAction($action);
		$this->setViewSave($viewSave);
		$this->setViewAction($viewAction);
		$this->setViewActionXML($viewActionXML);
		$this->setViewActionXSL($viewActionXSL);
		$this->setVarsNames($varsNames);
		$this->setLevel($level);
		$this->setTitle($title);
		$this->setXmlPagesData($pagesNames);

		$this->pattern = $url;
	}

	public function hasViewActionXML(): bool
	{
		return !empty($this->viewActionXML);
	}

	public function hasViewActionXSL(): bool
	{
		return !empty($this->viewActionXSL);
	}

	public function hasViewAction(): bool
	{
		return !empty($this->viewAction);
	}

	public function getXmlPagesNames(): array
	{
		return explode(",", $this->xmlPagesData);
	}

	public function hasVars()
	{
		return !empty($this->varsNames);
	}

	public function match($url)
	{
		if (preg_match('`^' . $this->url . '$`', $url, $matches)) {
			return $matches;
		} else {
			return false;
		}
	}

	public function setViewAction($viewAction)
	{
		if (is_string($viewAction)) {
			$this->viewAction = $viewAction;
		}
	}

	public function setViewSave($viewAction)
	{
		if (is_string($viewAction)) {
			$this->viewSave = $viewAction;
		}
	}

	public function setViewActionXML($viewAction)
	{
		if (is_string($viewAction)) {
			$this->viewActionXML = $viewAction;
		}
	}

	public function setViewActionxSL($viewAction)
	{
		if (is_string($viewAction)) {
			$this->viewActionXSL = $viewAction;
		}
	}

	public function setAction($action)
	{
		if (is_string($action)) {
			$this->action = $action;
		}
	}

	public function setUrl($url)
	{
		if (is_string($url)) {
			$this->url = $url;
		}
	}

	public function setModule($module)
	{
		if (is_string($module)) {
			$this->module = $module;
		}
	}

	public function setVarsNames(array $varsNames)
	{
		$this->varsNames = $varsNames;
	}

	public function setVars(array $vars)
	{
		$this->vars = $vars;
	}

	public function setLevel($level)
	{
		$this->level = $level;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function setXmlPagesData($xmlPagesData)
	{
		$this->xmlPagesData = $xmlPagesData;
	}

	public function action()
	{
		return $this->action;
	}

	public function viewSave()
	{
		return $this->viewSave;
	}

	public function viewAction()
	{
		return $this->viewAction;
	}

	public function viewActionXML()
	{
		return $this->viewActionXML;
	}

	public function viewActionXSL()
	{
		return $this->viewActionXSL;
	}

	public function module()
	{
		return $this->module;
	}

	public function url()
	{
		return $this->url;
	}

	public function level()
	{
		return $this->level;
	}

	public function title()
	{
		return $this->title;
	}

	public function pattern()
	{
		return $this->pattern;
	}

	public function vars()
	{
		return $this->vars;
	}

	public function varsNames()
	{
		return $this->varsNames;
	}

	public function xmlPagesData()
	{
		return $this->xmlPagesData;
	}
}
