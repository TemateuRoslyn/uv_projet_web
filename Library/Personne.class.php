<?php
	namespace Library;

	class Personne extends Entity
	{
		protected $nom;
		protected $prenom;
		protected $email;
		protected $telephone;
		protected $nationalite;

		public function nom()
		{
			return $this->nom;					
		}		

		public function prenom()
		{
			return $this->prenom;
		}

		public function email()
		{
			return $this->email;
		}

		public function telephone()
		{
			return $this->telephone;
		}

		public function nationalite()
		{
			return $this->nationalite;
		}

		public function setNom($nom)
		{
			if (is_string($nom)) 
			{
				$this->nom = $nom;	
			}
		}		

		public function setPrenom($prenom)
		{
			if (is_string($prenom)) 
			{
				$this->prenom = $prenom;	
			}
		}

		public function setEmail($email)
		{
			if (is_string($email)) 
			{
				$this->email = $email;	
			}
		}

		public function setTelephone($telephone)
		{
			if (is_string($telephone)) 
			{
				$this->telephone = $telephone;	
			}
		}

		public function setNationalite($nationalite)
		{
			if (is_string($nationalite)) 
			{
				$this->nationalite = $nationalite;	
			}
		}
	}