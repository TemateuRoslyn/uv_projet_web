<?php

namespace Library;

use DateTime;

class Managers extends PObject
{
	const NAMESPACE_SINGLETON = "5c7aaad54e51f";
	const NAMESPACE_SINGLETON_XML = "5c7aaad54e51fXML";
	const NAMESPACE_SINGLETON_PDO = "5c7aaad54e51fPDO";

	protected $api = null;
	protected $dao = null;
	protected $factories = array();
	protected $managers = array();

	protected $table = '';
	protected $classeTable = '';
	protected $namespace = '';

	public function __construct($api, $dao)
	{
		$this->api = $api;
		$this->dao = $dao;
		$this->namespace = self::NAMESPACE_SINGLETON . $this->api;

		if (!isset($GLOBALS[$this->namespace])) {
			$GLOBALS[$this->namespace] = [];
		}
		// if ($this->api == "XML" && !is_null($dao->documentURI)) {
		// 	$this->dao = new \DOMDocument("1.0", "UTF-8");
		// }

		$this->initTable();
	}

	public static function saveXMLFile()
	{
		// $this->testVar($managers);
		if (isset($GLOBALS[self::NAMESPACE_SINGLETON_XML])) {
			$managers = $GLOBALS[self::NAMESPACE_SINGLETON_XML];
			// var_dump($managers);
			// throw new \Exception("Error Processing Request", 1);

			foreach ($managers as $manager) {
				$manager->saveFile();
			}
		}
	}

	public function getManagerOf($module): Managers_api
	{
		if (!is_string($module) || empty($module)) {
			throw new \InvalidArgumentException('Le module spécifié est invalide');
		}
		$this->managers = $GLOBALS[$this->namespace];

		if ($this->dao == null) {
			throw new \RuntimeException("Veuillez définir un accès à une base de donnée dans le fichier appConfig.xml");
		}

		if (!isset($this->managers[$module])) {
			$manager = '\\AppliLib\\Models\\' . $module . 'Manager_' . $this->api;
			$this->managers[$module] = new $manager($this->api, $this->dao);
			$GLOBALS[$this->namespace] = $this->managers;
		}
		// $this->testVar($this->namespace);
		return $this->managers[$module];
	}

	/**
	 * Retourne les objets dont la date de modification correspond à l'intervalle définit
	 * Par défaut, l'intervalle considéré s'étale sur le week-end courant
	 * @param Entity[] $entities
	 * @param DateTime $start
	 * @param DateTime $end
	 * @return Entity[]
	 */
	public function filterInIntervalDate(array $entities, &$start = null, &$end = null): array
	{
		if (is_null($start)) {
			$start = DateTime::createFromFormat("m-d-Y", date("m-d-Y", strtotime('monday this week')));
		}
		if (is_null($end)) {
			$end = DateTime::createFromFormat("m-d-Y", date("m-d-Y", strtotime('sunday this week')));
		}

		$GLOBALS["filterInIntervalDateConfig"] = [$start, $end];
		$entities = array_filter($entities, function (Entity $entity) {
			$dayStart = $GLOBALS["filterInIntervalDateConfig"][0];
			$dayEnd = $GLOBALS["filterInIntervalDateConfig"][1];

			$dayEnd->setTime(23, 0);
			$dayStart->setTime(0, 0);
			return $dayStart <= $entity->dateModif() && $entity->dateModif() <= $dayEnd;
		});

		unset($GLOBALS["filterInIntervalDateConfig"]);
		return $entities;
	}

	/**
	 * @param Entity[] $entities
	 * @return Entity[]
	 */
	public function orderedByDate(array $entities): array
	{
		$tmp_entities = $entities;
		usort($tmp_entities, function (Entity $e1, Entity $e2) {
			if ($e1->dateModif() == $e2->dateModif()) {
				return 0;
			}
			return $e1->dateModif() < $e2->dateModif() ? -1 : 1;
		});
		return $tmp_entities;
	}

	/**
	 * @param Entity[] $entities
	 * @param EntityAssociation[] $entitiesAssociation
	 */
	public static function combineAssociationsToEntities(array $entities, array $entitiesAssociation)
	{
		foreach ($entities as $entity) {

			foreach ($entitiesAssociation as $assoc) {
				if ($index = $assoc->isToEntity($entity)) {
				}
			}
		}
	}

	/**
	 * @param EntityAssociation[] $associations
	 * @param Entity[] $entities
	 * @return EntityAssociation[]
	 */
	public function filterEntityAssociation(array $associations, array $entities): array
	{
		$result = [];
		$GLOBALS["function_filterEntityAssociation"] = $entities;
		$result = array_filter($associations, function (EntityAssociation $association) {
			/** @var Entity[] */
			$entities = $GLOBALS["function_filterEntityAssociation"];
			foreach ($entities as $entity) {
				if (($id = $association->hasEntityAssociationLinkHasName($entity->className())) && $entity->id() == $id) {
					// var_dump("ssfs");
					return true;
				}
			}
			return false;
		});

		return $result;
	}

	public function getFactoryOf($module)
	{
		if (!is_string($module) || empty($module)) {
			throw new \InvalidArgumentException('Le module spécifié est invalide');
		}

		if (!isset($this->factories[$module])) {
			$factory = '\\Library\\Factories\\' . $module . 'Factory_' . $this->api;
			$this->factories[$module] = new $factory($this->api, $this->dao);
		}

		return $this->factories[$module];
	}

	private function initTable()
	{
		$this->classeTable = str_replace(array('AppliLib', 'Factories', 'Factory_PDO', 'Manager_PDO', 'Library', 'Models', '\\', 'Manager_XML'), '', get_class($this));
		$this->table = strtolower(str_replace(array('AppliLib', 'Factories', 'Factory_PDO', 'Manager_PDO', 'Library', 'Models', '\\', 'Manager_XML'), '', get_class($this)));
	}

	public function dao()
	{
		return $this->dao;
	}
}
