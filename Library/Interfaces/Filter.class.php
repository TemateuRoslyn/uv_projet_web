<?php
namespace Library\Interfaces;

use Library\FilterConfig;
use Library\HTTPRequest;
use Library\HTTPResponse;
use Library\FilterChain;


interface Filter
{
    public function init(FilterConfig $config);
    public function destroy();
    public function doFilter(HTTPRequest $request, HTTPResponse $response, FilterChain $chain);
}