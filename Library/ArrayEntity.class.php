<?php

namespace Library;

class ArrayEntity extends PObject implements \ArrayAccess
{
	private $erreurs = array();
	private $descriptions = array();
	private $dateModif;
	private $dateInsert;

	public function __construct(array $donnees = array())
	{
		if (!empty($donnees)) {
			$this->hydrate($donnees);
		}
	}

	public function erreurs()
	{
		return $this->erreurs;
	}

	public function dateInsert()
	{
		return $this->dateInsert;
	}

	public function arrayDescriptions(): array
	{
		return [];
	}

	public function setDateInsert(\DateTime $dateInsert)
	{
		$this->dateInsert = $dateInsert;
	}

	public function addDescription(string $attr, $valeur)
	{
		$this->descriptions[$attr] = $valeur;
		// var_dump($this->descriptions);
	}

	public function setDescriptions(array $data)
	{
		$this->descriptions = $data;
	}

	public function dateModif()
	{
		return $this->dateModif;
	}

	public function getDescription(string $attr)
	{
		return $this->descriptions[$attr];
	}

	public function getDescriptions(): array
	{
		// var_dump($this->descriptions);
		return $this->descriptions;
	}
	//...........................................

	//Liste des setters 
	public function setDateModif(\DateTime $dateModif)
	{
		$this->dateModif = $dateModif;
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $attribut => $valeur) {
			$method = 'set' . ucfirst($attribut);
			if (is_callable(array($this, $method)) && !is_null($valeur)) {
				if (is_string($valeur) && strpos($attribut, "date") !== false) {
					$valeur = new \DateTime($valeur);
				}
				$this->$method($valeur);
			}
		}
	}

	public function offsetGet($var)
	{
		if (isset($this->$var) && is_callable(array($this, $var))) {
			return $this->$var();
		}
	}

	public function offsetSet($var, $value)
	{
		$method = 'set' . ucfirst($var);
		if (isset($this->$var) && is_callable(array($this, $method))) {
			$this->$method($value);
		}
	}

	public function offsetExists($var)
	{
		return isset($this->$var) && is_callable(array($this, $var));
	}

	public function offsetUnset($var)
	{
		throw new \Exception("Impossible de supprimer une quelconsue valeur");
	}
}
