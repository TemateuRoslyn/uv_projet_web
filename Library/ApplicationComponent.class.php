<?php

namespace Library;

abstract class ApplicationComponent extends PObject
{
	/**
	 * @var Application
	 */
	protected $app;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	public function app(): Application
	{
		return $this->app;
	}
}
