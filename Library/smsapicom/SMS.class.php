<?php
namespace \Library\smsapicom\;

use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\SmsapiException;

require_once '..\smsapi\Autoload.php';

class SMS extends \Library\PObject
{
	protected $smsFactory;
	protected $response;
	protected $errorMsg;

	public function __construct($login, $password)
	{
		$client = new \Client($login);
		$client->setPasswordHash(md5($password));

		$this->smsFactory = new \SmsFactory;
		$this->smsFactory->setClient($client);
	}

	public function smsSend($text, $destinateur, $header = 'cameroonschool')
	{
		try 
		{
			$actionSend = $smsapi->actionSend();

			$actionSend->setTo($destinateur);
			$actionSend->setText($text);
			$actionSend->setSender($header);

			$this->response = $actionSend->execute();
			$this->errorMsg = '';

		} 
		catch (SmsapiException $exception) 
		{
			$this->errorMsg = $exception->getMessage();
		}
	}

	public function response()
	{
		return $this->response;
	}
	// foreach ($response->getList() as $status) {
		// echo $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus();
	// }
}