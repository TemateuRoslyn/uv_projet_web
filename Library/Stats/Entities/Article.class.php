<?php
	namespace Library\Stats\Entities;

	class Article extends \Library\Entity
	{
		protected $nbreVisite;

		public function isValid()
		{
			return !empty($this->id);
		}

		public function nbreVisite()
		{
			return $this->nbreVisite;
		}

		public function setNbreVisite($nbreVisite)
		{
			$this->setInt("nbreVisite", $nbreVisite);
		}
	}