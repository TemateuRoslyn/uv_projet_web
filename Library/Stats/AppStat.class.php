<?php
	namespace Library\Stats;
	
	class AppStat extends \Library\ApplicationComponent
	{
		protected $managers = array();

		public function getManagerOf($classeName)
		{
			if (!isset($this->managers[$classeName])) 
			{
				if (!is_string($classeName) || empty($classeName))    
	    		{      
	    			throw new \InvalidArgumentException('Le module spécifié est invalide'); 
	    		}

	    		if (!isset($this->managers[$classeName]))    
	    		{
	    			$dao = new \DOMDocument;
					$dao->load(__DIR__.'/../../Applications/'.$this->app->name().'/Stats/'.lcfirst($classeName).'s.xml');
	    			
	    			$manager = '\\Library\\Stats\\Models\\'.$classeName.'Manager_XML';
	    			$this->managers[$classeName] = new $manager("XML", $dao);
	    		}
			}

			return $this->managers[$classeName];
		}
	}