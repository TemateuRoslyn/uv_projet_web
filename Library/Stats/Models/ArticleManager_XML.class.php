<?php
	namespace Library\Stats\Models;

	use \Library\Stats\Entities\Article;

	class ArticleManager_XML extends \Library\Managers_XML
	{
		public function like($id)
		{
			$article = $this->getUnique("_$id");
			if (is_null($article)) 
			{
				$article = new \Library\Stats\Entities\Article(array(
								"id" => $id,
								"nbreVisite" => 0,
							));
			}

			$article->setNbreVisite($article->nbreVisite()+1);
			$this->save($article);
		}
	}