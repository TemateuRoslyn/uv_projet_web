<?php

namespace Library\Stats;

class StatsUtilitary extends \Library\PObject
{
    public static function percent(float $nbre, float $divider): float
    {
        $divider = ($divider != 0) ? $divider : 1;
        return round(($nbre * 100) / $divider, 3);
    }

    public static function sum(array $data, string $mode): float
    {
        $result = 0;
        foreach ($data as $element) {
            $result += $element[$mode];
        }
        return $result;
    }

    public static function sumArraySummableValues(array $data): array
    {
        $results = [];
        foreach ($data as $line) {
            foreach ($line as $key => $value) {
                // var_dump($key . "  " . $value);
                if (is_numeric($value) || \is_int($value) || is_float($value)) {
                    if (!isset($results[$key])) {
                        $results[$key] = $value;
                    } else {
                        $results[$key] += $value;
                    }
                }
            }
        }
        // var_dump($results);
        return $results;
    }

    /**
     * Somme des éléments ayant une propriété spécifique
     * @param \Library\ArrayEntity[] $data
     * @param $value la valeur à additionner
     */
    public static function sumHavePropertyValue(array $data, string $propertySumValue): float
    {
        $result = 0;
        foreach ($data as $element) {
            if ($element->offsetExists($propertySumValue)) {
                // var_dump((float)$element[$propertySumValue]);
                $result += (float) $element[$propertySumValue];
                // var_dump($result . " <br>");
            }
        }
        return $result;
    }

    /**
     * Somme des éléments ayant une propriété spécifique
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     * @param string $fixe la valeur à fixer
     * @param $value la valeur à additionner
     */
    public static function sumHavePropertyValueFixed(array $data, string $property, string $fixe, string $propertySumValue): float
    {
        $result = 0;
        foreach ($data as $element) {
            // var_dump(isset($element->arrayDescriptions()[$property]));
            if (isset($element->arrayDescriptions()[$property]) && $element->arrayDescriptions()[$property] == $fixe) {
                $result += (float) $element->arrayDescriptions()[$propertySumValue];
            }
        }
        return $result;
    }

    /**
     * Compte les éléments ayant une propriété et une valeur spécifiées
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     * @param string $value la valeur 
     */
    public static function countHavePropertyValue(array $data, string $property, string $value): float
    {
        $result = 0;
        foreach ($data as $element) {
            if ($element->offsetExists($property) && (float) $element[$property] == $value) {
                $result++;
            }
        }
        // var_dump($result);
        return $result;
    }

    /**
     * Compte les éléments ayant une propriété et une valeur maximale spécifiées
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     * @param float $value la valeur maximale
     */
    public static function countHavePropertyMaxValue(array $data, string $property, float $value): float
    {
        $result = 0;
        foreach ($data as $element) {
            if ($element->offsetExists($property) && (float) $element[$property] > $value) {
                $result++;
            }
        }
        return $result;
    }

    /**
     * Compte les éléments ayant une propriété et une valeur maximale spécifiées
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     * @param float $value la valeur maximale
     */
    public static function countHavePropertyMaxEqualValue(array $data, string $property, float $value): float
    {
        $result = 0;
        foreach ($data as $element) {
            if ($element->offsetExists($property) && (float) $element[$property] >= $value) {
                $result++;
            }
        }
        return $result;
    }

    /**
     * Compte les éléments ayant une propriété et une valeur minimale spécifiées
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     * @param float $value la valeur minimale
     */
    public static function countHavePropertyMinValue(array $data, string $property, float $value): float
    {
        $result = 0;
        foreach ($data as $element) {
            if ($element->offsetExists($property) && (float) $element[$property] < $value) {
                $result++;
            }
        }
        return $result;
    }

    /**
     * Compte les éléments ayant une propriété spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     */
    public static function countHaveProperty(array $data, string $proterty): float
    {
        $result = 0;
        foreach ($data as $element) {
            if ($element->offsetExists($proterty)) {
                $result++;
            }
        }
        return $result;
    }

    /**
     * Pourcentage des éléments ayant une propriété spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string $proterty la propriété
     */
    public static function tauxHaveProperty(array $data, string $proterty): float
    {
        $nbre = self::countHaveProperty($data, $proterty);
        $length = \count($data);

        return ($length) ? $nbre * 100 / $length : 0;
    }

    /**
     * regroupe les éléments par sous groupes de nom une valeur spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string[] $proterties les propriétés
     */
    public static function regroupHaveProperties(array $data, array $properties): array
    {
        $groupe = [];
        foreach ($data as $element) {

            foreach ($properties as $property) {
                if ($element->offsetExists($property)) {
                    if (!isset($groupe[$property])) {
                        $groupe[$property] = array($element);
                    } else {
                        $groupe[$property][] = $element;
                    }
                }
            }
        }

        return $groupe;
    }

    /**
     * @param \Library\ArrayEntity[] $data
     * @param string[] $proterties les propriétés
     */
    public static function regroupHavePropertiesValues(array $data, array $properties): array
    {
        $groupe = [];
        foreach ($data as $element) {
            $save = true;

            foreach ($properties as $property => $value) {
                // var_dump($element);
                if ($element->arrayDescriptions()[$property] != $value) $save = false;
            }

            if ($save) $groupe[] = $element;
        }
        // var_dump("START");
        // var_dump($groupe);
        // var_dump($properties);
        return $groupe;
    }

    /**
     * regroupe les éléments de nom une valeur spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string[] $proterties les propriétés
     * @return \Library\ArrayEntity[]
     */
    public static function regroupHavePropertyValue(array $data, string $property, string $value): array
    {
        $GLOBALS["property"] = [$property => $value];
        return array_filter($data, function ($element) {
            $property = $GLOBALS["property"];
            foreach ($property as $key => $value) {
                // var_dump($element->arrayDescriptions()[$key]);
                return $element->arrayDescriptions()[$key] == $value;
            }
        });
    }

    /**
     * regroupe les éléments de nom une valeur spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string $property la valeur de la propriété
     * @return \Library\ArrayEntity[]
     */
    public static function groupByPropertyValue(array $data, string $property): array
    {
        $GLOBALS["property"] = $property;
        return array_filter($data, function ($element) {
            $property = $GLOBALS["property"];
            $data = $element->arrayDescriptions();

            return isset($data[$property]);
        });
    }

    /**
     * Selectionne un élément spécifique en fonction de la propriété fixée et d'une valeur spécifique
     * @param \Library\ArrayEntity[] $data
     * @param string[] $proterties les propriétés
     * @return \Library\ArrayEntity[]
     */
    public static function getUniqueElement(array $data, string $property, string $value): array
    {
        foreach ($data as $element) {
            // var_dump($element->arrayDescriptions()[$key]);
            if ($element->arrayDescriptions()[$property] != $value) {
                return $element;
            }
        }

        return [];
    }

    /**
     * regroupe les éléments de nom une valeur spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string[] $proterties les propriétés
     * @return \Library\ArrayEntity[]
     */
    public static function regroupHaveNotPropertyValue(array $data, string $property, string $value): array
    {
        $GLOBALS["property"] = [$property => $value];
        return array_filter($data, function ($element) {
            $property = $GLOBALS["property"];
            foreach ($property as $key => $value) {
                // var_dump($element->arrayDescriptions());
                // $this->testVar($element->arrayDescriptions());
                return $element->arrayDescriptions()[$key] != $value;
            }
        });
    }

    /**
     * regroupe les éléments de nom une valeur spécifiée
     * @param \Library\ArrayEntity[] $data
     * @param string[] $proterties les propriétés
     * @return \Library\ArrayEntity[]
     */
    public static function regroupHaveProperty(array $data, string $property): array
    {
        $groupe = [];
        foreach ($data as $element) {
            if ($element->offsetExists($property)) {
                $groupe[] = $element;
            }
        }

        return $groupe;
    }
}
