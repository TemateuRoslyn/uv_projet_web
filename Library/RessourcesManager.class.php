<?php

namespace Library;

use AppliLib\Entities\Couriel;

class RessourcesManager extends PObject
{
	public static function upload($filename, $fileLocation = "imgLabel")
	{
		$dir = __DIR__ . "/Ressources/";
		if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}

		// var_dump($_FILES); throw new Exception("Error Processing Request", 1);

		return move_uploaded_file($_FILES[$fileLocation]['tmp_name'], $dir . '/' . $filename);
	}

	public static function dowloadFile(string $disc_name_file, string $up_file_name)
	{
		$dir = __DIR__ . "/Ressources/";
		if (file_exists($dir . '/' . $disc_name_file)) {
			header('Content-Type: application/pdf');
			header('Content-Disposition: attachment; filename=' . $up_file_name);
			readfile($dir . '/' . $disc_name_file);
		}
	}

	// public static function delete(Document $document)
	// {
	// 	unlink(__DIR__ . '/../' . $document->chemin());
	// }
}
