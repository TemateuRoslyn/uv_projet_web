<?php

namespace Library;

abstract class Field extends PObject
{
	protected $errorMessage;
	protected $label;
	protected $placeholder;
	protected $name;
	protected $validators = array();
	protected $value;
	protected $length;
	protected $icon;
	protected $class;
	protected $disable;

	public function __construct(array $options = array())
	{
		if (!empty($options)) {
			$this->hydrate($options);
		}
	}

	abstract public function buildWidget();

	public function hydrate($options)
	{
		foreach ($options as $type => $value) {
			$method = 'set' . ucfirst($type);
			if (is_callable(array($this, $method))) {
				$this->$method($value);
			}
		}
	}

	public function isValid()
	{
		foreach ($this->validators as $validator) {
			if (!$validator->isValid($this->value)) {
				$this->errorMessage = $validator->errorMessage();
				return false;
			}
		}

		return true;
	}

	public function errorBuildMessage()
	{
		$widget = '';
		if (!empty($this->errorMessage)) {
			$widget .= '<label class="label label-warning">';
			$widget .= str_replace("'", "&apos;", $this->errorMessage);
			$widget .= '</label>';
		}

		return $widget;
	}

	// GETTERS

	public function label()
	{
		return $this->label;
	}

	public function length()
	{
		return $this->length;
	}

	public function name()
	{
		return $this->name;
	}

	public function validators()
	{
		return $this->validators;
	}

	public function value()
	{
		return $this->value;
	}

	public function getIcon()
	{
		return (!empty($this->icon)) ? '<span class="' . self::ICON[$this->icon] . '"></span>' : '';
	}

	public function placeholder()
	{
		return $this->placeholder;
	}

	public function class()
	{
		return $this->class;
	}

	public function disable()
	{
		return $this->disable;
	}

	// SETTERS 

	public function setLabel($label)
	{
		if (is_string($label)) {
			$this->label = $label;
		}
	}

	public function setLength($length)
	{
		$length = (int) $length;
		if ($length > 0) {
			$this->length = $length;
		}
	}

	public function setName($name)
	{
		if (is_string($name)) {
			$this->name = $name;
		}
	}

	public function setValidators(array $validators)
	{
		foreach ($validators as $validator) {
			if ($validator instanceof Validator && !in_array($validator, $this->validators)) {
				$this->validators[] = $validator;
			}
		}
	}

	public function setValue($value)
	{
		if ($value instanceof \DateTime) {
			$this->value = $value->format('d-m-Y H:i:s');
		} else {
			$this->value = $value;
		}
	}

	public function setIcon($icon)
	{
		if (is_string($icon)) {
			$this->icon = $icon;
		}
	}

	public function setPlaceholder($placeholder)
	{
		if (is_string($placeholder)) {
			$this->placeholder = $placeholder;
		}
	}

	public function setClass(string $class)
	{
		$this->class = $class;
	}

	public function setDisable(string $class)
	{
		$this->disable = $class;
	}
}
