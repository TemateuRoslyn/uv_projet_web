<?php

namespace Library;

class HTTPResponse extends ApplicationComponent
{
	protected $page;

	public function addHeader($header)
	{
		header($header);
	}

	public function redirectBack($current_location, $location_index)
	{
		$_SESSION["requ-back"] = $current_location;
		$_SESSION["requ-index"] = $location_index;

		header('Location: ' . $location_index);
		exit;
	}

	public function redirect($location)
	{
		Managers::saveXMLFile();
		// if (isset($_SESSION["requ-back"])) {
		// 	$location = $_SESSION["requ-back"];
		// 	unset($_SESSION["requ-back"]);

		// 	header('Location: ' . $location);
		// 	exit;
		// }

		// if ($location == ".") {
		// 	header("Location: " . $this->app->filigranne()->current()->location());
		// 	exit;
		// }

		// if ($location == "..") {
		// 	header("Location: " . $this->app->filigranne()->previous()->location());
		// 	exit;
		// }

		header('Location: ' . $location);
		exit;
	}

	public function redirect200()
	{
		$this->page = new Page($this->app);
		$this->page->setLocationModel(__DIR__ . '/../Errors/200.xsl', __DIR__ . '/../Errors/200.xml');

		$this->page->addVar('error-page', "error-page");
		$this->addHeader('HTTP/1.0 200');
		$this->send();
	}

	public function redirect410()
	{
		$this->page = new Page($this->app);
		$this->page->setLocationModel(__DIR__ . '/../Errors/410.xsl', __DIR__ . '/../Errors/410.xml');

		$this->page->addVar('error-page', "error-page");
		$this->addHeader('HTTP/1.0 410 Not Found');
		$this->send();
	}

	public function redirect404()
	{
		// $this->page = new Page($this->app);
		// $this->page->setLocationModel(__DIR__ . '/../Errors/404.xsl', __DIR__ . '/../Errors/404.xml');
		// $this->page->setLocationModel(__DIR__ . '/../Errors/404.xsl', __DIR__ . '/../Errors/404.xml');

		// $this->page->addVar('error-page', "error-page");
		// $this->addHeader('HTTP/1.0 404 Not Found');
		// $this->send();
		$this->app->httpResponse()->redirect('/note_found');
	}

	public function redirect500()
	{
		$this->page = new Page($this->app);
		$this->page->setLocationModel(__DIR__ . '/../Errors/500.xsl', __DIR__ . '/../Errors/500.xml');

		$this->page->addVar('error-page', "error-page");
		$this->addHeader('HTTP/1.0 500 Not Found');
		$this->send();
	}

	public function redirectPrevious()
	{
		$this->redirect($this->app->filigranne()->previous()->location());
	}

	public function send()
	{
		$this->_send();
	}

	private function _send()
	{
		if ($this->page->isPDF()) {
			$pdf = new XMLPage();
			$pdf->loadXMLData($this->page->getGeneratedPage());

			header('Content-Type: application/pdf; charset=UTF-8');
			$pdf->Output();
			exit();
		}

		if ($this->page->isTextWritter()) {
			exit($this->page->getGeneratedPage());
		}

		$content = "<!DOCTYPE html>";
		$content .= str_replace(array("<quote></quote>", "<quote />", "&lt;quote /&gt;", "&lt;quote/&gt;", "<quote/>"), "'", $this->page->getGeneratedPage());

		exit($content);
	}

	public function setPage(Page $page)
	{
		$this->page = $page;
	}

	public function page(): Page
	{
		return $this->page;
	}

	public function setCookie($name, $value = '', $expire = 0, $path = null, $domain = null, $secure = false, $httpOnly = true)
	{
		setcookie($name, $value, $expire, $path, $domain, $secure, $httpOnly);
	}
}
