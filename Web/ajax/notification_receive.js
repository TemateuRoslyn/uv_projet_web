//  -------------------   Les variables  -----------------------

/**
 * Premiere condition de selection
 */
 var select_items_list = document.querySelector("#select_condition")
 /**
  * seconde condition de selection
  */
 var parent_select_second = document.querySelector("#select_condition_second")
 
 var parent_select = document.querySelector("#div_selct_action")
 var select
 var select_second
 
 
 
 // -------------------  Les Evenements  ------------------------------
 
 
   //la soumission du formuliare
 
 
   function validateForm(event){
 
     
 
     if (!select){
       event.preventDefault();
     }
 
   }
 
     /**
      * La premiere condition de selection
      */
     
      select_items_list.addEventListener("change", function(){
       
       // on commence par supprimer le select s'il existe deja
       select  = document.querySelector("#select_created")
 
       // apres selection on le detruit
       if(select || (select_items_list.value === '0')){
         parent_select.removeChild(select)
         select= undefined       
       }
 
       if(select_items_list.value === '0' ){
         parent_select.style.display = "none"
       }else{
         // on recupere la liste des 
           runAjax("/dashbord/ajax/get_list", select_items_list.value+"/")
       }
 
     })
 
 
     //  on ecoute la seconde condition de selection
     /**
      * 
      */
   function listenSecondEvent(){
       parent_select_second.addEventListener("change", function(){
         // on selctionne le prochaine element et on fait les memme verification que
         //  a la premiere partie
         select_second = document.querySelector("#select_second_created")
         if(select_second){
           // on supprime le second select
           parent_select_second.removeChild(select_second)
           select_second = undefined       
         }
       })
 
     }

 // ------------------  Les Fonctions ---------------------
 
 function enfreindreFauteRegle(){
   
 }
 
 
 
 // ---------------------  Une Fontion  --------------------------------
 
 
 /**
  * Cette fonction lance la requete ajax
  * @param {string} path 
  * @param {string} datas 
  */
 var runAjax = (path, datas) => {
     //   Se diriger vers une autres page
     
     var xhr = getHttpRequest()
     // xhr.open('GET', 'http://localhost/demo', true)
     xhr.open('GET', path+datas, true)
     // xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest')
     xhr.send()
     
     
     // Pour suivre l'évolution de l'appel on utilisera un évènement
     
     xhr.onreadystatechange = function () {
         if (xhr.readyState === 4) {
             if (xhr.status === 200) {
                // contient le résultat de la page
                var datas = xhr.responseText.replace("<!DOCTYPE html>", "")
                var list = JSON.parse(datas)
            } else {
                // Le serveur a renvoyé un status d'erreur
            }
        }
    }
}




/**
 * creer l'objet XMLHttpRequest
 * @returns 
 */
var getHttpRequest = function () {
    var httpRequest = false;
  
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
      httpRequest = new XMLHttpRequest();
    //   if (httpRequest.overrideMimeType) {
    //     httpRequest.overrideMimeType('text/xml');
    //   }
    }
    else if (window.ActiveXObject) { // IE
      try {
        httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e) {
        try {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (e) {}
      }
    }
  
    if (!httpRequest) {
      alert('Abandon :( Impossible de créer une instance XMLHTTP');
      return false;
    }
  
    return httpRequest
  }