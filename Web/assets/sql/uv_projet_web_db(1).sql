-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 26 juil. 2021 à 05:23
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `uv_projet_web_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

DROP TABLE IF EXISTS `classe`;
CREATE TABLE IF NOT EXISTS `classe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom_classe` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `nom_classe`, `dateInsert`, `dateModif`) VALUES
(1, 'SIXIEME', '2021-07-21 21:15:49', '2021-07-21 21:15:49'),
(2, 'CINQUIEME', '2021-07-21 21:32:45', '2021-07-21 21:32:45'),
(3, 'QUATRIEME', '2021-07-21 21:32:59', '2021-07-21 21:32:59'),
(4, 'TROISIEME', '2021-07-21 21:33:07', '2021-07-21 21:33:07'),
(5, 'SECONDE_C', '2021-07-21 21:33:17', '2021-07-21 21:33:17'),
(6, 'SECONDE_A_ALLEMAND', '2021-07-21 21:33:24', '2021-07-21 21:33:24'),
(7, 'SECONDE_A_ESPAGNOLE', '2021-07-21 21:33:32', '2021-07-21 21:33:32'),
(8, 'PREMIERE_C', '2021-07-21 21:33:50', '2021-07-21 21:33:50'),
(9, 'PREMIERE_D', '2021-07-21 21:33:56', '2021-07-21 21:33:56'),
(10, 'PREMIERE_A_ALLEMAND', '2021-07-21 21:34:04', '2021-07-21 21:34:04'),
(11, 'PREMIERE_A_ESPAGNOLE', '2021-07-21 21:34:13', '2021-07-21 21:34:13'),
(12, 'TERMINALE_C', '2021-07-21 21:34:21', '2021-07-21 21:34:21'),
(13, 'TERMINALE_D', '2021-07-21 21:34:30', '2021-07-21 21:34:30'),
(14, 'TERMINALE_A_ALLEMAND', '2021-07-21 21:34:38', '2021-07-21 21:34:38'),
(15, 'TERMINALE_A_ESPAGNOLE', '2021-07-21 21:34:48', '2021-07-21 21:34:48');

-- --------------------------------------------------------

--
-- Structure de la table `conseil__discipline`
--

DROP TABLE IF EXISTS `conseil__discipline`;
CREATE TABLE IF NOT EXISTS `conseil__discipline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_cd` date NOT NULL,
  `heure_debut_cd` date NOT NULL,
  `heure_fin_cd` date NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `convocation`
--

DROP TABLE IF EXISTS `convocation`;
CREATE TABLE IF NOT EXISTS `convocation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` text NOT NULL,
  `date_convocation` text NOT NULL,
  `date_RV` date NOT NULL,
  `statut` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cour`
--

DROP TABLE IF EXISTS `cour`;
CREATE TABLE IF NOT EXISTS `cour` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_cour` date NOT NULL,
  `heure_debut` date NOT NULL,
  `heure_fin` date NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cour`
--

INSERT INTO `cour` (`id`, `date_cour`, `heure_debut`, `heure_fin`, `dateInsert`, `dateModif`) VALUES
(1, '2021-07-06', '2021-07-08', '2021-07-21', '2021-07-24 07:42:03', '2021-07-24 07:42:03');

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

DROP TABLE IF EXISTS `eleve`;
CREATE TABLE IF NOT EXISTS `eleve` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `solvable` text NOT NULL,
  `redoublant` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`id`, `solvable`, `redoublant`, `dateInsert`, `dateModif`) VALUES
(1, 'PAYE', 'Nouveau', '2021-07-21 21:39:32', '2021-07-21 21:39:32'),
(2, 'PAYE', 'Redoublant', '2021-07-21 21:46:27', '2021-07-21 21:46:27'),
(3, 'PAYE', 'Nouveau', '2021-07-22 04:18:41', '2021-07-22 04:18:41'),
(4, 'PAYE', 'Nouveau', '2021-07-22 04:20:26', '2021-07-22 04:20:26'),
(5, 'PAYE', 'Nouveau', '2021-07-22 04:22:05', '2021-07-22 04:22:05'),
(6, 'PAYE', 'Redoublant', '2021-07-22 04:23:31', '2021-07-22 04:23:31'),
(7, 'NON_PAYE', 'Redoublant', '2021-07-24 15:57:33', '2021-07-24 15:57:33'),
(8, 'PAYE', 'Redoublant', '2021-07-24 21:33:19', '2021-07-24 21:33:19'),
(9, 'NON_PAYE', 'Redoublant', '2021-07-24 22:04:05', '2021-07-24 22:04:05'),
(10, 'PAYE', 'Nouveau', '2021-07-25 18:44:37', '2021-07-25 18:44:37'),
(11, 'NON_PAYE', 'Nouveau', '2021-07-25 18:47:14', '2021-07-25 18:47:14'),
(12, 'PAYE', 'Nouveau', '2021-07-25 18:49:26', '2021-07-25 18:49:26'),
(13, 'PAYE', 'Nouveau', '2021-07-25 18:50:50', '2021-07-25 18:50:50'),
(14, 'PAYE', 'Nouveau', '2021-07-25 18:51:59', '2021-07-25 18:51:59'),
(15, 'PAYE', 'Redoublant', '2021-07-25 18:52:44', '2021-07-25 18:52:44'),
(16, 'PAYE', 'Nouveau', '2021-07-25 18:54:12', '2021-07-25 18:54:12'),
(17, 'PAYE', 'Nouveau', '2021-07-25 18:54:47', '2021-07-25 18:54:47'),
(18, 'NON_PAYE', 'Redoublant', '2021-07-25 18:55:39', '2021-07-25 18:55:39'),
(19, 'PAYE', 'Nouveau', '2021-07-25 18:55:54', '2021-07-25 18:55:54'),
(20, 'PAYE', 'Nouveau', '2021-07-25 18:56:50', '2021-07-25 18:56:50'),
(21, 'PAYE', 'Nouveau', '2021-07-25 18:58:07', '2021-07-25 18:58:07'),
(22, 'NON_PAYE', 'Nouveau', '2021-07-25 18:58:48', '2021-07-25 18:58:48'),
(23, 'PAYE', 'Nouveau', '2021-07-25 18:58:50', '2021-07-25 18:58:50'),
(24, 'NON_PAYE', 'Nouveau', '2021-07-25 19:02:44', '2021-07-25 19:02:44'),
(25, 'PAYE', 'Nouveau', '2021-07-25 19:02:56', '2021-07-25 19:02:56'),
(26, 'PAYE', 'Nouveau', '2021-07-25 19:04:18', '2021-07-25 19:04:18'),
(27, 'PAYE', 'Nouveau', '2021-07-25 19:05:24', '2021-07-25 19:05:24'),
(28, 'PAYE', 'Nouveau', '2021-07-25 21:33:20', '2021-07-25 21:33:20'),
(29, 'PAYE', 'Nouveau', '2021-07-25 21:34:46', '2021-07-25 21:34:46'),
(30, 'PAYE', 'Nouveau', '2021-07-25 21:36:01', '2021-07-25 21:36:01'),
(31, 'NON_PAYE', 'Redoublant', '2021-07-25 21:39:26', '2021-07-25 21:39:26'),
(32, 'PAYE', 'Nouveau', '2021-07-25 21:41:52', '2021-07-25 21:41:52'),
(33, 'PAYE', 'Nouveau', '2021-07-25 21:44:00', '2021-07-25 21:44:00'),
(34, 'PAYE', 'Nouveau', '2021-07-25 21:46:28', '2021-07-25 21:46:28'),
(35, 'PAYE', 'Nouveau', '2021-07-25 21:47:44', '2021-07-25 21:47:44'),
(36, 'PAYE', 'Nouveau', '2021-07-25 21:49:57', '2021-07-25 21:49:57'),
(37, 'PAYE', 'Redoublant', '2021-07-25 21:53:16', '2021-07-25 21:53:16'),
(38, 'PAYE', 'Nouveau', '2021-07-25 21:54:36', '2021-07-25 21:54:36'),
(39, 'PAYE', 'Nouveau', '2021-07-25 21:56:08', '2021-07-25 21:56:08'),
(40, 'PAYE', 'Nouveau', '2021-07-25 21:57:58', '2021-07-25 21:57:58'),
(41, 'PAYE', 'Nouveau', '2021-07-25 21:59:27', '2021-07-25 21:59:27'),
(42, 'PAYE', 'Nouveau', '2021-07-25 22:01:45', '2021-07-25 22:01:45'),
(43, 'PAYE', 'Nouveau', '2021-07-25 22:13:14', '2021-07-25 22:13:14'),
(44, 'PAYE', 'Nouveau', '2021-07-25 22:15:36', '2021-07-25 22:15:36'),
(45, 'PAYE', 'Nouveau', '2021-07-25 22:17:01', '2021-07-25 22:17:01'),
(46, 'PAYE', 'Nouveau', '2021-07-25 22:18:53', '2021-07-25 22:18:53'),
(47, 'PAYE', 'Nouveau', '2021-07-25 22:20:39', '2021-07-25 22:20:39'),
(48, 'PAYE', 'Nouveau', '2021-07-25 22:22:44', '2021-07-25 22:22:44'),
(49, 'PAYE', 'Nouveau', '2021-07-25 22:49:55', '2021-07-25 22:49:55'),
(50, 'PAYE', 'Nouveau', '2021-07-25 23:01:39', '2021-07-25 23:01:39'),
(51, 'PAYE', 'Nouveau', '2021-07-25 23:03:09', '2021-07-25 23:03:09'),
(52, 'PAYE', 'Nouveau', '2021-07-25 23:04:45', '2021-07-25 23:04:45'),
(53, 'PAYE', 'Nouveau', '2021-07-25 23:06:21', '2021-07-25 23:06:21'),
(54, 'PAYE', 'Nouveau', '2021-07-25 23:08:27', '2021-07-25 23:08:27'),
(55, 'PAYE', 'Nouveau', '2021-07-25 23:10:03', '2021-07-25 23:10:03'),
(56, 'PAYE', 'Nouveau', '2021-07-25 23:12:20', '2021-07-25 23:12:20'),
(57, 'PAYE', 'Nouveau', '2021-07-25 23:14:10', '2021-07-25 23:14:10'),
(58, 'PAYE', 'Nouveau', '2021-07-25 23:15:57', '2021-07-25 23:15:57'),
(59, 'PAYE', 'Nouveau', '2021-07-25 23:17:55', '2021-07-25 23:17:55'),
(60, 'PAYE', 'Nouveau', '2021-07-25 23:19:50', '2021-07-25 23:19:50'),
(61, 'PAYE', 'Nouveau', '2021-07-25 23:21:07', '2021-07-25 23:21:07'),
(62, 'PAYE', 'Nouveau', '2021-07-25 23:22:20', '2021-07-25 23:22:20'),
(63, 'PAYE', 'Nouveau', '2021-07-25 23:24:14', '2021-07-25 23:24:14'),
(64, 'PAYE', 'Nouveau', '2021-07-25 23:27:33', '2021-07-25 23:27:33'),
(65, 'PAYE', 'Nouveau', '2021-07-25 23:30:46', '2021-07-25 23:30:46'),
(66, 'PAYE', 'Nouveau', '2021-07-25 23:32:13', '2021-07-25 23:32:13'),
(67, 'PAYE', 'Nouveau', '2021-07-25 23:34:23', '2021-07-25 23:34:23'),
(68, 'PAYE', 'Nouveau', '2021-07-25 23:35:43', '2021-07-25 23:35:43'),
(69, 'PAYE', 'Nouveau', '2021-07-25 23:38:05', '2021-07-25 23:38:05'),
(70, 'PAYE', 'Nouveau', '2021-07-25 23:39:28', '2021-07-25 23:39:28'),
(71, 'PAYE', 'Nouveau', '2021-07-25 23:41:04', '2021-07-25 23:41:04'),
(72, 'PAYE', 'Nouveau', '2021-07-25 23:43:04', '2021-07-25 23:43:04'),
(73, 'PAYE', 'Redoublant', '2021-07-25 23:44:50', '2021-07-25 23:44:50'),
(74, 'PAYE', 'Nouveau', '2021-07-25 23:46:54', '2021-07-25 23:46:54'),
(75, 'PAYE', 'Redoublant', '2021-07-25 23:50:17', '2021-07-25 23:50:17'),
(76, 'PAYE', 'Nouveau', '2021-07-25 23:51:40', '2021-07-25 23:51:40'),
(77, 'PAYE', 'Nouveau', '2021-07-25 23:55:30', '2021-07-25 23:55:30'),
(78, 'PAYE', 'Nouveau', '2021-07-25 23:57:03', '2021-07-25 23:57:03'),
(79, 'PAYE', 'Nouveau', '2021-07-25 23:58:29', '2021-07-25 23:58:29'),
(80, 'PAYE', 'Nouveau', '2021-07-26 00:00:06', '2021-07-26 00:00:06'),
(81, 'PAYE', 'Nouveau', '2021-07-26 00:01:35', '2021-07-26 00:01:35'),
(82, 'PAYE', 'Nouveau', '2021-07-26 00:04:04', '2021-07-26 00:04:04'),
(83, 'PAYE', 'Nouveau', '2021-07-26 00:06:39', '2021-07-26 00:06:39'),
(84, 'PAYE', 'Nouveau', '2021-07-26 00:08:14', '2021-07-26 00:08:14'),
(85, 'PAYE', 'Nouveau', '2021-07-26 00:09:55', '2021-07-26 00:09:55'),
(86, 'PAYE', 'Nouveau', '2021-07-26 00:13:28', '2021-07-26 00:13:28'),
(87, 'PAYE', 'Redoublant', '2021-07-26 00:39:02', '2021-07-26 00:39:02'),
(88, 'PAYE', 'Nouveau', '2021-07-26 00:40:54', '2021-07-26 00:40:54'),
(89, 'PAYE', 'Nouveau', '2021-07-26 00:42:03', '2021-07-26 00:42:03'),
(90, 'PAYE', 'Nouveau', '2021-07-26 00:43:54', '2021-07-26 00:43:54'),
(91, 'PAYE', 'Nouveau', '2021-07-26 00:52:36', '2021-07-26 00:52:36'),
(92, 'PAYE', 'Redoublant', '2021-07-26 00:54:40', '2021-07-26 00:54:40'),
(93, 'PAYE', 'Nouveau', '2021-07-26 00:56:03', '2021-07-26 00:56:03'),
(94, 'PAYE', 'Nouveau', '2021-07-26 00:58:27', '2021-07-26 00:58:27'),
(95, 'PAYE', 'Nouveau', '2021-07-26 01:04:09', '2021-07-26 01:04:09'),
(96, 'PAYE', 'Nouveau', '2021-07-26 01:12:05', '2021-07-26 01:12:05'),
(97, 'PAYE', 'Nouveau', '2021-07-26 01:15:38', '2021-07-26 01:15:38');

-- --------------------------------------------------------

--
-- Structure de la table `fautes`
--

DROP TABLE IF EXISTS `fautes`;
CREATE TABLE IF NOT EXISTS `fautes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `Gravite` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `fautes`
--

INSERT INTO `fautes` (`id`, `nom`, `Gravite`, `dateInsert`, `dateModif`) VALUES
(8, 'voyou', 'AVERTISSEMENT', '2021-07-22 23:43:52', '2021-07-22 23:43:52'),
(11, 'insulte', 'BLAME', '2021-07-23 09:52:46', '2021-07-23 09:52:46'),
(27, 'test Cosmas', 'GRAVE', '2021-07-25 13:55:13', '2021-07-25 13:55:13'),
(18, 'bandit', 'EXCLUSION PONCTUELLE', '2021-07-24 06:45:35', '2021-07-24 06:45:35'),
(20, 'Interreaant', 'TOLERABLE', '2021-07-24 12:24:23', '2021-07-24 12:24:23'),
(24, 'vrv', 'TOLERABLE', '2021-07-24 13:38:28', '2021-07-24 13:38:28'),
(25, 'toto', 'MOYENNE', '2021-07-24 13:47:58', '2021-07-24 13:47:58'),
(26, 'negligence', 'TOLERABLE', '2021-07-24 21:10:25', '2021-07-24 21:10:25'),
(28, 'Sortie des cours', 'TOLERABLE', '2021-07-26 00:13:14', '2021-07-26 00:13:14'),
(29, 'Bagarrre', 'TOLERABLE', '2021-07-26 05:25:39', '2021-07-26 05:25:39'),
(30, 'Chant', 'TOLERABLE', '2021-07-26 05:34:55', '2021-07-26 05:34:55'),
(31, 'Danse', 'TOLERABLE', '2021-07-26 06:11:40', '2021-07-26 06:11:40');

-- --------------------------------------------------------

--
-- Structure de la table `membre_conseil`
--

DROP TABLE IF EXISTS `membre_conseil`;
CREATE TABLE IF NOT EXISTS `membre_conseil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_liste` date NOT NULL,
  `id_chef` int(11) NOT NULL,
  `id_surveillant_G` int(11) NOT NULL,
  `id_representant_E` int(11) NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` text NOT NULL,
  `view` int(11) NOT NULL,
  `id_emeteur` int(11) NOT NULL,
  `nom_emeteur` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `notification`
--

INSERT INTO `notification` (`id`, `libelle`, `view`, `id_emeteur`, `nom_emeteur`, `dateInsert`, `dateModif`) VALUES
(1, 'test', 0, 1, 'Maestros', '2021-07-24 12:20:55', '2021-07-24 12:20:55'),
(2, 'test', 0, 1, 'Maestros', '2021-07-24 12:21:54', '2021-07-24 12:21:54'),
(3, 'test', 0, 1, 'Maestros', '2021-07-24 12:23:54', '2021-07-24 12:23:54'),
(4, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 1, 'Kenfack', '2021-07-24 13:47:58', '2021-07-24 13:47:58'),
(5, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 1, 'Kenfack', '2021-07-24 21:10:25', '2021-07-24 21:10:25'),
(6, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 1, 'Kenfack', '2021-07-25 13:55:14', '2021-07-25 13:55:14'),
(7, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 19, 'Didier', '2021-07-26 00:13:14', '2021-07-26 00:13:14'),
(8, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 1, 'Kenfack', '2021-07-26 05:25:39', '2021-07-26 05:25:39'),
(9, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 1, 'Kenfack', '2021-07-26 05:34:55', '2021-07-26 05:34:55'),
(10, 'Une faute a Ã©tÃ© signalÃ©e concerant cette rÃ¨gle__ Article: ', 0, 1, 'Kenfack', '2021-07-26 06:11:40', '2021-07-26 06:11:40');

-- --------------------------------------------------------

--
-- Structure de la table `parents`
--

DROP TABLE IF EXISTS `parents`;
CREATE TABLE IF NOT EXISTS `parents` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profession` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `parents`
--

INSERT INTO `parents` (`id`, `profession`, `dateInsert`, `dateModif`) VALUES
(1, 'agriculteur', '2021-07-24 08:33:56', '2021-07-24 08:33:56'),
(2, 'chauffeur', '2021-07-26 02:42:00', '2021-07-26 02:42:00');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `date_de_naisssancce` date NOT NULL,
  `login_` text NOT NULL,
  `mot_de_passe` text NOT NULL,
  `lieu_de_naissance_` text NOT NULL,
  `photo` text NOT NULL,
  `email` text NOT NULL,
  `sexe` text NOT NULL,
  `Tel` int(11) NOT NULL,
  `role` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`id`, `nom`, `prenom`, `date_de_naisssancce`, `login_`, `mot_de_passe`, `lieu_de_naissance_`, `photo`, `email`, `sexe`, `Tel`, `role`, `dateInsert`, `dateModif`) VALUES
(63, 'Lucie', 'Ouinra', '2012-02-15', 'lucie', 'toto', 'N\'djamena', 'picture', 'lucie@gmail.com', 'Feminin', 8347938, '5', '2021-07-25 23:03:09', '2021-07-25 23:03:09'),
(62, 'Maurice', 'Tankeu', '2013-07-11', 'tankeu', 'toto', 'Douala', 'picture', 'fer@gmail.com', 'Masculin', 66874344, '5', '2021-07-25 23:01:39', '2021-07-25 23:01:39'),
(26, 'Simo', 'Urielle', '2010-08-04', 'simo', 'toto', 'foumban', 'pas encore', 's@gmail.com', 'Feminin', 696271416, '5', '2021-07-25 18:54:12', '2021-07-25 18:54:12'),
(25, 'Franck', 'Lambert', '2021-07-02', 'Franc', 'toto', 'Dschang', 'pas encore', 'franc@gmail.com', 'Masculin', 124555, '5', '2021-07-25 18:52:44', '2021-07-25 18:52:44'),
(24, 'donfack', 'clarisse', '1844-07-06', 'dg', 'sd', 'dschang', 'pas', 'dz@gmail.com', 'Feminin', 123, '5', '2021-07-25 18:51:59', '2021-07-25 18:51:59'),
(23, 'Kenfack', 'Ismael', '2000-07-01', 'kenfis', 'toto', 'Dschang', 'pas encore', 'kis@gmail.com', 'Masculin', 123456, '5', '2021-07-25 18:50:50', '2021-07-25 18:50:50'),
(22, 'DjinGab\'s', 'mon', '2000-07-07', 'd', 'sd', 'guelo', 'pas', 'd@gmail.com', 'Feminin', 123, '5', '2021-07-25 18:49:26', '2021-07-25 18:49:26'),
(21, 'Vouchakbe', 'Alexis', '1935-07-14', 'dd', 'toto', 'lere', 'pas', 'd@gmail.com', 'Masculin', 213, '5', '2021-07-25 18:47:14', '2021-07-25 18:47:14'),
(9, 'Kenfack Dongmo', 'Kevin', '2021-07-14', 'perso1', 'toto', 'Douala', 'pic', 'perso@gmail.com', 'Masculin', 1223, '2', '2021-07-24 05:29:25', '2021-07-24 05:29:25'),
(11, 'Zebaze Tsobny', 'Juvernal', '2021-07-23', 'perso3', 'toto', 'Tchibanga', 'picture', 'perso3@gmail.com', 'Masculin', 696271416, '3', '2021-07-24 05:32:50', '2021-07-24 05:32:50'),
(12, 'Temfack', 'Derick', '2021-07-22', 'prof3', 'toto', 'Douala', 'picture', 'prof3@gmail.com', 'Masculin', 31344, '4', '2021-07-24 07:52:32', '2021-07-24 07:52:32'),
(13, 'Nguemechia', 'Kevin', '2021-07-16', 'parent1', 'toto', 'Douala', 'pic', 'parent1@gmail.com', 'Masculin', 1231132, '6', '2021-07-24 08:33:56', '2021-07-24 08:33:56'),
(20, 'Hassane', 'Moustapha', '1996-05-08', 'HM', 'hm', 'Ndjamena', 'pas', 'hn@gmail.Vom', 'Masculin', 343456, '5', '2021-07-25 18:44:37', '2021-07-25 18:44:37'),
(112, 'kannahbet', 'Zeuh', '1998-07-07', 'unique', 'toto', 'N\'djamena', 'pas encore', 'zeuh@gmail.com', 'Masculin', 565656, '4', '2021-07-26 05:07:46', '2021-07-26 05:07:46'),
(111, 'Urielle', 'Jacques', '1990-07-18', 'jacque', 'toto', 'Dschang', 'picture', 'jaque@gmail.com', 'Masculin', 34536547, '6', '2021-07-26 02:42:00', '2021-07-26 02:42:00'),
(27, 'kannahbet', 'zeuh', '2021-07-04', 'zeuh', 'toto', 'N\'djamena', 'pas encore', 'kan@gmail.com', 'Masculin', 123345667, '5', '2021-07-25 18:54:47', '2021-07-25 18:54:47'),
(28, 'moustapha', 'charphadine', '2015-07-13', 'df', 'fg', 'pala', 'oui', 'm@gmail.com', 'Masculin', 345678899, '5', '2021-07-25 18:55:39', '2021-07-25 18:55:39'),
(29, 'Merveille', 'simo', '2021-07-04', 'simo', 'toto', 'Dschang', 'pas encore', 'simo@gmail.com', 'Masculin', 1245654, '5', '2021-07-25 18:55:54', '2021-07-25 18:55:54'),
(30, 'KENE ', 'Valdese', '2021-07-06', 'kene', 'toto', 'Douala', 'pas encore', 'kene@gmail.com', 'Masculin', 123445, '1', '2021-07-25 18:56:50', '2021-07-25 18:56:50'),
(31, 'DIDACE', 'KENFACK', '2021-07-08', 'didace', 'toto', 'Dschang', 'pas encore', 'didace@gmail.com', 'Masculin', 123344555, '5', '2021-07-25 18:58:07', '2021-07-25 18:58:07'),
(32, 'larassem', 'simplice', '2015-07-21', 'df', 'xc', 'pala', 'non', 'hn@gmail.Vom', 'Masculin', 897, '5', '2021-07-25 18:58:48', '2021-07-25 18:58:48'),
(33, 'KAMGRE', 'Maurelle', '2010-07-23', 'maurelle', 'toto', 'Douala', 'pas encore', 'm@gmail.com', 'Feminin', 653447633, '5', '2021-07-25 18:58:50', '2021-07-25 18:58:50'),
(34, 'hassanne', 'moustapha', '2021-07-14', 'kurohige', 'toto', 'N\'djamena', 'pas encore', 'f@gmail.com', 'Masculin', 123456, '1', '2021-07-25 19:01:33', '2021-07-25 19:01:33'),
(35, 'djdj', 'welp', '2021-07-01', 'hh', 'toto', 'sdf', 'gg', 'g@gmail', 'Masculin', 34455, '5', '2021-07-25 19:02:44', '2021-07-25 19:02:44'),
(36, 'Kamga', 'Yvan', '2009-07-19', 'yvan', 'toto', 'Dschang', 'pas encore', 'y@gmail.com', 'Masculin', 675404323, '5', '2021-07-25 19:02:56', '2021-07-25 19:02:56'),
(37, 'Didier', 'Deschamp', '2021-07-08', 'didier', 'TOTO', 'Dschang', 'pas encore', 'f@gmail.com', 'Masculin', 1234556, '5', '2021-07-25 19:04:18', '2021-07-25 19:04:18'),
(38, 'deko', 'fabrice', '2017-07-11', 'sd', 'toto', 'Douala', 'xc', 'c@gmail.com', 'Masculin', 23334, '5', '2021-07-25 19:05:24', '2021-07-25 19:05:24'),
(39, 'Franco', 'dzoyem', '2021-07-08', 'franco', 'toto', 'Dschang', 'pas encore', 'f@gmail.com', 'Masculin', 12323, '5', '2021-07-25 21:33:20', '2021-07-25 21:33:20'),
(40, 'Maestros', 'Bedel', '2018-05-09', 'bedel', 'toto', 'Dschang', 'pas encore', 'g@gmail.com', 'Masculin', 1234312, '5', '2021-07-25 21:34:46', '2021-07-25 21:34:46'),
(41, 'Fauka', 'janvier', '2008-07-10', 'fauka', 'toto', 'N\'djamena', 'pas encore', 'djdj@gmail.com', 'Masculin', 23333, '5', '2021-07-25 21:36:01', '2021-07-25 21:36:01'),
(42, 'Lilianne', 'sabine', '2008-06-04', 'liliane', 'toto', 'Douala', 'pas encore', 'lili@gmail.com', 'Feminin', 2465772, '5', '2021-07-25 21:39:26', '2021-07-25 21:39:26'),
(43, 'FelecitÃ©', 'Ouinra', '2013-06-12', 'ouinra', 'toto', 'N\'djamena', 'pas encore', 'ouinra@gmail.com', 'Feminin', 2345678, '5', '2021-07-25 21:41:52', '2021-07-25 21:41:52'),
(44, 'Sabine', 'laure', '2008-06-11', 'laure', 'toto', 'Moundou', 'pas encore', 'laure@gmail.com', 'Feminin', 658587678, '5', '2021-07-25 21:44:00', '2021-07-25 21:44:00'),
(45, 'Sabine', 'Chirac', '2021-07-08', 'chirac', 'toto', 'Abeche', 'pas encore', 'f@gmail.com', 'Feminin', 14426, '5', '2021-07-25 21:46:28', '2021-07-25 21:46:28'),
(46, 'Chantal', 'Koutou', '2010-06-17', 'koutou', 'toto', 'Moundou', 'picture', 'lic@gmail.com', 'Feminin', 3345467, '5', '2021-07-25 21:47:44', '2021-07-25 21:47:44'),
(47, 'Kaltouma', 'Nambaye', '2014-07-08', 'nana', 'toto', 'Moundou', 'picture', 'pic@gmail.com', 'Feminin', 767689, '5', '2021-07-25 21:49:57', '2021-07-25 21:49:57'),
(48, 'Jacqueline', 'Didac', '2015-05-06', 'didi', 'toto', 'Pala', 'pas encore', 'didi@gmail.com', 'Feminin', 567689, '5', '2021-07-25 21:53:16', '2021-07-25 21:53:16'),
(49, 'Dorine', 'palaye', '2016-06-15', 'dorine', 'toto', 'Pala', 'picture', 'pala@gmail.com', 'Feminin', 8969780, '5', '2021-07-25 21:54:36', '2021-07-25 21:54:36'),
(50, 'Gakdang', 'Rufine', '2012-07-11', 'gakdang', 'toto', 'N\'djamena', 'picture', 'gak@gmail', 'Feminin', 456789, '5', '2021-07-25 21:56:08', '2021-07-25 21:56:08'),
(51, 'Chippie', 'zeuh', '2013-06-13', 'chippie', 'toto', 'Moundou', 'picture', 'chippie@gmail.com', 'Feminin', 766772389, '5', '2021-07-25 21:57:58', '2021-07-25 21:57:58'),
(52, 'Franck', 'Lampard', '2021-07-22', 'lamp', 'toto', 'Moundou', 'picture', 'lamp@gmail.com', 'Masculin', 456778, '1', '2021-07-25 21:59:27', '2021-07-25 21:59:27'),
(53, 'Cecille', 'Bonivant', '2007-06-13', 'cecille', 'toto', 'Santchou', 'picture', 'cecile@gmail.com', 'Feminin', 567899, '5', '2021-07-25 22:01:45', '2021-07-25 22:01:45'),
(54, 'Flore', 'Chanceline', '2012-03-14', 'chance', 'toto', 'Foto', 'picture', 'chance@gmail.com', 'Feminin', 67257757, '5', '2021-07-25 22:13:14', '2021-07-25 22:13:14'),
(55, 'Withney', 'Chape', '2007-07-04', 'chape', 'toto', 'Foreke', 'picture', 'pic@gmail.com', 'Masculin', 676683478, '5', '2021-07-25 22:15:36', '2021-07-25 22:15:36'),
(56, 'Charlie', 'Tankeu', '2016-02-10', 'charlie', 'toto', 'Santchou', 'picture', 'charlie@gmail.com', 'Feminin', 456768909, '5', '2021-07-25 22:17:01', '2021-07-25 22:17:01'),
(57, 'Cyrille', 'Batchou', '2007-09-27', 'cyrille', 'toto', 'Foto', 'picture', 'cyrille@gmail.com', 'Masculin', 67538933, '5', '2021-07-25 22:18:53', '2021-07-25 22:18:53'),
(58, 'Derrick', 'pantchou', '2009-06-19', 'derrick', 'toto', 'Dschang', 'picture', 'derric@gmail.com', 'Masculin', 786943098, '5', '2021-07-25 22:20:39', '2021-07-25 22:20:39'),
(59, 'Bedel', 'Franck', '2011-06-09', 'FranBe', 'toto', 'Dschang', 'picture', 'fran@gmail.com', 'Masculin', 766879830, '5', '2021-07-25 22:22:44', '2021-07-25 22:22:44'),
(60, 'Singuila', 'rosignol', '2013-07-18', 'rosignol', 'toto', 'Dschang', 'picture', 'rosignol@gmail.com', 'Masculin', 676787344, '5', '2021-07-25 22:49:55', '2021-07-25 22:49:55'),
(61, 'Dongmo', 'Alexie', '2021-07-22', 'prof', 'toto', 'Douala', 'picture', 'toto@gmail.com', 'Masculin', 696271416, '4', '2021-07-25 22:50:22', '2021-07-25 22:50:22'),
(64, 'Samantha', 'Wampi', '2015-05-29', 'wampi', 'toto', 'Pala', 'picture', 'pic@gmail.com', 'Feminin', 567689, '5', '2021-07-25 23:04:45', '2021-07-25 23:04:45'),
(65, 'Samira', 'Adam', '2010-06-28', 'samira', 'toto', 'Abeche', 'picture', 'samira@gmail.com', 'Feminin', 65743646, '5', '2021-07-25 23:06:21', '2021-07-25 23:06:21'),
(66, 'Falmata', 'Sara', '2008-07-31', 'sara', 'toto', 'Abeche', 'picture', 'fran@gmail.com', 'Feminin', 5768946, '5', '2021-07-25 23:08:27', '2021-07-25 23:08:27'),
(67, 'Sonia', 'Hadjara', '2008-07-29', 'sonia', 'toto', 'N\'djamena', 'picture', 'sonia@gmail.com', 'Feminin', 576839534, '5', '2021-07-25 23:10:03', '2021-07-25 23:10:03'),
(68, 'FranÃ§ois ', 'Ndjekombe', '2013-07-25', 'francois', 'toto', 'Moundou', 'picture', 'francois@gmail.com', 'Masculin', 7689634, '5', '2021-07-25 23:12:20', '2021-07-25 23:12:20'),
(69, 'Monica', 'Shurle', '2012-07-05', 'monica', 'toto', 'Foreke', 'picture', 'monica@gmail.com', 'Feminin', 666874, '5', '2021-07-25 23:14:10', '2021-07-25 23:14:10'),
(70, 'Franck', 'Dimaria', '2014-10-24', 'dimaria', 'toto', 'N\'djamena', 'picture', 'dimaria@gmail.com', 'Masculin', 7687925, '5', '2021-07-25 23:15:57', '2021-07-25 23:15:57'),
(71, 'Robert', 'Dzoyem', '2009-08-11', 'robert', 'toto', 'Foto', 'picture', 'robert@gmail.com', 'Masculin', 6643484, '5', '2021-07-25 23:17:55', '2021-07-25 23:17:55'),
(72, 'Patrick', 'Evra Simeon', '2009-06-29', 'patrick', 'toto', 'Pala', 'picture', 'patrick@gmail.com', 'Masculin', 8874592, '5', '2021-07-25 23:19:50', '2021-07-25 23:19:50'),
(73, 'Varanne', 'Appolinaire', '2005-07-26', 'varrane', 'toto', 'Moundou', 'picture', 'varrane@gmail.com', 'Masculin', 567899, '5', '2021-07-25 23:21:07', '2021-07-25 23:21:07'),
(74, 'Dannielle', 'Sonia', '2005-07-29', 'danielle', 'toto', 'Santchou', 'picture', 'danielle@gmail.com', 'Feminin', 7890, '5', '2021-07-25 23:22:20', '2021-07-25 23:22:20'),
(75, 'Sensey', 'Zeuh', '2012-07-07', 'sensey', 'toto', 'Foto', 'picture', 'maestros@gmail.com', 'Masculin', 5879, '5', '2021-07-25 23:24:14', '2021-07-25 23:24:14'),
(76, 'Kobobe', 'Sift', '2013-05-23', 'kobobe', 'toto', 'Pala', 'picture', 'kobobe@gmail.com', 'Masculin', 2345678, '5', '2021-07-25 23:27:33', '2021-07-25 23:27:33'),
(77, 'Evra', 'Simeon', '2012-08-27', 'evra', 'toto', 'Sarh', 'picture', 'sarh@gmail.com', 'Masculin', 768790, '5', '2021-07-25 23:30:46', '2021-07-25 23:30:46'),
(78, 'FranÃ§ois', 'Didier', '2021-07-12', 'didierFranc', 'toto', 'Bamenda', 'picture', 'bam@gmail.com', 'Feminin', 89666, '5', '2021-07-25 23:32:13', '2021-07-25 23:32:13'),
(79, 'Bedel', 'Zeuh', '2010-09-09', 'bezeuh', 'toto', 'Bol', 'picture', 'bol@gmail.com', 'Masculin', 5768790, '5', '2021-07-25 23:34:23', '2021-07-25 23:34:23'),
(80, 'Jessica', 'Saurelle', '2005-07-12', 'saurelle', 'toto', 'Pala', 'picture', 'pala@gmail.com', 'Feminin', 576889, '5', '2021-07-25 23:35:43', '2021-07-25 23:35:43'),
(81, 'Arol', 'Shaf', '2009-12-01', 'shaf', 'toto', 'Santchou', 'picture', 'shaf@gmail', 'Masculin', 2334444, '1', '2021-07-25 23:38:05', '2021-07-25 23:38:05'),
(82, 'Chalie ', 'Tarlem', '2005-03-29', 'tarlem', 'toto', 'Farcha', 'picture', 'pic@gmail.com', 'Feminin', 6546664, '5', '2021-07-25 23:39:28', '2021-07-25 23:39:28'),
(83, 'Destin', 'patouki', '2013-09-02', 'patouki', 'toto', 'Pala', 'picture', 'welp@gmail.com', 'Masculin', 335434, '5', '2021-07-25 23:41:04', '2021-07-25 23:41:04'),
(84, 'Dante', 'Lampard', '2009-08-18', 'dante', 'toto', 'Santchou', 'picture', 'chippie@gmail.com', 'Masculin', 2442244, '5', '2021-07-25 23:43:04', '2021-07-25 23:43:04'),
(85, 'Kaltouma', 'Ache', '2006-08-14', 'ache', 'toto', 'Dschang', 'picture', 'cecile@gmail.com', 'Feminin', 324343, '5', '2021-07-25 23:44:50', '2021-07-25 23:44:50'),
(86, 'Gignac', 'Pierre', '2005-09-27', 'pierre', 'toto', 'Santchou', 'picture', 'PIERRE@gmail.com', 'Masculin', 412424, '5', '2021-07-25 23:46:54', '2021-07-25 23:46:54'),
(87, 'kaye', 'varrane', '2005-09-12', 'kaye', 'toto', 'N\'djamena', 'picture', 'kaye@gmail.com', 'Masculin', 23343, '5', '2021-07-25 23:50:17', '2021-07-25 23:50:17'),
(88, 'Alonzo', 'Dzoyem', '2013-09-12', 'alonzo', 'toto', 'Pala', 'picture', 'alonzo@gmail.com', 'Masculin', 343434, '5', '2021-07-25 23:51:40', '2021-07-25 23:51:40'),
(89, 'Pasto', 'Soft', '2013-10-01', 'pasto', 'toto', 'Paris', 'picture', 'feed@gmail.com', 'Masculin', 343434, '5', '2021-07-25 23:55:30', '2021-07-25 23:55:30'),
(90, 'Fred', 'leroy', '2008-03-27', 'fred', 'toto', 'Bol', 'picture', 'free@gmail.com', 'Masculin', 3435554, '5', '2021-07-25 23:57:03', '2021-07-25 23:57:03'),
(91, 'Olin', 'Mbaye', '2005-09-16', 'olin', 'toto', 'Bamenda', 'picture', 'fees@gmail.com', 'Masculin', 446454, '5', '2021-07-25 23:58:29', '2021-07-25 23:58:29'),
(92, 'George', 'path', '2010-09-01', 'george', 'toto', 'N\'djamena', 'picture', 'pic@gmail.com', 'Masculin', 24343434, '5', '2021-07-26 00:00:06', '2021-07-26 00:00:06'),
(93, 'Emmanuel', 'Franck', '2011-12-21', 'emmanuel', 'toto', 'Suisse', 'picture', 'pic@gmail.com', 'Masculin', 354565, '5', '2021-07-26 00:01:35', '2021-07-26 00:01:35'),
(94, 'Josua', 'Bani', '2008-07-28', 'bani', 'toto', 'Paris', 'picture', 'bani@gmail.comm', 'Masculin', 33354556, '5', '2021-07-26 00:04:04', '2021-07-26 00:04:04'),
(95, 'Evra', 'Macron', '2005-09-21', 'macron', 'toto', 'Bush', 'picture', 'kanzeuh23@gmail.com', 'Masculin', 345657, '5', '2021-07-26 00:06:39', '2021-07-26 00:06:39'),
(96, 'Bush', 'paul shaf', '2009-09-15', 'bush', 'toto', 'Pala', 'picture', 'bush@gmail.com', 'Masculin', 34433456, '5', '2021-07-26 00:08:14', '2021-07-26 00:08:14'),
(97, 'Pollin', 'Roger', '2009-09-30', 'pollin', 'tooto', 'N\'djamena', 'picture', 'pollin', 'Masculin', 34356, '5', '2021-07-26 00:09:55', '2021-07-26 00:09:55'),
(98, 'Sara', 'Debel', '2005-09-21', 'debel', 'toto', 'Farcha', 'picture', 'debel@gmail.com', 'Feminin', 45454545, '5', '2021-07-26 00:13:28', '2021-07-26 00:13:28'),
(99, 'Roger', 'Mila', '2005-09-29', 'mila', 'toto', 'Santchou', 'picture', 'mial@gmail.com', 'Masculin', 7634636, '5', '2021-07-26 00:39:02', '2021-07-26 00:39:02'),
(100, 'Henry', 'Prince Duchel', '2001-09-27', 'duchel', 'toto', 'Suisse', 'picture', 'duchel', 'Masculin', 78899, '5', '2021-07-26 00:40:54', '2021-07-26 00:40:54'),
(101, 'Prince', 'Harry', '2002-09-17', 'harry', 'toto', 'Pala', 'picture', 'harry', 'Masculin', 56768734, '5', '2021-07-26 00:42:03', '2021-07-26 00:42:03'),
(102, 'Thomas', 'Charlem Kamdem', '2013-08-19', 'kamdem', 'toto', 'Pala', 'picture', 'kamdem@gmail.com', 'Masculin', 8796056, '5', '2021-07-26 00:43:54', '2021-07-26 00:43:54'),
(103, 'Brenda', 'Roslyn', '2009-06-29', 'brenda', 'toto', 'Dschang', 'picture', 'brenda@gmail.com', 'Feminin', 67647646, '5', '2021-07-26 00:52:36', '2021-07-26 00:52:36'),
(104, 'Shakira', 'bondel', '2005-12-15', 'shakira', 'toto', 'Dschang', 'picture', 'shakira@gmail.com', 'Feminin', 4343434, '5', '2021-07-26 00:54:40', '2021-07-26 00:54:40'),
(105, 'Moustapha', 'Adam saleh', '2005-01-19', 'saleh', 'toto', 'Ati', 'picture', 'saleh@gmail.com', 'Masculin', 764545764, '5', '2021-07-26 00:56:03', '2021-07-26 00:56:03'),
(106, 'Ibrahim', 'Abba hassane', '2005-10-19', 'ibrahim', 'toto', 'Ati', 'picture', 'ibrahim@gmail.com', 'Masculin', 45675678, '5', '2021-07-26 00:58:27', '2021-07-26 00:58:27'),
(107, 'Abdel', 'Charfadine', '2005-03-28', 'abdel', 'toto', 'Abeche', 'picture', 'abdel@gmail.com', 'Masculin', 6634666, '5', '2021-07-26 01:04:09', '2021-07-26 01:04:09'),
(108, 'Abdelbakit', 'Danna', '2005-06-20', 'abdelbakit', 'toto', 'Abeche', 'picture', 'abdelbalit@gmail.com', 'Masculin', 56565656, '5', '2021-07-26 01:12:05', '2021-07-26 01:12:05'),
(109, 'Charles', 'Danna', '2005-05-26', 'charles', 'toto', 'Pala', 'picture', 'charles@gmail.com', 'Masculin', 56565656, '5', '2021-07-26 01:15:38', '2021-07-26 01:15:38'),
(110, 'Miguel', 'Charles', '1997-09-25', 'miguelC', 'toto', 'Dschang', 'picture', 'miguel@gmail.com', 'Masculin', 464766, '4', '2021-07-26 02:38:57', '2021-07-26 02:38:57');

-- --------------------------------------------------------

--
-- Structure de la table `personnel`
--

DROP TABLE IF EXISTS `personnel`;
CREATE TABLE IF NOT EXISTS `personnel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fonction` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `personnel`
--

INSERT INTO `personnel` (`id`, `fonction`, `dateInsert`, `dateModif`) VALUES
(1, 'SG', '2021-07-24 05:29:25', '2021-07-24 05:29:25'),
(2, 'cs', '2021-07-24 05:30:50', '2021-07-24 05:30:50'),
(3, 'cs', '2021-07-24 05:32:50', '2021-07-24 05:32:50'),
(4, 'cs', '2021-07-24 22:02:16', '2021-07-24 22:02:16'),
(5, 'Visiteur', '2021-07-25 18:36:36', '2021-07-25 18:36:36'),
(6, 'Visiteur', '2021-07-25 18:40:04', '2021-07-25 18:40:04'),
(7, 'Visiteur', '2021-07-25 19:01:33', '2021-07-25 19:01:33');

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

DROP TABLE IF EXISTS `professeur`;
CREATE TABLE IF NOT EXISTS `professeur` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `statut` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `professeur`
--

INSERT INTO `professeur` (`id`, `statut`, `dateInsert`, `dateModif`) VALUES
(1, 'Celibataire', '2021-07-21 20:44:42', '2021-07-21 20:44:42'),
(2, 'Celibataire', '2021-07-21 20:46:17', '2021-07-21 20:46:17'),
(3, 'Celibataire', '2021-07-24 07:52:32', '2021-07-24 07:52:32'),
(4, 'Celibataire', '2021-07-25 22:50:22', '2021-07-25 22:50:22'),
(5, 'celibataire', '2021-07-26 02:38:57', '2021-07-26 02:38:57'),
(6, 'celibataire', '2021-07-26 05:07:46', '2021-07-26 05:07:46');

-- --------------------------------------------------------

--
-- Structure de la table `reglement_iterieure`
--

DROP TABLE IF EXISTS `reglement_iterieure`;
CREATE TABLE IF NOT EXISTS `reglement_iterieure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEtablissement` varchar(100) NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reglement_iterieure`
--

INSERT INTO `reglement_iterieure` (`id`, `nomEtablissement`, `dateInsert`, `dateModif`) VALUES
(2, 'Etablissement de la Menoua', '2021-07-23 13:45:53', '2021-07-23 13:45:53'),
(3, 'Ecole Normal', '2021-07-23 13:46:13', '2021-07-23 13:46:13'),
(4, 'Lycee Bilingue', '2021-07-23 13:46:50', '2021-07-23 13:46:50');

-- --------------------------------------------------------

--
-- Structure de la table `regles`
--

DROP TABLE IF EXISTS `regles`;
CREATE TABLE IF NOT EXISTS `regles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle_regle` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `regles`
--

INSERT INTO `regles` (`id`, `libelle_regle`, `dateInsert`, `dateModif`) VALUES
(1, 'Le LycÃ©e Bilingue dâ€™Obala est un Ã©tablissement scolaire public et laÃ¯c dâ€™enseignement secondaire gÃ©nÃ©ral divisÃ© en deux sections : la section anglophone et la section francophone. Et, dans chacune de ces sections, on retrouve des classes bilingues. Le rÃ©gime des Ã©tudes est lâ€™Externat.', '2021-07-23 17:47:38', '2021-07-23 17:52:57'),
(2, 'Lâ€™admission au LycÃ©e en classe de 6Ã¨me ou de Â« Form One Â» se fait exclusivement sur Concours Officiel. Un autre concours interne est ensuite organisÃ© (par la dÃ©lÃ©gation rÃ©gionale) Ã  lâ€™issu duquel les admis sont orientÃ©s en 6Ã¨me bilingue et en Â« form one Â» bilingue. Dans toutes les autres classes, tout candidat ayant Ã©tÃ© apte aprÃ¨s un test dâ€™Admission ou Ã©tude de son dossier par le Chef dâ€™Etablissement, assistÃ© de la Commission Permanente, est dÃ©clarÃ© Ã©lÃ¨ve du LycÃ©e Bilingue dâ€™Obala. Lâ€™admission se fait aussi par Transfert pour les apprenants provenant des Ã©tablissements publics', '2021-07-23 17:50:18', '2021-07-23 17:50:18'),
(3, 'La qualitÃ© dâ€™Ã©lÃ¨ve se confirme par lâ€™inscription sur les listes officielles signÃ©es par le chef dâ€™Ã©tablissement, le paiement des contributions exigibles avec dÃ©livrance des reÃ§us homologuÃ©s, dÃ»ment signÃ©s par lâ€™intendant du lycÃ©e', '2021-07-23 17:53:59', '2021-07-23 17:53:59'),
(4, 'Tous les Ã©lÃ¨ves rÃ©guliÃ¨rement, ainsi que les anciens sâ€™Ã©tant acquittÃ©s de leurs devoirs dâ€™inscription acquiÃ¨rent de ce fait la qualitÃ© dâ€™Ã©lÃ¨ves Ã  part entiÃ¨re du lycÃ©e bilingue dâ€™Obala pour lâ€™annÃ©e scolaire en cours.', '2021-07-23 17:54:24', '2021-07-23 17:54:24'),
(5, 'Une fois les modalitÃ©s dâ€™admission sus mentionnÃ©s remplies, une autorisation dâ€™entrÃ©e est dÃ©livrÃ©e Ã  lâ€™Ã©lÃ¨ve par le surveillant gÃ©nÃ©ral, aprÃ¨s un engagement Ã©crit de lâ€™Ã©lÃ¨ve Ã  respecter le rÃ¨glement intÃ©rieur du lycÃ©e.', '2021-07-23 17:54:42', '2021-07-23 17:54:42'),
(6, 'Perd automatiquement la qualitÃ© dâ€™Ã©lÃ¨ve du LycÃ©e Bilingue dâ€™Obala, tout apprenant : * Nâ€™ayant pas rejoint lâ€™Ã©tablissement dans les trente (30) jours qui suivent son admission. * Ne sâ€™Ã©tant pas acquittÃ© de ses contributions exigibles dans les dÃ©lais prescrits. * Ayant renoncÃ© volontairement Ã  suivre les cours ou Ã  se prÃ©senter aux Ã©valuations. * Nâ€™ayant pas obtenu une moyenne au moins Ã©gale Ã  07,00/20. * Ayant Ã©tÃ© frappÃ© par la limite dâ€™Ã¢ge en fin dâ€™annÃ©e. * Dont le comportement ne donne pas satisfaction ou qui est jugÃ© comme tel par le Conseil de Discipline.', '2021-07-23 17:55:13', '2021-07-23 17:55:13'),
(7, 'Les Ã©lÃ¨ves sont tenus de participer Ã  tous les cours prÃ©vus par lâ€™emploi de temps officiel du LycÃ©e. Tout Ã©lÃ¨ve qui refuse de participer Ã  un cours donnÃ© sera frappÃ© dâ€™une peine dâ€™exclusion temporaire ou dÃ©finitive. Chaque Ã©lÃ¨ve est Ã©galement tenu de participer Ã  toutes les sÃ©ances de travail manuel, aux activitÃ©s post et pÃ©riscolaires dans lesquelles un rÃ´le lui aura Ã©tÃ© confiÃ©.', '2021-07-23 17:55:37', '2021-07-23 17:55:37'),
(8, 'Pendant les cours, les Ã©lÃ¨ves doivent respecter la place que leur assigne le plan de la classe Ã©tabli par le Professeur Principal, assistÃ© du Chef de Classe.', '2021-07-23 17:55:53', '2021-07-23 17:55:53'),
(9, 'Tout Ã©lÃ¨ve qui trouble un cours sera envoyÃ©, accompagnÃ© du Chef de Classe, Ã  son Surveillant de Secteur ou Ã  un Surveillant GÃ©nÃ©ral pour Ãªtre puni. Si le trouble provoquÃ© est grave, lâ€™intÃ©ressÃ© sera traduit au conseil de discipline.', '2021-07-23 17:56:13', '2021-07-23 17:56:13');

-- --------------------------------------------------------

--
-- Structure de la table `reparation`
--

DROP TABLE IF EXISTS `reparation`;
CREATE TABLE IF NOT EXISTS `reparation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `demarche_de_mediation` text NOT NULL,
  `id_faute` int(11) NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sanction_prevue`
--

DROP TABLE IF EXISTS `sanction_prevue`;
CREATE TABLE IF NOT EXISTS `sanction_prevue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle_sanction` text NOT NULL,
  `niveau_gravite` text NOT NULL,
  `motif` text NOT NULL,
  `duree_validite` text NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sanction_prevue`
--

INSERT INTO `sanction_prevue` (`id`, `libelle_sanction`, `niveau_gravite`, `motif`, `duree_validite`, `dateInsert`, `dateModif`) VALUES
(2, 'punitionintence', 'AVERTISSEMENT', 'refus d\'optemperer', '2021-07-21 00:00:00', '2021-07-24 21:34:57', '2021-07-24 21:34:57'),
(3, 'Negligence', 'EXCLUSION PONCTUELLE', 'Exces', '2021-07-23 00:00:00', '2021-07-25 08:51:27', '2021-07-25 08:51:27'),
(4, 'Sanction depuis Alexie', 'BLAME', 'Exclusion', '2021-07-15 00:00:00', '2021-07-25 08:54:59', '2021-07-25 08:54:59'),
(5, 'mis a pieds por 2 jours', 'BLAME', 'Sortie non autorisÃ©', '2021-07-27 00:00:00', '2021-07-26 04:39:56', '2021-07-26 04:39:56'),
(6, 'Exces', 'BLAME', 'repetition', '2021-07-20 00:00:00', '2021-07-26 05:13:33', '2021-07-26 05:13:33'),
(7, 'Renvoie', 'AVERTISSEMENT', 'bagare avec un camarade', '2021-07-28 00:00:00', '2021-07-26 05:28:14', '2021-07-26 05:28:14'),
(8, 'Test1', 'BLAME', 'suif', '2021-07-30 00:00:00', '2021-07-26 05:29:33', '2021-07-26 05:29:33');

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_association`
--

DROP TABLE IF EXISTS `_pideria_association`;
CREATE TABLE IF NOT EXISTS `_pideria_association` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `associationtable` varchar(100) DEFAULT NULL,
  `signature` varchar(255) NOT NULL,
  `sens` varchar(255) NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `signature` (`signature`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_pideria_association`
--

INSERT INTO `_pideria_association` (`id`, `associationtable`, `signature`, `sens`, `dateInsert`, `dateModif`) VALUES
(1, 'NULL', '_8RAzUOebEeuKaO3YEFKeCg', 'Convocation_Personnel', '2021-07-21 20:40:53', '2021-07-21 20:40:53'),
(2, 'NULL', '_8RmpMuebEeuKaO3YEFKeCg', 'Professeur_Cour', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(3, 'NULL', '_8RwaNeebEeuKaO3YEFKeCg', 'Cour_Classe', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(4, 'NULL', '_8R5kI-ebEeuKaO3YEFKeCg', 'Classe_Eleve', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(5, 'NULL', '_8R5kKuebEeuKaO3YEFKeCg', 'Eleve_Conseil__Discipline', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(6, 'NULL', '_8SDVJuebEeuKaO3YEFKeCg', 'Conseil__Discipline_Personnel', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(7, 'NULL', '_8SNGIeebEeuKaO3YEFKeCg', 'Conseil__Discipline_Membre_Conseil', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(8, 'NULL', '_8SWQEuebEeuKaO3YEFKeCg', 'Fautes_Sanction_Prevue', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(9, 'NULL', '_8SWQGeebEeuKaO3YEFKeCg', 'Fautes_Reparation', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(10, 'NULL', '_8SgBE-ebEeuKaO3YEFKeCg', 'Personne_Notification', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(11, 'NULL', '_8SpLAuebEeuKaO3YEFKeCg', 'Parents_Eleve', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(12, 'NULL', '_8SpLCeebEeuKaO3YEFKeCg', 'Reglement_Iterieure_Regles', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(13, 'NULL', '_8Sy8BeebEeuKaO3YEFKeCg', 'Sanction_Prevue_Regles', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(14, 'Sanction_Prevue', '_8T_O0OebEeuKaO3YEFKeCg', 'Convocation_Eleve', '2021-07-21 20:40:58', '2021-07-21 20:40:58'),
(15, 'Fautes', '_8U4msOebEeuKaO3YEFKeCg', 'Eleve_Regles', '2021-07-21 20:40:58', '2021-07-21 20:40:58');

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_generalisation`
--

DROP TABLE IF EXISTS `_pideria_generalisation`;
CREATE TABLE IF NOT EXISTS `_pideria_generalisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` varchar(256) NOT NULL,
  `child` varchar(256) NOT NULL,
  `signature` varchar(256) NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `signature` (`signature`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `_pideria_generalisation`
--

INSERT INTO `_pideria_generalisation` (`id`, `parent`, `child`, `signature`, `dateInsert`, `dateModif`) VALUES
(1, 'Personne', 'Professeur', 'Personne_Professeur', '2021-07-21 20:40:50', '2021-07-21 20:40:50'),
(2, 'Personne', 'Eleve', 'Personne_Eleve', '2021-07-21 20:40:51', '2021-07-21 20:40:51'),
(3, 'Personne', 'Personnel', 'Personne_Personnel', '2021-07-21 20:40:51', '2021-07-21 20:40:51'),
(4, 'Personne', 'Parents', 'Personne_Parents', '2021-07-21 20:40:52', '2021-07-21 20:40:52');

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_inner_generalisation`
--

DROP TABLE IF EXISTS `_pideria_inner_generalisation`;
CREATE TABLE IF NOT EXISTS `_pideria_inner_generalisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pideria_generalisation` int(11) NOT NULL,
  `idTableParent` int(11) NOT NULL,
  `idTable` int(11) NOT NULL,
  `signature` varchar(256) NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `signature` (`signature`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_pideria_inner_generalisation`
--

INSERT INTO `_pideria_inner_generalisation` (`id`, `id_pideria_generalisation`, `idTableParent`, `idTable`, `signature`) VALUES
(26, 2, 26, 16, '26.Personne.16.Eleve'),
(27, 2, 27, 17, '27.Personne.17.Eleve'),
(28, 2, 28, 18, '28.Personne.18.Eleve'),
(29, 2, 29, 19, '29.Personne.19.Eleve'),
(30, 2, 30, 20, '30.Personne.20.Eleve'),
(31, 2, 31, 21, '31.Personne.21.Eleve'),
(32, 2, 32, 22, '32.Personne.22.Eleve'),
(33, 2, 33, 23, '33.Personne.23.Eleve'),
(34, 3, 34, 7, '34.Personne.7.Personnel'),
(35, 2, 35, 24, '35.Personne.24.Eleve'),
(36, 2, 36, 25, '36.Personne.25.Eleve'),
(37, 2, 37, 26, '37.Personne.26.Eleve'),
(38, 2, 38, 27, '38.Personne.27.Eleve'),
(39, 2, 39, 28, '39.Personne.28.Eleve'),
(40, 2, 40, 29, '40.Personne.29.Eleve'),
(41, 2, 41, 30, '41.Personne.30.Eleve'),
(42, 2, 42, 31, '42.Personne.31.Eleve'),
(43, 2, 43, 32, '43.Personne.32.Eleve'),
(44, 2, 44, 33, '44.Personne.33.Eleve'),
(45, 2, 45, 34, '45.Personne.34.Eleve'),
(46, 2, 46, 35, '46.Personne.35.Eleve'),
(47, 2, 47, 36, '47.Personne.36.Eleve'),
(48, 2, 48, 37, '48.Personne.37.Eleve'),
(49, 2, 49, 38, '49.Personne.38.Eleve'),
(50, 2, 50, 39, '50.Personne.39.Eleve'),
(51, 2, 51, 40, '51.Personne.40.Eleve'),
(52, 2, 52, 41, '52.Personne.41.Eleve'),
(53, 2, 53, 42, '53.Personne.42.Eleve'),
(54, 2, 54, 43, '54.Personne.43.Eleve'),
(55, 2, 55, 44, '55.Personne.44.Eleve'),
(56, 2, 56, 45, '56.Personne.45.Eleve'),
(57, 2, 57, 46, '57.Personne.46.Eleve'),
(58, 2, 58, 47, '58.Personne.47.Eleve'),
(59, 2, 59, 48, '59.Personne.48.Eleve'),
(60, 2, 60, 49, '60.Personne.49.Eleve'),
(61, 1, 61, 4, '61.Personne.4.Professeur'),
(62, 2, 62, 50, '62.Personne.50.Eleve'),
(63, 2, 63, 51, '63.Personne.51.Eleve'),
(64, 2, 64, 52, '64.Personne.52.Eleve'),
(65, 2, 65, 53, '65.Personne.53.Eleve'),
(66, 2, 66, 54, '66.Personne.54.Eleve'),
(67, 2, 67, 55, '67.Personne.55.Eleve'),
(68, 2, 68, 56, '68.Personne.56.Eleve'),
(69, 2, 69, 57, '69.Personne.57.Eleve'),
(70, 2, 70, 58, '70.Personne.58.Eleve'),
(71, 2, 71, 59, '71.Personne.59.Eleve'),
(72, 2, 72, 60, '72.Personne.60.Eleve'),
(73, 2, 73, 61, '73.Personne.61.Eleve'),
(74, 2, 74, 62, '74.Personne.62.Eleve'),
(75, 2, 75, 63, '75.Personne.63.Eleve'),
(76, 2, 76, 64, '76.Personne.64.Eleve'),
(77, 2, 77, 65, '77.Personne.65.Eleve'),
(78, 2, 78, 66, '78.Personne.66.Eleve'),
(79, 2, 79, 67, '79.Personne.67.Eleve'),
(80, 2, 80, 68, '80.Personne.68.Eleve'),
(81, 2, 81, 69, '81.Personne.69.Eleve'),
(82, 2, 82, 70, '82.Personne.70.Eleve'),
(83, 2, 83, 71, '83.Personne.71.Eleve'),
(84, 2, 84, 72, '84.Personne.72.Eleve'),
(85, 2, 85, 73, '85.Personne.73.Eleve'),
(86, 2, 86, 74, '86.Personne.74.Eleve'),
(87, 2, 87, 75, '87.Personne.75.Eleve'),
(88, 2, 88, 76, '88.Personne.76.Eleve'),
(89, 2, 89, 77, '89.Personne.77.Eleve'),
(90, 2, 90, 78, '90.Personne.78.Eleve'),
(91, 2, 91, 79, '91.Personne.79.Eleve'),
(92, 2, 92, 80, '92.Personne.80.Eleve'),
(93, 2, 93, 81, '93.Personne.81.Eleve'),
(94, 2, 94, 82, '94.Personne.82.Eleve'),
(95, 2, 95, 83, '95.Personne.83.Eleve'),
(96, 2, 96, 84, '96.Personne.84.Eleve'),
(97, 2, 97, 85, '97.Personne.85.Eleve'),
(98, 2, 98, 86, '98.Personne.86.Eleve'),
(99, 2, 99, 87, '99.Personne.87.Eleve'),
(100, 2, 100, 88, '100.Personne.88.Eleve'),
(101, 2, 101, 89, '101.Personne.89.Eleve'),
(102, 2, 102, 90, '102.Personne.90.Eleve'),
(103, 2, 103, 91, '103.Personne.91.Eleve'),
(104, 2, 104, 92, '104.Personne.92.Eleve'),
(105, 2, 105, 93, '105.Personne.93.Eleve'),
(106, 2, 106, 94, '106.Personne.94.Eleve'),
(107, 2, 107, 95, '107.Personne.95.Eleve'),
(108, 2, 108, 96, '108.Personne.96.Eleve'),
(109, 2, 109, 97, '109.Personne.97.Eleve'),
(110, 1, 110, 5, '110.Personne.5.Professeur'),
(111, 4, 111, 2, '111.Personne.2.Parents'),
(112, 1, 112, 6, '112.Personne.6.Professeur');

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_inner_owner`
--

DROP TABLE IF EXISTS `_pideria_inner_owner`;
CREATE TABLE IF NOT EXISTS `_pideria_inner_owner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tableUp` varchar(255) DEFAULT NULL,
  `idTableUp` bigint(20) DEFAULT NULL,
  `tableDown` varchar(255) DEFAULT NULL,
  `idTableDown` bigint(20) DEFAULT NULL,
  `signature` varchar(256) NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `signature` (`signature`)
) ENGINE=MyISAM AUTO_INCREMENT=263 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_pideria_inner_owner`
--

INSERT INTO `_pideria_inner_owner` (`id`, `tableUp`, `idTableUp`, `tableDown`, `idTableDown`, `signature`) VALUES
(1, 'Classe', 1, 'Eleve', 1, 'Classe1Eleve1'),
(2, 'Classe', 2, 'Eleve', 2, 'Classe2Eleve2'),
(3, 'Classe', 1, 'Eleve', 3, 'Classe1Eleve3'),
(4, 'Classe', 1, 'Eleve', 4, 'Classe1Eleve4'),
(5, 'Classe', 1, 'Eleve', 5, 'Classe1Eleve5'),
(6, 'Classe', 1, 'Eleve', 6, 'Classe1Eleve6'),
(7, 'Eleve', 1, 'Fautes', 7, 'Eleve1Fautes7'),
(8, 'Eleve', 3, 'Fautes', 8, 'Eleve3Fautes8'),
(9, 'Eleve', 1, 'Fautes', 9, 'Eleve1Fautes9'),
(10, 'Eleve', 1, 'Fautes', 12, 'Eleve1Fautes12'),
(11, 'Eleve', 1, 'Fautes', 14, 'Eleve1Fautes14'),
(12, 'Reglement_Iterieure', 4, 'Regles', 1, 'Reglement_Iterieure4Regles1'),
(13, 'Reglement_Iterieure', 3, 'Regles', 2, 'Reglement_Iterieure3Regles2'),
(14, 'Reglement_Iterieure', 2, 'Regles', 3, 'Reglement_Iterieure2Regles3'),
(15, 'Reglement_Iterieure', 2, 'Regles', 4, 'Reglement_Iterieure2Regles4'),
(16, 'Reglement_Iterieure', 2, 'Regles', 5, 'Reglement_Iterieure2Regles5'),
(17, 'Reglement_Iterieure', 2, 'Regles', 6, 'Reglement_Iterieure2Regles6'),
(18, 'Reglement_Iterieure', 2, 'Regles', 7, 'Reglement_Iterieure2Regles7'),
(19, 'Reglement_Iterieure', 2, 'Regles', 8, 'Reglement_Iterieure2Regles8'),
(20, 'Reglement_Iterieure', 2, 'Regles', 9, 'Reglement_Iterieure2Regles9'),
(21, 'Eleve', 3, 'Fautes', 16, 'Eleve3Fautes16'),
(22, 'Eleve', 3, 'Fautes', 17, 'Eleve3Fautes17'),
(23, 'Fautes', 2, 'Fautes', 17, 'Fautes2Fautes17'),
(24, 'Eleve', 3, 'Fautes', 18, 'Eleve3Fautes18'),
(25, 'Fautes', 2, 'Fautes', 18, 'Fautes2Fautes18'),
(26, 'Eleve', 4, 'Sanction_Prevue', 1, 'Eleve4Sanction_Prevue1'),
(27, 'Regles', 2, 'Sanction_Prevue', 1, 'Regles2Sanction_Prevue1'),
(28, 'Fautes', 7, 'Sanction_Prevue', 1, 'Fautes7Sanction_Prevue1'),
(29, 'Cour', 1, 'Professeur', 3, 'Cour1Professeur3'),
(30, 'Eleve', 3, 'Parents', 1, 'Eleve3Parents1'),
(31, 'Eleve', 1, 'Fautes', 19, 'Eleve1Fautes19'),
(32, 'Fautes', 3, 'Fautes', 19, 'Fautes3Fautes19'),
(33, 'Eleve', 2, 'Fautes', 20, 'Eleve2Fautes20'),
(34, 'Fautes', 3, 'Fautes', 20, 'Fautes3Fautes20'),
(35, 'Eleve', 1, 'Fautes', 25, 'Eleve1Fautes25'),
(36, 'Fautes', 1, 'Fautes', 25, 'Fautes1Fautes25'),
(37, 'Eleve', 1, 'Notification', 4, 'Eleve1Notification4'),
(38, 'Classe', 4, 'Eleve', 7, 'Classe4Eleve7'),
(39, 'Eleve', 7, 'Classe', 4, 'Eleve7Classe4'),
(40, 'Eleve', 1, 'Fautes', 26, 'Eleve1Fautes26'),
(41, 'Fautes', 1, 'Fautes', 26, 'Fautes1Fautes26'),
(42, 'Eleve', 1, 'Notification', 5, 'Eleve1Notification5'),
(43, 'Classe', 9, 'Eleve', 8, 'Classe9Eleve8'),
(44, 'Eleve', 8, 'Classe', 9, 'Eleve8Classe9'),
(45, 'Eleve', 3, 'Sanction_Prevue', 2, 'Eleve3Sanction_Prevue2'),
(46, 'Regles', 2, 'Sanction_Prevue', 2, 'Regles2Sanction_Prevue2'),
(47, 'Fautes', 13, 'Sanction_Prevue', 2, 'Fautes13Sanction_Prevue2'),
(48, 'Classe', 8, 'Eleve', 9, 'Classe8Eleve9'),
(49, 'Eleve', 9, 'Classe', 8, 'Eleve9Classe8'),
(50, 'Eleve', 1, 'Sanction_Prevue', 3, 'Eleve1Sanction_Prevue3'),
(51, 'Regles', 1, 'Sanction_Prevue', 3, 'Regles1Sanction_Prevue3'),
(52, 'Fautes', 20, 'Sanction_Prevue', 3, 'Fautes20Sanction_Prevue3'),
(53, 'Eleve', 1, 'Sanction_Prevue', 4, 'Eleve1Sanction_Prevue4'),
(54, 'Regles', 2, 'Sanction_Prevue', 4, 'Regles2Sanction_Prevue4'),
(55, 'Fautes', 11, 'Sanction_Prevue', 4, 'Fautes11Sanction_Prevue4'),
(56, 'Eleve', 1, 'Fautes', 27, 'Eleve1Fautes27'),
(57, 'Fautes', 3, 'Fautes', 27, 'Fautes3Fautes27'),
(58, 'Eleve', 1, 'Notification', 6, 'Eleve1Notification6'),
(59, 'Classe', 2, 'Eleve', 10, 'Classe2Eleve10'),
(60, 'Eleve', 10, 'Classe', 2, 'Eleve10Classe2'),
(61, 'Classe', 2, 'Eleve', 11, 'Classe2Eleve11'),
(62, 'Eleve', 11, 'Classe', 2, 'Eleve11Classe2'),
(63, 'Classe', 2, 'Eleve', 12, 'Classe2Eleve12'),
(64, 'Eleve', 12, 'Classe', 2, 'Eleve12Classe2'),
(65, 'Classe', 3, 'Eleve', 13, 'Classe3Eleve13'),
(66, 'Eleve', 13, 'Classe', 3, 'Eleve13Classe3'),
(67, 'Classe', 2, 'Eleve', 14, 'Classe2Eleve14'),
(68, 'Eleve', 14, 'Classe', 2, 'Eleve14Classe2'),
(69, 'Classe', 3, 'Eleve', 15, 'Classe3Eleve15'),
(70, 'Eleve', 15, 'Classe', 3, 'Eleve15Classe3'),
(71, 'Classe', 1, 'Eleve', 16, 'Classe1Eleve16'),
(72, 'Eleve', 16, 'Classe', 1, 'Eleve16Classe1'),
(73, 'Classe', 3, 'Eleve', 17, 'Classe3Eleve17'),
(74, 'Eleve', 17, 'Classe', 3, 'Eleve17Classe3'),
(75, 'Classe', 2, 'Eleve', 18, 'Classe2Eleve18'),
(76, 'Eleve', 18, 'Classe', 2, 'Eleve18Classe2'),
(77, 'Classe', 3, 'Eleve', 19, 'Classe3Eleve19'),
(78, 'Eleve', 19, 'Classe', 3, 'Eleve19Classe3'),
(79, 'Classe', 3, 'Eleve', 20, 'Classe3Eleve20'),
(80, 'Eleve', 20, 'Classe', 3, 'Eleve20Classe3'),
(81, 'Classe', 3, 'Eleve', 21, 'Classe3Eleve21'),
(82, 'Eleve', 21, 'Classe', 3, 'Eleve21Classe3'),
(83, 'Classe', 2, 'Eleve', 22, 'Classe2Eleve22'),
(84, 'Eleve', 22, 'Classe', 2, 'Eleve22Classe2'),
(85, 'Classe', 1, 'Eleve', 23, 'Classe1Eleve23'),
(86, 'Eleve', 23, 'Classe', 1, 'Eleve23Classe1'),
(87, 'Classe', 2, 'Eleve', 24, 'Classe2Eleve24'),
(88, 'Eleve', 24, 'Classe', 2, 'Eleve24Classe2'),
(89, 'Classe', 1, 'Eleve', 25, 'Classe1Eleve25'),
(90, 'Eleve', 25, 'Classe', 1, 'Eleve25Classe1'),
(91, 'Classe', 3, 'Eleve', 26, 'Classe3Eleve26'),
(92, 'Eleve', 26, 'Classe', 3, 'Eleve26Classe3'),
(93, 'Classe', 4, 'Eleve', 27, 'Classe4Eleve27'),
(94, 'Eleve', 27, 'Classe', 4, 'Eleve27Classe4'),
(95, 'Classe', 3, 'Eleve', 28, 'Classe3Eleve28'),
(96, 'Eleve', 28, 'Classe', 3, 'Eleve28Classe3'),
(97, 'Classe', 3, 'Eleve', 29, 'Classe3Eleve29'),
(98, 'Eleve', 29, 'Classe', 3, 'Eleve29Classe3'),
(99, 'Classe', 3, 'Eleve', 30, 'Classe3Eleve30'),
(100, 'Eleve', 30, 'Classe', 3, 'Eleve30Classe3'),
(101, 'Classe', 3, 'Eleve', 31, 'Classe3Eleve31'),
(102, 'Eleve', 31, 'Classe', 3, 'Eleve31Classe3'),
(103, 'Classe', 3, 'Eleve', 32, 'Classe3Eleve32'),
(104, 'Eleve', 32, 'Classe', 3, 'Eleve32Classe3'),
(105, 'Classe', 1, 'Eleve', 33, 'Classe1Eleve33'),
(106, 'Eleve', 33, 'Classe', 1, 'Eleve33Classe1'),
(107, 'Classe', 1, 'Eleve', 34, 'Classe1Eleve34'),
(108, 'Eleve', 34, 'Classe', 1, 'Eleve34Classe1'),
(109, 'Classe', 1, 'Eleve', 35, 'Classe1Eleve35'),
(110, 'Eleve', 35, 'Classe', 1, 'Eleve35Classe1'),
(111, 'Classe', 1, 'Eleve', 36, 'Classe1Eleve36'),
(112, 'Eleve', 36, 'Classe', 1, 'Eleve36Classe1'),
(113, 'Classe', 1, 'Eleve', 37, 'Classe1Eleve37'),
(114, 'Eleve', 37, 'Classe', 1, 'Eleve37Classe1'),
(115, 'Classe', 1, 'Eleve', 38, 'Classe1Eleve38'),
(116, 'Eleve', 38, 'Classe', 1, 'Eleve38Classe1'),
(117, 'Classe', 1, 'Eleve', 39, 'Classe1Eleve39'),
(118, 'Eleve', 39, 'Classe', 1, 'Eleve39Classe1'),
(119, 'Classe', 4, 'Eleve', 40, 'Classe4Eleve40'),
(120, 'Eleve', 40, 'Classe', 4, 'Eleve40Classe4'),
(121, 'Classe', 4, 'Eleve', 41, 'Classe4Eleve41'),
(122, 'Eleve', 41, 'Classe', 4, 'Eleve41Classe4'),
(123, 'Classe', 4, 'Eleve', 42, 'Classe4Eleve42'),
(124, 'Eleve', 42, 'Classe', 4, 'Eleve42Classe4'),
(125, 'Classe', 4, 'Eleve', 43, 'Classe4Eleve43'),
(126, 'Eleve', 43, 'Classe', 4, 'Eleve43Classe4'),
(127, 'Classe', 4, 'Eleve', 44, 'Classe4Eleve44'),
(128, 'Eleve', 44, 'Classe', 4, 'Eleve44Classe4'),
(129, 'Classe', 4, 'Eleve', 45, 'Classe4Eleve45'),
(130, 'Eleve', 45, 'Classe', 4, 'Eleve45Classe4'),
(131, 'Classe', 4, 'Eleve', 46, 'Classe4Eleve46'),
(132, 'Eleve', 46, 'Classe', 4, 'Eleve46Classe4'),
(133, 'Classe', 4, 'Eleve', 47, 'Classe4Eleve47'),
(134, 'Eleve', 47, 'Classe', 4, 'Eleve47Classe4'),
(135, 'Classe', 4, 'Eleve', 48, 'Classe4Eleve48'),
(136, 'Eleve', 48, 'Classe', 4, 'Eleve48Classe4'),
(137, 'Classe', 4, 'Eleve', 49, 'Classe4Eleve49'),
(138, 'Eleve', 49, 'Classe', 4, 'Eleve49Classe4'),
(139, 'Cour', 1, 'Professeur', 4, 'Cour1Professeur4'),
(140, 'Classe', 5, 'Eleve', 50, 'Classe5Eleve50'),
(141, 'Eleve', 50, 'Classe', 5, 'Eleve50Classe5'),
(142, 'Classe', 5, 'Eleve', 51, 'Classe5Eleve51'),
(143, 'Eleve', 51, 'Classe', 5, 'Eleve51Classe5'),
(144, 'Classe', 5, 'Eleve', 52, 'Classe5Eleve52'),
(145, 'Eleve', 52, 'Classe', 5, 'Eleve52Classe5'),
(146, 'Classe', 5, 'Eleve', 53, 'Classe5Eleve53'),
(147, 'Eleve', 53, 'Classe', 5, 'Eleve53Classe5'),
(148, 'Classe', 5, 'Eleve', 54, 'Classe5Eleve54'),
(149, 'Eleve', 54, 'Classe', 5, 'Eleve54Classe5'),
(150, 'Classe', 5, 'Eleve', 55, 'Classe5Eleve55'),
(151, 'Eleve', 55, 'Classe', 5, 'Eleve55Classe5'),
(152, 'Classe', 5, 'Eleve', 56, 'Classe5Eleve56'),
(153, 'Eleve', 56, 'Classe', 5, 'Eleve56Classe5'),
(154, 'Classe', 5, 'Eleve', 57, 'Classe5Eleve57'),
(155, 'Eleve', 57, 'Classe', 5, 'Eleve57Classe5'),
(156, 'Classe', 5, 'Eleve', 58, 'Classe5Eleve58'),
(157, 'Eleve', 58, 'Classe', 5, 'Eleve58Classe5'),
(158, 'Classe', 5, 'Eleve', 59, 'Classe5Eleve59'),
(159, 'Eleve', 59, 'Classe', 5, 'Eleve59Classe5'),
(160, 'Classe', 2, 'Eleve', 60, 'Classe2Eleve60'),
(161, 'Eleve', 60, 'Classe', 2, 'Eleve60Classe2'),
(162, 'Classe', 2, 'Eleve', 61, 'Classe2Eleve61'),
(163, 'Eleve', 61, 'Classe', 2, 'Eleve61Classe2'),
(164, 'Classe', 2, 'Eleve', 62, 'Classe2Eleve62'),
(165, 'Eleve', 62, 'Classe', 2, 'Eleve62Classe2'),
(166, 'Classe', 2, 'Eleve', 63, 'Classe2Eleve63'),
(167, 'Eleve', 63, 'Classe', 2, 'Eleve63Classe2'),
(168, 'Classe', 2, 'Eleve', 64, 'Classe2Eleve64'),
(169, 'Eleve', 64, 'Classe', 2, 'Eleve64Classe2'),
(170, 'Classe', 6, 'Eleve', 65, 'Classe6Eleve65'),
(171, 'Eleve', 65, 'Classe', 6, 'Eleve65Classe6'),
(172, 'Classe', 6, 'Eleve', 66, 'Classe6Eleve66'),
(173, 'Eleve', 66, 'Classe', 6, 'Eleve66Classe6'),
(174, 'Classe', 6, 'Eleve', 67, 'Classe6Eleve67'),
(175, 'Eleve', 67, 'Classe', 6, 'Eleve67Classe6'),
(176, 'Classe', 6, 'Eleve', 68, 'Classe6Eleve68'),
(177, 'Eleve', 68, 'Classe', 6, 'Eleve68Classe6'),
(178, 'Classe', 6, 'Eleve', 69, 'Classe6Eleve69'),
(179, 'Eleve', 69, 'Classe', 6, 'Eleve69Classe6'),
(180, 'Classe', 6, 'Eleve', 70, 'Classe6Eleve70'),
(181, 'Eleve', 70, 'Classe', 6, 'Eleve70Classe6'),
(182, 'Classe', 6, 'Eleve', 71, 'Classe6Eleve71'),
(183, 'Eleve', 71, 'Classe', 6, 'Eleve71Classe6'),
(184, 'Classe', 6, 'Eleve', 72, 'Classe6Eleve72'),
(185, 'Eleve', 72, 'Classe', 6, 'Eleve72Classe6'),
(186, 'Classe', 7, 'Eleve', 73, 'Classe7Eleve73'),
(187, 'Eleve', 73, 'Classe', 7, 'Eleve73Classe7'),
(188, 'Classe', 7, 'Eleve', 74, 'Classe7Eleve74'),
(189, 'Eleve', 74, 'Classe', 7, 'Eleve74Classe7'),
(190, 'Classe', 7, 'Eleve', 75, 'Classe7Eleve75'),
(191, 'Eleve', 75, 'Classe', 7, 'Eleve75Classe7'),
(192, 'Classe', 7, 'Eleve', 76, 'Classe7Eleve76'),
(193, 'Eleve', 76, 'Classe', 7, 'Eleve76Classe7'),
(194, 'Classe', 7, 'Eleve', 77, 'Classe7Eleve77'),
(195, 'Eleve', 77, 'Classe', 7, 'Eleve77Classe7'),
(196, 'Classe', 7, 'Eleve', 78, 'Classe7Eleve78'),
(197, 'Eleve', 78, 'Classe', 7, 'Eleve78Classe7'),
(198, 'Classe', 1, 'Eleve', 79, 'Classe1Eleve79'),
(199, 'Eleve', 79, 'Classe', 1, 'Eleve79Classe1'),
(200, 'Classe', 7, 'Eleve', 80, 'Classe7Eleve80'),
(201, 'Eleve', 80, 'Classe', 7, 'Eleve80Classe7'),
(202, 'Classe', 7, 'Eleve', 81, 'Classe7Eleve81'),
(203, 'Eleve', 81, 'Classe', 7, 'Eleve81Classe7'),
(204, 'Classe', 8, 'Eleve', 82, 'Classe8Eleve82'),
(205, 'Eleve', 82, 'Classe', 8, 'Eleve82Classe8'),
(206, 'Classe', 8, 'Eleve', 83, 'Classe8Eleve83'),
(207, 'Eleve', 83, 'Classe', 8, 'Eleve83Classe8'),
(208, 'Classe', 8, 'Eleve', 84, 'Classe8Eleve84'),
(209, 'Eleve', 84, 'Classe', 8, 'Eleve84Classe8'),
(210, 'Classe', 8, 'Eleve', 85, 'Classe8Eleve85'),
(211, 'Eleve', 85, 'Classe', 8, 'Eleve85Classe8'),
(212, 'Eleve', 16, 'Fautes', 28, 'Eleve16Fautes28'),
(213, 'Fautes', 3, 'Fautes', 28, 'Fautes3Fautes28'),
(214, 'Eleve', 19, 'Notification', 7, 'Eleve19Notification7'),
(215, 'Classe', 8, 'Eleve', 86, 'Classe8Eleve86'),
(216, 'Eleve', 86, 'Classe', 8, 'Eleve86Classe8'),
(217, 'Classe', 8, 'Eleve', 87, 'Classe8Eleve87'),
(218, 'Eleve', 87, 'Classe', 8, 'Eleve87Classe8'),
(219, 'Classe', 8, 'Eleve', 88, 'Classe8Eleve88'),
(220, 'Eleve', 88, 'Classe', 8, 'Eleve88Classe8'),
(221, 'Classe', 8, 'Eleve', 89, 'Classe8Eleve89'),
(222, 'Eleve', 89, 'Classe', 8, 'Eleve89Classe8'),
(223, 'Classe', 9, 'Eleve', 90, 'Classe9Eleve90'),
(224, 'Eleve', 90, 'Classe', 9, 'Eleve90Classe9'),
(225, 'Classe', 9, 'Eleve', 91, 'Classe9Eleve91'),
(226, 'Eleve', 91, 'Classe', 9, 'Eleve91Classe9'),
(227, 'Classe', 9, 'Eleve', 92, 'Classe9Eleve92'),
(228, 'Eleve', 92, 'Classe', 9, 'Eleve92Classe9'),
(229, 'Classe', 9, 'Eleve', 93, 'Classe9Eleve93'),
(230, 'Eleve', 93, 'Classe', 9, 'Eleve93Classe9'),
(231, 'Classe', 9, 'Eleve', 94, 'Classe9Eleve94'),
(232, 'Eleve', 94, 'Classe', 9, 'Eleve94Classe9'),
(233, 'Classe', 9, 'Eleve', 95, 'Classe9Eleve95'),
(234, 'Eleve', 95, 'Classe', 9, 'Eleve95Classe9'),
(235, 'Classe', 9, 'Eleve', 96, 'Classe9Eleve96'),
(236, 'Eleve', 96, 'Classe', 9, 'Eleve96Classe9'),
(237, 'Classe', 9, 'Eleve', 97, 'Classe9Eleve97'),
(238, 'Eleve', 97, 'Classe', 9, 'Eleve97Classe9'),
(239, 'Cour', 1, 'Professeur', 5, 'Cour1Professeur5'),
(240, 'Eleve', 16, 'Parents', 2, 'Eleve16Parents2'),
(241, 'Eleve', 16, 'Sanction_Prevue', 5, 'Eleve16Sanction_Prevue5'),
(242, 'Regles', 9, 'Sanction_Prevue', 5, 'Regles9Sanction_Prevue5'),
(243, 'Fautes', 28, 'Sanction_Prevue', 5, 'Fautes28Sanction_Prevue5'),
(244, 'Cour', 1, 'Professeur', 6, 'Cour1Professeur6'),
(245, 'Eleve', 16, 'Sanction_Prevue', 6, 'Eleve16Sanction_Prevue6'),
(246, 'Regles', 3, 'Sanction_Prevue', 6, 'Regles3Sanction_Prevue6'),
(247, 'Fautes', 28, 'Sanction_Prevue', 6, 'Fautes28Sanction_Prevue6'),
(248, 'Eleve', 16, 'Fautes', 29, 'Eleve16Fautes29'),
(249, 'Fautes', 1, 'Fautes', 29, 'Fautes1Fautes29'),
(250, 'Eleve', 1, 'Notification', 8, 'Eleve1Notification8'),
(251, 'Eleve', 16, 'Sanction_Prevue', 7, 'Eleve16Sanction_Prevue7'),
(252, 'Regles', 3, 'Sanction_Prevue', 7, 'Regles3Sanction_Prevue7'),
(253, 'Fautes', 29, 'Sanction_Prevue', 7, 'Fautes29Sanction_Prevue7'),
(254, 'Eleve', 16, 'Sanction_Prevue', 8, 'Eleve16Sanction_Prevue8'),
(255, 'Regles', 2, 'Sanction_Prevue', 8, 'Regles2Sanction_Prevue8'),
(256, 'Fautes', 11, 'Sanction_Prevue', 8, 'Fautes11Sanction_Prevue8'),
(257, 'Eleve', 16, 'Fautes', 30, 'Eleve16Fautes30'),
(258, 'Fautes', 1, 'Fautes', 30, 'Fautes1Fautes30'),
(259, 'Eleve', 1, 'Notification', 9, 'Eleve1Notification9'),
(260, 'Eleve', 16, 'Fautes', 31, 'Eleve16Fautes31'),
(261, 'Fautes', 1, 'Fautes', 31, 'Fautes1Fautes31'),
(262, 'Eleve', 1, 'Notification', 10, 'Eleve1Notification10');

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_manager_session`
--

DROP TABLE IF EXISTS `_pideria_manager_session`;
CREATE TABLE IF NOT EXISTS `_pideria_manager_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_ip` varchar(256) NOT NULL,
  `session_id` varchar(256) NOT NULL,
  `timeInsert` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_ip` (`user_ip`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_owner`
--

DROP TABLE IF EXISTS `_pideria_owner`;
CREATE TABLE IF NOT EXISTS `_pideria_owner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idAssociation` bigint(20) NOT NULL,
  `classassociation` varchar(256) NOT NULL,
  `cardinaliteUp` varchar(25) NOT NULL,
  `cardinaliteDown` varchar(25) NOT NULL,
  `aggregation` varchar(25) DEFAULT NULL,
  `signature` varchar(255) NOT NULL,
  `dateInsert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `signature` (`signature`),
  KEY `idAssociation` (`idAssociation`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `_pideria_owner`
--

INSERT INTO `_pideria_owner` (`id`, `idAssociation`, `classassociation`, `cardinaliteUp`, `cardinaliteDown`, `aggregation`, `signature`, `dateInsert`, `dateModif`) VALUES
(1, 1, 'Personnel', '1', '1', '', '1Personnel', '2021-07-21 20:40:53', '2021-07-21 20:40:53'),
(2, 1, 'Convocation', '*', '', '', '1Convocation', '2021-07-21 20:40:53', '2021-07-21 20:40:53'),
(3, 2, 'Cour', '*', '', '', '2Cour', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(4, 2, 'Professeur', '1', '', '', '2Professeur', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(5, 3, 'Classe', '*', '', '', '3Classe', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(6, 3, 'Cour', '*', '', '', '3Cour', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(7, 4, 'Eleve', '*', '', '', '4Eleve', '2021-07-21 20:40:54', '2021-07-21 20:40:54'),
(8, 4, 'Classe', '1', '1', '', '4Classe', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(9, 5, 'Conseil__Discipline', '*', '', '', '5Conseil__Discipline', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(10, 5, 'Eleve', '1', '1', '', '5Eleve', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(11, 6, 'Personnel', '*', '1', '', '6Personnel', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(12, 6, 'Conseil__Discipline', '*', '', '', '6Conseil__Discipline', '2021-07-21 20:40:55', '2021-07-21 20:40:55'),
(13, 7, 'Membre_Conseil', '*', '1', '', '7Membre_Conseil', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(14, 7, 'Conseil__Discipline', '*', '', '', '7Conseil__Discipline', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(15, 8, 'Sanction_Prevue', '*', '', '', '8Sanction_Prevue', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(16, 8, 'Fautes', '1', '', '', '8Fautes', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(17, 9, 'Reparation', '*', '', '', '9Reparation', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(18, 9, 'Fautes', '1', '', '', '9Fautes', '2021-07-21 20:40:56', '2021-07-21 20:40:56'),
(19, 10, 'Notification', '*', '', '', '10Notification', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(20, 10, 'Personne', '1', '1', '', '10Personne', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(21, 11, 'Eleve', '*', '1', '', '11Eleve', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(22, 11, 'Parents', '*', '1', '', '11Parents', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(23, 12, 'Regles', '*', '1', '', '12Regles', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(24, 12, 'Reglement_Iterieure', '1', '', '', '12Reglement_Iterieure', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(25, 13, 'Regles', '1', '1', '', '13Regles', '2021-07-21 20:40:57', '2021-07-21 20:40:57'),
(26, 13, 'Sanction_Prevue', '*', '1', '', '13Sanction_Prevue', '2021-07-21 20:40:58', '2021-07-21 20:40:58'),
(27, 14, 'Eleve', '*', '', '', '14Eleve', '2021-07-21 20:40:58', '2021-07-21 20:40:58'),
(28, 14, 'Convocation', '1', '', '', '14Convocation', '2021-07-21 20:40:58', '2021-07-21 20:40:58'),
(29, 15, 'Regles', '*', '1', '', '15Regles', '2021-07-21 20:40:58', '2021-07-21 20:40:58'),
(30, 15, 'Eleve', '1', '', '', '15Eleve', '2021-07-21 20:40:58', '2021-07-21 20:40:58');

-- --------------------------------------------------------

--
-- Structure de la table `_pideria_secure_namespace`
--

DROP TABLE IF EXISTS `_pideria_secure_namespace`;
CREATE TABLE IF NOT EXISTS `_pideria_secure_namespace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(256) NOT NULL,
  `namespace` varchar(256) NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `link` (`link`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `_pideria_owner`
--
ALTER TABLE `_pideria_owner`
  ADD CONSTRAINT `_pideria_owner_association_contraints` FOREIGN KEY (`idAssociation`) REFERENCES `_pideria_association` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
