<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="wp-params.xsl"/>
	<xsl:param name="user-flash" />
	<xsl:param name="nbreVisiteTotal" />
	<xsl:param name="nbreVisiteForDay" />
	<xsl:param name="nbreConnectes" />
	
	<xsl:variable name="nbre_par_page">40</xsl:variable>

	<!-- PIDERIA TEMPLATE READERS -->
	<xsl:template match="pideria-param">
		<xsl:variable name="name" select="@name" />
		
		<xsl:value-of select="//pageScope/requestScope/*[name(.) = $name]/@value" disable-output-escaping="yes" />
	</xsl:template>
	<!-- PIDERIA TEMPLATE READERS -->

	<!-- PIDERIA TEMPLATE SESSION -->
	<xsl:template match="pideria-session">
		<xsl:variable name="name" select="@name" />
		
		<xsl:value-of select="//pageScope/sessionScope/*[name(.) = $name]/@value" />
	</xsl:template>

	<xsl:template name="function-session">
		<xsl:param name="param" />

		<xsl:variable name="result">
			<xsl:value-of select="//pageScope/sessionScope/*[name(.) = $param]" />
		</xsl:variable>
		
		<xsl:copy-of select="$result"/>
	</xsl:template>

	<!-- <xsl:template match="sessionScope" /> -->
	<xsl:template match="pageScope" />
	<!-- PIDERIA TEMPLATE SESSION -->

	<xsl:template match="link-def">
		<xsl:param name="link">#</xsl:param>
		<xsl:param name="name" />

		<xsl:element name="a">
			<xsl:for-each select="./a/@*">
				<xsl:copy />
			</xsl:for-each>

			<xsl:attribute name="href">
				<xsl:value-of select="$link" />
			</xsl:attribute>
			
			<xsl:choose>
				<xsl:when test="$name">
					<xsl:value-of select="$name" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="." />
				</xsl:otherwise>
			</xsl:choose>

			<xsl:for-each select="./a/*">
				<xsl:apply-templates select="." />
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

	<xsl:template match="child::node() | attribute::*" priority="-10">        
		<xsl:copy>            
			<xsl:apply-templates select="@*" />            
			<xsl:apply-templates select="node()" />        
		</xsl:copy>    
	</xsl:template>

	<xsl:template match="XSLinclude">
		<xsl:variable name="doc_" select="document(./@href)" />
		<xsl:apply-templates select="$doc_" />
	</xsl:template>

	<xsl:template name="function-test">
		<xsl:param name="string"> </xsl:param>

		<xsl:variable name="result">
			<xsl:value-of select="php:function('StyleSheet_Manager::text', $string)" />
		</xsl:variable>

		<xsl:copy-of select="$result" />
	</xsl:template> 

	<xsl:template match="data-extract">
		<xsl:variable name="model" select="." />

		<xsl:for-each select="document(./@href)/root/*">
			<xsl:variable name="current-element" select="." />

			<xsl:for-each select="$model/div">
				<xsl:copy>
					<xsl:apply-templates>
						<xsl:with-param name="link" select="concat('/inscription/school-', $current-element/@id)" />
						<xsl:with-param name="name" select="$current-element/@nom" />
					</xsl:apply-templates>
				</xsl:copy>
			</xsl:for-each>
		</xsl:for-each>

	</xsl:template>

	<xsl:template match="form-data" priority="-10">
	</xsl:template>

	<!-- <xsl:template match="content">
	</xsl:template> -->

	<xsl:template match="user-flash">
	  <xsl:if test="$user-flash">
		<span id="flash_content" style="display: none;"><xsl:value-of select="$user-flash" /></span>
	  </xsl:if>
	</xsl:template>
	
	<xsl:template name="instancier-date-time">
		<xsl:param name="date-time" />

		<xsl:variable name="date" select="substring-before($date-time, ' ')" />
		<xsl:variable name="time" select="substring-after($date-time, ' ')" />

		<xsl:choose>
		  <xsl:when test="$date">
			<xsl:value-of select="$date" />  
		  </xsl:when>
		  <xsl:otherwise>
			<xsl:value-of select="$date-time" />  
		  </xsl:otherwise>
		</xsl:choose>

		<!-- <xsl:text> à </xsl:text>
		<xsl:value-of select="$time" /> -->
	</xsl:template>

</xsl:stylesheet>