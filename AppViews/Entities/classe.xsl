<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-classe" select="//pageScope//element//Classe" />
	<xsl:variable name="data-classe-list" select="//pageScope//list-elements//Classe" />

	<xsl:template match="view-classe-nom_classe">
		<xsl:param name="view-classe" />
		<!--  -->
		<xsl:value-of select="$view-classe/@nom_classe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-classe-tab">
		<xsl:param name="view-classe-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-classe-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-classe" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>