<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-eleve" select="//pageScope//element//Eleve" />
	<xsl:variable name="data-eleve-list" select="//pageScope//list-elements//Eleve" />

	<xsl:template match="view-eleve-solvable">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@solvable" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-redoublant">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@redoublant" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-nom">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@nom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-prenom">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@prenom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-date_de_naisssancce">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@date_de_naisssancce" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-login_">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@login_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-mot_de_passe">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@mot_de_passe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-lieu_de_naissance_">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@lieu_de_naissance_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-photo">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@photo" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-email">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@email" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-sexe">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@sexe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-Tel">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@Tel" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-role">
		<xsl:param name="view-eleve" />
		<!--  -->
		<xsl:value-of select="$view-eleve/@role" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-eleve-tab">
		<xsl:param name="view-eleve-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-eleve-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-eleve" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>