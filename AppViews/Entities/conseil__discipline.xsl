<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-conseil__discipline" select="//pageScope//element//Conseil__Discipline" />
	<xsl:variable name="data-conseil__discipline-list" select="//pageScope//list-elements//Conseil__Discipline" />

	<xsl:template match="view-conseil__discipline-date_cd">
		<xsl:param name="view-conseil__discipline" />
		<!--  -->
		<xsl:value-of select="$view-conseil__discipline/@date_cd" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-conseil__discipline-heure_debut_cd">
		<xsl:param name="view-conseil__discipline" />
		<!--  -->
		<xsl:value-of select="$view-conseil__discipline/@heure_debut_cd" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-conseil__discipline-heure_fin_cd">
		<xsl:param name="view-conseil__discipline" />
		<!--  -->
		<xsl:value-of select="$view-conseil__discipline/@heure_fin_cd" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-conseil__discipline-tab">
		<xsl:param name="view-conseil__discipline-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-conseil__discipline-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-conseil__discipline" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>