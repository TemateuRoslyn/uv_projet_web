<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-fautes" select="//pageScope//element//Fautes" />
	<xsl:variable name="data-fautes-list" select="//pageScope//list-elements//Fautes" />

	<xsl:template match="view-fautes-nom">
		<xsl:param name="view-fautes" />
		<!--  -->
		<xsl:value-of select="$view-fautes/@nom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-fautes-Gravite">
		<xsl:param name="view-fautes" />
		<!--  -->
		<xsl:value-of select="$view-fautes/@Gravite" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-fautes-tab">
		<xsl:param name="view-fautes-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-fautes-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-fautes" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>