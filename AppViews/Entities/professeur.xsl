<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-professeur" select="//pageScope//element//Professeur" />
	<xsl:variable name="data-professeur-list" select="//pageScope//list-elements//Professeur" />

	<xsl:template match="view-professeur-statut">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@statut" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-nom">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@nom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-prenom">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@prenom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-date_de_naisssancce">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@date_de_naisssancce" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-login_">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@login_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-mot_de_passe">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@mot_de_passe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-lieu_de_naissance_">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@lieu_de_naissance_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-photo">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@photo" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-email">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@email" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-sexe">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@sexe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-Tel">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@Tel" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-role">
		<xsl:param name="view-professeur" />
		<!--  -->
		<xsl:value-of select="$view-professeur/@role" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-professeur-tab">
		<xsl:param name="view-professeur-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-professeur-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-professeur" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>