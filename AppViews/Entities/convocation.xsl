<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-convocation" select="//pageScope//element//Convocation" />
	<xsl:variable name="data-convocation-list" select="//pageScope//list-elements//Convocation" />

	<xsl:template match="view-convocation-libelle">
		<xsl:param name="view-convocation" />
		<!--  -->
		<xsl:value-of select="$view-convocation/@libelle" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-convocation-date_convocation">
		<xsl:param name="view-convocation" />
		<!--  -->
		<xsl:value-of select="$view-convocation/@date_convocation" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-convocation-date_RV">
		<xsl:param name="view-convocation" />
		<!--  -->
		<xsl:value-of select="$view-convocation/@date_RV" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-convocation-statut">
		<xsl:param name="view-convocation" />
		<!--  -->
		<xsl:value-of select="$view-convocation/@statut" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-convocation-tab">
		<xsl:param name="view-convocation-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-convocation-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-convocation" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>