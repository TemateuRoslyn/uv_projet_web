<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-personne" select="//pageScope//element//Personne" />
	<xsl:variable name="data-personne-list" select="//pageScope//list-elements//Personne" />

	<xsl:template match="view-personne-nom">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@nom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-prenom">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@prenom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-date_de_naisssancce">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@date_de_naisssancce" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-login_">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@login_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-mot_de_passe">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@mot_de_passe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-lieu_de_naissance_">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@lieu_de_naissance_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-photo">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@photo" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-email">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@email" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-sexe">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@sexe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-Tel">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@Tel" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-role">
		<xsl:param name="view-personne" />
		<!--  -->
		<xsl:value-of select="$view-personne/@role" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personne-tab">
		<xsl:param name="view-personne-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-personne-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-personne" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>