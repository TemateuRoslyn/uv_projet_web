<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-regles" select="//pageScope//element//Regles" />
	<xsl:variable name="data-regles-list" select="//pageScope//list-elements//Regles" />

	<xsl:template match="view-regles-libelle_regle">
		<xsl:param name="view-regles" />
		<!--  -->
		<xsl:value-of select="$view-regles/@libelle_regle" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-regles-tab">
		<xsl:param name="view-regles-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-regles-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-regles" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>