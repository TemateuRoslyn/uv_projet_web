<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-reparation" select="//pageScope//element//Reparation" />
	<xsl:variable name="data-reparation-list" select="//pageScope//list-elements//Reparation" />

	<xsl:template match="view-reparation-demarche_de_mediation">
		<xsl:param name="view-reparation" />
		<!--  -->
		<xsl:value-of select="$view-reparation/@demarche_de_mediation" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-reparation-id_faute">
		<xsl:param name="view-reparation" />
		<!--  -->
		<xsl:value-of select="$view-reparation/@id_faute" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-reparation-tab">
		<xsl:param name="view-reparation-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-reparation-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-reparation" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>