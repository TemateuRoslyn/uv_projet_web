<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-cour" select="//pageScope//element//Cour" />
	<xsl:variable name="data-cour-list" select="//pageScope//list-elements//Cour" />

	<xsl:template match="view-cour-date_cour">
		<xsl:param name="view-cour" />
		<!--  -->
		<xsl:value-of select="$view-cour/@date_cour" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-cour-heure_debut">
		<xsl:param name="view-cour" />
		<!--  -->
		<xsl:value-of select="$view-cour/@heure_debut" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-cour-heure_fin">
		<xsl:param name="view-cour" />
		<!--  -->
		<xsl:value-of select="$view-cour/@heure_fin" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-cour-tab">
		<xsl:param name="view-cour-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-cour-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-cour" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>