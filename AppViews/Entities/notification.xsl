<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-notification" select="//pageScope//element//Notification" />
	<xsl:variable name="data-notification-list" select="//pageScope//list-elements//Notification" />

	<xsl:template match="view-notification-libelle">
		<xsl:param name="view-notification" />
		<!--  -->
		<xsl:value-of select="$view-notification/@libelle" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-notification-view">
		<xsl:param name="view-notification" />
		<!--  -->
		<xsl:value-of select="$view-notification/@view" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-notification-id_emeteur">
		<xsl:param name="view-notification" />
		<!--  -->
		<xsl:value-of select="$view-notification/@id_emeteur" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-notification-nom_emeteur">
		<xsl:param name="view-notification" />
		<!--  -->
		<xsl:value-of select="$view-notification/@nom_emeteur" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-notification-tab">
		<xsl:param name="view-notification-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-notification-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-notification" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>