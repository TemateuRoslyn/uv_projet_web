<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-sanction_prevue" select="//pageScope//element//Sanction_Prevue" />
	<xsl:variable name="data-sanction_prevue-list" select="//pageScope//list-elements//Sanction_Prevue" />

	<xsl:template match="view-sanction_prevue-libelle_sanction">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@libelle_sanction" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-niveau_gravite">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@niveau_gravite" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-motif">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@motif" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-duree_validite">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@duree_validite" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-tab">
		<xsl:param name="view-sanction_prevue-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-sanction_prevue-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-sanction_prevue" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>