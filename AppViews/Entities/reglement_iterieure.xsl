<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-reglement_iterieure" select="//pageScope//element//Reglement_Iterieure" />
	<xsl:variable name="data-reglement_iterieure-list" select="//pageScope//list-elements//Reglement_Iterieure" />


	<xsl:template match="view-reglement_iterieure-nomEtablissement">
		<xsl:param name="view-reglement_iterieure" />
		<!--  -->
		<xsl:value-of select="$view-reglement_iterieure/@nomEtablissement" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-reglement_iterieure-tab">
		<xsl:param name="view-reglement_iterieure-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-reglement_iterieure-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-reglement_iterieure" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>