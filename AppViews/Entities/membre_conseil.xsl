<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-membre_conseil" select="//pageScope//element//Membre_Conseil" />
	<xsl:variable name="data-membre_conseil-list" select="//pageScope//list-elements//Membre_Conseil" />

	<xsl:template match="view-membre_conseil-id_liste">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_liste" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-id_chef">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_chef" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-id_surveillant_G">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_surveillant_G" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-id_representant_E">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_representant_E" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-tab">
		<xsl:param name="view-membre_conseil-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-membre_conseil-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-membre_conseil" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>