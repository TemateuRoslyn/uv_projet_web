<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-parents" select="//pageScope//element//Parents" />
	<xsl:variable name="data-parents-list" select="//pageScope//list-elements//Parents" />

	<xsl:template match="view-parents-profession">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@profession" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-nom">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@nom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-prenom">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@prenom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-date_de_naisssancce">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@date_de_naisssancce" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-login_">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@login_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-mot_de_passe">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@mot_de_passe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-lieu_de_naissance_">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@lieu_de_naissance_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-photo">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@photo" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-email">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@email" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-sexe">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@sexe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-Tel">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@Tel" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-role">
		<xsl:param name="view-parents" />
		<!--  -->
		<xsl:value-of select="$view-parents/@role" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-parents-tab">
		<xsl:param name="view-parents-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-parents-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-parents" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>