<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-personnel" select="//pageScope//element//Personnel" />
	<xsl:variable name="data-personnel-list" select="//pageScope//list-elements//Personnel" />

	<xsl:template match="view-personnel-fonction">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@fonction" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-nom">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@nom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-prenom">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@prenom" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-date_de_naisssancce">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@date_de_naisssancce" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-login_">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@login_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-mot_de_passe">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@mot_de_passe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-lieu_de_naissance_">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@lieu_de_naissance_" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-photo">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@photo" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-email">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@email" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-sexe">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@sexe" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-Tel">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@Tel" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-role">
		<xsl:param name="view-personnel" />
		<!--  -->
		<xsl:value-of select="$view-personnel/@role" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-personnel-tab">
		<xsl:param name="view-personnel-list"/>
		<xsl:variable name="context" select="." />
 
		<xsl:for-each select="$view-personnel-list">
			<xsl:variable name="element" select="." />
			<xsl:for-each select="$context">
				<xsl:apply-templates>
					<xsl:with-param name="view-personnel" select="$element" />
				</xsl:apply-templates>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>