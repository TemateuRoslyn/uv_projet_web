<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="data-fautesmanager_xml" select="//pageScope//element//FautesManager_XML" />
	<xsl:variable name="data-fautesmanager_xml-list" select="//pageScope//list-elements//FautesManager_XML" />
	<xsl:include href="./Entities/personne.xsl" />
	<xsl:include href="./Entities/reglement_iterieure.xsl" />
	<xsl:include href="./Entities/professeur.xsl" />
	<xsl:include href="./Entities/eleve.xsl" />
	<xsl:include href="./Entities/personnel.xsl" />
	<xsl:include href="./Entities/convocation.xsl" />
	<xsl:include href="./Entities/classe.xsl" />
	<xsl:include href="./Entities/reparation.xsl" />
	<xsl:include href="./Entities/cour.xsl" />
	<xsl:include href="./Entities/conseil__discipline.xsl" />
	<xsl:include href="./Entities/membre_conseil.xsl" />
	<xsl:include href="./Entities/regles.xsl" />
	<xsl:include href="./Entities/notification.xsl" />
	<xsl:include href="./Entities/parents.xsl" />
	<xsl:include href="./Entities/sanction_prevue.xsl" />
	<xsl:include href="./Entities/fautes.xsl" />

	<xsl:template match="child::node() | attribute::*">
		<xsl:param name="view-personne" />
		<xsl:param name="view-personne-list" />
		<xsl:param name="view-reglement_iterieure" />
		<xsl:param name="view-reglement_iterieure-list" />
		<xsl:param name="view-professeur" />
		<xsl:param name="view-professeur-list" />
		<xsl:param name="view-eleve" />
		<xsl:param name="view-eleve-list" />
		<xsl:param name="view-personnel" />
		<xsl:param name="view-personnel-list" />
		<xsl:param name="view-convocation" />
		<xsl:param name="view-convocation-list" />
		<xsl:param name="view-classe" />
		<xsl:param name="view-classe-list" />
		<xsl:param name="view-reparation" />
		<xsl:param name="view-reparation-list" />
		<xsl:param name="view-cour" />
		<xsl:param name="view-cour-list" />
		<xsl:param name="view-conseil__discipline" />
		<xsl:param name="view-conseil__discipline-list" />
		<xsl:param name="view-membre_conseil" />
		<xsl:param name="view-membre_conseil-list" />
		<xsl:param name="view-regles" />
		<xsl:param name="view-regles-list" />
		<xsl:param name="view-notification" />
		<xsl:param name="view-notification-list" />
		<xsl:param name="view-parents" />
		<xsl:param name="view-parents-list" />
		<xsl:param name="view-sanction_prevue" />
		<xsl:param name="view-sanction_prevue-list" />
		<xsl:param name="view-fautes" />
		<xsl:param name="view-fautes-list" />
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="node()">
				<xsl:with-param name="view-personne" select="$view-personne" />
				<xsl:with-param name="view-personne-list" select="$view-personne-list" />
				<xsl:with-param name="view-reglement_iterieure" select="$view-reglement_iterieure" />
				<xsl:with-param name="view-reglement_iterieure-list" select="$view-reglement_iterieure-list" />
				<xsl:with-param name="view-professeur" select="$view-professeur" />
				<xsl:with-param name="view-professeur-list" select="$view-professeur-list" />
				<xsl:with-param name="view-eleve" select="$view-eleve" />
				<xsl:with-param name="view-eleve-list" select="$view-eleve-list" />
				<xsl:with-param name="view-personnel" select="$view-personnel" />
				<xsl:with-param name="view-personnel-list" select="$view-personnel-list" />
				<xsl:with-param name="view-convocation" select="$view-convocation" />
				<xsl:with-param name="view-convocation-list" select="$view-convocation-list" />
				<xsl:with-param name="view-classe" select="$view-classe" />
				<xsl:with-param name="view-classe-list" select="$view-classe-list" />
				<xsl:with-param name="view-reparation" select="$view-reparation" />
				<xsl:with-param name="view-reparation-list" select="$view-reparation-list" />
				<xsl:with-param name="view-cour" select="$view-cour" />
				<xsl:with-param name="view-cour-list" select="$view-cour-list" />
				<xsl:with-param name="view-conseil__discipline" select="$view-conseil__discipline" />
				<xsl:with-param name="view-conseil__discipline-list" select="$view-conseil__discipline-list" />
				<xsl:with-param name="view-membre_conseil" select="$view-membre_conseil" />
				<xsl:with-param name="view-membre_conseil-list" select="$view-membre_conseil-list" />
				<xsl:with-param name="view-regles" select="$view-regles" />
				<xsl:with-param name="view-regles-list" select="$view-regles-list" />
				<xsl:with-param name="view-notification" select="$view-notification" />
				<xsl:with-param name="view-notification-list" select="$view-notification-list" />
				<xsl:with-param name="view-parents" select="$view-parents" />
				<xsl:with-param name="view-parents-list" select="$view-parents-list" />
				<xsl:with-param name="view-sanction_prevue" select="$view-sanction_prevue" />
				<xsl:with-param name="view-sanction_prevue-list" select="$view-sanction_prevue-list" />
				<xsl:with-param name="view-fautes" select="$view-fautes" />
				<xsl:with-param name="view-fautes-list" select="$view-fautes-list" />
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="content" priority="10">
			<xsl:apply-templates select="node()">
				<xsl:with-param name="view-personne" select="$data-personne" />
				<xsl:with-param name="view-personne-list" select="$data-personne-list" />
				<xsl:with-param name="view-reglement_iterieure" select="$data-reglement_iterieure" />
				<xsl:with-param name="view-reglement_iterieure-list" select="$data-reglement_iterieure-list" />
				<xsl:with-param name="view-professeur" select="$data-professeur" />
				<xsl:with-param name="view-professeur-list" select="$data-professeur-list" />
				<xsl:with-param name="view-eleve" select="$data-eleve" />
				<xsl:with-param name="view-eleve-list" select="$data-eleve-list" />
				<xsl:with-param name="view-personnel" select="$data-personnel" />
				<xsl:with-param name="view-personnel-list" select="$data-personnel-list" />
				<xsl:with-param name="view-convocation" select="$data-convocation" />
				<xsl:with-param name="view-convocation-list" select="$data-convocation-list" />
				<xsl:with-param name="view-classe" select="$data-classe" />
				<xsl:with-param name="view-classe-list" select="$data-classe-list" />
				<xsl:with-param name="view-reparation" select="$data-reparation" />
				<xsl:with-param name="view-reparation-list" select="$data-reparation-list" />
				<xsl:with-param name="view-cour" select="$data-cour" />
				<xsl:with-param name="view-cour-list" select="$data-cour-list" />
				<xsl:with-param name="view-conseil__discipline" select="$data-conseil__discipline" />
				<xsl:with-param name="view-conseil__discipline-list" select="$data-conseil__discipline-list" />
				<xsl:with-param name="view-membre_conseil" select="$data-membre_conseil" />
				<xsl:with-param name="view-membre_conseil-list" select="$data-membre_conseil-list" />
				<xsl:with-param name="view-regles" select="$data-regles" />
				<xsl:with-param name="view-regles-list" select="$data-regles-list" />
				<xsl:with-param name="view-notification" select="$data-notification" />
				<xsl:with-param name="view-notification-list" select="$data-notification-list" />
				<xsl:with-param name="view-parents" select="$data-parents" />
				<xsl:with-param name="view-parents-list" select="$data-parents-list" />
				<xsl:with-param name="view-sanction_prevue" select="$data-sanction_prevue" />
				<xsl:with-param name="view-sanction_prevue-list" select="$data-sanction_prevue-list" />
				<xsl:with-param name="view-fautes" select="$data-fautes" />
				<xsl:with-param name="view-fautes-list" select="$data-fautes-list" />
			</xsl:apply-templates>
	</xsl:template>

</xsl:stylesheet>