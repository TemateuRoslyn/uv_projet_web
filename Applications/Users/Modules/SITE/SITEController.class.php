<?php
	namespace Applications\USERS\Modules\SITE;
	use AppliLib\ABackController;
	use Library\Page;
	use AppliLib\EUser;
	use AppliLib\Entities\Classe;
	use AppliLib\Entities\Eleve;
	use AppliLib\Entities\Personne;
	use  AppliLib\FormBuilder\PersonneFormBuilder;

	
	 class SITEController extends ABackController
            {

/**
 * Home:
 */
public function executeprincipal(\Library\HTTPRequest $request){}
public function executeabout(\Library\HTTPRequest $request){}
public function executehome(\Library\HTTPRequest $request){

}
public function executegallery(\Library\HTTPRequest $request){}
public function executeevents(\Library\HTTPRequest $request){}
public function executeviews(\Library\HTTPRequest $request){}
public function executeContact(\Library\HTTPRequest $request){}
public function executenotification_views_site(\Library\HTTPRequest $request){}
public function executeSetting(\Library\HTTPRequest $request){}

/**
 * Mon compte
 */
public function executeAccount(\Library\HTTPRequest $request){
	$role = $request->app()->user()->getAttribute(EUser::USER_ROLE);
	$id = $request->app()->user()->getAttribute(EUser::USER_ID);
	$select_manager = NULL;
	$liste_sanction = NULL;
	$liste_fautes = NULL;
	

	if($role == PersonneFormBuilder::ROLE_SUPER_ADMINISTRATEUR 
	|| $role == PersonneFormBuilder::ROLE_ADMINISTRATEUR 
	|| $role == PersonneFormBuilder::ROLE_SURVEILLANT_GENERAL 
	|| $role == PersonneFormBuilder::ROLE_PERSONNEL_ADMINISTRATIF){
		$select_manager = $this->managers->getManagerOf("Personnel")->getUnique($id);
	}else if($role == PersonneFormBuilder::ROLE_PROFESSEUR){
		$select_manager = $this->managers->getManagerOf("Professeur")->getUnique($id);
	}else if($role == PersonneFormBuilder::ROLE_ELEVE){
		$select_manager = $this->managers->getManagerOf("Eleve")->getUnique($id);

		$eleve = new Eleve(["id" => $id]);
		$liste_fautes = $this->managers->getManagerOf("Fautes")->getAssociateTo($eleve);
		$liste_sanction = $this->managers->getManagerOf("Sanction_Prevue")->getAssociateTo($eleve);

		// on ajoute
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $liste_fautes);
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $liste_sanction);
	}else {
		$select_manager = $this->managers->getManagerOf("Parents")->getUnique($id);
	}
	
	$this->page()->addVar(Page::NS_ENTITY, [$select_manager]);
	
}
public function executeCONNECTION(\Library\HTTPRequest $request){}
public function executepersonnel_reparation_lister(\Library\HTTPRequest $request){}
public function executeenseignant_reparation_lister(\Library\HTTPRequest $request){}
public function executereparation_ajouter(\Library\HTTPRequest $request){}
public function executereparation_supprimer(\Library\HTTPRequest $request){}
public function executereparation_lire(\Library\HTTPRequest $request){}
public function executereparation_lister(\Library\HTTPRequest $request){}
public function executereparation_modifier(\Library\HTTPRequest $request){}
public function executeVISUALISER_LA_LISTE_ELEVES(\Library\HTTPRequest $request){}
public function executeCONSULTER_LE_REGLEMENT(\Library\HTTPRequest $request){}
public function executechoisir_un_ele_ve_et_lister_ses_fautes(\Library\HTTPRequest $request){}
public function executeprendre_une_salle(\Library\HTTPRequest $request){}
public function executefaire_la_liste_des_membres_convoques(\Library\HTTPRequest $request){}
public function executeAppliquer_la_decision_du_conseil(\Library\HTTPRequest $request){}
public function executeChoisir_un_conseil_de_discipline_deja_tenu(\Library\HTTPRequest $request){}
public function executeSaisir_le_compte_rendu_d_un_conseil_de_discipline(\Library\HTTPRequest $request){}
public function executechoisir_une_salle(\Library\HTTPRequest $request){}
public function executefaire_l_appel_et_enregister_les_fautes_possibles(\Library\HTTPRequest $request){}
public function executeVerifier_les_annulation_et_l_absence_d_un_professeu(\Library\HTTPRequest $request){}
public function executeEtablir_le_programme_de_cours(\Library\HTTPRequest $request){}
/**
 * Consulter le reglement interieur
 */
public function executeConsulter_le_reglement_nterieur(\Library\HTTPRequest $request){
	$reglement = $this->managers->getManagerOf("Regles")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $reglement);
}
public function executeChoisir_l_eleve(\Library\HTTPRequest $request){}
public function executevoir_la_liste_des_eleves(\Library\HTTPRequest $request){}
public function executeVoir_la_liste_des_sanction_prevu_par_le_reglement(\Library\HTTPRequest $request){}
public function executeEspacce_Surveillant_generale(\Library\HTTPRequest $request){}
public function executeSANCTIONNER_L_ELEVE(\Library\HTTPRequest $request){}
public function executeSAISIR_UNE_CONVOCATION(\Library\HTTPRequest $request){}
public function executeProgrammer_un_conseil_de_discipline(\Library\HTTPRequest $request){}
public function executeAJOUTER_UNE_FAUTE(\Library\HTTPRequest $request){}
public function executeGERER_LES_NOTES(\Library\HTTPRequest $request){}
public function executeCONSULTATION_SES_INFOS_PERSONNELLES(\Library\HTTPRequest $request){}
public function executeAJOUTER_UNE_SANCTION(\Library\HTTPRequest $request){}
public function executeESPACE_ENSEIGNANT(\Library\HTTPRequest $request){}
public function executeESPACE_ELEVE(\Library\HTTPRequest $request){}

public function executeenseignant_eleves_lister(\Library\HTTPRequest $request){

	// Dans le cas ou quelqu'un autrue qu'un enseignant tente d.acceder

	if($request->app()->user()->getAttribute(EUser::USER_ROLE) != PersonneFormBuilder::ROLE_PROFESSEUR){
		$request->app()->forWardURI("/geniusclassrooms", true);
	}

	$eleves = $this->managers->getManagerOf("Eleve")->getAssociateTo(new Classe(["id" => $request->getData("id")]));
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $eleves);
	
	// on recupere le nom de la classe
	$classe = $this->managers->getManagerOf("Classe")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$classe]);

	// on garde la salle selection en variable de session
	$this->app->user()->setAttribute("classeleve", $classe['nom_classe']);


	$this->useView("enseignant_eleves_lister");
}

public function executeenseignant_classes_lister(\Library\HTTPRequest $request){
	// Dans le cas ou quelqu'un autrue qu'un enseignant tente d.acceder

	if($request->app()->user()->getAttribute(EUser::USER_ROLE) != PersonneFormBuilder::ROLE_PROFESSEUR){
		$request->app()->forWardURI("/geniusclassrooms", true);
	}

	// on ressort la liste des eleves en Base de donnees et on se dirige vers la gestion des leves avec
	
	$classes = $this->managers->getManagerOf("Classe")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $classes);
		$this->useView("enseignant_classes_lister");
}

// cette faction fait apparaitre le dashbord de l'enseignant sur ses eleves
public function executeenseignant_eleves_dash(\Library\HTTPRequest $request){
	// Dans le cas ou quelqu'un autrue qu'un enseignant tente d.acceder

	if($request->app()->user()->getAttribute(EUser::USER_ROLE) != PersonneFormBuilder::ROLE_PROFESSEUR){
		$request->app()->forWardURI("/geniusclassrooms", true);
	}

	// on recupere l'entite eleve selectionne
	$eleve = $this->managers->getManagerOf("Eleve")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$eleve]);


	// cc
	// on recupere les fautes commises par cet eleve
	$fautes = $this->managers->getManagerOf("Fautes")->getAssociateTo(
		new Eleve(["id" => $request->getData("id")])
	);
	$this->page()->addVar(Page::NS_ENTITIES_LIST, [$fautes]);

	// on recupere les sanction de l'eleve
	$sanction = $this->managers->getManagerOf("Sanction_Prevue")->getAssociateTo(
		new Eleve(["id" => $request->getData("id")])
	);
	$this->page()->addVar(Page::NS_ENTITIES_LIST, [$sanction]);

	$this->useView("enseignant_eleves_dash");

}



// AJAX
/**
 * Cette fonction nous permet de recuper de maniere assyncrhone la liste des sanction et des eleves
 */
public function executeAJAX_GET_LISTE_FAUTES_SANCTIONS(\Library\HTTPRequest $request){
		// on recupere la liste des sanctions et fautes

		$eleve = new Eleve(["id" => $request->app()->user()->getAttribute(EUser::USER_ID)]);

		$liste_fautes = $this->managers->getManagerOf("Fautes")->getAssociateTo($eleve);
		$liste_sanctions = $this->managers->getManagerOf("Sanction_Prevue")->getAssociateTo($eleve);

		// translate
		$liste_faute_tab = EUser::parseFautesToTab($liste_fautes);
		$liste_sanctions_tab = EUser::parseSanctionsToTab($liste_sanctions);
		
		$datas = [
			"fautes"    => $liste_faute_tab,
			"sanctions" => $liste_sanctions_tab
		];

		echo(json_encode($datas));

		$this->page()->withoutLayout();
		http_response_code(200);
		$this->useView("ajax_view");
}


}