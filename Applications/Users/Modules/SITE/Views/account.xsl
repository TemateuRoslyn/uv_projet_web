<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />


	<!-- on recupere la variable de session -->
	<xsl:variable name="session" select="//pageScope/sessionScope" />

	<xsl:variable name="lerole" select="$session/role/@value" />
	<xsl:variable name="nom" select="$session/nom/@value" />
	<xsl:variable name="prenom" select="$session/prenom/@value" />
	<xsl:variable name="date_de_naisssancce" select="$session/date_de_naisssancce/@value" />
	<xsl:variable name="lieu_de_naissance_" select="$session/lieu_de_naissance_/@value" />
	<xsl:variable name="email" select="$session/email/@value" />
	<xsl:variable name="sexe" select="$session/sexe/@value" />
	<xsl:variable name="tel" select="$session/tel/@value" />


	<!-- nom -->
	<xsl:template match="nom">
		<xsl:value-of select="$nom" disable-output-escaping="yes"/>
	</xsl:template>

		<!-- prenom -->
	<xsl:template match="prenom">
		<xsl:value-of select="$prenom" disable-output-escaping="yes"/>
	</xsl:template>

	<!-- naissance -->
	<xsl:template match="datenaiss">
	<xsl:value-of select="$date_de_naisssancce" disable-output-escaping="yes"/>
	</xsl:template>

	<!-- lieu -->

	<xsl:template match="naissance">
		<xsl:value-of select="$lieu_de_naissance_" disable-output-escaping="yes"/>
	</xsl:template>

<!-- email -->
		<xsl:template match="email">
		<xsl:value-of select="$email" disable-output-escaping="yes"/>
	</xsl:template>
	<!-- tel -->
	<xsl:template match="tel">
		<xsl:value-of select="$tel" disable-output-escaping="yes"/>
	</xsl:template>

	<!-- sexe -->
	<xsl:template match="sexe">
		<xsl:value-of select="$sexe" disable-output-escaping="yes"/>
	</xsl:template>



	
	





		


</xsl:stylesheet>