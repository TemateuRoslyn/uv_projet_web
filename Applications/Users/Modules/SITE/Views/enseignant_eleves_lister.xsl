<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:template match="view-action-link">
		<xsl:param name="view-eleve"></xsl:param>
		<a href="/geniusclassrooms/prof/gestion/eleves/dash{$view-eleve/@id}/"><button class="btn btn-warning">param</button></a>
		<!-- <a href="/dashbord/eleves/update{$view-eleve/@id}/"><span class="fa fa-edit" style="color: orange"></span></a>
		<a href="/dashbord/eleves/delete{$view-eleve/@id}/"><span class="fa fa-trash" style="color: red"></span></a> -->
	</xsl:template>

	<xsl:template match="view-titre">
		<xsl:param name="view-classe" />
			<div class="card-header">
               <h3 class="card-title">
			   		<xsl:value-of select="$view-classe/@nom_classe" disable-output-escaping="yes"/>
			   </h3>
            </div>
	</xsl:template>

</xsl:stylesheet>