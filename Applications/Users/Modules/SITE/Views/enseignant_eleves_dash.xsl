<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<!-- on recupere la classe de l1eleve en session -->

	<xsl:variable name="session" select="//pageScope/sessionScope" />
	<xsl:variable name="classe" select="$session/classeleve/@value" />

									
			<!-- <xsl:param name="view-fautes"></xsl:param> -->
	

<xsl:template match="eleve-form-view">
		<xsl:param name="view-eleve" />
		
			<div class="elve-info-card" style="width: 80%">
				<img class="elve-picture" src="/Web/assets/site-picture/eleves/maestros.jpg" alt="Profile Eleve"/>
				<div class="information">
					<p class=""><span class="eleve-field eleve-nom">Nom : </span><xsl:value-of select="$view-eleve/@nom" disable-output-escaping="yes"/></p>
					<p class=""><span class="eleve-field eleve-nom">Prénom : </span><xsl:value-of select="$view-eleve/@prenom" disable-output-escaping="yes"/></p>
					<p class=""><span class="eleve-field eleve-nom">Classe : </span><xsl:value-of select="$classe" disable-output-escaping="yes"/></p>
				</div>

            </div>
</xsl:template>

		
	
			
                   
	



</xsl:stylesheet>