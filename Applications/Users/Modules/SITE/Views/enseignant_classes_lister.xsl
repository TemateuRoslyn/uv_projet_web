<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />


	<xsl:template match="view-action-link">
	<xsl:param name="view-classe"></xsl:param>
		<a href="/geniusclassrooms/prof/gestion/eleves/lister{$view-classe/@id}/"><span class="fa fa-list" style="color: green"></span></a>
	</xsl:template>

</xsl:stylesheet>