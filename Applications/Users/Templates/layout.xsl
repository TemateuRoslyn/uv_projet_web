<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="./../../../AppViews/libxsl.xsl" />

	<xsl:output method="html" encoding="UTF-8" />

	<xsl:param name="content-action-view" />

	<xsl:template match="content-action-view">
		<xsl:value-of select="$content-action-view" disable-output-escaping="yes" />
	</xsl:template>

	<xsl:variable name="session" select="//pageScope/sessionScope" />

	<xsl:template match="pideria-session">
        <!-- <xsl:value-of select="$session/role/@value" /> -->
        <xsl:variable name="lerole" select="$session/role/@value" />
		<!-- S'il s'agit du surveillant general -->
		<xsl:if test="$lerole=3" >
			<div class="dropdown-item"><a href="">SG Administration</a></div>
		</xsl:if>
        <xsl:if test="$lerole=4" >
			<div class="dropdown-item"><a href="/geniusclassrooms/prof/gestion/classes/lister">Gérer mes Classes</a></div>
		</xsl:if>
		<xsl:if test="$lerole=5" >
			<!-- <div class="dropdown-item"><a href="">Espace Elève</a></div> -->
		</xsl:if>
		
			
			

	</xsl:template>


</xsl:stylesheet>