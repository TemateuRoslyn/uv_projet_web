<?php

namespace Applications\Backend;

use Library\Interfaces\Filter;

class AuthFilter implements Filter
{
    public function init(\Library\FilterConfig $config)
    {
    }

    public function doFilter(\Library\HTTPRequest $request, \Library\HTTPResponse $response, \Library\FilterChain $chain)
    {
        // var_dump("kfkjfk");
        if ($request->app()->user()->adminAuthentificated() || $request->requestURI() == "/admin/school/administration/register") {
            $request->app()->user()->setProfile("sup-admin");
            $request->app()->user()->setAdminAuthentificated();

            $chain->doFilter($request, $response);
        } else {
            $request->app()->forWardURI("/admin/connexion", false);
            $chain->doFilter($request, $response);
            // $controller = new Modules\Connexion\ConnexionController($this, 'Connexion', 'index', "Connexion");
        }
    }

    public function destroy()
    {
    }
}
