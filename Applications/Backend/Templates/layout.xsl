<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../../Frontend/Templates/layout.xsl" />

	<xsl:output method="html" encoding="UTF-8" />

	<xsl:param name="page-pdf" />
	
	<xsl:template match="page-pdf">
		<xsl:if test="not($page-pdf)">
			<xsl:apply-templates />
		</xsl:if>
	</xsl:template>

	<xsl:template match="data-extract">
	</xsl:template>
</xsl:stylesheet>