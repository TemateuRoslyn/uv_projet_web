<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="./../../../AppViews/libxsl.xsl" />

	<xsl:output method="html" encoding="UTF-8" />

	<xsl:param name="content-action-view" />

	<xsl:template match="content-action-view">
		<xsl:value-of select="$content-action-view" disable-output-escaping="yes" />
	</xsl:template>

</xsl:stylesheet>