<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
	<!-- 
		Variable devant contenir la valeur de l'attribut
		$this->page->addVar("formulaire", $formHandler->form()->createView());

		Pour les valeurs textuelles, il est necessaire de déclarer un paramètre attendu
	 -->
	<xsl:param name="formulaire" />

	<xsl:template match="foramulaire-inscription">
		<xsl:value-of select="$formulaire" disable-output-escaping="yes"/>
	</xsl:template>

</xsl:stylesheet>