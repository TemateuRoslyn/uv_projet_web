<?php

namespace Applications\Frontend\Modules\TestModule;

use AppliLib\Entities\Cours;
use AppliLib\Entities\Professeur;
use AppliLib\Entities\Regles;
use DateTime;
use Library\BackController;
use Library\HTTPRequest;
use Library\Page;

class TestModuleController extends BackController
{
	//Test sur le transfére d'une entité vers la vue
	public function executeIndex(HTTPRequest $request)
	{
		// var_dump(md5(uniqid(time())));
		//Creation d'une entité
		// $rule = new Professeur([
		// 	"id_personne" => 1,
		// 	"nom" => "1",
		// 	"prenom" => "1",
		// 	"date_de_naisssancce" => new DateTime(),
		// 	"login_" => "1",
		// 	"mot_de_passe" => "1",
		// 	"photo" => "1",
		// 	"Email" => "1",
		// 	"lieu_de_naissance" => "1",
		// 	"Tel" => "1",
		// 	"Sexe" => "4",
		// 	"Statut" => "1",
		// ]);
		/** @var Professeur */
		// $rule = $this->managers->getManagerOf("Professeur")->getList();
		// $rule = $this->managers->getManagerOf("Professeur")->delete();
		// $rule = $this->managers->getManagerOf("Professeur")->getUnique(1);
		// $rule->prepareToModify();
		// $rule->setSexe(9);
		// var_dump($_SESSION);
		// $rule->setLibelle_regle("libelle_regle_4");
		// $this->managers->getManagerOf("Professeur")->save($rule);
		// $this->page()->addVar("regle", [$rule]);
		// $this->testVar($rule);
	}

	//Test sur le transfére d'une entité vers la vue
	public function executeIndex2(HTTPRequest $request)
	{
		// var_dump(md5(uniqid(time())));
		//Creation d'une entité
		// $cours = new Cours();
		// $cours->setAnneeAcad("2020-2021");
		// $cours->setIntitule("mon cours");
		// $result = [$cours];

		// $cours1 = new Cours();
		// $cours1->setAnneeAcad("2020-2021");
		// $cours1->setIntitule("mon cours 3");
		// $result[] = $cours1;

		// // Transfert de l'entité à la page
		// $this->page()->addVar(Page::NS_ENTITY, [$cours]);
		// //Transfère d'une liste d'entités la vue
		// $this->page()->addVar(Page::NS_ENTITIES_LIST, $result);
	}

	//Méthode de définition d'une insertion d'une donnée en base de données
	public function executeInsertCours(HTTPRequest $request)
	{
		/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Cours", $request, 1);
		// $formHandler
		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('Cours défini !');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("afficher-cours" . $formHandler->form()->entity()->id() . "/");
		}

		$this->page->addVar("formulaire", $formHandler->form()->createView());
	}

	public function executeReponseDevoirs(HTTPRequest $request)
	{
		$this->page->addVar('title', 'Reponse !');

		$formHandler = $this->formHandlerFromEntity("Devoirreponse", $request, null);
		/** @var Devoirreponse */
		$reponse = $formHandler->form()->entity();
		$reponse->setNote(-1);

		if ($formHandler->process()) {
			$eleve = new Eleve(["id" => $this->app()->user()->getAttribute("idEleve")]);
			$devoirs = new Devoir(["id" => $request->getData("idDevoir")]);

			$this->managers->getManagerOf("Devoirreponse")->addAssociationClass([$eleve], $devoirs, [$formHandler->form()->entity()]);
			// $this->managers->getManagerOf("Cours")->addAssociations([$formHandler->form()->entity()], new Cours(["id" => $request->getData("idCours")]));
			// $this->testVar("");
			$this->app->httpResponse()->redirect('..');
		}

		$this->page->addVar("form", $formHandler->form()->createView());
	}

	public function executeModifyProportion(\Library\HTTPRequest $request)
	{
		$this->page->addVar('title', 'Admin | Nouveau Planning');

		$eval = $this->managers->getManagerOf("Evalproportion")->getUnique($request->getData("id"));
		if (is_null($eval)) $this->app->httpResponse()->redirect404();
		// $this->testVar($eval);
		$formHandler = $this->formHandlerFromEntity("Evalproportion", $request, $eval);
		$formHandler->form()->entity()->setIdEtablissement($eval->idEtablissement());
		// $this->testVar($formHandler->form()->entity());
		if ($formHandler->process()) {
			$this->app->user()->setFlash('Programme modifié.');
			$this->app->httpResponse()->redirect('/s.admin/school/planning/');
		}

		$this->page->addForm($formHandler->form()->createXMLView());
		$this->page->addVar("form-title", "modifié le planning!");
	}

	public function executeAddAssociation(HTTPRequest $request)
	{
		$this->page->addVar('title', 'Reponse !');

		$formHandler = $this->formHandlerFromEntity("Devoirreponse", $request, null);

		if ($formHandler->process()) {
			$eleve = new Eleve(["id" => $this->app()->user()->getAttribute("idEleve")]);
			$devoirs = new Devoir(["id" => $request->getData("idDevoir")]);

			$this->managers->getManagerOf("Devoirreponse")->addAssociationClass([$eleve], $devoirs, [$formHandler->form()->entity()]);
			$this->managers->getManagerOf("Cours")->addAssociations([$formHandler->form()->entity()], new Cours(["id" => $request->getData("idCours")]));
			// $this->testVar("");
			$this->app->httpResponse()->redirect('..');
		}
	}

	public function executeShow(HTTPRequest $request)
	{
		$devoir = $this->managers->getManagerOf("Devoir")->getUnique($request->getData("id"));
		$response = $this->managers->getManagerOf("Devoirreponse")->getAssociateClassTo(new Eleve(["id" => $this->app()->user()->getAttribute(EUsers::ATTRIB_USER_ID_ELEVE)]));
		// $this->testVar($response);
		$comments = [];

		if (!empty($response)) {
			/** @var Managers_PDO */
			$managers = $this->managers->getManagerOf("Commentaire");
			$comments = $managers->getAssociateToScheme($response[0], Entity_PDO::SENS_UP, ["Personne" => Entity_PDO::SENS_UP]);
			// $this->testVar($comments[0]->arrayDescriptions());
		}
	}

	public function executeShowCours(HTTPRequest $request)
	{
		//Permet la récupération d'un cours en BD a partir de son identifiant
		$cours = $this->managers->getManagerOf("Cours")->getUnique($request->getParam("idCours"));
		//Récupertion du manager du Cours: CoursManager_PDO
		$managers = $this->managers->getManagerOf("Cours");

		$this->page()->addVar(Page::NS_ENTITY, [$cours]);
		$this->useView("index");
	}

	public function executeShowListCours(HTTPRequest $request)
	{
		$cours = $this->managers->getManagerOf("Cours")->getList();

		$this->page()->addVar(Page::NS_ENTITIES_LIST, $cours);
		$this->useView("index");
	}

	public function executeSubscribeUser(\Library\HTTPRequest $request)
	{
		$device = new Device([
			"linknotification" => $request->postData("link"),
			"profile" => $this->app()->user()->profile(),
			"profilesign" => "personne" . $this->app()->user()->getAttribute(EUsers::ATTRIB_USER_ID_PERSONNE),
		]);
		/** @var Device[] */
		$devices = $this->managers->getManagerOf("Device")->getAssociateTo(new Personne(["id" => $this->app()->user()->getAttribute(EUsers::ATTRIB_USER_ID_PERSONNE)]));

		$this->managers->getManagerOf("Device")->save($device);
		$this->managers->getManagerOf("Personne")->addAssociations([$device], new Personne(["id" => $this->app()->user()->getAttribute(EUsers::ATTRIB_USER_ID_PERSONNE)]));
	}

	//Insertion d'un fichier
	public function executeProfile(HTTPRequest $request)
	{
		if ($request->method() == "POST") {
			// si la requete est de type poste il recupere l'objet 
			$upFile = $request->getUpFile("profileImage");
			/** @var Personne */
			$personne = $this->getElementExist("Personne", $this->app()->user()->getAttribute("idPersonne"));
			$personne->setProfileImage($upFile->fileName());

			move_uploaded_file($upFile->fileUpName(), dirname(__FILE__) . "/../../../../Web/img/" . $upFile->fileName());
			$this->app()->user()->setFlash("Profile Enrégistré!");

			$this->managers->getManagerOf("Personne")->save($personne);
			$this->app()->httpResponse()->redirect("/");
		}
	}
}
