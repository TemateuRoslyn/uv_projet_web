<?php

namespace AppliLib\Models;

class DepartementManager_PDO extends \Library\Managers_PDO
{
    public function __construct($api, \PDO $dao)
    {
        parent::__construct($api, $dao);
    }

    public function forEts(int $idEts): array
    {
        // bindFetchAllRequest retourne un tableau du nom de l'entite
        return $this->bindFetchAllRequest(
            "SELECT depart.*, CONCAT(pers.nom, ' ',pers.prenom) pNom 
             FROM departement depart 
             LEFT JOIN chefdepartement chefD 
             ON chefD.idDepartement = depart.id
             LEFT JOIN administrateur adminE 
             ON adminE.id = chefD.idAdministrateur
             LEFT JOIN personne pers 
             ON adminE.idPersonne = pers.id
             WHERE depart.idEtablissement = $idEts"
        );
    }

    public function forAdmin(int $idAdmin): array
    {
        // bindFetchAllRequest: retourne un element unique
        // ils retourne toujours soit une entite, soit un tableau d'entites
        return $this->bindFetchRequest(
            "SELECT dep.* FROM departement dep 
             JOIN chefdepartement chef 
             ON chef.idDepartement = dep.id
             WHERE chef.idAdministrateur = $idAdmin"
        );
    }
}
