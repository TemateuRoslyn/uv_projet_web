<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:template match="view-roreglement_iterieurele-element">
	<xsl:param name="view-reglement_iterieure"></xsl:param>
		<xsl:value-of select="$view-reglement_iterieure/@id" disable-output-escaping="yes"/>
	</xsl:template>

</xsl:stylesheet>