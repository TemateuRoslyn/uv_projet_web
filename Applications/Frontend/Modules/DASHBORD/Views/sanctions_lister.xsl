<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />


	<xsl:template match="view-action-link">
	<xsl:param name="view-sanction_prevue"></xsl:param>
	<div class="view-action-link">
		<div><a href="/dashbord/sanctions/view{$view-sanction_prevue/@id}/" class="action-link-item link-item-view"><span class="fa fa-eye" ></span></a></div>
		<div><a href="/dashbord/sanctions/update{$view-sanction_prevue/@id}/" class="action-link-item link-item-edit"><span class="fa fa-edit"></span></a></div>
		<div><a href="/dashbord/sanctions/delete{$view-sanction_prevue/@id}/" class="action-link-item link-item-delete"><span class="fa fa-trash"></span></a></div>
	</div>

	</xsl:template>

</xsl:stylesheet>