<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:template match="view-eleve-element">
	<xsl:param name="view-eleve"></xsl:param>
		<xsl:value-of select="$view-eleve/@id" disable-output-escaping="yes"/>
	</xsl:template>
</xsl:stylesheet>