<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:template match="view-action-link">
	<xsl:param name="view-classe"></xsl:param>
		<a href="/dashbord/classes/view{$view-classe/@id}/"><span class="fa fa-eye" style="color: green"></span></a>
		<a href="/dashbord/classes/update{$view-classe/@id}/"><span class="fa fa-edit" style="color: orange"></span></a>
		<a href="/dashbord/classes/delete{$view-classe/@id}/"><span class="fa fa-trash" style="color: red"></span></a>
	</xsl:template>
</xsl:stylesheet>