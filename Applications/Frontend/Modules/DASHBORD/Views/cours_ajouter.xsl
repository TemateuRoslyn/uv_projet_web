<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:param name="form" />

	<xsl:template match="cours-form-add">
		<xsl:value-of select="$form" disable-output-escaping="yes"/>
	</xsl:template>

</xsl:stylesheet>