<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

		<xsl:param name="form" />

	<xsl:template match="professeur-form-add">
		<xsl:value-of select="$form" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="list-cours">
		<xsl:param name="view-cour" />
		<option value="{$view-cour/@id}"><xsl:value-of select="$view-cour/@date_cour" disable-output-escaping="yes"/></option>
		
	</xsl:template>

	
</xsl:stylesheet>