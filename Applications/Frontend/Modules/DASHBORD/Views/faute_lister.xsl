<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
<xsl:template match="view-action-link">
	<xsl:param name="view-fautes"></xsl:param>
		<a href="/dashbord/fautes/view{$view-fautes/@id}/"><span class="fa fa-eye" style="color: green"></span></a>
		<a href="/dashbord/fautes/update{$view-fautes/@id}/"><span class="fa fa-edit" style="color: orange"></span></a>
		<a href="/dashbord/fautes/delete{$view-fautes/@id}/"><span class="fa fa-trash" style="color: red"></span></a>
	</xsl:template>
</xsl:stylesheet>