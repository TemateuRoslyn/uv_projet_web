<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:param name="form" />
	<xsl:template match="sanction-form-add">
		<xsl:value-of select="$form" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="list-classes">
		<xsl:param name="view-classe" />
		<option value="{$view-classe/@id}"><xsl:value-of select="$view-classe/@nom_classe" disable-output-escaping="yes"/></option>
	</xsl:template>

	<xsl:template match="list-regle">
		<xsl:param name="view-regles" />
		<option value="{$view-regles/@id}"><xsl:value-of select="$view-regles/@libelle_regle" disable-output-escaping="yes"/></option>
	</xsl:template>

	<xsl:template match="list-fautes">
		<xsl:param name="view-fautes" />
		<option value="{$view-fautes/@id}"><xsl:value-of select="$view-fautes/@nom" disable-output-escaping="yes"/></option>
	</xsl:template>



</xsl:stylesheet>