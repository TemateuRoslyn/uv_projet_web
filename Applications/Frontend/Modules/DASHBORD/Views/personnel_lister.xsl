<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
<xsl:template match="view-action-link">
	<xsl:param name="view-personnel"></xsl:param>
		<a href="/dashbord/personnel_administratif/view{$view-personnel/@id}/"><span class="fa fa-eye" style="color: green"></span></a>
		<a href="/dashbord/personnel_administratif/update{$view-personnel/@id}/"><span class="fa fa-edit" style="color: orange"></span></a>
		<a href="/dashbord/personnel_administratif/delete{$view-personnel/@id}/"><span class="fa fa-trash" style="color: red"></span></a>
	</xsl:template>
</xsl:stylesheet>