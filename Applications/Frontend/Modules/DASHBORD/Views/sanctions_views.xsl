<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

	<xsl:template match="view-sanction_prevue-libelle_sanction">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@libelle_sanction" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-niveau_gravite">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@niveau_gravite" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-motif">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@motif" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-sanction_prevue-duree_validite">
		<xsl:param name="view-sanction_prevue" />
		<!--  -->
		<xsl:value-of select="$view-sanction_prevue/@duree_validite" disable-output-escaping="yes"/>
	</xsl:template>

</xsl:stylesheet>