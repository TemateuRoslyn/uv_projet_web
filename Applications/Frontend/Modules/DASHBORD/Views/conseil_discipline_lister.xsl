<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />

<xsl:template match="view-action-link">
	<xsl:param name="view-conseil_discipline"></xsl:param>
		<a href="/dashbord/conseil_disciplines/view{$view-conseil_discipline/@id}/"><span class="fa fa-eye" style="color: green"></span></a>
		<a href="/dashbord/conseil_disciplines/update{$view-conseil_discipline/@id}/"><span class="fa fa-edit" style="color: orange"></span></a>
		<a href="/dashbord/conseil_disciplines/delete{$view-conseil_discipline/@id}/"><span class="fa fa-trash" style="color: red"></span></a>
	</xsl:template>
</xsl:stylesheet>