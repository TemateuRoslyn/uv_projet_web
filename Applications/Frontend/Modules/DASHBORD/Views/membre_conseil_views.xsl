<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
<xsl:template match="view-membre_conseil-element">
	<xsl:param name="view-membre_conseil"></xsl:param>
		<xsl:value-of select="$view-membre_conseil/@id" disable-output-escaping="yes"/>
	</xsl:template>
</xsl:stylesheet>