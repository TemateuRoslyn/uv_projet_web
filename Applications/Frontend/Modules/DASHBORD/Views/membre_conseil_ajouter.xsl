<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
<xsl:param name="form" />

	<xsl:template match="membre-form-add">
		<xsl:value-of select="$form" disable-output-escaping="yes"/>
	</xsl:template>



	
	<xsl:template match="view-membre_conseil-id_liste">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_liste" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-id_chef">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_chef" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-id_surveillant_G">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_surveillant_G" disable-output-escaping="yes"/>
	</xsl:template>

	<xsl:template match="view-membre_conseil-id_representant_E">
		<xsl:param name="view-membre_conseil" />
		<!--  -->
		<xsl:value-of select="$view-membre_conseil/@id_representant_E" disable-output-escaping="yes"/>
	</xsl:template>

</xsl:stylesheet>