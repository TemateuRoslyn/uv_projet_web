<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
		<xsl:template match="regle-form-add">
			<xsl:value-of select="$form" disable-output-escaping="yes"/>
		</xsl:template>

	<!-- on affiche le reglement interieur -->
		<xsl:template match="list-reglement">
			<xsl:param name="view-reglement_iterieure" />
			<option value="{$view-reglement_iterieure/@id}"><xsl:value-of select="$view-reglement_iterieure/@nomEtablissement" disable-output-escaping="yes"/></option>
		</xsl:template>
</xsl:stylesheet>