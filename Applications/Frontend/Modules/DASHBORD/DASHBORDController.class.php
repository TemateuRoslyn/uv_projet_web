<?php
	namespace Applications\FRONTEND\Modules\DASHBORD;
	use AppliLib\ABackController;
	use Library\Page;
	use  AppliLib\Entities\Classe;
	use  AppliLib\Entities\Reglement_Iterieure;
	use  AppliLib\Entities\Regles;
	use  AppliLib\Entities\Eleve;
	use  AppliLib\Entities\Cour;
	use  AppliLib\Entities\Fautes;
	use  AppliLib\EUser;
	use AppliLib\FormHeaders\NotificationFormHeader;

	 class DASHBORDController extends ABackController
            {

/**
 * Cours - CRUD
 */
public function executecours_views(\Library\HTTPRequest $request){
	$cour = $this->managers->getManagerOf("Cour")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$cour]);

}
/**
 * Cours : Listing
 */
public function executecours_lister(\Library\HTTPRequest $request){
	$cours = $this->managers->getManagerOf("Cour")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $cours);
		$this->useView("cours_lister");
}
// suppression d'un cours
public function executecours_supprimer(\Library\HTTPRequest $request){
	$cours = $this->managers->getManagerOf("Cour")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}

	
public function executecours_modifier(\Library\HTTPRequest $request){
	
		$formHandler = $this->formHandler("Cour", $request,$request->getParam("id"));

		if ($formHandler->process()) {
			
			$this->app->user()->setFlash('add record');

			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}

/**
 * ajouter un cours
 */
public function executecours_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Cour", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}

/**
 * Role: Views
 */
public function executerole_view(\Library\HTTPRequest $request){
	$role = $this->managers->getManagerOf("Role")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$role]);
}

/**
 * Role: listing
 */
public function executerole_lister(\Library\HTTPRequest $request){
		$role = $this->managers->getManagerOf("Role")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $role);
		$this->useView("role_lister");
}
/**
 * Role: Modifier
 */
public function executerole_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Role", $request, $request->getData("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
/**
 * Role: supprimer
 */
// public function executerole_supprimer(\Library\HTTPRequest $request){
	// $role = $this->managers->getManagerOf("Role")->delete($request->getData("id"));
	// $this->app->httpResponse()->redirect("../list");
// }


/**
 * --------------------		ROLE	 ----------------------------------
 */

/**
 * Role - Ajout
 */
public function executerole_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Role", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
public function executeSetting_A(\Library\HTTPRequest $request){}
public function executeAccount_A(\Library\HTTPRequest $request){}
	// vue d'un enseignant
public function executeenseignant_views(\Library\HTTPRequest $request){
	$professeur = $this->managers->getManagerOf("Professeur")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$professeur]);
}
public function executemembre_conseil_views(\Library\HTTPRequest $request){
	$membre_conseil = $this->managers->getManagerOf("membre_conseil")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$membre_conseil]);
}
//non fait
public function executeconseil_discipline_views(\Library\HTTPRequest $request){}
	// vue d'une regle
public function executeregle_views(\Library\HTTPRequest $request){
	$regle = $this->managers->getManagerOf("Regles")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$regle]);
}
// vue d'une faute
public function executefaute_views(\Library\HTTPRequest $request){
	$faute = $this->managers->getManagerOf("Fautes")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$faute]);
}
//vue d'un eleve
public function executeeleve_views(\Library\HTTPRequest $request){
	$eleve = $this->managers->getManagerOf("Eleve")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$eleve]);
}
public function executeavertissment_views(\Library\HTTPRequest $request){
	$avertissement = $this->managers->getManagerOf("Avertissement")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$avertissement]);
}
public function executeconvocation_views(\Library\HTTPRequest $request){
	$convocation = $this->managers->getManagerOf("Convocation")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$convocation]);
}
	//classe views
public function executeclasse_views(\Library\HTTPRequest $request){
	
	$eleves = $this->managers->getManagerOf("Eleve")->getAssociateTo(
		new Classe(["id" => $request->getData("id")])
	);
	
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $eleves);
	$classe = $this->managers->getManagerOf("Classe")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$classe]);
}
// vue d'une personnel
public function executepersonnel_views(\Library\HTTPRequest $request){
	$personnel = $this->managers->getManagerOf("Personnel")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$personnel]);
}
public function executenotification_views(\Library\HTTPRequest $request){}

//Sanction view
public function executesanctions_views(\Library\HTTPRequest $request){
	$blame = $this->managers->getManagerOf("Sanction_Prevue")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$blame]);
}

//exclusion view
public function executeexclusion_views(\Library\HTTPRequest $request){
	$exclusion = $this->managers->getManagerOf("Exclusion_definitive")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$exclusion]);
}

//exclusion pontuelle view
public function executeexclusion_ponctuelle_views(\Library\HTTPRequest $request){
	$exclusion_pontuelle = $this->managers->getManagerOf("Exclusion_Ponctuelle")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$exclusion_pontuelle]);
}

// lister les exclusions ponctuelle
public function executeexclusion_ponctuelle_lister(\Library\HTTPRequest $request){
	$exclusion_pontuelle = $this->managers->getManagerOf("Exclusion_ponctuelle")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $exclusion_pontuelle);
	$this->useView("exclusion_ponctuelle_lister");

}
//exclusion pontuelle suprimée
public function executeexclusion_ponctuelle_supprimer(\Library\HTTPRequest $request){
	$exclusion_pontuelle = $this->managers->getManagerOf("Exclusion_Ponctuelle")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}

//exclusion pontuelle modifiée
public function executeexclusion_ponctuelle_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Exclusion_Ponctuelle", $request, $request->getData("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
	
}

// ajout d une exclusion ponctuelle
public function executeexclusion_ponctuelle_ajouter(\Library\HTTPRequest $request){

	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Exclusion_ponctuelle", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
// lister les exclusions
public function executeexclusion_lister(\Library\HTTPRequest $request){
		$exclusion = $this->managers->getManagerOf("Exclusion_definitive")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $exclusion);
		$this->useView("exclusion_lister");

}
//exclusion suprimée
public function executeexclusion_supprimer(\Library\HTTPRequest $request){
	$exclusion = $this->managers->getManagerOf("Exclusion_definitive")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}

//exclusion modifié
public function executeexclusion_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Exclusion_definitive", $request, $request->getData("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
// ajout d une exclusion
public function executeexclusion_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Exclusion_definitive", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
//lister les sanction
public function executesanctions_lister(\Library\HTTPRequest $request){
	$sanctions = $this->managers->getManagerOf("Sanction_Prevue")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $sanctions);
	$this->useView("sanctions_lister");

}

//Sanction suprimer
public function executesanctions_supprimer(\Library\HTTPRequest $request){
	$blame = $this->managers->getManagerOf("Sanction_Prevue")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}

//Sanction madifier
public function executesanctions_modifier(\Library\HTTPRequest $request){
	
		$formHandler = $this->formHandler("Sanction_Prevue", $request, $request->getData("id"));

		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		// on recupere les salle de classe 	
	$classe = $this->managers->getManagerOf("Classe")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
	// on listes les regles
	$regles = $this->managers->getManagerOf("Regles")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $regles);

	// on listes les fautes
	$fautes = $this->managers->getManagerOf("Fautes")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $fautes);


		$this->page->addVar("form", $formHandler->form()->createView());
}

//ajout d'une Sanction
public function executesanctions_ajouter(\Library\HTTPRequest $request){

		$formHandler = $this->formHandler("Sanction_Prevue", $request);
		if ($formHandler->process()) {
			$this->managers->getManagerOf("Eleve")->addAssociations([$formHandler->form()->entity()], new Eleve(["id" => $request->postData("select_created_name")]));
			$this->managers->getManagerOf("Regles")->addAssociations([$formHandler->form()->entity()], new Regles(["id" => $request->postData("regles")]));
			$this->managers->getManagerOf("Fautes")->addAssociations([$formHandler->form()->entity()], new Fautes(["id" => $request->postData("fautes")]));
		
			$this->app->user()->setFlash('add record');
			$this->app->httpResponse()->redirect("list");
		}

		// on recupere les salle de classe 	
	$classe = $this->managers->getManagerOf("Classe")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
	// on listes les regles
	$regles = $this->managers->getManagerOf("Regles")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $regles);

	// on listes les fautes
	$fautes = $this->managers->getManagerOf("Fautes")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $fautes);

	$this->page->addVar("form", $formHandler->form()->createView());

}
public function executenotification_lister(\Library\HTTPRequest $request){}
public function executenotification_supprimer(\Library\HTTPRequest $request){}
public function executenotification_modifier(\Library\HTTPRequest $request){}
/**
 * Notification: ajouter
 */
public function executenotification_ajouter(\Library\HTTPRequest $request){
	die("Arret depuis notification");
}
/**
 * Personnelle Adminitratif: Listing
 */
public function executepersonnel_lister(\Library\HTTPRequest $request){
	$personnel = $this->managers->getManagerOf("Personnel")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $personnel);
		$this->useView("personnel_lister");
}
//suppression d'un personnel
public function executepersonnel_supprimer(\Library\HTTPRequest $request){
	$personnel = $this->managers->getManagerOf("Personnel")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
	//modification d'un personnel
public function executepersonnel_modifier(\Library\HTTPRequest $request){
		/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Personnel", $request,$request->getParam("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
public function executeconvocation_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Convocation", $request, $request->getData("id"));
		
		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());

}
public function executeconvocation_supprimer(\Library\HTTPRequest $request){
	$convocation = $this->managers->getManagerOf("Convocation")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
	/**
 * --------------------		Classe	 ----------------------------------
 */

/**
 * Ajout d'un Classe
 */
public function executeclasse_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Classe", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}

/**
 * Personnel Administratif: Ajout
 */
public function executepersonnel_ajouter(\Library\HTTPRequest $request){
		/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Personnel", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}

// lister un avertissement
public function executeavertissement_lister(\Library\HTTPRequest $request){
	$avertissement = $this->managers->getManagerOf("Avertissement")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $avertissement);
		$this->useView("avertissement_lister");
}
	
/**
 * Classe: listing
 */
public function executeclasse_lister(\Library\HTTPRequest $request){
	$classe = $this->managers->getManagerOf("Classe")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
		$this->useView("classe_lister");
}
/**
 * Regle: listing
 */
public function executeregle_lister(\Library\HTTPRequest $request){
	$regle = $this->managers->getManagerOf("Regles")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $regle);
		$this->useView("regle_lister");
}
//suppression d'une regle
public function executeregle_supprimer(\Library\HTTPRequest $request){
	$regle = $this->managers->getManagerOf("Regles")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}

	//modification d'une regle
public function executeregle_modifier(\Library\HTTPRequest $request){
		/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Regles", $request,$request->getParam("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		// on recupere le reglement interieur
		$reglement = $this->managers->getManagerOf("Reglement_Iterieure")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $reglement);

		$this->page->addVar("form", $formHandler->form()->createView());
}
	//suppression d'une classe
public function executeclasse_supprimer(\Library\HTTPRequest $request){
	$classe = $this->managers->getManagerOf("Classe")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
	//lister convocation
public function executeconvocation_lister(\Library\HTTPRequest $request){
	$convocation = $this->managers->getManagerOf("Convocation")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $convocation);
	$this->useView("convocation_lister");
}

//avertissement suprimé
public function executeavertissement_supprimer(\Library\HTTPRequest $request){
	$avertissement = $this->managers->getManagerOf("Avertissement")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}


	//lister conseil de discipline
public function executeconseil_discipline_lister(\Library\HTTPRequest $request){
	$conseil__Discipline = $this->managers->getManagerOf("Conseil__Discipline")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $conseil__Discipline);
	$this->useView("conseil_discipline_lister");
}
public function executeconseil_discipline_supprimer(\Library\HTTPRequest $request){
	$conseil__Discipline = $this->managers->getManagerOf("Conseil_discipline")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
public function executeconseil_discipline_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Conseil_discipline", $request, $request->getData("id"));
/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
public function executeavertissement_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Avertissement", $request, $request->getData("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
	//modification d'une classe
public function executeclasse_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Classe", $request,$request->getData("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
/**
 * Role: supprimer
 */
public function executerole_supprimer(\Library\HTTPRequest $request){
	$role = $this->managers->getManagerOf("Role")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
	//lister les membres du conseil
public function executemembre_conseil_lister(\Library\HTTPRequest $request){
	$membre_conseil = $this->managers->getManagerOf("membre_conseil")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $membre_conseil);
	$this->useView("membre_conseil_lister");
}
public function executemembre_conseil_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Membre_Conseil", $request, $request->getData("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
	// modification d'un eleve
public function executeeleve_modifier(\Library\HTTPRequest $request){

		$formHandler = $this->formHandler("Eleve", $request,$request->getParam("id"));
	
		if ($formHandler->process()) {
			// ssocier l'eleve a la salle
			$this->managers->getManagerOf("Classe")->addAssociations(
				[$formHandler->form()->entity()]
				, new Classe(["id" => $request->postData("classe")])
			);
			// mettre en relation la salle et l'eleve
			$this->managers->getManagerOf("Eleve")->addAssociations(
				[new Classe(["id" => $request->postData("classe")])]
				, $formHandler->form()->entity()
			);
	
			$this->app->httpResponse()->redirect('list');
		}
			
			$classe = $this->managers->getManagerOf("Classe")->getList();
			$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
			$this->page->addVar("form", $formHandler->form()->createView());
}
/**
 * Eleve: lister
 */
public function executeeleve__lister(\Library\HTTPRequest $request){
	$eleves = $this->managers->getManagerOf("Eleve")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $eleves);
		$this->useView("eleve__lister");
}
//suppression d'un Eleve
public function executeeleve_supprimer(\Library\HTTPRequest $request){
	$eleve = $this->managers->getManagerOf("Eleve")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
//suppression 
public function executefaute_supprimer(\Library\HTTPRequest $request){
	$faute = $this->managers->getManagerOf("Fautes")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
 //modification
public function executeenseignant__modifier(\Library\HTTPRequest $request){
/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Professeur", $request,$request->getParam("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
	//ajout membre du conseil
public function executemembre_conseil_ajouter(\Library\HTTPRequest $request){
	
		$formHandler = $this->formHandler("Membre_Conseil", $request);

		if ($formHandler->process()) {
			
			$this->app->user()->setFlash('add record');
			$this->app->httpResponse()->redirect("list");
		}

		// --------  ICI on liste les membres   ------------
		$personnel = $this->managers->getManagerOf("Personnel")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $personnel);
		
		$elevees = $this->managers->getManagerOf("Eleve")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $elevees);


		

		$this->page->addVar("form", $formHandler->form()->createView());

}
// modification d'une faute
public function executefaute_modifier(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Fautes", $request,$request->getParam("id"));

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("../list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
// suppression d'un enseignant
public function executeenseignant__supprimer(\Library\HTTPRequest $request){
	$professeur = $this->managers->getManagerOf("Professeur")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
	//lister une faute 
public function executefaute_lister(\Library\HTTPRequest $request){
	$fautes = $this->managers->getManagerOf("Fautes")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $fautes);
	$this->useView("faute_lister");
}

	//lister enseignant
public function executeenseignant__lister(\Library\HTTPRequest $request){
	$professeur = $this->managers->getManagerOf("Professeur")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $professeur);
	$this->useView("enseignant__lister");
}
/**
 * Reglement Interieu: Lister
 */
public function executereglement_interieur_lister(\Library\HTTPRequest $request){
	$reglement_interieur = $this->managers->getManagerOf("Reglement_Iterieure")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $reglement_interieur);
	$this->useView("reglement_interieur_lister");
}


	
public function executereglement_interieur_views(\Library\HTTPRequest $request){
	$reglement = $this->managers->getManagerOf("Reglement_Iterieure")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$reglement]);
}
public function executereglement_interieur_supprimer(\Library\HTTPRequest $request){}

public function executereglement_interieur_modifier(\Library\HTTPRequest $request){
		
	$formHandler = $this->formHandler("Reglement_Iterieure", $request,$request->getParam("id"));

	
	if ($formHandler->process()) {
		
		$this->app->user()->setFlash('add record');
		$this->app->httpResponse()->redirect("../list");
	}

	$this->page->addVar("form", $formHandler->form()->createView());
}


//membre du conseil suprimé
public function executemembre_conseil_supprimer(\Library\HTTPRequest $request){
	$membre_conseil = $this->managers->getManagerOf("membre_conseil")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}
	//ajout convocation
public function executeconvocation_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Convocation", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
//conseil de discipline ajouter
public function executeconseil_discipline_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Conseil__Discipline", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
/**
 * Reglement Onterieur : Ajouter
 */
public function executereglement_interieur_ajouter(\Library\HTTPRequest $request){
	
		$formHandler = $this->formHandler("Reglement_Iterieure", $request);

		if ($formHandler->process()) {
			$this->app->user()->setFlash('add record');
			$this->app->httpResponse()->redirect("list");
		}
		$this->page->addVar("form", $formHandler->form()->createView());
		// die("insertion ok");
}

//ajout d'un avertissement
public function executeavertissment_ajouter(\Library\HTTPRequest $request){

	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Avertissement", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}

		$this->page->addVar("form", $formHandler->form()->createView());
}
/**
 * Eleve: ajouter
 */
public function executeeleve_ajouter(\Library\HTTPRequest $request){
		
	$formHandler = $this->formHandlerFromEntity("Eleve", $request, null);
	
	if ($formHandler->process()) {
		// ssocier l'eleve a la salle
		$this->managers->getManagerOf("Classe")->addAssociations(
			[$formHandler->form()->entity()]
			, new Classe(["id" => $request->postData("classe")])
		);
		// mettre en relation la salle et l'eleve
		$this->managers->getManagerOf("Eleve")->addAssociations(
			[new Classe(["id" => $request->postData("classe")])]
			, $formHandler->form()->entity()
		);

		$this->app->httpResponse()->redirect('list');
	}
		
		$classe = $this->managers->getManagerOf("Classe")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
		
		$this->page->addVar("form", $formHandler->form()->createView());
	}



	// public function executefaute_ajouter(\Library\HTTPRequest $request){

	
	// 	$formHandler = $this->formHandler("Fautes", $request);
	// 	$eleves = new Eleve(["id" => $request->postData("select_created_name")]);
	// 	// $regles = new Regles(["id" => $request->postData("select_created_seconde_name")])
		
	// 	if ($formHandler->process()) {
	// 		$this->managers->getManagerOf("Fautes")->addAssociationClass([$eleves], [$regle], [$formHandler->form()->entity()]);
	// 		// $this->managers->getManagerOf("Eleve")->addAssociations([$formHandler->form()->entity()], [$regle],  $eleves);
	// 		$this->app->httpResponse()->redirect("list");
	// 	}
		
		
	// 	$classe = $this->managers->getManagerOf("Classe")->getList();
	// 	$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
		
		
	// 	$eleves = $this->managers->getManagerOf("Eleve")->getList();
	// 	$this->page()->addVar(Page::NS_ENTITIES_LIST, $eleves);
	
	// 	$this->page->addVar("form", $formHandler->form()->createView());
	// }
	
	
	//ajout d'une faute
	public function executefaute_ajouter(\Library\HTTPRequest $request){
		
		$formHandler = $this->formHandler("Fautes", $request);
		
		if ($formHandler->process()) {
			
			
			
			
			//on associe l'eleve a la faute
			$this->managers->getManagerOf("Eleve")->addAssociations(
				[$formHandler->form()->entity()]
				, new Eleve(["id" => $request->postData("select_created_name")])
			);
			// associer la faute a la regle
			$this->managers->getManagerOf("Regles")->addAssociations(
				[$formHandler->form()->entity()]
				,new Fautes(["id" => $request->postData("regles")])
			);

			// Les variable de la Notification
			$regle_slectionne_id = $request->postData("regles");
			$regle_slectionne_entity = $this->managers->getManagerOf("Regles")->getUnique($regle_slectionne_id);
			$regle_slectionne_libelle = $regle_slectionne_entity->libelle_regle();
			
			// on recupere les infos de gar en session
			$personne_courante_id = $this->app()->user()->getAttribute(EUser::USER_ID);
			$personne_courante_nom = $this->app()->user()->getAttribute(EUser::USER_NOM);

				//on cree la notification
	
				$notification = ABackController::createNotification(
				$request
				,'Une faute a été signalée concerant cette règle__ Article: '
				,$personne_courante_id
				,$personne_courante_nom);
				
				// On insert la notification
				$this->managers->getManagerOf("Notification")->save($notification);

				//Pour finir, on associe la notification a un eleve
				$this->managers->getManagerOf("Eleve")->addAssociations(
					[$notification]
					,new Eleve(["id" => $personne_courante_id])
				);
				
		$this->app->httpResponse()->redirect("list");
	}
	// on recupere les salle de classe 	
	$classe = $this->managers->getManagerOf("Classe")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
	
	// on listes les regles
	$regles = $this->managers->getManagerOf("Regles")->getList();
	$this->page()->addVar(Page::NS_ENTITIES_LIST, $regles);
	
	// on cree la notification
	
		
		$this->page->addVar("form", $formHandler->form()->createView());
	}
	
	/**
 * Regles: ajouter 
 */
public function executeregle_ajouter(\Library\HTTPRequest $request){

		$formHandler = $this->formHandler("Regles", $request);

		if ($formHandler->process()) {
			$this->managers->getManagerOf("Regles")->addAssociations([$formHandler->form()->entity()], new Reglement_Iterieure(["id" => $request->postData("regles")]));
			$this->app->user()->setFlash('add record');
			$this->app->httpResponse()->redirect("list");
		}

			// on recupere le reglement interieur
		$reglement = $this->managers->getManagerOf("Reglement_Iterieure")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $reglement);

		$this->page->addVar("form", $formHandler->form()->createView());
}

//ajout d'un enseignant
public function executeenseignant_ajouter(\Library\HTTPRequest $request){

	
	
	$formHandler = $this->formHandler("Professeur", $request);
	
	if ($formHandler->process()) {
		$this->managers->getManagerOf("Cour")->addAssociations([$formHandler->form()->entity()], new Cour(["id" => $request->postData("cours")]));
		
		$this->app->user()->setFlash('add record');
		$this->app->httpResponse()->redirect("list");
		// $request->app()->forWardURI("/dashbord/notifications/add", false);
		}
		$cours = $this->managers->getManagerOf("Cour")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $cours);
		$this->page->addVar("form", $formHandler->form()->createView());
}
public function executeESPACE_ADMINISTRATION(\Library\HTTPRequest $request){
	
}


// PARENT
public function executeparent_ajouter(\Library\HTTPRequest $request){
	/**
		 * Chargement des utilitaire de configuration de l'entité: Cours
		 * Dans l'ordre, nous avons :
		 * 1 => FormHeader : CoursFormHeader
		 * 2 => FormBuilder: CoursFormBuilder
		 * 3 => Cours->isValid()
		 */
		$formHandler = $this->formHandler("Parents", $request);

		/**
		 * process insére l'entité valide en BD
		 */
		if ($formHandler->process()) {
			/**
			 * Une fois l'entité insérée en BD on récupérer son ientifiant en faisant:
			 * $formHandler->form()->entity()->id();
			 */

			$this->managers->getManagerOf("Eleve")->addAssociations([$formHandler->form()->entity()], new Eleve(["id" => $request->postData("select_created_name")]));

			
			$this->app->user()->setFlash('add record');
			//Redirection vers une URL de votre choix, 
			$this->app->httpResponse()->redirect("list");
		}
		
			// on recupere les salle de classe 	
		$classe = $this->managers->getManagerOf("Classe")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);
		$this->page->addVar("form", $formHandler->form()->createView());
}
public function executeparent_lister(\Library\HTTPRequest $request){
	$parents = $this->managers->getManagerOf("Parents")->getList();
		$this->page()->addVar(Page::NS_ENTITIES_LIST, $parents);
		$this->useView("parent_lister");
}

/**
 * Cours - CRUD
 */
public function executeparent_views(\Library\HTTPRequest $request){
	$parents = $this->managers->getManagerOf("Parents")->getUnique($request->getData("id"));
	$this->page()->addVar(Page::NS_ENTITY, [$parents]);

}
public function executeparent_modifier(\Library\HTTPRequest $request){

	
		$formHandler = $this->formHandler("Parents", $request,$request->getParam("id"));

	
		if ($formHandler->process()) {
			
			$this->app->user()->setFlash('add record');
			$this->app->httpResponse()->redirect("../list");
		}

			// on recupere les salle de classe 	
			$classe = $this->managers->getManagerOf("Classe")->getList();
			$this->page()->addVar(Page::NS_ENTITIES_LIST, $classe);

		$this->page->addVar("form", $formHandler->form()->createView());
}
public function executeparent_supprimer(\Library\HTTPRequest $request){
	$cours = $this->managers->getManagerOf("Parents")->delete($request->getData("id"));
	$this->app->httpResponse()->redirect("../list");
}



	// -------------------------------ACTIONS SUR LES REQUETES AJAX ------------------------------------
	
	
	
	
	/**AJAX
	 * Cette fonction va permettre de selction de mnire dynamique tout les eleves d'une classe precise
	 */
	public function executeSELECT_ELEVES_LISTER(\Library\HTTPRequest $request){
		
		// on recupere dynamiquemen la liste de tout les eleves
		$classe = new Classe(["id" => $request->getData("id")]);
		$eleves = $this->managers->getManagerOf("Eleve")->getAssociateTo($classe);
		
		// var_dump(EUser::parseEleveToTab($eleves));


		echo(json_encode(EUser::parseEleveToTab($eleves)));
		// die();
		http_response_code(200);

		// die();

		$this->page()->withoutLayout();
	}

	/**
	 * Cette fonction lliste toutes les regles 
	 */
	public function executeAJAX_SELECT_REGLES_LISTER(\Library\HTTPRequest $request){
		
		$regles = $this->managers->getManagerOf("Regles")->getList();
		$datas = json_encode(EUser::parseEleveToTab($regles));
		// die($datas);
		http_response_code(200);
		// $this->page()->withoutLayout();
	}

	// --------------------- LES NOTIFICATION SUR LES ELEVES   --------------------

	
	public function executeAJAX_NOTIFIER_ELEVE_FAUTES(\Library\HTTPRequest $request){

		
		// insertion en base de donne
		// $notification_heder = new NotificationFormHeader($request);
		// $notification = $notification_heder->entity();
		
		// $notification->setLibelle();
		// $notification->setView(0);
		// $notification->setId_emeteur();
		// $notification->setNom_emeteur();

			// insertion en
		// $this->managers->getManagerOf("Notification")->save($notification);


		$this->page()->withoutLayout();
		
		
		
		http_response_code(200);
		$this->useView("standart");
	}
	
	
}