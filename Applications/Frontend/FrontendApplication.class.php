<?php


namespace Applications\Frontend;

class FrontendApplication extends \Library\Application
{
	public function __construct()
	{
		parent::__construct();
		$this->name = "Frontend";
	}

	public function run()
	{

		// try {
		$controller = $this->getController();
		$controller->execute();

		// $this->httpResponse->setPage($controller->page());
		$this->httpResponse->send();
		// } catch (\Exception $th) {
		// 	$this->httpResponse()->redirect410();
		// } catch (\Error $th) {
		// 	$this->httpResponse()->redirect410();
		// }
	}
}
