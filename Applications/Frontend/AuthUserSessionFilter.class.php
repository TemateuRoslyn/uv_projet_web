<?php
namespace Applications\Frontend;
use AppliLib\EUser;
use AppliLib\FormBuilder\PersonneFormBuilder;
use Library\Interfaces\Filter;
class AuthUserSessionFilter implements Filter
{
    public function init(\Library\FilterConfig $config)
    {
    } 

    public function doFilter(\Library\HTTPRequest $request, \Library\HTTPResponse $response, \Library\FilterChain $chain)
    {
        
        if ($request->app()->user()->adminAuthentificated()) {
            // si un user veut acceder a la partie admin sans les droits necessaire
             if($request->app()->user()->getAttribute(EUser::USER_ROLE) != PersonneFormBuilder::ROLE_ADMINISTRATEUR){
            // die("mort");
            $response->redirect('/geniusclassrooms');
	        }
	// on ressort la liste des eleves en Base de donnees
            // $request->app()->user()->setAdminAuthentificated(false);
        } else {
            // faire une redirection dans l'application
            // $request->app()->forWardURI("/authentification/sign_in", false);
            $response->redirect("/authentification/sign_in");
        }
        $chain->doFilter($request, $response);
    }

    public function destroy()
    {
    }
}
