<?php
	namespace Applications\Authentification\Modules\Authentification;
	use AppliLib\ABackController;
	use AppliLib\EUser;
	use AppliLib\FormBuilder\PersonneFormBuilder;
	use Library\Page;
	 class AuthentificationController extends ABackController
            {


public function executedeconnexion(\Library\HTTPRequest $request){
	$this->app->user()->setAdminAuthentificated(false);
	$this->app->httpResponse()->redirect('/dashbord');
}
/**
 * action de connection
 */
public function executeconnexion(\Library\HTTPRequest $request){
	
	/**
	 * On definit les manager 
	 */ 
	$managers_eleve = $this->managers->getManagerOf("Eleve")->getList();
	$managers_personnel = $this->managers->getManagerOf("Personnel")->getList();
	$managers_professeur = $this->managers->getManagerOf("Professeur")->getList();
	$managers_parents = $this->managers->getManagerOf("Parents")->getList();

	/**
	 * On definit les tableau php associes a chaque managers
	 */
	$tab_eleve 		= EUser::parseEleveToTab($managers_eleve);
	$tab_personnel  =  EUser::parsePersonnelToTab($managers_personnel);
	$tab_professeur =  EUser::parseProfToTab($managers_professeur);
	$tab_parents    =  EUser::parseParentToTab($managers_parents);


	

	/**
	 * Les entites associes
	 */
	$eleve  = NULL;
	$personnel  = NULL;
	$professeur  = NULL;
	$parents  = NULL;
	$entity_selected = NULL;

	/**
	 * Les test de connection
	 */

	//  __ Elves  __
	
	if(($request->method() == 'POST')){

		// -- si un eleve se coonnecte
		$eleve = EUser::search_login_password(
			$request->postData("login")
			,  $request->postData("password")
			, $tab_eleve, $managers_eleve
		);
		// -- correspondance personel
		$personnel =  EUser::search_login_password(
			$request->postData("login")
			,  $request->postData("password")
			, $tab_personnel, $managers_personnel
		);
		// correspondance professeur
		$professeur = EUser::search_login_password(
			$request->postData("login")
			,  $request->postData("password")
			, $tab_professeur, $managers_professeur
		);

		// correspondance parents

		$parents = EUser::search_login_password(
			$request->postData("login")
			,  $request->postData("password")
			, $tab_parents, $managers_parents
		);


		
		$entity_selected = EUser::select_entity(
			$eleve
			,$personnel
			,$professeur
			,$parents
		);
		

	if($entity_selected != NULL)
	{
		$this->app->user()->setAdminAuthentificated();
		foreach ($entity_selected->properties() as $key => $value) 
		{
			if (!is_object($value)) 
			{
				$this->app->user()->setAttribute($key, $value);
			}
			$this->app->user()->setAttribute(EUser::USER_ID, $entity_selected->id());
		}

	}

		if($this->app->user()->getAttribute(EUser::USER_ROLE) == PersonneFormBuilder::ROLE_ADMINISTRATEUR){

			$this->app->httpResponse()->redirect('/dashbord');
		}else{
			$this->app->httpResponse()->redirect('/geniusclassrooms');
		}


		
		
	}
}

}