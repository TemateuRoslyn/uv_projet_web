<?php

namespace Applications\Frontend\Modules\Connexion;

use AppliLib\Entities\Personne;
use Library\BackController;
use Library\HTTPRequest;
use Library\Page;

class ConnexionController extends BackController
{
	public function executeIndex(HTTPRequest $request)
	{
		/** @var PersonneManager_PDO */
		$managers = $this->managers->getManagerOf("Personne");
		if (($user = $managers->getConnect($request->getData("login"), $request->getData("login"))) !== null) {

			$this->app->user()->setAttribute("id" . ucfirst($user->personnalite()), $user->id());
			$this->app->user()->setProfile($user->personnalite());

			$this->app->user()->setAdminAuthentificated();
			foreach ($user->properties() as $key => $value) {
				if (!is_object($value)) {
					$this->app->user()->setAttribute($key, $value);
				}
			}

			// $this->testVarù($abonn);
			$this->app->httpResponse()->redirect('/');
		}
	}
}
