<?php


namespace Applications\Authentification;

class AuthentificationApplication extends \Library\Application
{
	public function __construct()
	{
		parent::__construct();
		$this->name = "Authentification";
	}

	public function run()
	{

		// try {
		$controller = $this->getController();
		$controller->execute();

		// $this->httpResponse->setPage($controller->page());
		$this->httpResponse->send();
		// } catch (\Exception $th) {
		// 	$this->httpResponse()->redirect410();
		// } catch (\Error $th) {
		// 	$this->httpResponse()->redirect410();
		// }
	}
}
