<?php
	namespace AppliLib\FormBuilder;
	class PersonneFormBuilder extends \Library\FormBuilder
        {
            //RewriteRule ^(.*)$ Web/frontend.php [QSA,L]

            const ROLE_ADMINISTRATEUR = 2;
            const ROLE_PERSONNEL_ADMINISTRATIF = 3;
            const ROLE_PROFESSEUR = 4;
            const ROLE_ELEVE = 5;
            const ROLE_VISITEUR = 6;

            public function build() { $this->form->add(new \Library\Fields\StringField(array(
                'name' => 'nom',
                'placeholder' => ' Champs : NOM',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'prenom',
                'placeholder' => ' Champs : PRENOM',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\DateField(array(
                'name' => 'date_de_naisssancce',
                'placeholder' => ' Champs : DATE_DE_NAISSSANCCE',
                'validators' => array(
                    
                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'login_',
                'placeholder' => ' Champs : LOGIN_',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\PasswordField(array(
                'name' => 'mot_de_passe',
                'placeholder' => ' Champs : MOT_DE_PASSE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\SelectField(array(
                    'name' => 'role',
                    'placeholder' => 'Selectionnez un role',
                    'options' => [
                        self::ROLE_ADMINISTRATEUR => 'ADMINISTRATEUR',
                        self::ROLE_PERSONNEL_ADMINISTRATIF => 'PERSONNEL_ADMINISTRATIF',
                        self::ROLE_PROFESSEUR => 'PROFESSEUR',
                        self::ROLE_ELEVE => 'ELEVE',
                        self::ROLE_VISITEUR => 'VISITEUR',

                    ],
                    'validators' => array(
                        new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),
    
                    ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'lieu_de_naissance_',
                'placeholder' => ' Champs : LIEU_DE_NAISSANCE_',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'photo',
                'placeholder' => ' Champs : PHOTO',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\EmailField(array(
                'name' => 'email',
                'placeholder' => ' Champs : EMAIL',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\StringField(array(
                'name' => 'sexe',
                'placeholder' => ' Champs : SEXE',
                'validators' => array(
                    new \Library\Validators\NotNullValidator('Merci de spécifier une valeur'),

                ),
            )))->add(new \Library\Fields\NumberField(array(
                'name' => 'Tel',
                'placeholder' => ' Champs : TEL',
                'validators' => array(
                    new \Library\Validators\MinNumberValidator('La valeur spécifiée doit être positive', -1),

                ),
            )));}
        }
