<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="UTF-8" />
	<xsl:include href="../../../../../AppViews/libxsl.xsl" />
	
    <xsl:variable 
        name="session" 
        select="//pageScope/sessionScope" />

     <xsl:variable 
        name="page" 
        select="//pageScope" />
    
    <!-- PIDERIA TEMPLATE SESSION -->
	<xsl:template match="pideria-session">
        <xsl:variable name="name" select="@name" />
        <xsl:value-of select="$session/nom/@value" />
        <xsl:if test="$name='toto'" ></xsl:if>
        <xsl:for-each select=""></xsl:for-each>
	</xsl:template>

</xsl:stylesheet>